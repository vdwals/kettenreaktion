package de.vdwals.kettenreaktion.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import de.vdwals.kettenreaktion.implement.BugReport;

/**
 * Erzeugt einen Splashscreen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class SplashScreen extends JPanel {
    /**
     * Der Screen
     */
    private Image             bild;
    
    /**
     * Default SerialID
     */
    private static final long serialVersionUID = -8036235580698742382L;
    
    /**
     * Erzeugt den SplashScreen.
     */
    public SplashScreen() {
        this.bild = null;
        try {
            this.bild = ImageIO.read(ClassLoader
                    .getSystemResource("gui/images/splash.xrt")); //$NON-NLS-1$
            
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
        this.setPreferredSize(new Dimension(this.bild.getWidth(this), this.bild
                .getHeight(this)));
    }
    
    /**
     * Gibt das Bild zurueck.
     * 
     * @return Bild
     */
    public Image getBild() {
        return this.bild;
    }
    
    @Override
    public void paint(Graphics g) {
        g.drawImage(this.bild, 0, 0, this.getWidth(), this.getHeight(), this);
    }
    
}
