package de.vdwals.kettenreaktion.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.optionen.GrafikOption;
import de.vdwals.kettenreaktion.optionen.Optionen;
import de.vdwals.kettenreaktion.optionen.ProgrammOptionen;
import de.vdwals.kettenreaktion.optionen.SpielOptionen;
import de.vdwals.kettenreaktion.optionen.StatistikOptionen;
import de.vdwals.kettenreaktion.optionen.gui.GrafikOptGUI;
import de.vdwals.kettenreaktion.optionen.gui.ProgrammOptGUI;
import de.vdwals.kettenreaktion.optionen.gui.SpielOptGUI;
import de.vdwals.kettenreaktion.optionen.gui.StatsOptGUI;
import de.vdwals.kettenreaktion.optionen.io.SettingsIO;
import de.vdwals.kettenreaktion.statistik.Statistik;

/**
 * GUI fuer Spieleinstellungen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Einstellungen extends JFrame implements CaretListener,
        ChangeListener, ActionListener {
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = 6356424037842416199L;
    
    // Komponenten
    private JButton           ok, uebernehmen, cancel, standard;
    private JTabbedPane       tabs;
    private ProgrammOptGUI    programmGUI;
    private StatsOptGUI       statistikGUI;
    private SpielOptGUI       spielOptGUI;
    private GrafikOptGUI      grafikGUI;
    
    private Optionen[]        subOptionen;
    private SettingsIO        io;
    
    /**
     * Standardkonstruktor.
     * 
     * @param einstellungen
     *            Settings
     */
    public Einstellungen() {
        this.io = new SettingsIO();
        try {
            this.io.load();
            this.subOptionen = this.io.getOptionen();
        } catch (Exception e) {
            BugReport.recordBug(e);
            
            // Einstellungen erzeugen
            SpielOptionen spielOption = new SpielOptionen();
            ProgrammOptionen progOpt = new ProgrammOptionen();
            // Groesste Chipzahl abfragen
            int[] maxChips = new int[spielOption.getMods().length];
            for (int i = 0; i < maxChips.length; i++)
                maxChips[i] = spielOption.getMods()[i].getMaxChips();
            GrafikOption anzeigeOption = new GrafikOption(
                    4 + maxChips[ArrOps.getMax(maxChips)]);
            StatistikOptionen statsOption = new StatistikOptionen();
            
            // In Arrays uebergeben
            this.subOptionen = new Optionen[4];
            this.subOptionen[0] = progOpt;
            this.subOptionen[1] = spielOption;
            this.subOptionen[2] = statsOption;
            this.subOptionen[3] = anzeigeOption;
        }
        
        // GUIs erzeugen
        this.programmGUI = new ProgrammOptGUI(this,
                (ProgrammOptionen) this.subOptionen[0]);
        this.spielOptGUI = new SpielOptGUI(this,
                (SpielOptionen) this.subOptionen[1]);
        this.statistikGUI = new StatsOptGUI(this,
                (StatistikOptionen) this.subOptionen[2]);
        this.grafikGUI = new GrafikOptGUI(this,
                (GrafikOption) this.subOptionen[3]);
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
        
        if (arg0.getSource() == this.getUebernehmen()) {
            this.save();
            this.repaint();
            
        } else if (arg0.getSource() == this.getOk()) {
            this.save();
            this.dispose();
            
        } else if (arg0.getSource() == this.getStandard()) {
            for (Optionen element : this.subOptionen)
                if (element != null)
                    element.toDefault();
            this.repaint();
            
        } else if (arg0.getSource() == this.getCancel())
            this.dispose();
    }
    
    /**
     * Erzeugt die Oberflaeche.
     */
    public void build() {
        // Subkomponenten erzeugen
        this.programmGUI.build();
        this.spielOptGUI.build();
        this.grafikGUI.build();
        this.statistikGUI.build();
        
        this.setLayout(new BorderLayout(5, 5));
        
        // Tabs erzeugen
        this.tabs = new JTabbedPane();
        this.tabs.add(this.programmGUI, "Programm");
        this.tabs.add(this.spielOptGUI, "Spiel");
        this.tabs.add(this.grafikGUI, "Grafik");
        this.tabs.add(this.statistikGUI, "Statistik");
        
        this.add(this.tabs, BorderLayout.CENTER);
        
        // Buttons einfuegen
        JPanel bottom = new JPanel(new BorderLayout(5, 5));
        JPanel buttons = new JPanel(new GridLayout(0, 4, 5, 5));
        
        buttons.add(this.getStandard());
        buttons.add(this.getOk());
        buttons.add(this.getUebernehmen());
        buttons.add(this.getCancel());
        
        bottom.add(buttons, BorderLayout.EAST);
        
        this.add(bottom, BorderLayout.SOUTH);
        
        this.pack();
    }
    
    @Override
    public void caretUpdate(CaretEvent e) {
        this.getUebernehmen().setEnabled(true);
    }
    
    /**
     * @return the cancel
     */
    private JButton getCancel() {
        if (this.cancel == null) {
            this.cancel = new JButton("Abbrechen");
            this.cancel.addActionListener(this);
            this.cancel.addChangeListener(this);
            this.cancel.setToolTipText("Einstellungen verwerfen und schlie�en");
        }
        return this.cancel;
    }
    
    public GrafikOption getGrafOpt() {
        return (GrafikOption) this.subOptionen[3];
    }
    
    /**
     * @return the ok
     */
    private JButton getOk() {
        if (this.ok == null) {
            this.ok = new JButton("Ok");
            this.ok.addActionListener(this);
            this.ok.addChangeListener(this);
            this.ok.setToolTipText("Einstellungen speichern und schlie�en");
        }
        return this.ok;
    }
    
    public ProgrammOptionen getProgOpt() {
        return (ProgrammOptionen) this.subOptionen[0];
    }
    
    public SpielOptionen getSpielOpt() {
        return (SpielOptionen) this.subOptionen[1];
    }
    
    /**
     * Gibt den Button zum Zuruecksetzen zurueck.
     * 
     * @return the standard
     */
    private JButton getStandard() {
        if (this.standard == null) {
            this.standard = new JButton("Standard");
            this.standard.addActionListener(this);
            this.standard.addChangeListener(this);
            this.standard.setToolTipText("Stellt die Standardwerte wieder her");
        }
        return this.standard;
    }
    
    public Statistik getStatistik() {
        return this.getSpielOpt().getStatistik();
    }
    
    public StatistikOptionen getStatistikOpt() {
        return (StatistikOptionen) this.subOptionen[2];
    }
    
    /**
     * @return the uebernehmen
     */
    private JButton getUebernehmen() {
        if (this.uebernehmen == null) {
            this.uebernehmen = new JButton("�bernehmen");
            this.uebernehmen.addActionListener(this);
            this.uebernehmen.addChangeListener(this);
            this.uebernehmen.setEnabled(false);
            this.uebernehmen.setToolTipText("Einstellungen speichern");
        }
        return this.uebernehmen;
    }
    
    public void save() {
        this.programmGUI.save();
        this.statistikGUI.save();
        this.spielOptGUI.save();
        this.grafikGUI.save();
        
        this.io.setOptionen(this.subOptionen);
        
        try {
            this.io.save();
            this.getUebernehmen().setEnabled(false);
        } catch (Exception e) {
            BugReport.recordBug(e);
        }
        
    }
    
    public void setSpielOpt(SpielOptionen spielOpt) {
        this.subOptionen[1] = spielOpt;
    }
    
    public void startSpiel(boolean start) {
        this.tabs.setEnabledAt(1, !start);
    }
    
    @Override
    public void stateChanged(ChangeEvent arg0) {
        this.getUebernehmen().setEnabled(true);
    }
    
}
