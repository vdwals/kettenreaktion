package de.vdwals.kettenreaktion.gui;

import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.spiel.Spielkern;

/**
 * Klasse zur Simulation eine KI-Kampfes
 * 
 * @author Dennis van der Wals
 * 
 */
public class Simulation extends Fenster {
    /**
     * Default SerialID
     */
    private static final long serialVersionUID = 1L;
    
    // Komponenten
    private JProgressBar[]    gewonnen;
    private JProgressBar      gesamt;
    private JLabel            namen[], zeiten[], remain, elapsed, requierd;
    private JToggleButton     stop;
    
    // Einstellungen
    private Einstellungen     einstellungen;
    private boolean           pause;
    private int               loop;
    private Thread            run;
    
    /**
     * Standardkonstruktor
     * 
     * @param ki
     */
    public Simulation(Einstellungen einstellungen) {
        super("Simulation");
        this.einstellungen = einstellungen;
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
        String s = null;
        try {
            s = JOptionPane.showInputDialog("Anzahl der Simulationen");
        } catch (Exception e) {
            BugReport.recordBug(e);
        }
        
        // Falsche Eingaben abfangen
        if ((s == null) || !Pattern.matches("\\d+", s))
            this.loop = 100;
        else {
            this.loop = Integer.parseInt(s);
            if (this.loop <= 0)
                this.loop = 100;
        }
    }
    
    /**
     * Gibt einen Fortschrittsbalken fuer die gesamte Simulation zurueck.
     * 
     * @return Fortschrittsbalken fuer die gesamte Simulation
     */
    public JProgressBar getAnzeige() {
        if (this.gesamt == null) {
            this.gesamt = new JProgressBar(0, this.loop);
            this.gesamt.setOrientation(SwingConstants.HORIZONTAL);
            this.gesamt.setStringPainted(true);
            this.gesamt.setToolTipText("Simulierte Spiele: "
                    + this.gesamt.getValue() + "/" + this.loop);
        }
        return this.gesamt;
    }
    
    /**
     * Gibt ein Label fuer die vergangene Simulationszeit zurueck.
     * 
     * @return Label fuer die vergangene Simulationszeit
     */
    public JLabel getElapsed() {
        if (this.elapsed == null) {
            this.elapsed = new JLabel("--:--");
            this.elapsed.setToolTipText("Verstrichene Zeit");
        }
        return this.elapsed;
    }
    
    /**
     * Gibt eine Liste mit Fortschrittsbalken fuer die Anzahl der gewonnenen
     * Spiele jeder KI zurueck.
     * 
     * @return Fortschrittsbalken fuer die Anzahl der gewonnenen Spiele jeder KI
     */
    public JProgressBar[] getGewonnen() {
        if (this.gewonnen == null) {
            this.gewonnen = new JProgressBar[this.einstellungen.getSpielOpt()
                    .getKi().length];
            for (int i = 0; i < this.gewonnen.length; i++) {
                this.gewonnen[i] = new JProgressBar(0, this.loop);
                this.gewonnen[i].setStringPainted(true);
                this.gewonnen[i].setToolTipText("Gewonnene Spiele: "
                        + this.gewonnen[i].getValue() + "/" + this.loop);
            }
        }
        return this.gewonnen;
    }
    
    /**
     * Gibt eine Liste mit KI-Namen zurueck.
     * 
     * @return Liste mit KI-Namen
     */
    public JLabel[] getNamen() {
        if (this.namen == null) {
            this.namen = new JLabel[this.einstellungen.getSpielOpt().getKi().length];
            for (int i = 0; i < this.namen.length; i++)
                this.namen[i] = new JLabel(this.einstellungen.getSpielOpt()
                        .getKi()[i].toString());
        }
        return this.namen;
    }
    
    /**
     * Gibt ein Label fuer die verbleibende Simulationszeit zurueck
     * 
     * @return Label fuer die verbleibende Simulationszeit
     */
    public JLabel getRemain() {
        if (this.remain == null) {
            this.remain = new JLabel("--:--");
            this.remain.setToolTipText("Erwartete verbleibende Zeit");
        }
        return this.remain;
    }
    
    /**
     * Gibt ein Label fuer die benoetigte Simulationszeit zurueck
     * 
     * @return Label fuer die benoetigte Simulationszeit
     */
    public JLabel getRequierd() {
        if (this.requierd == null) {
            this.requierd = new JLabel("--:--");
            this.requierd.setToolTipText("Erwartete Gesamtlaufzeit");
        }
        return this.requierd;
    }
    
    /**
     * Gibt einen Button zum pausieren des Threads zurueck.
     * 
     * @return Button zum pausieren des Threads
     */
    public JToggleButton getStop() {
        if (this.stop == null) {
            this.stop = new JToggleButton("Pause");
            this.stop.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    // Pausieren
                    if (Simulation.this.stop.isSelected()) {
                        if (Simulation.this.run.isAlive())
                            Simulation.this.pause = true;
                        Simulation.this.stop.setText("Weiter");
                        
                    } else {
                        
                        // Pause aufheben
                        if (Simulation.this.run.isAlive())
                            Simulation.this.pause = false;
                        Simulation.this.stop.setText("Pause");
                    }
                }
            });
        }
        return this.stop;
    }
    
    /**
     * Gibt eine Liste mit durchschnittlichen Berechnungszeiten der KIs zurueck.
     * 
     * @return Liste mit durchschnittlichen Berechnungszeiten der KIs
     */
    public JLabel[] getZeiten() {
        if (this.zeiten == null) {
            this.zeiten = new JLabel[this.einstellungen.getSpielOpt().getKi().length];
            for (int i = 0; i < this.zeiten.length; i++)
                this.zeiten[i] = new JLabel();
        }
        return this.zeiten;
    }
    
    /**
     * Formatiert die Zeitanzeige.
     * 
     * @param nanos
     *            vergangene Zeit in ns
     * @return Formatierte Ausgabe
     */
    private String round(long nanos) {
        String out = "";
        double time = 0.0;
        if (nanos > 1000) {
            time = ((double) nanos / 1000);
            out = "\u03BCs";
        }
        
        if (time > 1000) {
            time = time / 1000;
            out = "ms";
        }
        
        out = String.format("%01.4f %s", time, out);
        return out;
    }
    
    /**
     * Startet die Simulation
     */
    public void simulieren() {
        this.run = new Thread(new Runnable() {
            
            @Override
            public void run() {
                // LivePanel waehrend Simulation beenden
                boolean backup = Simulation.this.einstellungen
                        .getStatistikOpt().isLivePanel();
                Simulation.this.einstellungen.getStatistikOpt().setLivePanel(
                        false);
                
                // Berechnungszeit aufzeichnung
                long[] time = new long[Simulation.this.einstellungen
                        .getSpielOpt().getKi().length];
                long simStart = System.nanoTime();
                
                /*
                 * Zaehler, damit die Zeit nur bei jedem Prozent aktualisiert
                 * wird
                 */
                double aktualisiereZeit = 0.01;
                
                // Durchlaeufe
                for (int i = 0; i < Simulation.this.loop; i++) {
                    
                    // Neues Spiel erstellen
                    Spielkern spiel = new Spielkern(
                            Simulation.this.einstellungen, (byte) 0);
                    
                    // Reihenfolge durchmischen
                    if (Simulation.this.einstellungen.getSpielOpt().isRandom())
                        Simulation.this.einstellungen.getSpielOpt().shuffle();
                    
                    // Rundenzaehler
                    int[] counter = new int[Simulation.this.einstellungen
                            .getSpielOpt().getKi().length];
                    long[] timeRound = new long[Simulation.this.einstellungen
                            .getSpielOpt().getKi().length];
                    
                    while (!spiel.isOver()) {
                        int spieler = spiel.getAktiverSpieler();
                        
                        Simulation.this.einstellungen.getSpielOpt().getKi()[spieler]
                                .setChipsToSet(spiel.getChipsToSet());
                        
                        // Start der Zeitmessung
                        long start = System.nanoTime();
                        
                        // Chips abfragen
                        Point[] chips = Simulation.this.einstellungen
                                .getSpielOpt().getKi()[spieler].getChips(spiel
                                .getSpielbrett());
                        
                        // Zeitmessung beenden
                        timeRound[spieler] += System.nanoTime() - start;
                        
                        // Runde beenden
                        spiel.beendeRunde(chips);
                        
                        // Rundenzaehler erhoehen
                        counter[spieler]++;
                    }
                    
                    // Speichern der KI-Berechnungen und der Durchschnittszeit
                    for (int j = 0; j < Simulation.this.einstellungen
                            .getSpielOpt().getKi().length; j++)
                        /*
                         * Gesamte Berechnungszeit durch die Anzahl der Zuege
                         * teilen
                         */
                        time[Simulation.this.einstellungen.getSpielOpt()
                                .getOriginalPosition()[j]] += timeRound[j]
                                / counter[j];
                    int gewinner = Simulation.this.einstellungen.getSpielOpt()
                            .getOriginalPosition()[ArrOps.getMax(spiel
                            .getKonto())];
                    
                    // Progressbar aktualisieren
                    Simulation.this.gewonnen[gewinner]
                            .setValue(Simulation.this.gewonnen[gewinner]
                                    .getValue() + 1);
                    Simulation.this.gesamt.setValue(Simulation.this.gesamt
                            .getValue() + 1);
                    
                    // Tooltipps aktualisieren
                    Simulation.this.gewonnen[gewinner]
                            .setToolTipText("Gewonnene Spiele: "
                                    + Simulation.this.gewonnen[gewinner]
                                            .getValue() + "/"
                                    + Simulation.this.loop);
                    Simulation.this.gesamt.setToolTipText("Simulierte Spiele: "
                            + Simulation.this.gesamt.getValue() + "/"
                            + Simulation.this.loop);
                    
                    // Aktualisiere nach jedem Prozent die Zeit
                    if (Simulation.this.gesamt.getPercentComplete() >= aktualisiereZeit) {
                        
                        // Vergangene Zeit berechnen
                        long elapsed = System.nanoTime() - simStart;
                        
                        // Benoetigte Zeit hochrechnen
                        long requiered = (elapsed * Simulation.this.loop)
                                / (i + 1);
                        
                        // Anzeige aktualisieren
                        Simulation.this.elapsed.setText(Simulation.this
                                .toTime(elapsed));
                        Simulation.this.remain.setText(Simulation.this
                                .toTime(requiered - elapsed));
                        Simulation.this.requierd.setText(Simulation.this
                                .toTime(requiered));
                        
                        // Schwellwert zur Aktualisierung um 1 Prozent erhoehen
                        aktualisiereZeit = Simulation.this.gesamt
                                .getPercentComplete() + 0.01;
                    }
                    
                    // Wenn Thread pausiert:
                    if (Simulation.this.pause) {
                        
                        // Durchschnitt der Zeit bilden und plotten
                        for (int j = 0; j < time.length; j++)
                            Simulation.this.zeiten[j].setText(Simulation.this
                                    .round(time[j] / i));
                        
                        // Speicher aufraeumen
                        System.gc();
                        
                        // Endlosschleife solange Thread pausiert ist
                        while (Simulation.this.pause)
                            try {
                                Thread.sleep(1000);
                                
                                // Pausezeit wird aus Zeitberechnung entfernt
                                simStart += 1000000000;
                            } catch (InterruptedException e) {
                                BugReport.recordBug(e);
                            }
                    }
                }
                
                // Durchschnitt der Zeit bilden und plotten
                for (int j = 0; j < time.length; j++)
                    Simulation.this.zeiten[j].setText(Simulation.this
                            .round(time[j] / Simulation.this.loop));
                
                // LivePanel zuruecksetzen
                Simulation.this.einstellungen.getStatistikOpt().setLivePanel(
                        backup);
                
                // Speicher leeren
                System.gc();
            }
        });
        this.run.start();
    }
    
    /**
     * Erzeugt die graphische Oberflaeche
     */
    public void start() {
        this.setLayout(new GridLayout(0, 1, 10, 10));
        
        // Fortschrittsbalken fuer den ganzen Verlauf
        this.add(this.getAnzeige());
        
        // Informationen ueber die Laufzeit
        JPanel p = new JPanel();
        p.setLayout(new GridLayout(1, 3, 10, 0));
        p.add(this.getElapsed());
        p.add(this.getRemain());
        p.add(this.getRequierd());
        this.add(p);
        
        // Hinzufuegen der KIs
        for (int i = 0; i < this.einstellungen.getSpielOpt().getKi().length; i++) {
            p = new JPanel();
            p.setLayout(new GridLayout(1, 3, 10, 0));
            p.add(this.getNamen()[i]);
            p.add(this.getGewonnen()[i]);
            p.add(this.getZeiten()[i]);
            this.add(p);
        }
        
        // Pausebutton hinzufuegen
        p = new JPanel();
        p.setLayout(new GridLayout(1, 3, 10, 0));
        p.add(new JLabel());
        p.add(new JLabel());
        p.add(this.getStop());
        this.add(p);
        this.pack();
        this.setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - this
                        .getWidth()) / 2, (Toolkit.getDefaultToolkit()
                        .getScreenSize().height - this.getHeight()) / 2);
        // Icon setzen
        try {
            this.setIconImage(ImageIO.read(this.getClass().getResource(
                    "images/radioaktiv.xrt")));
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
        this.setVisible(true);
        this.simulieren();
    }
    
    /**
     * Verkuerzter Zugriff auf new SimpleDateFormat("mm:ss").format(new
     * Date(millis))
     * 
     * @param millis
     *            vergangene Zeit in Millisekunden
     * @return mm:ss
     */
    private String toTime(long nanos) {
        long millis = nanos / 1000000;
        return new SimpleDateFormat("mm:ss").format(new Date(millis));
    }
}
