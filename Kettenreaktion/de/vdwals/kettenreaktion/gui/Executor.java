package de.vdwals.kettenreaktion.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JWindow;
import javax.swing.UIManager;
import javax.swing.table.TableModel;

import de.vdwals.kettenreaktion.editor.gui.KartenEditor;
import de.vdwals.kettenreaktion.editor.gui.KartenVorschau;
import de.vdwals.kettenreaktion.editor.io.KarteIOData;
import de.vdwals.kettenreaktion.gui.language.Messages;
import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.implement.Meta;
import de.vdwals.kettenreaktion.spiel.Spielkern;
import de.vdwals.kettenreaktion.spiel.gui.Spielfenster;
import de.vdwals.kettenreaktion.spiel.io.SaveGameIO;
import de.vdwals.kettenreaktion.spiel.ki.Border;
import de.vdwals.kettenreaktion.spiel.ki.Dichte;
import de.vdwals.kettenreaktion.spiel.ki.Greedy;
import de.vdwals.kettenreaktion.spiel.ki.Heuristic;
import de.vdwals.kettenreaktion.spiel.ki.Strategie;
import de.vdwals.kettenreaktion.spiel.ki.Zufall;

/**
 * Oberflaeche zum erstellen eines Spiels.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Executor extends Fenster implements ActionListener, WindowListener {
    // Komponenten
    private JComboBox<?>      spielerOption;
    private JTextField        spielerName;
    private JButton           spielen, simulieren, karteLaden, editor,
            hinzufuegen, entfernen;
    private JTable            spielerListe;
    private JCheckBox         turnier, random;
    private KartenVorschau    mapPreview;
    private JMenuBar          menu;
    
    // Spieleinstellungen
    private Einstellungen     optionen;
    private Spielkern         aktuelleTurnierRunde;
    private byte              turnierRunde;
    
    // Ausfuehrungsprogramme
    private Simulation        aktuelleSimulation;
    private Spielfenster      aktuellesSpiel;
    private KartenEditor      me;
    
    /**
     * Default SerialID
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Mainmethode - Startet das Spiel
     * 
     * @param args
     *            keine Funktion
     */
    public static void main(String[] args) {
        Messages.loadLang();
        try {
            Executor c = new Executor();
            c.build();
        } catch (Exception e) {
            BugReport.recordBug(e);
        }
    }
    
    /**
     * Standardkonstruktor
     */
    public Executor() {
        super();
        
        // Splashscreen zeigen
        this.showSplash();
        
        // Design laden
        try {
            UIManager
                    .setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e1) {
            BugReport.recordBug(e1);
        }
        
        this.optionen = new Einstellungen();
        this.optionen.addWindowListener(this);
        this.optionen.build();
        this.menu = new ExecutorMenu(new SaveGameIO(this.optionen, null),
                this.optionen, this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        // Spiel oder Simulation starten
        if ((e.getSource() == this.getSimulieren())
                || (e.getSource() == this.getSpielStarten())) {
            
            // Weniger als 2 Spieler abfangen
            if (this.optionen.getSpielOpt().getKi().length < 2)
                JOptionPane.showMessageDialog(null,
                        "Sie m�ssen mindestens 2 Spieler ausw�hlen.");
            else if (e.getSource() == this.getSpielStarten()) {
                
                if (this.getTurnier().isSelected())
                    this.startTurnier();
                else {
                    // Durchmischen wenn gewuenscht
                    if (this.getRandom().isSelected())
                        this.optionen.getSpielOpt().shuffle();
                    
                    // Spiel starten
                    this.startGame(new Spielfenster(new Spielkern(
                            this.optionen, (byte) 0), this.optionen));
                }
                
            } else if ((e.getSource() == this.simulieren)
                    && this.nurKi(this.optionen.getSpielOpt().getKi())) {
                
                // Simulation erstellen und starten
                this.setVisible(false);
                this.aktuelleSimulation = new Simulation(this.optionen);
                this.aktuelleSimulation.addWindowListener(this);
                this.aktuelleSimulation.start();
                
            } else
                // Spieler fuer Simulation abfangen
                JOptionPane.showMessageDialog(null,
                        "Spieler k�nnen nicht simuliert werden.");
            
        } else if (e.getSource() == this.getKarteLaden())
            try {
                // Versuche Karte zu Laden
                KarteIOData mapIO = new KarteIOData();
                boolean[][] karte = mapIO.load();
                
                if (karte != null) {
                    // Karte uebernehmen
                    this.setVisible(false);
                    this.optionen.getSpielOpt().setKarte(karte);
                    this.setVisible(true);
                }
                
            } catch (Exception a) {
                BugReport.recordBug(a);
                // Info, wenn Karte nicht geladen werden konnte
                JOptionPane.showMessageDialog(this,
                        "Karte konnte nicht geladen werden.");
            }
        else if (e.getSource() == this.getEditor()) {
            
            if (this.me == null) {
                
                // Erzeuge MapEditor
                this.me = new KartenEditor(this.optionen.getSpielOpt());
                this.me.build();
                this.me.addWindowListener(this);
                
            }
            
            // Holt den MapEditor hervor
            this.me.setVisible(true);
            
            // Mache Fenster unsichtbar
            this.setVisible(false);
            
        } else if (e.getSource() == this.getHinzufuegen()) {
            
            // Spieler hinzufuegen
            this.addSelection();
            
            this.getSpielerName().setText("Spieler");
            
        } else if (e.getSource() == this.getEntfernen())
            // Loescht einen Spieler
            if ((this.getSpielerListe().getSelectedRows() != null)
                    && (this.getSpielerListe().getSelectedRows().length > 0))
                this.delete(this.getSpielerListe().getSelectedRows());
    }
    
    /**
     * Fuegt einen Spieler der Liste hinzu.
     */
    private void addSelection() {
        // Nur hinzufuegen, wenn noch nicht alle Spieler eingetragen wurden
        if (this.optionen.getSpielOpt().getKi().length < this.optionen
                .getSpielOpt().getMaxSpieler()) {
            
            Strategie ki = null;
            String name = this.getSpielerName().getText();
            switch (this.getSpielerOption().getSelectedIndex()) {
            
            /*
             * Erzeugt eine KI fuer 1=KI-Dichte, 2=KI-Dichte+, 3=KI-Heuristic,
             * 4=KI-Greedy, 5=KI-Greedy+, 6=KI-Border, 7=KI-Random
             */
                case 1:
                    ki = new Dichte(false);
                    break;
                case 2:
                    ki = new Dichte(true);
                    break;
                case 3:
                    ki = new Heuristic();
                    break;
                case 4:
                    ki = new Greedy(false);
                    break;
                case 5:
                    ki = new Greedy(true);
                    break;
                case 6:
                    ki = new Border();
                    break;
                case 7:
                    ki = new Zufall();
                    break;
                default:
                    break;
            }
            
            // KIs Standardnamen zuweisen, wenn nicht anders angegeben
            if ((ki != null) && name.equals("Spieler"))
                name = ki.toString();
            
            // Spieler hinzufuegen
            int spielerAnzahl = this.optionen.getSpielOpt().getKi().length + 1;
            
            // Neue Arrays vorbereiten und mit alten Werten fuellen
            Strategie[] kiBuffer = new Strategie[spielerAnzahl];
            String[] namenBuffer = new String[spielerAnzahl];
            for (int i = 0; i < this.optionen.getSpielOpt().getKi().length; i++) {
                kiBuffer[i] = this.optionen.getSpielOpt().getKi()[i];
                namenBuffer[i] = this.optionen.getSpielOpt().getNamen()[i];
            }
            // Neuen Spieler hinzufuegen und neue Arrays zurueckgeben
            kiBuffer[spielerAnzahl - 1] = ki;
            namenBuffer[spielerAnzahl - 1] = name;
            this.optionen.getSpielOpt().setNamen(namenBuffer);
            this.optionen.getSpielOpt().setKi(kiBuffer);
            this.repaint();
        }
    }
    
    /**
     * Startet das Programm.
     */
    private void build() {
        // Fenster initialisieren
        this.setResizable(false);
        this.setTitle("Reaktor v:" + Meta.v);
        
        this.setJMenuBar(this.menu);
        
        JPanel left = new JPanel(new GridLayout(2, 0, 5, 5));
        JPanel right = new JPanel(new BorderLayout(5, 5));
        
        JPanel leftBottom = new JPanel(new GridLayout(4, 1, 5, 5));
        JPanel rightBottom = new JPanel(new GridLayout(0, 2, 5, 5));
        
        JPanel addPlayer = new JPanel(new BorderLayout(5, 5));
        JPanel settings = new JPanel(new GridLayout(0, 4, 5, 5));
        JPanel startButtons = new JPanel(new GridLayout(0, 2, 5, 5));
        
        JPanel addPlayerLeft = new JPanel(new GridLayout(0, 2, 5, 5));
        JPanel addPlayerRight = new JPanel(new GridLayout(0, 2, 5, 5));
        
        // Erste Zeile
        addPlayerLeft.add(this.getSpielerName());
        addPlayerLeft.add(this.getSpielerOption());
        addPlayerRight.add(this.getHinzufuegen());
        addPlayerRight.add(this.getEntfernen());
        
        addPlayer.add(addPlayerLeft, BorderLayout.CENTER);
        addPlayer.add(addPlayerRight, BorderLayout.EAST);
        
        // Zweite Zeile
        settings.add(new JLabel("Turnier"));
        settings.add(this.getTurnier());
        settings.add(new JLabel("Mischen"));
        settings.add(this.getRandom());
        
        // Dritte Zeile
        startButtons.add(this.getSpielStarten());
        startButtons.add(this.getSimulieren());
        
        leftBottom.add(addPlayer);
        // Naechste Zeile
        leftBottom.add(settings);
        // Naechste Zeile
        leftBottom.add(new JLabel());
        // Naechste Zeile
        leftBottom.add(startButtons);
        
        rightBottom.add(this.getKarteLaden());
        rightBottom.add(this.getEditor());
        
        left.add(this.getSpielerListe());
        left.add(leftBottom);
        
        right.add(this.getMapPreview(), BorderLayout.CENTER);
        right.add(rightBottom, BorderLayout.SOUTH);
        
        this.setLayout(new BorderLayout(5, 5));
        
        this.add(left, BorderLayout.CENTER);
        this.add(right, BorderLayout.EAST);
        
        this.pack();
        
        // In die Mitte positionieren
        this.setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - this
                        .getWidth()) / 2, (Toolkit.getDefaultToolkit()
                        .getScreenSize().height - this.getHeight()) / 2);
        
        this.addWindowListener(this);
        this.setVisible(true);
        this.toFront();
    }
    
    /**
     * Loescht den Spieler aus der Liste.
     * 
     * @param selected
     *            Index des zu loeschenden Spielers
     */
    private void delete(int[] selected) {
        
        // Soll alles geloescht werden?
        if (selected.length == this.optionen.getSpielOpt().getKi().length) {
            this.optionen.getSpielOpt().setKi(new Strategie[0]);
            this.optionen.getSpielOpt().setNamen(new String[0]);
        } else if (selected.length < this.optionen.getSpielOpt().getKi().length) {
            
            Strategie[] kiBuffer = this.optionen.getSpielOpt().getKi();
            String[] namenBuffer = this.optionen.getSpielOpt().getNamen();
            
            // Namen loeschen
            for (int i = 0; i < selected.length; i++)
                namenBuffer[selected[i]] = null;
            
            // Defragmentieren und Verschieben der KIs
            int freiesFeld = 0;
            int besetztesFeld = 0;
            while ((freiesFeld < namenBuffer.length)
                    && (besetztesFeld < namenBuffer.length)) {
                // Finde erstes freies Feld von Unten
                while ((namenBuffer[freiesFeld] != null)
                        && (freiesFeld < namenBuffer.length)
                        && (besetztesFeld < namenBuffer.length)) {
                    freiesFeld++;
                    besetztesFeld++;
                }
                // Finde folgendes besetztes Feld
                while ((namenBuffer[besetztesFeld] == null)
                        && (besetztesFeld < namenBuffer.length))
                    besetztesFeld++;
                // Wenn Indizes in einem gueltigem Bereich
                if ((freiesFeld < namenBuffer.length)
                        && (besetztesFeld < namenBuffer.length)) {
                    // Verschiebe besetztes Feld auf unbesetzte Position
                    namenBuffer[freiesFeld] = namenBuffer[besetztesFeld];
                    kiBuffer[freiesFeld] = kiBuffer[besetztesFeld];
                    /*
                     * Loesche die alten Positionen und schiebe die Zeiger um
                     * eines weiter
                     */
                    namenBuffer[besetztesFeld] = null;
                    kiBuffer[besetztesFeld] = null;
                    freiesFeld++;
                    besetztesFeld++;
                }
            }
            
            // Kuerze die Arrays:
            Strategie[] kiShort = new Strategie[kiBuffer.length
                    - selected.length];
            String[] namenShort = new String[kiBuffer.length - selected.length];
            for (int i = 0; i < namenShort.length; i++) {
                namenShort[i] = namenBuffer[i];
                kiShort[i] = kiBuffer[i];
            }
            
            // Zurueckgeben der neuen Arrays
            this.optionen.getSpielOpt().setKi(kiShort);
            this.optionen.getSpielOpt().setNamen(namenShort);
        }
        this.repaint();
    }
    
    /**
     * Erzeugt den Button zum Starten des Editors.
     * 
     * @return Button zum Starten des Editors
     */
    private JButton getEditor() {
        if (this.editor == null) {
            this.editor = new JButton("Editor");
            this.editor.addActionListener(this);
            this.editor.setToolTipText("<html>�ffnet den Karteneditor</html>");
        }
        return this.editor;
    }
    
    /**
     * Gibt den Button zum Entfernen eines Spielers zurueck.
     * 
     * @return Button zum Entfernen
     */
    private JButton getEntfernen() {
        if (this.entfernen == null) {
            this.entfernen = new JButton("-");
            this.entfernen
                    .setToolTipText("<html>Entfernt die markierten Spieler aus der Liste</html>");
            this.entfernen.addActionListener(this);
        }
        return this.entfernen;
    }
    
    /**
     * Gibt den Button zum Hinzufuegen eines Spielers zurueck.
     * 
     * @return Button zum Hinzufuegen
     */
    private JButton getHinzufuegen() {
        if (this.hinzufuegen == null) {
            this.hinzufuegen = new JButton("+");
            this.hinzufuegen.addActionListener(this);
            this.hinzufuegen
                    .setToolTipText("<html>F�gt den Spieler der Liste hinzu</html>");
        }
        return this.hinzufuegen;
    }
    
    /**
     * Erzeugt den Button zum Laden einer Karte.
     * 
     * @return Button zum Laden einer Karte
     */
    private JButton getKarteLaden() {
        if (this.karteLaden == null) {
            this.karteLaden = new JButton("Karte laden");
            this.karteLaden.addActionListener(this);
            this.karteLaden
                    .setToolTipText("<html>�ffnet ein Dateiauswahlfenster<br>zum Laden einer Karte</html>");
        }
        return this.karteLaden;
    }
    
    /**
     * Erstellt den String Array fuer die Spielerauswahloptionen.
     * 
     * @return String Array fuer die Spielerauswahloptionen
     */
    private String[] getKiOptions() {
        String[] s = { "Spieler", "KI-Density", "KI-Density+", "KI-Heuristic",
                "KI-Greedy", "KI-Greedy+", "KI-Border", "KI-Random" };
        return s;
    }
    
    /**
     * Erzeugt die MapPreview.
     * 
     * @return MapPreview
     */
    private KartenVorschau getMapPreview() {
        if (this.mapPreview == null)
            this.mapPreview = new KartenVorschau(this.optionen.getSpielOpt());
        return this.mapPreview;
    }
    
    /**
     * Gibt eine CheckBox zur Auswahl einer zufaelligen Startreihenfolge
     * zurueck.
     * 
     * @return CheckBox zur Auswahl einer zufaelligen Startreihenfolge
     */
    private JCheckBox getRandom() {
        if (this.random == null) {
            this.random = new JCheckBox();
            this.random
                    .setToolTipText("<html>Die Reihenfolge der Spieler wird<br>jede Runde zuf\u00E4llig erstellt</html>");
        }
        return this.random;
    }
    
    /**
     * Gibt den Button zum Starten einer Simulation zurueck.
     * 
     * @return Button zum Starten einer Simulation
     */
    private JButton getSimulieren() {
        if (this.simulieren == null) {
            this.simulieren = new JButton("Simulation");
            this.simulieren.addActionListener(this);
            this.simulieren
                    .setToolTipText("<html>Startet eine Simulation mit den<br>\u00FCbergebenen Einstellungen</html>");
        }
        return this.simulieren;
    }
    
    /**
     * Gibt eine Liste mit am Spiel teilnehmenden Spielern zurueck.
     * 
     * @return Liste mit am Spiel teilnehmenden Spielern
     */
    private JTable getSpielerListe() {
        if (this.spielerListe == null) {
            this.spielerListe = new JTable(this.optionen.getSpielOpt()
                    .getMaxSpieler(), 2);
            this.spielerListe.setToolTipText("Liste aller Spieler");
        }
        return this.spielerListe;
    }
    
    /**
     * Gibt eine Liste fuer Spielernamen zurueck.
     * 
     * @return Liste fuer Textfelder zur Eingabe von Spielernamen
     */
    private JTextField getSpielerName() {
        if (this.spielerName == null)
            this.spielerName = new JTextField("Spieler");
        return this.spielerName;
    }
    
    /**
     * Erzeugt eine neue Auswahlliste fuer 0=Spieler, 1=KI-Dichte, 2=KI-Dichte+,
     * 3=KI-Random, 4=KI-Greedy, 5=KI-Greedy+, 6=KI-Border, 7=KI-GreedyOld
     * 
     * @return
     */
    private JComboBox<?> getSpielerOption() {
        if (this.spielerOption == null)
            this.spielerOption = new JComboBox<Object>(this.getKiOptions());
        return this.spielerOption;
    }
    
    /**
     * Erzeugt den Button zum Starten eines Spieles zurueck.
     * 
     * @return Button zum Starten eines Spieles
     */
    private JButton getSpielStarten() {
        if (this.spielen == null) {
            this.spielen = new JButton("Spiel");
            this.spielen.addActionListener(this);
            this.spielen
                    .setToolTipText("<html>Startet ein Spiel mit den<br>\u00FCbergebenen Einstellungen</html>");
        }
        return this.spielen;
    }
    
    /**
     * Gibt eine CheckBox zur Auswahl der Aufzeichnung zurueck.
     * 
     * @return CheckBox zur Auswahl der Aufzeichnung
     */
    private JCheckBox getTurnier() {
        if (this.turnier == null) {
            this.turnier = new JCheckBox();
            this.turnier
                    .setToolTipText("<html><h3>Aktiviert den Turniermodus</h3>Im Turniermodus werden so viele Spiele wie es Spieler gibt gespielt.<br>Jedes Spiel beginnt ein anderer Spieler, die Reihenfolge wird beibehalten.<br>Man gewinnt, wenn man nach allen Spielen die h�chste Punktzahl hat.</html>");
        }
        return this.turnier;
    }
    
    /**
     * Startet die naechste Turnierrunde.
     */
    private void naechsteRunde() {
        // Fenster schliessen und loeschen
        this.aktuellesSpiel.gameClose();
        this.aktuellesSpiel.dispose();
        this.aktuellesSpiel = null;
        
        if (!this.aktuelleTurnierRunde.isOver()) {
            JOptionPane.showMessageDialog(null, "Turnier abgebrochen");
            // Speicher leeren und Fenster sichtbar machen
            System.gc();
            this.pack();
            this.setVisible(true);
        } else {
            this.optionen.getSpielOpt().getTurnierPunkte()[this.turnierRunde] = this.aktuelleTurnierRunde
                    .getKonto();
            this.aktuelleTurnierRunde = null;
            int[] zwischenSumme = new int[this.optionen.getSpielOpt()
                    .getTurnierPunkte()[this.turnierRunde].length];
            
            for (int spieler = 0; spieler < this.optionen.getSpielOpt()
                    .getTurnierPunkte().length; spieler++)
                for (int runde = 0; runde < this.optionen.getSpielOpt()
                        .getTurnierPunkte()[spieler].length; runde++)
                    zwischenSumme[spieler] += this.optionen.getSpielOpt()
                            .getTurnierPunkte()[runde][spieler];
            
            // Ausgabe der Zwischentabelle
            int[] reihenfolge = ArrOps.indexSort(ArrOps
                    .arrayCopy(zwischenSumme));
            StringBuilder ausgabe = new StringBuilder();
            ausgabe.append("<html><h3>Spiel "
                    + this.turnierRunde
                    + " zuende</h3><table border='0'><tr><td>Platz</td><td>Spieler</td>");
            
            for (int i = 0; i <= this.turnierRunde; i++)
                ausgabe.append("<td>" + (i + 1) + ". Runde</td>");
            ausgabe.append("<td>Gesamt</td></tr>");
            
            if (this.optionen.getProgOpt().isDebug()
                    && (zwischenSumme[reihenfolge[0]] > zwischenSumme[reihenfolge[1]]))
                BugReport.recordBug(new Exception("Falsche Reihenfolge: "
                        + ArrOps.arrayToString(zwischenSumme) + "- r: "
                        + ArrOps.arrayToString(reihenfolge)));
            
            for (int spieler = 0; spieler < reihenfolge.length; spieler++) {
                ausgabe.append("<tr><td>"
                        + (spieler + 1)
                        + ".</td><td>"
                        + this.optionen.getSpielOpt().getNamen()[reihenfolge[reihenfolge.length
                                - 1 - spieler]] + "</td>");
                for (int runde = 0; runde <= this.turnierRunde; runde++)
                    ausgabe.append("<td>"
                            + this.optionen.getSpielOpt().getTurnierPunkte()[runde][reihenfolge[reihenfolge.length
                                    - 1 - spieler]] + "</td>");
                ausgabe.append("<td>"
                        + zwischenSumme[reihenfolge[reihenfolge.length - 1
                                - spieler]] + "</td></tr>");
            }
            ausgabe.append("</table></html>");
            
            JOptionPane.showMessageDialog(null, ausgabe.toString());
            
            if (this.turnierRunde < (this.optionen.getSpielOpt().getKi().length - 1)) {
                this.aktuelleTurnierRunde = new Spielkern(this.optionen,
                        ++this.turnierRunde);
                
                // Spiel starten
                this.startGame(new Spielfenster(this.aktuelleTurnierRunde,
                        this.optionen));
            } else {
                // Speicher leeren und Fenster sichtbar machen
                System.gc();
                this.pack();
                this.setVisible(true);
            }
        }
    }
    
    /**
     * Ueberprueft ob nur KIs gewaehlt wurden.
     * 
     * @param ki
     *            Liste mit KIs
     * @return Wahrheitswert
     */
    private boolean nurKi(Strategie[] ki) {
        for (Strategie element : ki)
            if (element == null)
                return false;
        return true;
    }
    
    @Override
    public void repaint() {
        this.pack();
        
        // Tabelle aktualisieren
        TableModel model = this.getSpielerListe().getModel();
        int i = 0;
        for (i = 0; i < this.optionen.getSpielOpt().getKi().length; i++) {
            model.setValueAt(this.optionen.getSpielOpt().getNamen()[i], i, 0);
            model.setValueAt(
                    (this.optionen.getSpielOpt().getKi()[i] == null) ? "Spieler"
                            : this.optionen.getSpielOpt().getKi()[i], i, 1);
        }
        for (int j = i; j < model.getRowCount(); j++) {
            model.setValueAt("", j, 0);
            model.setValueAt("", j, 1);
        }
        
        this.getMapPreview().aktualisieren();
        super.repaint();
    }
    
    /**
     * Erstellt einen Dialog der Ja/Nein/Abbrechen zur verfuegung stellt
     * 
     * @return Auswahl: 0 = ja, 1 = nein, 2 = abbrechen
     */
    private int saveQuestion() {
        String[] options = { "Ja", "Nein", "Abbrechen" };
        return JOptionPane
                .showOptionDialog(
                        null,
                        "Es wurde noch nicht gespeichert. M\u00F6chten Sie nun speichern?",
                        "Speichern", JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
    }
    
    /**
     * Erzeugt einen SplashScreen
     */
    private void showSplash() {
        final SplashScreen t = new SplashScreen();
        if (t.getBild() != null)
            new Thread(new Runnable() {
                
                @Override
                public void run() {
                    // Neues Fenster erzeugen
                    JWindow w = new JWindow();
                    
                    // Bild laden
                    w.add(t);
                    w.pack();
                    w.toFront();
                    w.setLocation(
                            (Toolkit.getDefaultToolkit().getScreenSize().width - w
                                    .getWidth()) / 2,
                            (Toolkit.getDefaultToolkit().getScreenSize().height - w
                                    .getHeight()) / 2);
                    w.setVisible(true);
                    try {
                        // Fuer 5000 Millisekunden warten
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        BugReport.recordBug(e);
                    }
                    // Fenster schlie�en und loeschen
                    w.dispose();
                    w = null;
                }
            }).start();
    }
    
    /**
     * Startet ein neues Spiel
     * 
     * @param g
     *            Spiel
     */
    protected void startGame(Spielfenster g) {
        this.setVisible(false);
        this.aktuellesSpiel = g;
        g.addWindowListener(this);
        g.build();
    }
    
    /**
     * Startet ein Turnier.
     */
    private void startTurnier() {
        this.optionen
                .getSpielOpt()
                .setTurnierPunkte(
                        new int[this.optionen.getSpielOpt().getKi().length][this.optionen
                                .getSpielOpt().getKi().length]);
        this.turnierRunde = 0;
        
        this.aktuelleTurnierRunde = new Spielkern(this.optionen,
                this.turnierRunde);
        
        // Spiel starten
        this.startGame(new Spielfenster(this.aktuelleTurnierRunde,
                this.optionen));
    }
    
    @Override
    public void windowActivated(WindowEvent e) {
    }
    
    @Override
    public void windowClosed(WindowEvent e) {
        // if (this.aktuellesSpiel == null)
        // setVisible(true);
    }
    
    @Override
    public void windowClosing(WindowEvent e) {
        if (e.getSource() == this) {
            // Programm beenden
            
            // TODO Reaktiviern
            // this.optionen.getStatistik().save();
            this.optionen.save();
            
            // Programm beenden
            System.exit(0);
            
        } else if (e.getSource() == this.aktuelleSimulation) {
            
            // Simulation schliessen
            this.aktuelleSimulation.dispose();
            this.aktuelleSimulation = null;
            this.pack();
            this.setVisible(true);
            
        } else if (e.getSource() == this.aktuellesSpiel) {
            // Spiel schlie�en
            
            if (this.getTurnier().isSelected()) {
                int state = 0;
                if (!this.aktuelleTurnierRunde.isOver()) {
                    // Turnier abbrechen
                    String[] options = { "Ja", "Nein" };
                    state = JOptionPane
                            .showOptionDialog(
                                    null,
                                    "M�chten Sie das Turnier abbrechen? Alle erspielten Werte gehen dabei veroren.",
                                    "Turnierabbruch",
                                    JOptionPane.YES_NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE, null,
                                    options, options[0]);
                }
                switch (state) {
                    case 0:
                        // Turnier fortsetzen
                        // TODO �berpr�fen
                        this.naechsteRunde();
                        break;
                    case 1:
                        // Zurueck zum Spiel
                        this.aktuellesSpiel.setVisible(true);
                        this.setVisible(false);
                        break;
                    default:
                        break;
                }
                
            } else if (!this.aktuellesSpiel.isSaved())
                // Spiel noch nicht gespeichert
                switch (this.saveQuestion()) {
                    case 0:
                        // Spiel speichern
                        this.aktuellesSpiel.save();
                    case 1:
                        // Fenster schliessen und loeschen
                        this.aktuellesSpiel.gameClose();
                        this.aktuellesSpiel.dispose();
                        this.aktuellesSpiel = null;
                        this.pack();
                        this.setVisible(true);
                        break;
                    default:
                        break;
                }
            else {
                
                // Fenster schliessen und loeschen
                this.aktuellesSpiel.gameClose();
                this.aktuellesSpiel.dispose();
                this.aktuellesSpiel = null;
                this.pack();
                this.setVisible(true);
            }
            
        } else if (e.getSource() == this.me)
            // Karteneditor schliessen
            if (!this.me.isSaved())
                // Karte noch nicht gespeichert
                switch (this.saveQuestion()) {
                    case 0:
                        // Karte speichern
                        this.me.save();
                    case 1:
                        // Fenster schliessen
                        this.me.dispose();
                        this.pack();
                        this.setVisible(true);
                        break;
                    default:
                        break;
                }
            else
                // Fenster schliessen
                this.me.dispose();
    }
    
    @Override
    public void windowDeactivated(WindowEvent e) {
    }
    
    @Override
    public void windowDeiconified(WindowEvent e) {
    }
    
    @Override
    public void windowIconified(WindowEvent e) {
    }
    
    @Override
    public void windowOpened(WindowEvent e) {
    }
}
