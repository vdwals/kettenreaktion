package de.vdwals.kettenreaktion.gui;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import de.vdwals.kettenreaktion.implement.BugReport;

/**
 * Abstrakte Oberklasse fuer alle Fenster.
 * 
 * @author Dennis van der Wals
 * 
 */
public abstract class Fenster extends JFrame {
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = -8335945770793081462L;
    
    /**
     * Standardkonstruktor.
     */
    public Fenster() {
        super();
        
        // Icon setzen
        try {
            this.setIconImage(ImageIO.read(ClassLoader
                    .getSystemResource("gui/images/radioaktiv.xrt")));
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
    }
    
    /**
     * Standardkonstruktor, erwartet Fenstertitel
     * 
     * @param title
     *            Fenstertitel
     */
    public Fenster(String title) {
        super(title);
        
        // Icon setzen
        try {
            this.setIconImage(ImageIO.read(ClassLoader
                    .getSystemResource("gui/images/radioaktiv.xrt")));
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
    }
    
}
