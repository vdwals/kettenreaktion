package de.vdwals.kettenreaktion.gui.help;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 * Kleines Fenster zum Anzeigen einer Programmhilfe. Bilder ins Netz auslagern
 * (In den html-Dateien)
 * 
 * @author Dennis van der Wals
 * 
 */
public class Help extends JFrame implements HyperlinkListener {
    /**
     * Serial-ID
     */
    private static final long serialVersionUID = -7775340591408026245L;
    private JEditorPane       inhalt, kontext;
    
    /**
     * Standardkonstruktor. Laedt die Vordefinierten Hilfeseiten.
     * 
     * @throws IOException
     */
    public Help() throws IOException {
        this.inhalt = new JEditorPane(
                ClassLoader.getSystemResource("gui/help/index_de.html"));
        this.inhalt.addHyperlinkListener(this);
        this.inhalt.setEditable(false);
        
        this.kontext = new JEditorPane(
                ClassLoader.getSystemResource("gui/help/einleitung_de.html"));
        this.kontext.addHyperlinkListener(this);
        this.kontext.setEditable(false);
        
        this.setLayout(new BorderLayout(5, 5));
        
        JScrollPane inhaltContainer = new JScrollPane(this.inhalt);
        JScrollPane kontextContainer = new JScrollPane(this.kontext);
        
        this.add(inhaltContainer, BorderLayout.WEST);
        this.add(kontextContainer, BorderLayout.CENTER);
        
        this.pack();
        this.setVisible(false);
    }
    
    @Override
    public void hyperlinkUpdate(HyperlinkEvent arg0) {
        HyperlinkEvent.EventType typ = arg0.getEventType();
        
        if (typ == HyperlinkEvent.EventType.ACTIVATED)
            if (arg0.getSource() == this.inhalt)
                try {
                    this.kontext.setPage(arg0.getURL());
                } catch (IOException e) {
                }
            else
                try {
                    Desktop.getDesktop().browse(
                            new URI(arg0.getURL().toString()));
                } catch (Exception e) {
                }
    }
    
}
