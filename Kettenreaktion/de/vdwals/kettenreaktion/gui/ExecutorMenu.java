package de.vdwals.kettenreaktion.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.implement.Feedback;
import de.vdwals.kettenreaktion.implement.Meta;
import de.vdwals.kettenreaktion.spiel.gui.Spielfenster;
import de.vdwals.kettenreaktion.spiel.io.SaveGameIO;

/**
 * Klasse fuer die Menueleiste des Spieles.
 * 
 * @author Dennis van der Wals
 * 
 */
public class ExecutorMenu extends JMenuBar implements ActionListener {
    // Komponenten
    private JMenu             hilfe, file;
    private JMenuItem         anleitung, statistik, load, einstellungen, ueber,
            feedback;
    private Icon              statI, loadI, helpI, aboutI, settingsI;
    
    // Spieleinstellungen
    private SaveGameIO        io;
    private Einstellungen     optionen;
    private Executor          ex;
    private Feedback          fdb;
    
    /**
     * SerialID
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Standardkonstruktor.
     * 
     * @param save
     *            FileHandler zum speichern des Spieles
     * @param einstellungen
     *            Spieleinstellungen
     */
    public ExecutorMenu(SaveGameIO save, Einstellungen einstellungen,
            Executor ex) {
        this.optionen = einstellungen;
        this.io = save;
        this.ex = ex;
        this.fdb = new Feedback();
        this.loadIcons();
        this.add(this.getFile());
        this.add(this.getHilfe());
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getSource() == this.getLaden())
            try {
                this.io.load();
                // Einstellungen uebernehmen
                this.optionen.setSpielOpt(this.io.getEinstellungen());
                
                // Spiel uebernehmen
                this.ex.startGame(new Spielfenster(this.io.getSpiel(),
                        this.optionen));
            } catch (IOException e) {
                BugReport.recordBug(e);
                JOptionPane.showMessageDialog(this,
                        "Spielstand konnte nicht geladen werden.");
            }
        else if (arg0.getSource() == this.getEinstellungen())
            // Einstellungen zeigen
            this.optionen.setVisible(true);
        else if (arg0.getSource() == this.getStatistik())
            ;
        else if (arg0.getSource() == this.getAnleitung())
            this.optionen.getProgOpt().getHilfe().setVisible(true);
        else if (arg0.getSource() == this.getUeber())
            this.getAbout();
        else if (arg0.getSource() == this.getFeedback())
            this.fdb.setVisible(true);
    }
    
    /**
     * @return the about
     */
    public void getAbout() {
        String[] options = { "Ok" };
        String about = "<html>Kettenreaktion<br><br>Version: "
                + Meta.v
                + "<br>Entwickelt: 05.07.2011<br><br>"
                + "<a href='www.dionysoft.kilu.de'>www.dionysoft.kilu.de</a><br>"
                + "<br>Spielidee: Prof. Dr. Kinzel<br>"
                + "Software: Dennis van der Wals<br>"
                + "Mitentwicklung und Idee:<br>"
                + "Peter Embacher, Matthias Sch�tz, Martin Winnerlein<br>"
                + "2D Graphiken von Jason Simas <a href='chart2d.sourceforge.net'>chart2d.sourceforge.net</a>";
        JOptionPane.showOptionDialog(null, about, "�ber...",
                JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                options, options[0]);
    }
    
    /**
     * Gibt den Menueeintrag fuer die Hilfe zurueck.
     * 
     * @return Menueeintrag fuer die Hilfe
     */
    private JMenuItem getAnleitung() {
        if (this.anleitung == null) {
            this.anleitung = new JMenuItem("Anleitung", this.helpI);
            this.anleitung.addActionListener(this);
            this.anleitung.setToolTipText("Zeigt die Spielanleitung");
        }
        return this.anleitung;
    }
    
    /**
     * Gibt den Menueeintrag fuer die Einstellungen zurueck.
     * 
     * @return Menueeintrag fuer die Einstellungen
     */
    private JMenuItem getEinstellungen() {
        if (this.einstellungen == null) {
            this.einstellungen = new JMenuItem("Einstellungen", this.settingsI);
            this.einstellungen.addActionListener(this);
            this.einstellungen
                    .setToolTipText("\u00D6ffnet die Spieleinstellungen");
        }
        return this.einstellungen;
    }
    
    /**
     * Gibt den Menueeintrag fuer die Feedback zurueck.
     * 
     * @return Menueeintrag fuer die Feedback
     */
    private JMenuItem getFeedback() {
        if (this.feedback == null) {
            this.feedback = new JMenuItem("Feedback");
            this.feedback.addActionListener(this);
        }
        return this.feedback;
    }
    
    /**
     * Gibt das Spielmenue zurueck.
     * 
     * @return Spielmenue
     */
    private JMenu getFile() {
        if (this.file == null) {
            this.file = new JMenu("Spiel");
            this.file.add(this.getStatistik());
            this.file.addSeparator();
            this.file.add(this.getLaden());
            this.file.addSeparator();
            this.file.add(this.getEinstellungen());
        }
        return this.file;
    }
    
    /**
     * Gibt das Hilfemenue zurueck,
     * 
     * @return Hilfemenue
     */
    private JMenu getHilfe() {
        if (this.hilfe == null) {
            this.hilfe = new JMenu("Hilfe");
            this.hilfe.add(this.getAnleitung());
            this.hilfe.add(this.getUeber());
            this.hilfe.addSeparator();
            this.hilfe.add(this.getFeedback());
        }
        return this.hilfe;
    }
    
    /**
     * Gibt den Menueeintrag zum Speichern zurueck.
     * 
     * @return Menueeintrag zum Speichern
     */
    private JMenuItem getLaden() {
        if (this.load == null) {
            this.load = new JMenuItem("Spiel laden", this.loadI);
            this.load.addActionListener(this);
            this.load.setToolTipText("L�dt ein Spiel");
        }
        return this.load;
    }
    
    /**
     * Gibt den Menueeintrag fuer die Statistik zurueck.
     * 
     * @return Menueeintrag fuer die Statistik
     */
    private JMenuItem getStatistik() {
        if (this.statistik == null) {
            this.statistik = new JMenuItem("Statistik", this.statI);
            this.statistik.addActionListener(this);
            this.statistik.setToolTipText("\u00D6ffnet die Statistik");
        }
        return this.statistik;
    }
    
    /**
     * Gibt den Menueeintrag fuer die Programminformationen zurueck.
     * 
     * @return Menueeintrag fuer die Programminformationen
     */
    private JMenuItem getUeber() {
        if (this.ueber == null) {
            this.ueber = new JMenuItem("\u00DCber das Programm", this.aboutI);
            this.ueber.addActionListener(this);
        }
        return this.ueber;
    }
    
    /**
     * Erstellt die Icons.
     */
    private void loadIcons() {
        this.statI = new ImageIcon(this.getClass().getResource(
                "images/application_view_list.xrt"));
        this.loadI = new ImageIcon(this.getClass().getResource(
                "images/Upload.xrt"));
        this.helpI = new ImageIcon(this.getClass().getResource(
                "images/help.xrt"));
        this.aboutI = new ImageIcon(this.getClass().getResource(
                "images/info.xrt"));
        this.settingsI = new ImageIcon(this.getClass().getResource(
                "images/settings.xrt"));
    }
}
