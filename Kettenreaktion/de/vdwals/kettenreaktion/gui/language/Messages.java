package de.vdwals.kettenreaktion.gui.language;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Scanner;

import de.vdwals.kettenreaktion.implement.BugReport;

/**
 * Klasse zur Verwaltung verschiedener Sprachen.
 * 
 * @author Dennis van der Wals
 */
public class Messages {
    private static String         BUNDLE_NAME     = "gui.language.messages_de";   //$NON-NLS-1$
                                                                                   
    private static ResourceBundle RESOURCE_BUNDLE = ResourceBundle
                                                          .getBundle(BUNDLE_NAME);
    private static File           lang            = new File("data/lang.ini");    //$NON-NLS-1$
                                                                                   
    /**
     * Gibt die Sprache zurueck.
     * 
     * @return de = Deutsch, en = Englisch, sv = Schwedisch
     */
    public static String getLang() {
        return BUNDLE_NAME.substring(BUNDLE_NAME.length() - 2);
    }
    
    /**
     * Gibt die vorgesehene Nachricht zurueck.
     * 
     * @param key
     *            Schluessel
     * @return Nachricht
     */
    public static String getString(String key) {
        try {
            return RESOURCE_BUNDLE.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
    
    /**
     * Setzt die gewuenschte Sprache.
     * 
     * @param l
     *            de = Deutsch, en = Englisch, sv = Schwedisch
     * @throws IOException
     */
    public static void lang(String l) throws IOException {
        BUNDLE_NAME = "gui.language.messages_" + l; //$NON-NLS-1$
        RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
        FileWriter fw = new FileWriter(lang);
        fw.write(l);
        fw.close();
    }
    
    /**
     * Laedt die gespeicherte Sprache.
     */
    public static void loadLang() {
        try {
            Scanner scan = new Scanner(lang);
            lang(scan.next());
            scan.close();
        } catch (Exception e) {
            try {
                File ort = new File("data");
                ort.mkdir();
                lang.createNewFile();
                lang("de"); //$NON-NLS-1$
            } catch (IOException e1) {
                BugReport.recordBug(e1);
            }
        }
    }
}
