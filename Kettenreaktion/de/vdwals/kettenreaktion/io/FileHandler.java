package de.vdwals.kettenreaktion.io;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

/**
 * Klasse zur Standardkommunikation mit Dateien.
 * 
 * @author Dennis van der Wals
 * 
 */
public abstract class FileHandler {
    protected String     fileExtension;
    protected final byte BIT_MENGE      = 6;
    protected final byte BIT_BREITE     = 2;
    protected final byte DB_VERSION     = 1;
    protected final byte INTEGER_LAENGE = 4;
    protected final byte MAX_BIT        = 8;
    
    public abstract byte[] generateData();
    
    /**
     * Laedt Daten aus einer Datei.
     * 
     * @throws IOException
     *             Falls Datei nicht gelesen werden kann
     * @throws Exception
     */
    public abstract void load() throws Exception;
    
    /**
     * Oeffnet eine Datei in einem Auswahlfenster.
     * 
     * @param save
     *            Soll die Datei gespeichert werden.
     * @param name
     *            Name der zu selektierenden Datei
     * @param ort
     *            Standardordner
     * @return gewaehlte Datei
     */
    protected File open(boolean save, String name, String ort) {
        
        // Erstellt den benoetigten Ordner, falls nicht vorhanden
        File f = null;
        
        if (ort != null) {
            f = new File(ort + "/");
            if (!f.exists())
                f.mkdir();
        }
        
        JFileChooser fc = new JFileChooser(f);
        fc.setDialogTitle("Datei w\u00E4hlen");
        if (name != null)
            fc.setSelectedFile(new File(name + this.fileExtension));
        fc.setFileFilter(new FileFilter() {
            
            @Override
            public boolean accept(File f) {
                return (f.getName().endsWith(FileHandler.this.fileExtension) || f
                        .isDirectory());
            }
            
            @Override
            public String getDescription() {
                return "*" + FileHandler.this.fileExtension;
            }
        });
        int state = 0;
        if (save)
            state = fc.showSaveDialog(null);
        else
            state = fc.showOpenDialog(null);
        if (state == JFileChooser.APPROVE_OPTION)
            return fc.getSelectedFile();
        return null;
    }
    
    /**
     * Schreibt Daten in eine Datei.
     * 
     * @throws IOException
     *             Falls Datei nicht beschrieben werden kann
     */
    public abstract void save() throws IOException;
}
