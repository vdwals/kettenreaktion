package de.vdwals.kettenreaktion.spiel;

import java.awt.Point;
import java.util.LinkedList;

import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.spiel.mod.Mod;

/**
 * Klasse zum ausloesen einer Reaktion. Errechnet das neue Brett, sowie Anzahl
 * der Reaktionen, Anzahl der gewonnenen Chips und Anzahl der Chips aus der
 * Bank.
 * 
 * @author Dennis van der Wals
 * 
 */
public class SecureReaktor {
    /**
     * Klasse zum speichern der Aenderungen waehrend der Reaktion.
     * 
     * @author Dennis van der Wals
     */
    public class ReaktorPoint extends Point {
        
        /**
         * Default SerialID
         */
        private static final long        serialVersionUID = 1L;
        /**
         */
        private byte                     change;
        /**
         */
        private LinkedList<ReaktorPoint> folgende;
        /**
         */
        private ReaktorPoint             vorgaender;
        
        /**
         * Erzeugt einen neuen Punkt und speichert den Wert +1 als Aenderung.
         * 
         * @param p
         *            Punkt, an dem die Aenderung stattfindet.
         * @param vorgaenger
         *            Quellpunkt der Reaktion, die hierher fuehrte
         */
        private ReaktorPoint(Point p, ReaktorPoint vorgaenger) {
            super(p);
            this.change = 1;
            this.folgende = new LinkedList<ReaktorPoint>();
            this.vorgaender = vorgaenger;
        }
        
        /**
         * Fuegt einen Punkt an den Koordinaten als Nachfolger an, wenn er im
         * Feld ist.
         * 
         * @param x
         *            x-Koordinate
         * @param y
         *            y-Koordinate
         */
        private void add(int x, int y) {
            Point p = new Point(x, y);
            if (SecureReaktor.this.isInFeld(p))
                this.folgende.add(new ReaktorPoint(p, this));
        }
        
        /**
         * Fuegt einen Punkt als Nachfolger an, wenn er im Feld ist.
         * 
         * @param p
         *            Nachfolgender Punkt
         */
        private void add(ReaktorPoint p) {
            if (SecureReaktor.this.isInFeld(p))
                this.folgende.add(p);
        }
        
        /**
         * Gibt zurueck, welche Aenderungen vorgenommen wurden.
         * 
         * @return Aenderung
         */
        public int getChange() {
            return this.change;
        }
        
        /**
         * Gibt eine LinkedList mit allen folgenden Punkten zurueck
         * 
         * @return Alle folgenden Aenderungen
         */
        public LinkedList<ReaktorPoint> getFolgende() {
            return this.folgende;
        }
        
        /**
         * Gibt den Wert des Feldes zurueck.
         * 
         * @return Wert des Feldes
         */
        public int getValue() {
            return SecureReaktor.this.neuesBrett[this.x][this.y];
        }
        
        /**
         * Gibt zurueck, ob der Chip von einem Spieler gesetzt wurde.
         * 
         * @return Wahrheitswert
         */
        public boolean isGesetzt() {
            return (this.vorgaender == null);
        }
        
        /**
         * Laesst den Punkt reagieren und gibt den neuen Punkt mit reduzierter
         * Punktzahl zurueck.
         * 
         * @return neuen Punkt mit neuer Aenderung
         */
        private ReaktorPoint reagiere() {
            
            // Neuen Punkt erzeugen
            ReaktorPoint p = new ReaktorPoint(new Point(this.x, this.y), this);
            p.change = (byte) -SecureReaktor.this.mod.getChipsFuerReaktion();
            
            // Nachbarn ermitteln
            Point[] borders = SecureReaktor.this.mod.getNachbarn(p);
            for (Point border : borders)
                this.add(border.x, border.y);
            this.add(p);
            return p;
        }
        
    }
    
    /**
     */
    private int          gewinn;
    /**
     */
    private int          reaktionen;
    /**
     */
    private int          ausBank;
    /**
     */
    private byte         neuesBrett[][];
    /**
     */
    private Mod          mod;
    
    /**
     */
    private ReaktorPoint aenderung;
    
    public SecureReaktor(Mod mod) {
        this.mod = mod;
    }
    
    /**
     * Zaehlt, wie viele der uebergebenen Chips aus dem Spielfeld "fallen".
     * 
     * @param borders
     *            zu ueberpruefende Chips, meist Reaktionschips
     * @return Anzahl der Chips, welche aus dem Spielfeld fallen
     */
    private int countChipsGoOut(Point[] borders) {
        int counter = 0;
        for (int i = 0; i < borders.length; i++)
            if (!this.isInFeld(borders[i]))
                counter++;
        return counter;
    }
    
    /**
     * Gibt den Kopf des Aenderungsbaumes der letzten Reaktion zurueck.
     * 
     * @return Kopf des Aenderungsbaumes
     */
    public ReaktorPoint getAenderung() {
        return this.aenderung;
    }
    
    /**
     * Gibt zurueck, wie viele Chips nach der letzten Reaktion aus der Bank
     * ausgezahlt werden muessen. Entspricht der Anzahl der Reaktionen, die
     * nicht am Rand stattfanden, also bei der kein Chip verloren ging.
     * 
     * @return Anzahl der Chips, die aus der Bank entfernt werden muessen
     */
    public int getAusBank() {
        return this.ausBank;
    }
    
    /**
     * Gibt den Gewinn der letzten Reaktion zurueck. Als Gewinn werden alle
     * Chips gezaehlt, die aus dem Spielfeld fallen.
     * 
     * @return Gewinn der letzten Reaktion
     */
    public int getGewinn() {
        return this.gewinn;
    }
    
    /**
     * Gibt eine Kopie des Spielfelds zurueck, welches nach der letzten Reaktion
     * entstand.
     * 
     * @return Ausreagiertes Spielfeld mit hinterlegtem Chip
     */
    public byte[][] getNeuesBrett() {
        return ArrOps.arrayCopy(this.neuesBrett);
    }
    
    /**
     * Gibt die Anzahl der Reaktionen zurueck, die waehrend der letzten
     * Kettenreaktion stattfanden.
     * 
     * @return Anzahl der letzten Reaktionen
     */
    public int getReaktionen() {
        return this.reaktionen;
    }
    
    /**
     * Gibt zurueck, ob der Punkt p innerhalb ger Grenzen des Spielfeldes liegt.
     * 
     * @param p
     *            Punkt
     * @return Wahrheitswert
     */
    private boolean isInFeld(Point p) {
        if ((p.x < 0) || (p.x >= this.neuesBrett.length) || (p.y < 0)
                || (p.y >= this.neuesBrett[0].length))
            return false;
        return (this.neuesBrett[p.x][p.y] != -1);
    }
    
    /**
     * Berechnet die Kettenreaktion einer Eingabe, speichert/ueberschreibt
     * Anzahl der Reaktionen, Anzahl der gewonnenen Chips und Anzahl der Chips
     * aus der Bank.
     * 
     * @param brett
     *            Aktuelles Spielbrett
     * @param chips
     *            gelegte Chips
     * @param bf
     *            Soll der Breitensuchealgorithmus benutzt werden
     * @param ki
     *            Wird es fuer KI berechnet
     * @return neues Spielfeld
     */
    public byte[][] reaktion(byte[][] brett, Point chip, boolean bf) {
        // Initialisiert die Variablen
        this.gewinn = this.reaktionen = this.ausBank = 0;
        this.neuesBrett = ArrOps.arrayCopy(brett);
        
        this.aenderung = new ReaktorPoint(chip, null);
        LinkedList<ReaktorPoint> stapel = new LinkedList<ReaktorPoint>();
        if (bf)
            return this.reaktionBF(stapel);
        
        return this.reaktionDF(stapel);
    }
    
    /**
     * Berechnet die Kettenreaktion einer Eingabe nach dem
     * Breitensuchealgorithmus, speichert/ueberschreibt Anzahl der Reaktionen,
     * Anzahl der gewonnenen Chips und Anzahl der Chips aus der Bank.
     * 
     * @param stapel
     *            Liste mit abzuarbeitenden Punkten
     * @return resultierendes Spielfeld
     */
    private byte[][] reaktionBF(LinkedList<ReaktorPoint> stapel) {
        stapel.offer(this.aenderung);
        while (!stapel.isEmpty()) {
            ReaktorPoint p = stapel.poll();
            
            // Ueberpruft, ob eine Reaktion stattfindet
            if (p.getValue() >= this.mod.getChipsFuerReaktion()) {
                Point[] nachbarn = this.mod.getNachbarn(p);
                
                // Erhoeht den Zaehler um 1
                this.reaktionen++;
                
                // Ueberprueft, ob wir in einer Ecke sind fuer Extrachips
                int tmpGewinn = this.countChipsGoOut(nachbarn);
                
                /*
                 * Reduziert die Chips der Bank um 1 wenn die Reaktion nicht am
                 * Spielfeldrand stattfand
                 */
                if (tmpGewinn > 0)
                    this.gewinn += tmpGewinn;
                else
                    this.ausBank++;
                
                /*
                 * Die Chips abziehen und den Folgepunkt mit weniger Chips
                 * speichern
                 */
                ReaktorPoint tmp = p.reagiere();
                this.neuesBrett[p.x][p.y] -= this.mod.getChipsFuerReaktion();
                stapel.addAll(p.getFolgende());
                stapel.remove(tmp);
                
            } else {
                
                // Legt einen Chip auf das Feld
                this.neuesBrett[p.x][p.y]++;
                
                // Wenn Reaktionausloeser wieder zur Ueberpruefung anmelden
                if (p.getValue() >= this.mod.getChipsFuerReaktion())
                    stapel.offer(p);
            }
        }
        return this.neuesBrett;
    }
    
    /**
     * Berechnet die Kettenreaktion einer Eingabe nach dem
     * Tiefensuchealgorithmus, speichert/ueberschreibt Anzahl der Reaktionen,
     * Anzahl der gewonnenen Chips und Anzahl der Chips aus der Bank.
     * 
     * @param stapel
     *            Liste mit abzuarbeitenden Chips
     * @return resultierendes Spielfeld
     */
    private byte[][] reaktionDF(LinkedList<ReaktorPoint> stapel) {
        stapel.push(this.aenderung);
        while (!stapel.isEmpty()) {
            
            ReaktorPoint p = stapel.pop();
            
            // Ueberpruft, ob eine Reaktion stattfindet
            if (p.getValue() >= this.mod.getChipsFuerReaktion()) {
                
                Point[] nachbarn = this.mod.getNachbarn(p);
                
                // Erhoeht den Zaehler um 1
                this.reaktionen++;
                
                // Ueberprueft, ob wir in einer Ecke sind fuer Extrachips
                int tmpGewinn = this.countChipsGoOut(nachbarn);
                
                /*
                 * Reduziert die Chips der Bank um 1 wenn die Reaktion nicht am
                 * Spielfeldrand stattfand
                 */
                if (tmpGewinn > 0)
                    this.gewinn += tmpGewinn;
                else
                    this.ausBank++;
                
                /*
                 * Die Chips abziehen und den Folgepunkt mit weniger Chips
                 * speichern
                 */
                this.neuesBrett[p.x][p.y] -= this.mod.getChipsFuerReaktion();
                ReaktorPoint tmp = p.reagiere();
                for (ReaktorPoint reaktorPoint : p.getFolgende())
                    stapel.push(reaktorPoint);
                stapel.remove(tmp);
                
            } else {
                
                // Wert um einen Chip erhoehen
                this.neuesBrett[p.x][p.y]++;
                
                // Wenn Reaktionsausloeser wieder zur Ueberpruefung anmelden
                if (p.getValue() >= this.mod.getChipsFuerReaktion())
                    stapel.push(p);
            }
        }
        return this.neuesBrett;
    }
    
    /**
     * Berechnet die Kettenreaktion einer Eingabe, speichert/ueberschreibt
     * Anzahl der Reaktionen, Anzahl der gewonnenen Chips und Anzahl der Chips
     * aus der Bank. Besonders schnell, da keine unnoetigen Aufzeichnungen
     * gemacht werden.
     * 
     * @param brett
     *            Spielbrett - bleibt unveraendert, da es kopiert wird
     * @param chip
     *            Spielstein
     * @return resultierendes Spielfeld
     */
    public byte[][] reaktionKI(byte[][] brett, Point chip) {
        // Initialisiert die Variablen
        this.gewinn = this.reaktionen = this.ausBank = 0;
        this.neuesBrett = ArrOps.arrayCopy(brett);
        LinkedList<Point> stapel = new LinkedList<Point>();
        stapel.offer(chip);
        
        while (!stapel.isEmpty()) {
            
            Point p = stapel.poll();
            
            // Ueberprueft, ob es im Feld liegt
            if (this.isInFeld(p))
                // Ueberpruft, ob eine Reaktion stattfindet
                if (this.neuesBrett[p.x][p.y] >= this.mod
                        .getChipsFuerReaktion()) {
                    Point[] nachbarn = this.mod.getNachbarn(p);
                    
                    // Erhoeht den Zaehler um 1
                    this.reaktionen++;
                    
                    // Ueberprueft, ob wir in einer Ecke sind fuer Extrachips
                    int tmp = this.countChipsGoOut(nachbarn);
                    
                    /*
                     * Reduziert die Chips der Bank um 1 wenn die Reaktion nicht
                     * am Spielfeldrand stattfand
                     */
                    if (tmp > 0)
                        this.gewinn += tmp;
                    else
                        this.ausBank++;
                    
                    // Erstellt die Chips, welche Reagieren
                    for (Point element : nachbarn)
                        stapel.offer(new Point(element.x, element.y));
                    
                    this.neuesBrett[p.x][p.y] += -this.mod
                            .getChipsFuerReaktion();
                    
                } else {
                    
                    // Wenn Reaktionsausloeser zur Ueberpruefung einreihen
                    if (this.neuesBrett[p.x][p.y] == (this.mod
                            .getChipsFuerReaktion() - 1))
                        stapel.offer(p);
                    
                    // Wert um einen Chip erhoehen
                    this.neuesBrett[p.x][p.y] += 1;
                }
        }
        return this.neuesBrett;
    }
    
}
