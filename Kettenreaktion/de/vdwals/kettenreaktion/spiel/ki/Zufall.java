package de.vdwals.kettenreaktion.spiel.ki;

import java.awt.Point;

/**
 * Strategie, die Chips zufaellig uebers Brett verteilt.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Zufall extends Strategie {
    
    /**
     * Standardkonstruktor.
     */
    public Zufall() {
        super("KI-Random");
    }
    
    @Override
    public Point[] getChips(byte[][] Brett) {
        Point[] chips = new Point[this.chipsToSet];
        for (int i = 0; i < this.chipsToSet; i++) {
            int y = Math.round((float) Math.random() * (Brett[0].length - 1));
            int x = Math.round((float) Math.random() * (Brett.length - 1));
            chips[i] = new Point(x, y);
            if (Brett[x][y] == -1)
                i--;
        }
        return chips;
    }
    
}
