package de.vdwals.kettenreaktion.spiel.ki;

import java.awt.Point;
import java.util.LinkedList;
import java.util.TreeSet;

import de.vdwals.kettenreaktion.implement.ArrOps;

/**
 * Strategie die den bestmoeglichen Zug bestimmt.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Heuristic extends Strategie {
    /**
     * Klasse um einen Punkt mit seinen Nachfolgern sowie den Gewinn mit den der
     * Nachfolgern zu speichern
     * 
     * @author Dennis van der Wals
     */
    private class HeuristicPoint extends Point {
        /**
         * Default SerialID
         */
        private static final long         serialVersionUID = 1L;
        public LinkedList<HeuristicPoint> equiFolgende;
        /**
         */
        public int                        gewinn;
        public int                        nachfolgeGewinn;
        
        /**
         * Standardkonstruktor, wie ein Punkt.
         * 
         * @param x
         *            x-Koordinate
         * @param y
         *            y-Koordinate
         */
        public HeuristicPoint(int x, int y) {
            super(x, y);
            this.equiFolgende = new LinkedList<HeuristicPoint>();
            this.gewinn = this.nachfolgeGewinn = 0;
        }
        
        /**
         * Fuegt einen Punkt der Liste hinzu und speichert dessen Gewinn ab
         * 
         * @param g
         *            Nachfolgender Punkt
         */
        public void add(HeuristicPoint g) {
            this.equiFolgende.add(g);
            
            // Herausfiltern der zu kleinen Eintraege
            if (g.getGewinn() > this.nachfolgeGewinn) {
                LinkedList<HeuristicPoint> toDelete = new LinkedList<HeuristicPoint>();
                for (HeuristicPoint point : this.equiFolgende)
                    if (point.getGewinn() < (g.getGewinn() * Heuristic.this.FAKTOR))
                        toDelete.add(point);
                this.equiFolgende.removeAll(toDelete);
            }
            this.nachfolgeGewinn = g.getGewinn();
        }
        
        /**
         * Loescht die Liste der Nachfolger und setzt den Gewinn zurueck.
         */
        public void clear() {
            this.equiFolgende.clear();
            this.nachfolgeGewinn = 0;
        }
        
        /**
         * Gibt den Gewinn der Zuege zurueck.
         * 
         * @return Gewinn der Zuege
         */
        public int getGewinn() {
            return this.gewinn + this.nachfolgeGewinn;
        }
        
        /**
         * Gibt aus der Liste der Nachfolger mit maximalem Gewinn einen
         * zufaelligen zurueck.
         * 
         * @return Zufaelliger Nachfolger
         */
        @SuppressWarnings("unused")
        public HeuristicPoint getNachfolger() {
            int index = Math.round((float) Math.random()
                    * (this.equiFolgende.size() - 1));
            return (this.equiFolgende.size() > 0) ? this.equiFolgende
                    .get(index) : null;
        }
    }
    
    /**
     * Klasse zum Sammeln von Spielbrettern
     * 
     * @author Dennis van der Wals
     */
    private class Map {
        /**
         */
        private byte[][] karte;
        /**
         */
        private Point[]  chips;
        
        public Map(byte[][] brett, HeuristicPoint chip1, HeuristicPoint chip2,
                HeuristicPoint chip3) {
            this.chips = new Point[3];
            this.chips[0] = chip1;
            this.chips[1] = chip2;
            this.chips[2] = chip3;
            this.karte = Heuristic.this.reaktion(brett, chip1);
            this.karte = Heuristic.this.reaktion(this.karte, chip2);
            this.karte = Heuristic.this.reaktion(this.karte, chip3);
        }
        
        /**
         * @return
         */
        public Point[] getChips() {
            return this.chips;
        }
        
        public long getHash() {
            return ArrOps.hash(this.karte);
        }
        
        /**
         * @return
         */
        public byte[][] getKarte() {
            return this.karte;
        }
    }
    
    /**
     */
    private final double FAKTOR = 0.7;
    
    /**
     * Standardkonstruktor.
     * 
     * @param zufall
     *            Bestimmt, ob ein Zufallsmodus genutzt werden soll.
     */
    public Heuristic() {
        super("KI-Heuristic");
    }
    
    /*
     * Ermittle die bestmoegliche Chipkombination und alle Kombinationen die
     * Innerhalb des akzeptablen Intervalls unterhalb des maximums liegen nach
     * Greedy. erstelle eine Liste mit all diesen Kombinationen und
     * Permutationen daraus. Ermittle aus alle diesen Moeglichkeiten jene, die
     * dem nachfolgendem Spieler den geringsten bestmoeglichen Wert uebrig
     * lassen.
     */
    
    /**
     * Ermittelt aus einer uebergebenen Auswahl von zu legenden Chips die beste
     * Reihenfolge um den Gegnern zu schaden
     * 
     * @param brett
     *            Spielfeld
     * @param first
     *            Chips
     * @return Chips
     */
    private Point[] getBest(byte[][] brett, HeuristicPoint first) {
        TreeSet<Long> maps = new TreeSet<Long>();
        int minWin = Integer.MAX_VALUE;
        Point[] chips = null;
        
        // Alle Chipkombinationen probieren:
        for (HeuristicPoint chip1 : first.equiFolgende)
            for (HeuristicPoint chip2 : chip1.equiFolgende)
                for (HeuristicPoint chip3 : chip2.equiFolgende) {
                    Map karte = new Map(ArrOps.arrayCopy(brett), chip1, chip2,
                            chip3);
                    if (!maps.contains(karte.getHash())) {
                        maps.add(karte.getHash());
                        HeuristicPoint nextPlayer = new HeuristicPoint(0, 0);
                        this.loop(karte.getKarte(), nextPlayer, this.chipsToSet);
                        if (nextPlayer.nachfolgeGewinn < minWin) {
                            minWin = nextPlayer.nachfolgeGewinn;
                            chips = karte.getChips();
                        }
                    }
                }
        return chips;
    }
    
    @Override
    public Point[] getChips(byte[][] brett) {
        if (this.chipsToSet < 0)
            return null;
        HeuristicPoint chip0 = new HeuristicPoint(0, 0);
        
        // Schleifen finden alle Chipkombinationen mit hohen Gewinnaussichten
        for (int x1 = 0; x1 < brett.length; x1++)
            for (int y1 = 0; y1 < brett[0].length; y1++) {
                HeuristicPoint chip1 = new HeuristicPoint(x1, y1);
                byte[][] brett1 = this.reaktion(brett, chip1);
                chip1.gewinn = this.getGewinn() + this.getAusBank();
                /*
                 * Findet die effizientesten Punkte fuer den zweiten und dritten
                 * Chip
                 */
                for (int x2 = x1; x2 < brett1.length; x2++)
                    for (int y2 = y1; y2 < brett1[0].length; y2++) {
                        // Neuer Punkt wird angelegt und das Brett berechnet
                        HeuristicPoint chip2 = new HeuristicPoint(x2, y2);
                        byte[][] brett2 = this.reaktion(brett1, chip2);
                        chip2.gewinn = this.getGewinn() + this.getAusBank();
                        // Findet den effizientesten Punkt fuer den dritten Chip
                        for (int x3 = 0; x3 < brett2.length; x3++)
                            for (int y3 = 0; y3 < brett2[0].length; y3++) {
                                HeuristicPoint chip3 = new HeuristicPoint(x3,
                                        y3);
                                this.reaktion(brett2, chip3);
                                chip3.gewinn = this.getGewinn()
                                        + this.getAusBank();
                                
                                // Gewinn ermitteln:
                                int gewinn = chip1.gewinn + chip2.gewinn
                                        + chip3.gewinn;
                                if (gewinn >= (chip0.nachfolgeGewinn * 0.7)) {
                                    chip2.add(chip3);
                                    chip1.add(chip2);
                                    chip0.add(chip1);
                                }
                            }
                    }
            }
        return this.getBest(brett, chip0);
    }
    
    /**
     * Findet den besten Punkt zum Setzen eines Chips.
     * 
     * @param brett
     *            Das Spielbrett auf dem simuliert wird
     * @param vorgaenger
     *            der Chipvorgaenger, der gelgt wurde
     * @param level
     *            Gibt an, wie viele Zuege vorberechnet werden sollen
     */
    private void loop(byte[][] brett, HeuristicPoint vorgaenger, byte level) {
        /*
         * Da die Reihenfolge der Chips egal ist und sich einige Kombinationen
         * wiederholen, muessen die Koordinaten unter denen des Vorgaengers
         * nicht ueberprueft werde.
         */
        for (int x = vorgaenger.x; x < brett.length; x++)
            for (int y = vorgaenger.y; y < brett[0].length; y++)
                // Ueberspringe ungueltige Felder
                if (brett[x][y] != -1) {
                    HeuristicPoint chip = new HeuristicPoint(x, y);
                    byte[][] neuesBrett = this.reaktion(
                            ArrOps.arrayCopy(brett), chip);
                    chip.gewinn = this.getGewinn() + this.getAusBank();
                    if (level > 1)
                        this.loop(neuesBrett, chip, (byte) (level - 1));
                    /*
                     * Ist der Punkt besser als seine Vorgaenger, wird er
                     * genommen
                     */
                    if (chip.getGewinn() >= vorgaenger.nachfolgeGewinn) {
                        if (chip.getGewinn() > vorgaenger.nachfolgeGewinn)
                            vorgaenger.clear();
                        vorgaenger.add(chip);
                    }
                }
    }
    
}
