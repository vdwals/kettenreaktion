/* ********************************************************************
 * Kettenreaktion *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de *
 * *
 * This library is free software; you can redistribute it and/or *
 * modify it under the terms of the GNU Lesser General Public *
 * License as published by the Free Software Foundation; either *
 * version 2.1 of the License, or (at your option) any later version. *
 * *
 * This library is distributed in the hope that it will be useful, *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU *
 * Lesser General Public License for more details. *
 * *
 * You should have received a copy of the GNU Lesser General Public *
 * License along with this library; if not, write to the *
 * Free Software Foundation, Inc., *
 * 51 Franklin St, Fifth Floor, *
 * Boston, MA 02110-1301 USA *
 * *
 * Or get it online: *
 * http://www.gnu.org/copyleft/lesser.html *
 * ********************************************************************
 */
package de.vdwals.kettenreaktion.spiel.ki;

import java.awt.Point;

import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.spiel.Reaktor;

/**
 * Abstrakte Klasse fuer Spielstrategien (KI). Stellt Standardfunktionen bereit.
 * 
 * @author Dennis van der Wals
 * 
 */
public abstract class Strategie {
    /**
     */
    protected byte   chipsToSet;
    /**
     */
    private Reaktor  reaktor;
    
    /**
     */
    protected String name;
    
    /**
     * Standardkonstruktor.
     * 
     * @param name
     *            Name der KI
     */
    public Strategie(String name) {
        this.chipsToSet = 3;
        this.name = name;
    }
    
    /**
     * Erstellt eine wertgleiche Kopie des Originals zum Bearbeiten.
     * 
     * @param original
     *            Originales Brett
     * @return wertgleiche Kopie
     */
    protected byte[][] copy(byte[][] original) {
        return ArrOps.arrayCopy(original);
    }
    
    /**
     * Erstellt eine wertgleiche Kopie des Originals zum Bearbeiten.
     * 
     * @param original
     *            Originales Array
     * @return wertgleiche Kopie
     */
    protected int[] copy(int[] original) {
        return ArrOps.arrayCopy(original);
    }
    
    /**
     * Gibt die Anzahl der Chips zurueck, die bei der letzten Kettenreaktion aus
     * der Bank genommen werden muessen.
     * 
     * @return Anzahl der Chips aus der Bank
     */
    protected int getAusBank() {
        return this.reaktor.getAusBank();
    }
    
    /**
     * Gibt die durch die KI bestimmten Positionen der zu legenden Chips zurueck
     */
    public abstract Point[] getChips(byte[][] brett);
    
    /**
     * Gibt die Anzahl der Chips zurueck, die durch die letzte Reaktion gewonnen
     * wurde.
     * 
     * @return Anzahl der gewonnenen Chips
     */
    protected int getGewinn() {
        return this.reaktor.getExtraGewinn();
    }
    
    /**
     * Gibt die Anzahl der Reaktionen zurueck, die bei der letzten
     * Kettenreaktion stattfanden.
     * 
     * @return Anzahl der Reaktionen
     */
    protected int getReaktionen() {
        return this.reaktor.getReaktionen();
    }
    
    /**
     * Gibt das Spielfeld zurueck, welches nach setzen des Chips entsteht.
     * 
     * @param spielfeld
     *            Spielfeld - wird veraendert
     * @param x
     *            x-Koordinate
     * @param y
     *            y-Koordinate
     * @return neues Spielfeld
     */
    protected byte[][] reaktion(byte[][] spielfeld, Point chip) {
        return this.reaktor.reaktionKI(spielfeld, chip);
    }
    
    /**
     * Setzt, wie viele Chips gesetzt werden duerfen.
     * 
     * @param chipsToSet
     *            Anzahl zu setzender Chips
     */
    public void setChipsToSet(byte chipsToSet) {
        this.chipsToSet = (chipsToSet > 0) ? chipsToSet : 0;
    }
    
    /**
     * Setzt den Reaktor der Strategie.
     * 
     * @param reaktor
     *            Reaktor
     */
    public void setReaktor(Reaktor reaktor) {
        this.reaktor = reaktor;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
}
