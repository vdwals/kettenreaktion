package de.vdwals.kettenreaktion.spiel.ki;

import java.awt.Point;
import java.util.LinkedList;
import java.util.Random;

import de.vdwals.kettenreaktion.implement.ArrOps;

/**
 * Strategie die den bestmoeglichen Zug bestimmt.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Greedy extends Strategie {
    /**
     * Klasse um einen Punkt mit seinen Nachfolgern sowie den Gewinn mit den der
     * Nachfolgern zu speichern
     * 
     * @author Dennis van der Wals
     */
    private class GreedyPoint extends Point {
        /**
         * Default SerialID
         */
        private static final long      serialVersionUID = 1L;
        public LinkedList<GreedyPoint> equiFolgende;
        /**
         */
        public int                     gewinn;
        public int                     nachfolgeGewinn;
        
        /**
         * Standardkonstruktor, wie ein Punkt.
         * 
         * @param x
         *            x-Koordinate
         * @param y
         *            y-Koordinate
         */
        public GreedyPoint(int x, int y) {
            super(x, y);
            this.equiFolgende = new LinkedList<GreedyPoint>();
            this.gewinn = this.nachfolgeGewinn = 0;
        }
        
        /**
         * Fuegt einen Punkt der Liste hinzu und speichert dessen Gewinn ab
         * 
         * @param g
         *            Nachfolgender Punkt
         */
        public void add(GreedyPoint g) {
            this.equiFolgende.add(g);
            this.nachfolgeGewinn = g.getGewinn();
        }
        
        /**
         * Loescht die Liste der Nachfolger und setzt den Gewinn zurueck.
         */
        public void clear() {
            this.equiFolgende.clear();
            this.nachfolgeGewinn = 0;
        }
        
        /**
         * Gibt den Gewinn der Zuege zurueck.
         * 
         * @return Gewinn der Zuege
         */
        public int getGewinn() {
            return this.gewinn + this.nachfolgeGewinn;
        }
        
        /**
         * Gibt aus der Liste der Nachfolger mit maximalem Gewinn einen
         * zufaelligen zurueck.
         * 
         * @return Zufaelliger Nachfolger
         */
        public GreedyPoint getNachfolger() {
            int index = 0;
            if (Greedy.this.random)
                index = Math.round((float) Math.random()
                        * (this.equiFolgende.size() - 1));
            else
                for (int i = 0; i < this.equiFolgende.size(); i++)
                    if (this.distance(this.equiFolgende.get(i)) > this
                            .distance(this.equiFolgende.get(index)))
                        index = i;
            return (this.equiFolgende.size() > 0) ? this.equiFolgende
                    .get(index) : null;
        }
    }
    
    /**
     */
    private boolean       random;
    /**
     */
    protected GreedyPoint lastChip;
    
    /**
     * Standardkonstruktor.
     * 
     * @param zufall
     *            Bestimmt, ob ein Zufallsmodus genutzt werden soll.
     */
    public Greedy(boolean zufall) {
        super((zufall) ? "KI-Greedy+" : "KI-Greedy"); //$NON-NLS-1$ //$NON-NLS-2$
        this.random = zufall;
    }
    
    @Override
    public Point[] getChips(byte[][] brett) {
        if (this.chipsToSet < 0)
            return null;
        GreedyPoint chip0 = new GreedyPoint(0, 0);
        this.loop(brett, chip0, this.chipsToSet);
        GreedyPoint lastChip = chip0;
        this.lastChip = chip0.getNachfolger();
        Point[] chips = new Point[this.chipsToSet];
        for (int i = 0; i < this.chipsToSet; i++) {
            lastChip = lastChip.getNachfolger();
            chips[i] = lastChip;
        }
        // Durchmischen
        Random r = new Random();
        for (int i = 0; i < chips.length; i++) {
            int rand = r.nextInt(chips.length);
            Point tmp = chips[i];
            chips[i] = chips[rand];
            chips[rand] = tmp;
        }
        return chips;
    }
    
    /**
     * Findet den besten Punkt zum Setzen eines Chips.
     * 
     * @param brett
     *            Das Spielbrett auf dem simuliert wird
     * @param vorgaenger
     *            der Chipvorgaenger, der gelgt wurde
     * @param level
     *            Gibt an, wie viele Zuege vorberechnet werden sollen
     */
    private void loop(byte[][] brett, GreedyPoint vorgaenger, byte level) {
        /*
         * Da die Reihenfolge der Chips egal ist und sich einige Kombinationen
         * wiederholen, muessen die Koordinaten unter denen des Vorgaengers
         * nicht ueberprueft werde.
         */
        for (int x = vorgaenger.x; x < brett.length; x++)
            for (int y = vorgaenger.y; y < brett[0].length; y++)
                // Ueberspringe ungueltige Felder
                if (brett[x][y] != -1) {
                    GreedyPoint chip = new GreedyPoint(x, y);
                    byte[][] neuesBrett = this.reaktion(
                            ArrOps.arrayCopy(brett), chip);
                    chip.gewinn = this.getGewinn() + this.getAusBank();
                    if (level > 1)
                        this.loop(neuesBrett, chip, (byte) (level - 1));
                    /*
                     * Ist der Punkt besser als seine Vorgaenger, wird er
                     * genommen
                     */
                    if (chip.getGewinn() >= vorgaenger.nachfolgeGewinn) {
                        if (chip.getGewinn() > vorgaenger.nachfolgeGewinn)
                            vorgaenger.clear();
                        vorgaenger.add(chip);
                    }
                }
    }
    
}
