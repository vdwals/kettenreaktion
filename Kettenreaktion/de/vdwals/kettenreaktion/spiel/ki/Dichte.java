package de.vdwals.kettenreaktion.spiel.ki;

import java.awt.Point;
import java.util.LinkedList;

/**
 * Ermittelt die Punkte fuer die Chips indem jedes Feld gewichtet wird nach
 * Anzahl der benachbarten Steine geteilt durch deren Abstand zum untersuchten
 * Punkt.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Dichte extends Strategie {
    /**
     */
    private boolean random;
    
    /**
     * Erzeugt eine neue Strategie.
     * 
     * @param zufall
     *            Soll zufaellig unter den Maxima ausgewaehlt werden, oder nicht
     */
    public Dichte(boolean zufall) {
        super((zufall) ? "KI-Density+" : "KI-Density");
        this.random = zufall;
    }
    
    @Override
    public Point[] getChips(byte[][] brett) {
        if (this.chipsToSet < 1)
            return null;
        Point[] chips = new Point[this.chipsToSet];
        chips[0] = this.getMax(this.getDichte(brett));
        for (int i = 1; i < this.chipsToSet; i++) {
            brett = this.copy(this.reaktion(brett, chips[i - 1]));
            chips[i] = this.getMax(this.getDichte(brett));
        }
        return chips;
    }
    
    /**
     * Errechnet die Dichte eines jeden Punktes als Summe ueber die Anzahl der
     * Chips geteilt durch deren Abstand zum Punkt.
     * 
     * @param brett
     *            Spielfeld - unveraendert
     * @return ein Feld mit eingetragener Dichte
     */
    private double[][] getDichte(byte[][] brett) {
        double[][] gewicht = new double[brett.length][brett[0].length];
        
        // x-Koordinatenschleife
        for (int x = 0; x < brett.length; x++)
            // y-Koordinatenschleife
            for (int y = 0; y < brett[x].length; y++)
                // Ungueltige Felder Ueberpsringen
                if (brett[x][y] != -1)
                    /*
                     * Gewicht des Punktes wird auf die anderen Koordinaten
                     * verteilt
                     */
                    for (int i = 0; i < gewicht.length; i++)
                        for (int j = 0; j < gewicht[i].length; j++)
                            if (brett[i][j] != -1)
                                gewicht[i][j] += ((double) brett[x][y] / ((Math
                                        .abs(i - x) + Math.abs(j - y)) + 1));
        return gewicht;
    }
    
    /**
     * Bestimmt dan Punkt auf dem Feld mit dem hoechsten Wert. Gibt entweder das
     * erste oder ein zufaelliges, gleichwertiges Feld zurueck.
     * 
     * @param dichte
     *            zu Analysierendes Feld
     * @return Koordinaten des hoechsten Punktes
     */
    private Point getMax(double[][] dichte) {
        LinkedList<Point> maxima = new LinkedList<Point>();
        Point max = new Point();
        
        // x-Koordinaten durchlaufen
        for (int x = 0; x < dichte.length; x++)
            // y-Koordinaten durchlaufen
            for (int y = 0; y < dichte[x].length; y++)
                // ueberpruefen, ob der Wert hoeher ist, als der bisher hoechste
                if (Math.round((float) dichte[x][y]) > Math
                        .round((float) dichte[max.x][max.y])) {
                    maxima.clear();
                    max = new Point(x, y);
                    maxima.add(max);
                } else if (Math.round((float) dichte[x][y]) == Math
                        .round((float) dichte[max.x][max.y]))
                    // Bei gleicher Groesse in die Liste aufnehmen
                    maxima.add(new Point(x, y));
        // Zuf�llige Auswahl
        if (this.random)
            max = maxima.get((int) (Math.random() * (maxima.size() - 1)));
        return max;
    }
}
