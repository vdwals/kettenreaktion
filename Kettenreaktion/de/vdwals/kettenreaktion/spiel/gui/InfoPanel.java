package de.vdwals.kettenreaktion.spiel.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * Infopanel fuer Kettenreaktion.
 * 
 * @author Dennis van der Wals
 * 
 */
public class InfoPanel extends JPanel {
    /**
     * Default SerialID
     */
    private static final long serialVersionUID = 1L;
    
    // Komponenten
    private JLabel            toPlaceLabel, bankLabel, spieler[], konto[],
            status[], sanduhr;
    private Icon              aktiveIcon, waitIcon;
    private JPanel            subPanel;
    private TitledBorder      rahmen;
    
    // Spielernamen
    private String[]          name;
    
    /**
     * Standardkonstruktor.
     * 
     * @param name
     *            Spielernamen
     */
    public InfoPanel(String[] name, JButton next) {
        this.name = name;
        this.aktiveIcon = new ImageIcon(
                ClassLoader.getSystemResource("gui/images/Box_Green.xrt"));
        this.waitIcon = new ImageIcon(
                ClassLoader.getSystemResource("gui/images/Box_Red.xrt"));
        
        JPanel p = new JPanel();
        p.setLayout(new GridLayout(name.length, 0, 5, 5));
        this.getStatus();
        this.getSpieler();
        this.getKonto();
        for (int i = 0; i < name.length; i++) {
            
            JPanel subP = new JPanel(new BorderLayout(5, 5));
            subP.add(this.status[i], BorderLayout.WEST);
            
            JPanel subStat = new JPanel(new BorderLayout(5, 5));
            subStat.add(this.spieler[i], BorderLayout.CENTER);
            subStat.add(this.konto[i], BorderLayout.EAST);
            
            subP.add(subStat, BorderLayout.CENTER);
            
            p.add(subP);
        }
        
        this.subPanel = new JPanel(new GridLayout(0, 1, 5, 5));
        this.subPanel.add(this.getSanduhr());
        this.subPanel.add(this.getToPlaceLabel());
        this.subPanel.add(this.getBankLabel());
        this.subPanel.add(next);
        
        this.setLayout(new BorderLayout(5, 5));
        this.add(p, BorderLayout.NORTH);
        this.add(this.subPanel, BorderLayout.SOUTH);
        this.setBorder(this.getRahmen());
    }
    
    /**
     * Gibt ein Label fuer die zu Chipzahl der Bank zurueck.
     * 
     * @return Label fuer die zu Chipzahl der Bank
     */
    private JLabel getBankLabel() {
        if (this.bankLabel == null) {
            this.bankLabel = new JLabel();
            this.bankLabel.setBorder(new TitledBorder(BorderFactory
                    .createEtchedBorder(), "Bank"));
        }
        return this.bankLabel;
    }
    
    /**
     * Gibt Labels fuer die Konton zurueck.
     * 
     * @return Labels fuer die Konton
     */
    private JLabel[] getKonto() {
        if (this.konto == null) {
            this.konto = new JLabel[this.name.length];
            for (int i = 0; i < this.name.length; i++) {
                this.konto[i] = new JLabel();
                this.konto[i].setBorder(BorderFactory.createEtchedBorder());
            }
        }
        return this.konto;
    }
    
    /**
     * Erzeugt den Panelrahmen und gibt ihn zurueck.
     * 
     * @return Panelrahmen
     */
    private TitledBorder getRahmen() {
        if (this.rahmen == null)
            this.rahmen = new TitledBorder(BorderFactory.createEtchedBorder(),
                    "Runde 1");
        return this.rahmen;
    }
    
    /**
     * Erzeugt das Label mit einer Sanduhr und gibt es zurueck.
     * 
     * @return Label mit einer Sanduhr
     */
    public JLabel getSanduhr() {
        if (this.sanduhr == null) {
            this.sanduhr = new JLabel(new ImageIcon(
                    ClassLoader.getSystemResource("gui/images/sanduhr.xrt")));
            this.sanduhr.setVisible(false);
        }
        return this.sanduhr;
    }
    
    /**
     * Erzeugt die Liste mit Spielern.
     * 
     * @return gibt die Liste zurueck
     */
    private JLabel[] getSpieler() {
        if (this.spieler == null) {
            this.spieler = new JLabel[this.name.length];
            for (int i = 0; i < this.name.length; i++)
                this.spieler[i] = new JLabel(this.name[i]);
        }
        return this.spieler;
    }
    
    /**
     * Gibt Labels fuer die Spielerstadien zurueck.
     * 
     * @return Labels fuer die Spielerstadien
     */
    private JLabel[] getStatus() {
        if (this.status == null) {
            this.status = new JLabel[this.name.length];
            for (int i = 0; i < this.name.length; i++)
                this.status[i] = new JLabel();
        }
        return this.status;
    }
    
    /**
     * Gibt das SubPanel mit den Chipinformationen zurueck.
     * 
     * @return SubPanel mit den Chipinformationen
     */
    public JPanel getSubPanel() {
        return this.subPanel;
    }
    
    /**
     * Gibt ein Label fuer die zu setzende Chipzahl zurueck.
     * 
     * @return Label fuer die zu setzende Chipzahl
     */
    private JLabel getToPlaceLabel() {
        if (this.toPlaceLabel == null) {
            this.toPlaceLabel = new JLabel();
            this.toPlaceLabel.setBorder(new TitledBorder(BorderFactory
                    .createEtchedBorder(), "Zu setzen"));
        }
        return this.toPlaceLabel;
    }
    
    /**
     * Berechnet die Anzeige neu.
     * 
     * @param aktiv
     *            Aktiver Spieler
     * @param konto
     *            Konten der Spieler
     * @param bank
     *            Anzahl der Bankchips
     * @param chipsToPlace
     *            Anzahl der zu legenden Chips
     * @param runde
     *            Runde
     */
    public void recalc(byte aktiv, int[] konto, int bank, byte chipsToPlace,
            int runde) {
        // Spieler werden aktualisiert
        for (int i = 0; i < this.getSpieler().length; i++) {
            this.getKonto()[i].setText(String.format("%03d", konto[i]));
            this.getStatus()[i].setIcon(this.waitIcon);
        }
        // Aktiven Spieler einfaerben
        this.getStatus()[aktiv].setIcon(this.aktiveIcon);
        
        this.getToPlaceLabel().setText("" + chipsToPlace);
        this.getBankLabel().setText("" + bank);
        this.getRahmen().setTitle("Runde " + runde);
        this.paintImmediately(0, 0, this.getWidth(), this.getHeight());
    }
    
}
