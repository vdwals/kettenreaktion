package de.vdwals.kettenreaktion.spiel.gui.pfadalgorithmen;

import java.util.LinkedList;

import de.vdwals.kettenreaktion.spiel.Reaktor.ReaktorPoint;

/**
 * Geht Ebene fuer Ebene durch die Reaktionen
 * 
 * @author Dennis van der Wals
 * 
 */
public class BreadthFirst extends PfadSuche {
    
    @Override
    public LinkedList<ReaktorPoint> getNext() {
        // Wenn kein Element mehr in der Warteschlange, nehme naechsten Urpsrung
        if (this.warteschlange.size() == 0)
            // Wenn kein Ursprung mehr da, gib null zurueck
            if (this.aenderungsBaum.size() == 0)
                return null;
            else
                return this.firstChip();
        // Nehme naechstes Element aus der Warteschlange
        ReaktorPoint next = this.warteschlange.poll();
        if ((next.getFolgende().size() == 0) || (next.getFolgende() == null))
            return this.firstChip();
        /*
         * Trage alle Nachfolger in die Warteschlange ein, die selbst Nachfolger
         * haben
         */
        for (ReaktorPoint reaktorPoint : next.getFolgende())
            if (!(reaktorPoint.getFolgende().size() <= 0)
                    && !(reaktorPoint.getFolgende() == null))
                this.warteschlange.offer(reaktorPoint);
        // Gib die Liste der Nachfolger zurueck
        return next.getFolgende();
    }
    
    @Override
    public boolean hasNext() {
        if ((this.aenderungsBaum == null)
                || ((this.aenderungsBaum.size() == 0) && (this.warteschlange
                        .size() == 0)))
            return false;
        return ((this.aenderungsBaum.size() > 0) || (this.warteschlange
                .getFirst().getFolgende().size() > 0));
    }
    
}
