package de.vdwals.kettenreaktion.spiel.gui.pfadalgorithmen;

import java.util.LinkedList;

import de.vdwals.kettenreaktion.spiel.Reaktor.ReaktorPoint;

/**
 * @author Dennis
 */
public abstract class PfadSuche {
    /**
     */
    protected LinkedList<ReaktorPoint> aenderungsBaum;
    /**
     */
    protected LinkedList<ReaktorPoint> warteschlange;
    
    protected LinkedList<ReaktorPoint> firstChip() {
        // Fuege den Urpsrung in die Liste und gib ihn zurueck
        this.warteschlange.offer(this.aenderungsBaum.poll());
        return this.warteschlange;
    }
    
    /**
     * Gibt alle Knoten der naechsten Suchebene zurueck
     * 
     * @return
     */
    public abstract LinkedList<ReaktorPoint> getNext();
    
    /**
     * Gibt zurueck, ob es noch eine Ebene mit Punkten gibt
     * 
     * @return
     */
    public abstract boolean hasNext();
    
    /**
     * Reinitialisiert den Algorithmus
     * 
     * @param aenderungen
     *            Graphenurspruenge
     */
    public void setAenderungsBaum(LinkedList<ReaktorPoint> aenderungen) {
        this.aenderungsBaum = aenderungen;
        this.warteschlange = new LinkedList<ReaktorPoint>();
    }
    
}
