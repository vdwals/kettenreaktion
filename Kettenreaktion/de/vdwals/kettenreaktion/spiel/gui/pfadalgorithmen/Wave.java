package de.vdwals.kettenreaktion.spiel.gui.pfadalgorithmen;

import java.util.LinkedList;

import de.vdwals.kettenreaktion.spiel.Reaktor.ReaktorPoint;

/**
 * Algorithmus gibt eine Liste mit aequitemporalen Aenderungen zurueck
 * 
 * @author Dennis van der Wals
 * 
 */
public class Wave extends PfadSuche {
    
    @Override
    public LinkedList<ReaktorPoint> getNext() {
        // Wenn kein Element mehr in der Warteschlange, nehme naechsten Urpsrung
        if (this.warteschlange.size() == 0)
            // Wenn kein Ursprung mehr da, gib null zurueck
            if (this.aenderungsBaum.size() == 0)
                return null;
            else
                // Fuege den Urpsrung in die Liste und gib ihn zurueck
                this.warteschlange.offer(this.aenderungsBaum.poll());
        
        // Kopiere die ganze Warteschlange
        LinkedList<ReaktorPoint> next = new LinkedList<ReaktorPoint>();
        next.addAll(this.warteschlange);
        this.warteschlange.clear();
        
        /*
         * Trage alle Nachfolger in die Warteschlange ein
         */
        for (ReaktorPoint reaktorPoint : next)
            this.warteschlange.addAll(reaktorPoint.getFolgende());
        
        // Gib die Liste der Nachfolger zurueck
        return next;
    }
    
    @Override
    public boolean hasNext() {
        return ((this.aenderungsBaum != null) && ((this.aenderungsBaum.size() > 0) || (this.warteschlange
                .size() > 0)));
    }
    
}
