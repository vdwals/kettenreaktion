package de.vdwals.kettenreaktion.spiel.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.util.LinkedList;

import javax.swing.JPanel;

import de.vdwals.kettenreaktion.gui.Einstellungen;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.spiel.Reaktor.ReaktorPoint;
import de.vdwals.kettenreaktion.spiel.gui.pfadalgorithmen.PfadSuche;
import de.vdwals.kettenreaktion.statistik.gui.LivePanel;

/**
 * Klasse dient der farblichen Anzeige des uebergebenen Spielfeldes.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Spielbrettinterface extends JPanel {
    /**
     * Default Serial
     */
    private static final long serialVersionUID = 1L;
    
    // Spieleinstellungen,
    private Einstellungen     einstellungen;
    
    // Interne Variablen
    /*
     * Extravariablen zum einhalten der Seitenverhaeltnisse, da die Funktion zur
     * Groesseneinstellung aufgerufen wird, bevor ein Feld existiert. Das fuehrt
     * zum Absturz.
     */
    private int               brettBreite, brettHoehe;
    byte[][]                  spielbrett;
    public Thread             animation;
    
    /**
     * Standardkonstruktor
     * 
     * @param einstellungen
     *            Spieleinstellungen
     */
    public Spielbrettinterface(Einstellungen einstellungen) {
        this.einstellungen = einstellungen;
        einstellungen.getSpielOpt().getMod()
                .setFensterDimension(this.getWidth(), this.getHeight());
        this.brettBreite = this.brettHoehe = 1;
        
        // Gegen das flackern
        this.setDoubleBuffered(true);
    }
    
    /**
     * Legt einen Chip auf das Spielfeld, beeinfluss nicht das Spielfeld.
     * 
     * @param x
     *            x-Koordinate
     * @param y
     *            y-Koordinate
     */
    public void addChip(int x, int y) {
        if (this.spielbrett[x][y] != -1)
            this.spielbrett[x][y]++;
    }
    
    /**
     * Legt eine bestimmte Anzahl an Chips auf ein Feld, beeinflusst das
     * Spielfeld nicht
     * 
     * @param anzahl
     */
    private void addChips(int x, int y, int anzahl) {
        if (this.spielbrett[x][y] != -1)
            this.spielbrett[x][y] += anzahl;
    }
    
    /**
     * Zaehlt die sichtbaren Chips auf dem Feld.
     * 
     * @return Anzahl der sichtbaren Chips.
     */
    public int countVisibleChips() {
        int count = 0;
        for (byte[] element : this.spielbrett)
            for (byte element2 : element)
                if (element2 != -1)
                    count += element2;
        return count;
    }
    
    /**
     * Bestimmt die zu malende Farbe anhand der Chipanzahl
     * 
     * @return Farbe fuer das Feld
     */
    private Color getColor(int i) {
        return this.einstellungen.getGrafOpt().getFarbe(i + 3);
    }
    
    @Override
    public void paint(Graphics d) {
        boolean s = true;
        Graphics2D g = (Graphics2D) d;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        
        g.setColor(this.einstellungen.getGrafOpt().getFarbe(3));
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        
        Polygon feld = null;
        
        for (int x = 0; x < this.brettBreite; x++) {
            for (int y = 0; y < this.brettHoehe; y++) {
                
                // Polygon von Mod holen
                feld = this.einstellungen.getSpielOpt().getMod()
                        .getPolygon(x, y);
                
                if (this.spielbrett[x][y] != -1) {
                    
                    g.setColor(this.einstellungen.getGrafOpt().getFarbe(0));
                    g.fillPolygon(feld);
                    // Umrandung des Feldes zeichnen
                    g.setColor(Color.BLACK);
                    g.drawPolygon(feld);
                    
                    // Chips zeichnen
                    for (int i = 0; i < this.spielbrett[x][y]; i++) {
                        
                        Ellipse2D chip = this.einstellungen.getSpielOpt()
                                .getMod().getOval(x, y, i);
                        
                        // Bereich fuellen
                        g.setColor(this.getColor(i + 1));
                        g.fill(chip);
                        
                        // Umrandung zeichnen
                        g.setColor(Color.BLACK);
                        g.draw(chip);
                    }
                    
                    // Umrandung zeichnen
                    g.setColor(Color.BLACK);
                    Ellipse2D oval = this.einstellungen.getSpielOpt().getMod()
                            .getMiddle(x, y);
                    g.draw(oval);
                    
                    // Abwechselnd Mitte einfaerben
                    if (s)
                        g.setColor(this.einstellungen.getGrafOpt().getFarbe(1));
                    else
                        g.setColor(this.einstellungen.getGrafOpt().getFarbe(2));
                    g.fill(oval);
                }
                s = !s;
            }
            s = !s;
        }
    }
    
    /**
     * Zeichnet nacheinander die Veraenderungen ein um die Reaktionen so
     * anschaulich zu machen.
     * 
     * @param aenderungen
     *            Liste mit Reaktionsbaeumen
     * @param panel
     *            Panel zur Aktuallisierung der ChipsToPlace-Anzeige
     */
    public void plottChanges(LinkedList<ReaktorPoint> aenderungen,
            PfadSuche pfadSuchAlgoithmus, final LivePanel livepanel) {
        final PfadSuche p = pfadSuchAlgoithmus;
        p.setAenderungsBaum(aenderungen);
        this.animation = new Thread(new Runnable() {
            
            @Override
            public void run() {
                Rectangle r = null;
                
                // Alle Aenderungen printen
                while (p.hasNext()) {
                    
                    /*
                     * Minimierung des Flackerns: Aktuallisierungsbereich in die
                     * Mitte setzen.
                     */
                    int xlinks = Spielbrettinterface.this.brettBreite;
                    int yoben = Spielbrettinterface.this.brettHoehe;
                    int xrechts = 0;
                    int yunten = 0;
                    
                    for (ReaktorPoint reaktorPoint : p.getNext()) {
                        
                        // Aktuallisierungsfeld anpassen
                        if (reaktorPoint.x < xlinks)
                            xlinks = reaktorPoint.x;
                        if (reaktorPoint.y < yoben)
                            yoben = reaktorPoint.y;
                        if (reaktorPoint.x > xrechts)
                            xrechts = reaktorPoint.x;
                        if (reaktorPoint.y > yunten)
                            yunten = reaktorPoint.y;
                        
                        // Spielbrettanzeige manipulieren
                        Spielbrettinterface.this.addChips(reaktorPoint.x,
                                reaktorPoint.y, reaktorPoint.getChange());
                    }
                    
                    // Geaenderten Ausschnitt bestimmen und neu zeichnen:
                    r = Spielbrettinterface.this.einstellungen.getSpielOpt()
                            .getMod()
                            .getRectangle(xlinks, xrechts, yoben, yunten);
                    Spielbrettinterface.this.paintImmediately(r);
                    
                    // Pause
                    try {
                        Thread.sleep(Spielbrettinterface.this.einstellungen
                                .getGrafOpt().getSleep());
                    } catch (InterruptedException e) {
                        BugReport.recordBug(e);
                    }
                }
            }
        });
        this.animation.start();
    }
    
    /**
     * Entfernt einen Chip vom Spielfeld, beeinfluss nicht das Spielfeld.
     * 
     * @param x
     *            x-Koordinate
     * @param y
     *            y-Koordinate
     */
    public void removeChip(int x, int y) {
        if (this.spielbrett[x][y] != -1)
            this.spielbrett[x][y]--;
    }
    
    @Override
    public void repaint() {
        if (this.einstellungen != null) {
            // Ist die Hoehe kleiner, als die skallierte Breite?
            if (this.getHeight() < (int) (this.getWidth() * this.einstellungen
                    .getSpielOpt().getMod().getScale()))
                this.setSize((int) (this.getHeight() / this.einstellungen
                        .getSpielOpt().getMod().getScale()), this.getHeight());
            else if (this.getWidth() < (int) (this.getHeight() / this.einstellungen
                    .getSpielOpt().getMod().getScale()))
                this.setSize(this.getWidth(),
                        (int) (this.getWidth() * this.einstellungen
                                .getSpielOpt().getMod().getScale()));
            
            // Wurde die Fenstergroesse geaendert?
            if ((this.getHeight() != this.einstellungen.getSpielOpt().getMod()
                    .getFensterHoehe())
                    || (this.getWidth() != this.einstellungen.getSpielOpt()
                            .getMod().getFensterBreite()))
                this.einstellungen.getSpielOpt().getMod()
                        .setFensterDimension(this.getWidth(), this.getHeight());
        }
        
        super.repaint();
    }
    
    /**
     * Setter fuer das Brett.
     * 
     * @param spielbrett
     *            zu uebernehmendes Spielbrett
     */
    public void setBrett(byte[][] spielbrett) {
        this.brettBreite = spielbrett.length;
        this.brettHoehe = spielbrett[0].length;
        this.spielbrett = spielbrett;
        
        this.repaint();
    }
    
}
