package de.vdwals.kettenreaktion.spiel.gui;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.LinkedList;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.MouseInputListener;

import de.vdwals.kettenreaktion.gui.Einstellungen;
import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.io.FileHandler;
import de.vdwals.kettenreaktion.spiel.Spielkern;
import de.vdwals.kettenreaktion.spiel.Reaktor.ReaktorPoint;
import de.vdwals.kettenreaktion.spiel.io.SaveGameIO;

/**
 * Oberflaeche fuer Kettenreaktion.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Spielfenster extends JFrame implements MouseInputListener,
        ActionListener {
    /**
     * Default SerialID
     */
    private static final long   serialVersionUID = 1L;
    
    // Komponenten
    private Spielbrettinterface spielOberflaeche;
    private InfoPanel           infoPanel;
    private Menu                menu;
    private JButton             next;
    
    // Interne Variablen
    private Spielkern           spiel;
    private Einstellungen       einstellungen;
    private Point[]             chips;
    private byte                chipNumber;
    private FileHandler         save;
    private boolean             first, winnerShown;
    
    /**
     * Standardkonstruktor.
     * 
     * @param spiel
     *            Spiel, das gespielt werden soll
     * @param einstellungen
     *            Einstellungen des Spiels
     */
    public Spielfenster(Spielkern spiel, Einstellungen einstellungen) {
        super("Spiel");
        this.einstellungen = einstellungen;
        this.spiel = spiel;
        this.first = true;
        this.winnerShown = false;
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
        // Komponenten initialisieren:
        this.infoPanel = new InfoPanel(einstellungen.getSpielOpt().getNamen(),
                this.getNext());
        this.save = new SaveGameIO(einstellungen, spiel);
        this.menu = new Menu(this.save, einstellungen);
        this.spielOberflaeche = new Spielbrettinterface(einstellungen);
        this.spielOberflaeche.setBrett(spiel.getSpielbrett());
        this.spielOberflaeche.addMouseListener(this);
        
        // Variablen initialisieren
        this.chips = new Point[this.einstellungen.getSpielOpt()
                .getMaxChipsToSet()];
        this.chipNumber = 0;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.getNext())
            // Ersten Zug abfangen
            if (this.first) {
                
                this.first = false;
                this.getNext().setText("Zug beenden");
                this.getNext().setEnabled(false);
                
                // KI-Zug initialisieren
                this.kiZug();
                
            } else
                this.nextRound();
    }
    
    /**
     * Programm starten
     */
    public void build() {
        
        this.setJMenuBar(this.menu);
        this.setLayout(new BorderLayout(5, 5));
        
        this.add(this.spielOberflaeche, BorderLayout.CENTER);
        this.add(this.infoPanel, BorderLayout.WEST);
        
        this.setSize(600, 500);
        
        // In die Mitte positionieren
        this.setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - this
                        .getWidth()) / 2, (Toolkit.getDefaultToolkit()
                        .getScreenSize().height - this.getHeight()) / 2);
        
        // Icon setzen
        try {
            this.setIconImage(ImageIO.read(ClassLoader
                    .getSystemResource("gui/images/radioaktiv.xrt")));
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
        
        this.setVisible(true);
        
        // Anzeige aktualisieren
        this.spielOberflaeche.repaint();
        this.infoPanel.recalc(this.spiel.getAktiverSpieler(),
                this.spiel.getKonto(), this.spiel.getBank(),
                (byte) (this.spiel.getChipsToSet() - this.chipNumber),
                this.spiel.getRunde());
    }
    
    /**
     * Schliesst das Spiel, die Aufzeichnungen und gibt die Einstellungen frei.
     */
    public void gameClose() {
        
        // Statistik aktualisieren
        this.einstellungen.getStatistik().beendeSpiel(
                (this.spiel.isOver() && !this.winnerShown),
                this.spiel.getKonto(),
                this.einstellungen.getSpielOpt().getNamen(),
                this.spiel.getMaxKettenreaktion(),
                this.einstellungen.getSpielOpt().getKi());
        
        if (this.spiel.isOver() && !this.winnerShown) {
            this.winnerShown = true;
            int[] konto = this.spiel.getKonto();
            
            // Ausgabe der Siegestabelle
            int[] reihenfolge = ArrOps.indexSort(ArrOps.arrayCopy(konto));
            StringBuilder ausgabe = new StringBuilder();
            ausgabe.append("Spiel zuende\n");
            
            if (this.einstellungen.getProgOpt().isDebug()
                    && (konto[reihenfolge[0]] > konto[reihenfolge[1]]))
                BugReport.recordBug(new Exception("Falsche Reihenfolge: "
                        + ArrOps.arrayToString(konto) + "- r: "
                        + ArrOps.arrayToString(reihenfolge)));
            
            for (int i = 0; i < reihenfolge.length; i++)
                ausgabe.append((i + 1)
                        + ". - "
                        + this.einstellungen.getSpielOpt().getNamen()[reihenfolge[reihenfolge.length
                                - 1 - i]] + "\t: "
                        + konto[reihenfolge[reihenfolge.length - 1 - i]] + "\n");
            
            JOptionPane.showMessageDialog(null, ausgabe.toString());
            
            this.spielOberflaeche.setEnabled(false);
            this.infoPanel.setEnabled(false);
        }
        
        // Einstellungen freigeben
        this.spiel.gameClose();
    }
    
    /**
     * Erzeugt den Button fuer das Beenden des Zuges.
     * 
     * @return Button zum Beenden des Zuges
     */
    public JButton getNext() {
        if (this.next == null) {
            // Ist der erste Spieler KI oder Mensch?
            if (this.einstellungen.getSpielOpt().getKi()[0] != null)
                this.next = new JButton("Spiel starten");
            else {
                this.next = new JButton("Zug Beenden");
                this.first = false;
            }
            this.next.setToolTipText("Beendet den Zug");
            this.next.addActionListener(this);
        }
        return this.next;
    }
    
    /**
     * Gibt zurueck, ob das Spiel zuende gespielt wurde.
     * 
     * @return Wahrheitswert
     */
    public boolean isOver() {
        return this.spiel.isOver();
    }
    
    /**
     * Gibt zurueck, ob das Spiel zuende gespielt oder gespeichert wurde.
     * 
     * @return Wahrheitswert
     */
    public boolean isSaved() {
        return this.spiel.isOver()
                || this.einstellungen.getSpielOpt().isSaved();
    }
    
    /**
     * Fuehrt den Zug der KI durch.
     */
    private void kiZug() {
        // Warteicon aktivieren
        this.infoPanel.getSanduhr().setVisible(true);
        
        // Uebermittelt der KI wie viele Steine sie setzen darf,
        this.einstellungen.getSpielOpt().getKi()[this.spiel.getAktiverSpieler()]
                .setChipsToSet(this.spiel.getChipsToSet());
        
        // Fragt ab, welche Steine die KI setzen will
        this.chips = this.einstellungen.getSpielOpt().getKi()[this.spiel
                .getAktiverSpieler()].getChips(this.spiel.getSpielbrett());
        
        this.nextRound();
    }
    
    @Override
    public void mouseClicked(MouseEvent arg0) {
        if (!this.spiel.isOver()
                && (this.einstellungen.getSpielOpt().getKi()[this.spiel
                        .getAktiverSpieler()] == null)) {
            
            // Chip gelegt oder entfernt
            if ((arg0.getButton() == MouseEvent.BUTTON1)
                    && (this.chipNumber < this.spiel.getChipsToSet())) {
                
                // Ermittelt das gewaehlte Feld
                Point chip = this.einstellungen.getSpielOpt().getMod()
                        .getKoordinaten(arg0.getPoint().x, arg0.getPoint().y);
                
                // Speichert den Chip in einem Array
                this.chips[this.chipNumber++] = chip;
                
                // Auf das Spielfeld legen
                this.spielOberflaeche.addChip(chip.x, chip.y);
            } else if ((arg0.getButton() == MouseEvent.BUTTON3)
                    && (this.chipNumber > 0)) {
                
                // Letzten Chip nehmen und Zaehler reduzieren
                Point chip = this.chips[--this.chipNumber];
                this.chips[this.chipNumber] = null;
                
                // Vom Spielfeld nehmen
                this.spielOberflaeche.removeChip(chip.x, chip.y);
                
            }
            
            // Anzeige auffrischen:
            this.infoPanel.recalc(this.spiel.getAktiverSpieler(),
                    this.spiel.getKonto(), this.spiel.getBank(),
                    (byte) (this.spiel.getChipsToSet() - this.chipNumber),
                    this.spiel.getRunde());
            
        }
    }
    
    @Override
    public void mouseDragged(MouseEvent arg0) {
    }
    
    @Override
    public void mouseEntered(MouseEvent arg0) {
    }
    
    @Override
    public void mouseExited(MouseEvent arg0) {
    }
    
    @Override
    public void mouseMoved(MouseEvent arg0) {
    }
    
    @Override
    public void mousePressed(MouseEvent arg0) {
    }
    
    @Override
    public void mouseReleased(MouseEvent arg0) {
    }
    
    /**
     * Aktiviert die naechste Runde des Spiels
     */
    private void nextRound() {
        // Button sperren
        this.getNext().setEnabled(false);
        
        // Spielbrett zuruecksetzen
        this.spielOberflaeche.setBrett(this.spiel.getSpielbrett());
        
        // naechste Runde starten und Veraenderungen speichern
        LinkedList<ReaktorPoint> aenderungen = this.spiel
                .beendeRunde(this.chips);
        
        // Variablen zuruecksetzen
        this.chips = new Point[this.einstellungen.getSpielOpt()
                .getMaxChipsToSet()];
        this.chipNumber = 0;
        
        // Aenderungen printen:
        this.printAenderungen(aenderungen);
        
        // Chipzahl Konsistenz pruefen
        if (this.einstellungen.getProgOpt().isDebug())
            if (this.spiel.getChips() != this.spielOberflaeche
                    .countVisibleChips())
                BugReport.recordBug(new IllegalArgumentException(
                        "Chips auf dem Feld = " + this.spiel.getChips()
                                + "; Chips zu sehen = "
                                + this.spielOberflaeche.countVisibleChips()));
        
        // Ueberpruefen, ob das Spiel zuenden ist
        if (this.spiel.isOver())
            this.gameClose();
        else if (this.einstellungen.getSpielOpt().getKi()[this.spiel
                .getAktiverSpieler()] != null)
            this.kiZug();
        else
            // Menschzug
            this.getNext().setEnabled(true);
    }
    
    /**
     * Zeichnet alle Aenderungen am Ende des Zuges auf die Oberflaeche.
     * 
     * @param aenderungen
     *            Anderungsbaum
     */
    public void printAenderungen(LinkedList<ReaktorPoint> aenderungen) {
        // Chipanimation
        if (this.einstellungen.getGrafOpt().isShowA()) {
            this.spielOberflaeche.plottChanges(aenderungen, this.einstellungen
                    .getGrafOpt().getPfadsucheAlg(), this.einstellungen
                    .getStatistik().getAktuellesSpiel().getLivePanel());
            try {
                // Auf Animation warten
                this.spielOberflaeche.animation.join();
            } catch (InterruptedException e) {
                BugReport.recordBug(e);
            }
        } else
            this.spielOberflaeche.setBrett(this.spiel.getSpielbrett());
        
        // Anzeige auffrischen
        this.infoPanel.recalc(this.spiel.getAktiverSpieler(),
                this.spiel.getKonto(), this.spiel.getBank(),
                (byte) (this.spiel.getChipsToSet() - this.chipNumber),
                this.spiel.getRunde());
        this.spielOberflaeche.setBrett(this.spiel.getSpielbrett());
        
        // Graphen erneuern
        if (this.einstellungen.getStatistikOpt().isLivePanel()) {
            this.einstellungen
                    .getStatistik()
                    .getAktuellesSpiel()
                    .updateLivePanel(
                            this.einstellungen.getStatistik()
                                    .getAktuellesSpiel().getAktuelleRunde());
            try {
                this.einstellungen.getStatistik().getAktuellesSpiel().t.join();
            } catch (InterruptedException e) {
                BugReport.recordBug(e);
            }
        }
        
        // Warteicon entfernen
        this.infoPanel.getSanduhr().setVisible(false);
    }
    
    /**
     * Speichert das Spiel
     */
    public void save() {
        try {
            this.save.save();
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
    }
    
}
