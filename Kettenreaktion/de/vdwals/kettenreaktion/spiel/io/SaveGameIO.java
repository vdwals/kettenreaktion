package de.vdwals.kettenreaktion.spiel.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import de.vdwals.kettenreaktion.gui.Einstellungen;
import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.io.FileHandler;
import de.vdwals.kettenreaktion.optionen.SpielOptionen;
import de.vdwals.kettenreaktion.spiel.Spielkern;
import de.vdwals.kettenreaktion.spiel.ki.Border;
import de.vdwals.kettenreaktion.spiel.ki.Dichte;
import de.vdwals.kettenreaktion.spiel.ki.Greedy;
import de.vdwals.kettenreaktion.spiel.ki.Heuristic;
import de.vdwals.kettenreaktion.spiel.ki.Strategie;
import de.vdwals.kettenreaktion.spiel.ki.Zufall;

/**
 * Klasse zum Speichern und Laden eines Spieles.
 * 
 * @author Dennis van der Wals
 * 
 */
public class SaveGameIO extends FileHandler {
    private Einstellungen einstellungen;
    private Spielkern     spiel;
    private final byte    DB_VERSION = 3;
    
    /**
     * Standardkonstruktor.
     * 
     * @param einstellungen
     *            Spieleinstellungen
     * @param spiel
     *            Spiel
     */
    public SaveGameIO(Einstellungen einstellungen, Spielkern spiel) {
        super();
        this.einstellungen = einstellungen;
        this.spiel = spiel;
        this.fileExtension = ".sav";
    }
    
    @Override
    public byte[] generateData() {
        // TODO SettingsIO.generateData implementieren
        byte[] output = new byte[1048576];
        int counter = 0;
        
        output[counter++] = this.DB_VERSION;
        output[counter++] = (byte) this.einstellungen.getSpielOpt().getKi().length;
        
        // Aktiven Spieler schreiben
        output[counter++] = this.spiel.getAktiverSpieler();
        
        // Kontostaende schreiben
        int[] intBuffer = this.spiel.getKonto();
        for (int element : intBuffer)
            for (byte i : ArrOps.IntToByteArray(element))
                output[counter++] = i;
        
        // Kettenreaktionen schreiben
        intBuffer = this.spiel.getMaxKettenreaktion();
        for (int element : intBuffer)
            for (byte i : ArrOps.IntToByteArray(element))
                output[counter++] = i;
        
        /* Multibit */
        // Ueberspringliste speichern
        boolean[] bits = new boolean[this.spiel.getSkip().length + 3];
        byte counter4boolean = 0;
        boolean[] skip = this.spiel.getSkip();
        for (boolean b : skip)
            bits[counter4boolean++] = b;
        
        // Neu seit Version 2:
        bits[counter4boolean++] = this.einstellungen.getSpielOpt().isWuerfeln();
        
        // Modus
        boolean[] modusBits = ArrOps.toBits(this.einstellungen.getSpielOpt()
                .getModIndex(), this.BIT_BREITE);
        for (int i = 0; i < this.BIT_BREITE; i++)
            bits[counter4boolean++] = modusBits[i];
        
        for (int i = 0; i <= (bits.length / this.MAX_BIT); i++)
            output[counter++] = ArrOps.toByte(ArrOps.getPartOfArray(bits, i
                    * this.MAX_BIT,
                    Math.min((i + 1) * this.MAX_BIT, bits.length)));
        
        // Neu seit Version 2:
        output[counter++] = this.einstellungen.getSpielOpt().getWuerfelStart();
        output[counter++] = this.einstellungen.getSpielOpt().getWuerfelEnd();
        
        // Kartendimensionen schreiben
        byte[][] karte = this.spiel.getSpielbrett();
        output[counter++] = (byte) karte.length;
        output[counter++] = (byte) karte[0].length;
        
        // Karte schreiben
        for (byte[] spalt : karte)
            for (byte feld : spalt)
                output[counter++] = feld;
        
        for (String name : this.einstellungen.getSpielOpt().getNamen()) {
            for (byte bs : name.getBytes())
                output[counter++] = bs;
            output[counter++] = 0;
        }
        
        // Anzahl der Bankchips schreiben
        byte[] tmp = ArrOps.IntToByteArray(this.spiel.getBank());
        for (int i = 0; i < this.INTEGER_LAENGE; i++)
            output[counter++] = tmp[i];
        
        output[counter++] = this.einstellungen.getSpielOpt().getStartChips();
        
        // Anzahl aller Chips schreiben
        tmp = ArrOps.IntToByteArray(this.einstellungen.getSpielOpt()
                .getGesamtChips());
        for (int i = 0; i < this.INTEGER_LAENGE; i++)
            output[counter++] = tmp[i];
        return ArrOps.getPartOfArray(output, 0, ++counter);
    }
    
    /**
     * Gibt die Einstellungen zurueck.
     * 
     * @return Einstellungen
     */
    public SpielOptionen getEinstellungen() {
        return this.einstellungen.getSpielOpt();
    }
    
    /**
     * Gibt das Spiel zurueck.
     * 
     * @return Spiel
     */
    public Spielkern getSpiel() {
        return this.spiel;
    }
    
    /**
     * Laedt die Einstellungen aus einer Speicherdatei und speichert diese und
     * das Spiel intern.
     * 
     * @throws IOException
     */
    @Override
    public void load() throws IOException {
        // Datei einlesen
        File f = this.open(false, null, "savegames");
        byte[] input = new byte[(int) f.length()];
        FileInputStream fis = new FileInputStream(f);
        fis.read(input);
        fis.close();
        
        // TODO ueberarbeiten
        // Fuer jeden eintrag eine Klasse erstellen und speichern:
        byte counter = 0;
        byte version = input[counter++];
        if (version <= this.DB_VERSION) {
            byte spieleranzahl = input[counter++];
            byte aktiverSpieler = input[counter++];
            
            int[] konto = new int[spieleranzahl];
            for (int i = 0; i < konto.length; i++)
                konto[i] = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(input,
                        counter, counter += this.INTEGER_LAENGE));
            
            int[] maxKettenreaktion = new int[spieleranzahl];
            for (int i = 0; i < konto.length; i++)
                maxKettenreaktion[i] = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
            
            // Kombinationsbits aufsammeln
            boolean[] tmp = new boolean[spieleranzahl + 3];
            byte counter4boolean = 0;
            byte[] byte4bits = new byte[(tmp.length / this.MAX_BIT) + 1];
            for (int i = 0; i < byte4bits.length; i++) {
                boolean[] part = ArrOps.toBits(input[counter++], Math.min(
                        (tmp.length - (i * this.MAX_BIT)), this.MAX_BIT));
                for (boolean element : part)
                    tmp[counter4boolean++] = element;
            }
            
            counter4boolean = 0;
            // Spruenge auslesen
            boolean[] skip = new boolean[spieleranzahl];
            for (int i = 0; i < konto.length; i++)
                skip[i] = tmp[counter4boolean++];
            
            // Neu seit Version 2:
            boolean wuerfeln = false;
            if (version >= 2)
                wuerfeln = tmp[counter4boolean++];
            
            byte modus = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                    counter4boolean, counter4boolean += this.BIT_BREITE));
            
            // Neu seit Version 2:
            byte wuerfelStart = 1;
            byte wuerfelEnde = 6;
            if (version == 2) {
                wuerfelStart = input[counter++];
                wuerfelEnde = input[counter++];
            }
            
            byte x = input[counter++];
            byte y = input[counter++];
            
            // Karte auslesen
            byte[][] brett = new byte[x][y];
            boolean[][] karte = new boolean[x][y];
            for (x = 0; x < karte.length; x++)
                for (y = 0; y < karte[x].length; y++) {
                    brett[x][y] = input[counter++];
                    if (brett[x][y] != -1)
                        karte[x][y] = true;
                    else
                        karte[x][y] = false;
                }
            
            // Namen auslesen
            String[] namen = new String[spieleranzahl];
            Strategie[] ki = new Strategie[spieleranzahl];
            for (int i = 0; i < namen.length; i++) {
                
                byte[] tmpName = new byte[input.length - counter];
                byte tmpCounter = 0;
                while (input[counter] != 0)
                    tmpName[tmpCounter++] = input[counter++];
                counter++;
                namen[i] = new String(ArrOps.getPartOfArray(tmpName, 0,
                        tmpCounter));
                
                // KIs identifizieren
                if (namen[i].startsWith("KI-")) {
                    
                    if (namen[i].equals("KI-Density"))
                        ki[i] = new Dichte(false);
                    else if (namen[i].equals("KI-Density+"))
                        ki[i] = new Dichte(true);
                    else if (namen[i].equals("KI-Random"))
                        ki[i] = new Zufall();
                    else if (namen[i].equals("KI-Greedy"))
                        ki[i] = new Greedy(false);
                    else if (namen[i].equals("KI-Greedy+"))
                        ki[i] = new Greedy(true);
                    else if (namen[i].equals("KI-Border"))
                        ki[i] = new Border();
                    else if (namen[i].equals("KI-Heuristic"))
                        ki[i] = new Heuristic();
                } else
                    ki[i] = null;
            }
            int bankChips = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(input,
                    counter, counter += this.INTEGER_LAENGE));
            
            // Ab Version 3
            byte startChips = this.einstellungen.getSpielOpt().getStartChips();
            if (version >= 3)
                startChips = input[counter++];
            
            int gesamtChips = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(
                    input, counter, counter += this.INTEGER_LAENGE));
            
            // Einstellungen uebernehmen
            this.einstellungen.setSpielOpt(new SpielOptionen());
            this.einstellungen.getSpielOpt().setGesamtChips(gesamtChips);
            this.einstellungen.getSpielOpt().setKarte(karte);
            this.einstellungen.getSpielOpt().setKi(ki);
            this.einstellungen.getSpielOpt().setNamen(namen);
            this.einstellungen.getSpielOpt().setModIndex(modus);
            this.einstellungen.getSpielOpt().setWuerfeln(wuerfeln);
            this.einstellungen.getSpielOpt().setWuerfelEnd(wuerfelEnde);
            this.einstellungen.getSpielOpt().setWuerfelStart(wuerfelStart);
            this.einstellungen.getSpielOpt().setStartChips(startChips);
            
            // Spiel erstellen
            this.spiel = new Spielkern(this.einstellungen, konto,
                    aktiverSpieler, brett, bankChips, skip, maxKettenreaktion);
            
            // Spiel als gespeichert markieren
            this.einstellungen.getSpielOpt().setSaved(true);
        } else
            throw new IOException("Unbekannte DB_Version: " + version);
        
    }
    
    /**
     * Speichert eine Spiel und die dazugehoerigen Einstellungen.
     * 
     * @throws IOException
     */
    @Override
    public void save() throws IOException {
        
        // Datei auswaehlen
        File saveGame = this.open(true, "SaveGame", "savegames"); //$NON-NLS-1$ //$NON-NLS-2$
        
        // Dateiendung pruefen
        if (!saveGame.getName().endsWith(this.fileExtension)) {
            
            // Dateiendung anhaengen
            File tmp = saveGame;
            saveGame = new File(saveGame + this.fileExtension);
            
            // Datei ohne Dateiendung loeschen
            if (tmp.exists())
                tmp.delete();
        }
        
        FileOutputStream fos = new FileOutputStream(saveGame);
        fos.write(this.generateData());
        fos.close();
        
        // Spiel als gespeichert markieren
        this.einstellungen.getSpielOpt().setSaved(true);
    }
    
}
