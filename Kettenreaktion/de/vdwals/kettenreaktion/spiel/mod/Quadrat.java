package de.vdwals.kettenreaktion.spiel.mod;

import java.awt.Point;
import java.awt.Polygon;

/**
 * Klasse zum Spielen auf quadratischen Feldern.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Quadrat extends Mod {
    
    /**
     * Standardkonstruktor
     */
    public Quadrat() {
        super("Quadrat", (byte) 4);
    }
    
    @Override
    protected void calcFeldDimension() {
        this.feldBreite = this.fensterBreite / this.karte.length;
        this.feldHoehe = this.fensterHoehe / this.karte[0].length;
    }
    
    @Override
    protected Point getMittelpunkt(int xPosition, int yPosition) {
        return new Point((int) (this.feldBreite * (xPosition + 0.5)),
                (int) (this.feldHoehe * (yPosition + 0.5)));
    }
    
    @Override
    public Point[] getNachbarn(Point p) {
        Point[] nachbarn = new Point[this.ecken];
        nachbarn[0] = new Point(p.x - 1, p.y);
        nachbarn[1] = new Point(p.x, p.y + 1);
        nachbarn[2] = new Point(p.x + 1, p.y);
        nachbarn[3] = new Point(p.x, p.y - 1);
        return nachbarn;
    }
    
    @Override
    public Polygon getPolygon(int xPosition, int yPosition) {
        // x-Koordinaten berechnen
        int[] x = new int[this.ecken];
        x[0] = (int) (xPosition * this.feldBreite);
        x[1] = (int) (x[0] + this.feldBreite);
        x[2] = x[1];
        x[3] = x[0];
        
        // y-Koordinaten berechnen
        int[] y = new int[this.ecken];
        y[0] = (int) (yPosition * this.feldHoehe);
        y[1] = y[0];
        y[2] = (int) (y[0] + this.feldHoehe);
        y[3] = y[2];
        
        // Polygon zurueckgeben
        return new Polygon(x, y, this.ecken);
    }
    
    @Override
    public double getScale() {
        return 1.0;
    }
}
