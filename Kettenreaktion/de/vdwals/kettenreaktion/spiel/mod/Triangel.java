package de.vdwals.kettenreaktion.spiel.mod;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

/**
 * Klasse zum Spielen auf Dreieckigen Feldern
 * 
 * @author Dennis van der Wals
 * 
 */
public class Triangel extends Mod {
    /**
     */
    private final double FAKTOR = Math.tan(Math.toRadians(60));
    
    /**
     * Standardkonstrutkor
     */
    public Triangel() {
        super("Triangle", (byte) 3);
    }
    
    @Override
    protected void calcFeldDimension() {
        this.feldBreite = (2 * this.fensterBreite) / (this.karte.length + 1);
        this.feldHoehe = this.fensterHoehe / this.karte[0].length;
    }
    
    @Override
    protected double getMaxRadius() {
        return this.feldBreite / (2 * this.FAKTOR);
    }
    
    @Override
    protected Point2D getMittelpunkt(int xPosition, int yPosition) {
        double fromBase = this.feldHoehe / (this.FAKTOR * this.FAKTOR);
        return new Point2D.Double(
                ((xPosition + 1) * this.feldBreite) / 2,
                (yPosition * this.feldHoehe)
                        + ((this.up(xPosition, yPosition)) ? (this.feldHoehe - fromBase)
                                : fromBase));
    }
    
    @Override
    public Point[] getNachbarn(Point p) {
        Point[] nachbarn = new Point[this.ecken];
        nachbarn[0] = new Point(p.x - 1, p.y);
        nachbarn[1] = new Point(p.x + 1, p.y);
        
        if (this.up(p.x, p.y))
            nachbarn[2] = new Point(p.x, p.y + 1);
        else
            nachbarn[2] = new Point(p.x, p.y - 1);
        return nachbarn;
    }
    
    @Override
    public Polygon getPolygon(int xPosition, int yPosition) {
        int[] x = new int[this.ecken];
        int[] y = new int[this.ecken];
        
        double halbeFeldbreite = this.feldBreite / 2;
        
        x[0] = (int) (xPosition * halbeFeldbreite);
        x[1] = (int) (x[0] + halbeFeldbreite);
        x[2] = (int) (x[0] + this.feldBreite);
        
        y[0] = (int) (this.up(xPosition, yPosition) ? ((double) yPosition + 1)
                * this.feldHoehe : yPosition * this.feldHoehe);
        y[1] = (int) (this.up(xPosition, yPosition) ? yPosition
                * this.feldHoehe : ((double) yPosition + 1) * this.feldHoehe);
        y[2] = y[0];
        
        return new Polygon(x, y, this.ecken);
    }
    
    @Override
    public Rectangle getRectangle(int xLinks, int xRechts, int yOben, int yUnten) {
        // Verschiebung der ungeraden Zeilen beachten
        Rectangle von = this.getPolygon(xLinks, yOben).getBounds();
        Rectangle bis = this.getPolygon(xRechts, yUnten).getBounds();
        return new Rectangle((int) (von.getX() - this.feldBreite),
                (int) von.getY(),
                (int) (bis.getX() + bis.getWidth() + this.feldBreite),
                (int) (bis.getY() + bis.getHeight() + this.feldHoehe));
    }
    
    @Override
    public double getScale() {
        return ((double) this.karte[0].length / (this.karte.length + 1))
                * this.FAKTOR;
    }
    
    /**
     * Ueberprueft, ob die Spitze des Dreiecks nach Oben zeigt.
     * 
     * @param x
     *            x-Koordinate
     * @param y
     *            y-Koordinate
     * @return Wahrheitswert
     */
    private boolean up(int x, int y) {
        return ((x + y) % 2) == 0;
    }
    
}
