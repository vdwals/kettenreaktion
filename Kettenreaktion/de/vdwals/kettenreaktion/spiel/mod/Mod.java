package de.vdwals.kettenreaktion.spiel.mod;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.LinkedList;
import java.util.Random;

/**
 * Klasse fuer einfache Spielbrettmodifikationen
 * 
 * @author Dennis van der Wals
 * 
 */
public abstract class Mod {
    private final int     FIELD_DIMENSION = 6;
    private String        name;
    protected byte        ecken;
    protected boolean[][] karte;
    protected double      fensterBreite, fensterHoehe, feldBreite, feldHoehe;
    private int           maxChips;
    
    /**
     * Erzeugt einen neuen Mod mit einem Namen.
     * 
     * @param name
     *            Name des Mods
     */
    public Mod(String name, byte ecken) {
        this.name = name;
        this.ecken = ecken;
        this.maxChips = (ecken - 1) * 2;
        this.fensterBreite = 0;
        this.fensterHoehe = 0;
        this.karte = this.getStandardKarte();
    }
    
    /**
     * Berechnet die Felddimensionen.
     */
    protected abstract void calcFeldDimension();
    
    /**
     * Erzeugt das Standardspielbrett.
     * 
     * @return Standardspielbrett
     */
    public byte[][] generateDefaultMap() {
        byte[][] brett = new byte[this.karte.length][this.karte[0].length];
        boolean s = true;
        for (int x = 0; x < brett.length; x++) {
            for (int y = 0; y < brett[0].length; y++)
                if (this.karte[x][y]) {
                    brett[x][y] = 1;
                    if (s)
                        brett[x][y] = 2;
                    s = !s;
                } else
                    brett[x][y] = -1;
            s = !s;
        }
        return brett;
    }
    
    /**
     * Erstellt eine zufaellig gefuellte Karte.
     * 
     * @return zufaellig gefuelltes Spielbrett
     */
    public byte[][] generateRandomMap() {
        // Standardkarte laden und auswerten
        byte[][] brett = this.generateDefaultMap();
        LinkedList<Point> felder = new LinkedList<Point>();
        int chips = 0;
        for (int x = 0; x < brett.length; x++)
            for (int y = 0; y < brett[0].length; y++)
                if (this.karte[x][y]) {
                    felder.add(new Point(x, y));
                    chips += brett[x][y];
                }
        // Felder mit Chips zuruecksetzen
        for (Point point : felder)
            brett[point.x][point.y] = 0;
        // Chips zufaellig verteilen
        Random r = new Random();
        while (chips > 0) {
            chips--;
            // Waehle ein zufaelliges Feld
            Point p = felder.get(r.nextInt(felder.size()));
            brett[p.x][p.y]++;
            // Loesche das Feld aus der Liste
            if (brett[p.x][p.y] == (this.getChipsFuerReaktion() - 1))
                felder.remove(p);
        }
        
        return brett;
    }
    
    /**
     * Gibt zurueck, bei wie vielen Chips eine Reaktion startet
     * 
     * @return Gibt die Anzahl der fuer eine Reaktion notwendigen Chips zurueck
     */
    public byte getChipsFuerReaktion() {
        return this.ecken;
    }
    
    /**
     * Gibt die eingestellte Fensterbreite zurueck.
     * 
     * @return eingestellte Fensterbreite
     */
    public double getFensterBreite() {
        return this.fensterBreite;
    }
    
    /**
     * Gibt die eingestellte Fensterhoehe zurueck.
     * 
     * @return eingestellte Fensterhoehe
     */
    public double getFensterHoehe() {
        return this.fensterHoehe;
    }
    
    /**
     * Gibt die Karte des Spielfeldes zurueck.
     * 
     * @return Karte des Spielfeldes
     */
    public boolean[][] getKarte() {
        return this.karte;
    }
    
    /**
     * Gibt die Matrixkoordinaten zu den Mauskoordinaten zurueck.
     * 
     * @param x
     *            x-Koordinate der Maus
     * @param y
     *            y-Koordinate der Maus
     * @return Punkt in der Matrix
     */
    public Point getKoordinaten(int xPosition, int yPosition) {
        for (int x = 0; x < this.karte.length; x++)
            for (int y = 0; y < this.karte[0].length; y++)
                if (this.getPolygon(x, y).contains(xPosition, yPosition))
                    return new Point(x, y);
        return null;
    }
    
    /**
     * Gibt die maximale Anzahl der Chips zurueck, die auf dem Feld landen
     * koennen.
     * 
     * @return maximale Anzahl der Chips
     */
    public int getMaxChips() {
        return this.maxChips;
    }
    
    /**
     * Gibt den Radius des ersten Chips zurueck.
     * 
     * @return Radius des ersten Chips
     */
    protected double getMaxRadius() {
        return this.feldBreite / 2;
    }
    
    /**
     * Gibt die Werte fuer die zu zeichnende Mitte zurueck.
     * 
     * @param xPosition
     *            x-Koordinate auf Karte
     * @param yPosition
     *            y-Koordinate auf Karte
     * @param fensterBreite
     *            Breite des Fensters in Pixel
     * @param fensterHoehe
     *            Hoehe des Fensters in Pixel
     * @return Ellipse2D
     */
    public Ellipse2D getMiddle(int xPosition, int yPosition) {
        double[] oval = new double[3];
        
        Point2D mitte = this.getMittelpunkt(xPosition, yPosition);
        
        double r = this.getRadiusOfMitte();
        
        oval[0] = mitte.getX() - r;
        oval[1] = mitte.getY() - r;
        oval[2] = r * 2;
        
        return new Ellipse2D.Double(oval[0], oval[1], oval[2], oval[2]);
    }
    
    /**
     * Gibt die Koordinaten des Kreismittelpunktes des gewaehlten Feldes
     * zurueck.
     * 
     * @param xPosition
     *            x-Koordinate des Felder in der Matrix
     * @param yPosition
     *            y-Koordinate des Feldes in der Matrix
     * @return Point - Mittelpunkt
     */
    protected abstract Point2D getMittelpunkt(int xPosition, int yPosition);
    
    /**
     * Gibt die Nachbarfelder zu einem Feld zurueck.
     * 
     * @param p
     *            Feld, dessen Nachbarn ermittelt werden sollen
     * @return Array mit Nachbarfeldern
     */
    public abstract Point[] getNachbarn(Point p);
    
    /*
     * Theoreme: Maximale Chipanzahl pro Feld: (ecken - 1) * 2; Anzahl der
     * Kreise = ((ecken - 1) * 2) + 1, fuer die Mitte -> 2 * ecken - 1;
     * Skalierungsfaktor fuer das Oval: Anzahl der Kreise * 2 -> 4 * ecken - 2;
     */
    /**
     * Gibt die Werte fuer den zu zeichnenden Chip zurueck.
     * 
     * @param xPosition
     *            x-Koordinate auf Karte
     * @param yPosition
     *            y-Koordinate auf Karte
     * @param feldBreite
     *            Breite des Fensters in Pixel
     * @param feldHoehe
     *            Hoehe des Fensters in Pixel
     * @param chip
     *            der Wievielte Chip soll gezeichnet werden
     * @return Ellipse2D
     */
    public Ellipse2D getOval(int xPosition, int yPosition, int chip) {
        double[] oval = new double[3];
        
        Point2D mitte = this.getMittelpunkt(xPosition, yPosition);
        double r = this.getRadius(chip);
        
        oval[0] = mitte.getX() - r;
        oval[1] = mitte.getY() - r;
        oval[2] = r * 2;
        
        return new Ellipse2D.Double(oval[0], oval[1], oval[2], oval[2]);
    }
    
    /**
     * 
     * Gibt das darzustellende Polygon zurueck.
     * 
     * @param xPosition
     *            x-Koordinate auf Karte
     * @param yPosition
     *            y-Koordinate auf Karte
     * @param feldBreite
     *            Breite des Fensters in Pixel
     * @param feldHoehe
     *            Hoehe des Fensters in Pixel
     * @return Polygon
     */
    public abstract Polygon getPolygon(int xPosition, int yPosition);
    
    /**
     * Gibt den Radius des nten Chips zurueck.
     * 
     * @param chip
     *            Anzahl der liegenden Chips
     * @return Radius des zu legenden Chips
     */
    protected double getRadius(int chip) {
        return (((this.getMaxChips() - chip) * this.getMaxRadius()) / this
                .getMaxChips());
    }
    
    /**
     * Gibt den Radius des Mittelpunktes zurueck.
     * 
     * @return Radius des Mittelpunktes
     */
    protected double getRadiusOfMitte() {
        return this.getMaxRadius() / (this.getMaxChips() + 2);
    }
    
    /**
     * Gibt ein Rechteck zurueck, dass alle Felder im Intervall enthaelt.
     * 
     * @param xLinks
     *            linkes Feld in x-Richtung
     * @param xRechts
     *            rechtes Feld in x-Richtung
     * @param yOben
     *            oberes Feld in y-Richtung
     * @param yUnten
     *            unteres Feld in y-Richtung
     * @param feldBreite
     *            Breite des Fensters in Pixel
     * @param feldHoehe
     *            Hoehe des Fensters in Pixel
     * @return Rechteck
     */
    public Rectangle getRectangle(int xLinks, int xRechts, int yOben, int yUnten) {
        return new Rectangle((int) (xLinks * this.feldBreite),
                (int) (yOben * this.feldHoehe),
                (int) (((xRechts - xLinks) + 1) * this.feldBreite),
                (int) (((yUnten - yOben) + 1) * this.feldHoehe));
    }
    
    /**
     * Gibt das Verhaeltnis von Breite zur Hoehe zurueck.
     * 
     * @return Verhaeltnis von Breite zur Hoehe (H/B)
     */
    public abstract double getScale();
    
    /**
     * Gibt den Skallierungswert fuer die Chips zurueck.
     * 
     * @return Skallierungswert
     */
    protected double getSkal() {
        return ((4.0 * this.ecken) - 2.0);
    }
    
    /**
     * Gibt die Standardkarte zurueck.
     * 
     * @return Standardkarte
     */
    public boolean[][] getStandardKarte() {
        boolean[][] karte = new boolean[this.FIELD_DIMENSION][this.FIELD_DIMENSION];
        for (int x = 0; x < karte.length; x++)
            for (int y = 0; y < karte[0].length; y++)
                karte[x][y] = true;
        return karte;
    }
    
    /**
     * Setzt die Fensterbreite fuer die Berechnungen
     * 
     * @param fensterBreite
     *            Fensterbreite
     * @param fensterHoehe
     *            Fensterhoehe
     */
    public void setFensterDimension(int fensterBreite, int fensterHoehe) {
        this.fensterBreite = fensterBreite;
        this.fensterHoehe = fensterHoehe;
        this.calcFeldDimension();
    }
    
    /**
     * Setzt die Karte des Spielfeldes.
     * 
     * @param karte
     *            Karte des Spielfeldes
     */
    public void setKarte(boolean[][] karte) {
        this.karte = karte;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
}
