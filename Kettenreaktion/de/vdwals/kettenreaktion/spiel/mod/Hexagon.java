package de.vdwals.kettenreaktion.spiel.mod;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

/**
 * Die Klasse ist zum Spielen auf Sechseckigen Feldern
 * 
 * @author Dennis van der Wals
 * 
 */
public class Hexagon extends Mod {
    private final static double FAKTOR          = Math.sin(Math.toRadians(60));
    private final int           FIELD_DIMENSION = 8;
    
    /**
     * Standardkonstruktor.
     */
    public Hexagon() {
        super("Hexagon", (byte) 6);
    }
    
    @Override
    protected void calcFeldDimension() {
        this.feldBreite = this.fensterBreite / this.karte.length;
        this.feldHoehe = (int) (this.fensterHoehe / ((0.75 * this.karte[0].length) + 0.25));
    }
    
    @Override
    public byte[][] generateDefaultMap() {
        byte[][] brett = new byte[this.karte.length][this.karte[0].length];
        byte i = 2;
        boolean t = true;
        for (int x = 0; x < brett.length; x++) {
            for (int y = 0; y < brett[0].length; y++) {
                if (this.karte[x][y])
                    brett[x][y] = i;
                else
                    brett[x][y] = -1;
                
                // Zwischen den Werten swappen
                if (t)
                    i--;
                else
                    i++;
                
                // Grenzen einhalten
                if (i == 5)
                    i = 2;
                else if (i == 1)
                    i = 4;
                
                // Swapboolean swappen
                t = !t;
            }
            // Fuer naechste Spalte erhoehen
            i++;
            
            // Grenzen einhalten
            if (i == 5)
                i = 2;
        }
        return brett;
    }
    
    @Override
    protected Point2D getMittelpunkt(int xPosition, int yPosition) {
        double x = 0;
        if ((yPosition % 2) == 0)
            x = this.feldBreite * (xPosition + 0.5);
        else
            x = (this.feldBreite * (xPosition + 0.5)) + (this.feldBreite / 2);
        return new Point2D.Double(x, this.feldHoehe
                * ((0.75 * yPosition) + 0.5));
    }
    
    @Override
    public Point[] getNachbarn(Point p) {
        Point[] nachbarn = new Point[this.ecken];
        nachbarn[0] = new Point(p.x - 1, p.y);
        nachbarn[1] = new Point(p.x, p.y + 1);
        nachbarn[2] = new Point(p.x + 1, p.y);
        nachbarn[3] = new Point(p.x, p.y - 1);
        /*
         * Bei ungerade Zeilen sind Nachbarn oben-/untenrechts, sonst
         * oben-/untenlinks
         */
        if ((p.y % 2) == 1) {
            nachbarn[4] = new Point(p.x + 1, p.y + 1);
            nachbarn[5] = new Point(p.x + 1, p.y - 1);
        } else {
            nachbarn[4] = new Point(p.x - 1, p.y + 1);
            nachbarn[5] = new Point(p.x - 1, p.y - 1);
        }
        return nachbarn;
    }
    
    @Override
    public Polygon getPolygon(int xPosition, int yPosition) {
        /*
         * x_n,0 = (0|y) fuer n%2 = 0 x_n,0 = (fensterBreite/2|y) fuer n%2 = 1
         */
        // x-Koordinaten berechnen
        int[] x = new int[this.ecken];
        x[0] = (int) (xPosition * this.feldBreite);
        
        double bHalbe = this.feldBreite / 2;
        
        if ((yPosition % 2) == 1)
            x[0] += (int) bHalbe;
        
        x[1] = (int) (x[0] + bHalbe);
        x[2] = (int) (x[0] + this.feldBreite);
        
        for (int i = 3; i < x.length; i++)
            x[i] = x[5 - i];
        
        // y-Koordinaten berechnen
        int[] y = new int[this.ecken];
        double hHalbe = this.feldHoehe / 2;
        double hViertel = this.feldHoehe / 4;
        
        y[0] = (int) (hViertel + ((double) yPosition * 3 * hViertel));
        y[1] = (int) (y[0] - hViertel);
        y[2] = y[0];
        y[3] = (int) (y[0] + hHalbe);
        y[4] = (int) (y[1] + this.feldHoehe);
        y[5] = y[3];
        
        return new Polygon(x, y, this.ecken);
    }
    
    @Override
    public Rectangle getRectangle(int xLinks, int xRechts, int yOben, int yUnten) {
        // Verschiebung der ungeraden Zeilen beachten
        Rectangle von = this.getPolygon(xLinks, yOben).getBounds();
        Rectangle bis = this.getPolygon(xRechts, yUnten).getBounds();
        return new Rectangle((int) (von.x - this.feldBreite), von.y,
                (int) (bis.x + bis.width + this.feldBreite), (int) (bis.y
                        + bis.height + this.feldHoehe));
    }
    
    @Override
    public double getScale() {
        return ((0.75 * this.karte[0].length) + 0.25)
                / (this.karte.length * FAKTOR);
    }
    
    @Override
    public boolean[][] getStandardKarte() {
        boolean[][] karte = new boolean[this.FIELD_DIMENSION][this.FIELD_DIMENSION];
        for (int x = 0; x < karte.length; x++)
            for (int y = 0; y < karte[0].length; y++)
                // Die ungeraden Reihen sind gekuerzt.
                if (((y % 2) == 1) && (x == (karte.length - 1)))
                    karte[x][y] = false;
                else
                    karte[x][y] = true;
        return karte;
    }
    
    @Override
    public void setKarte(boolean[][] karte) {
        for (int x = 0; x < karte.length; x++)
            for (int y = 0; y < karte[0].length; y++)
                // Die ungeraden Reihen sind gekuerzt.
                if (((y % 2) == 1) && (x == (karte.length - 1)))
                    karte[x][y] = false;
                else
                    karte[x][y] = true;
        this.karte = karte;
    }
    
}
