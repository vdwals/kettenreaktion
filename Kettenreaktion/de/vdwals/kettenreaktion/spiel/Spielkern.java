package de.vdwals.kettenreaktion.spiel;

import java.awt.Point;
import java.util.LinkedList;
import java.util.Random;

import de.vdwals.kettenreaktion.gui.Einstellungen;
import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.spiel.Reaktor.ReaktorPoint;

/**
 * Kettenreaktionsspiel
 * 
 * @author Dennis van der Wals
 * 
 */
public class Spielkern {
    // Interne Spielvariablen
    private byte          aktiverSpieler, spielbrett[][], chipsToSet[];
    private int           bank, maxKettenreaktion[], konto[], runde;
    private boolean[]     skip;
    private Random        wuerfel;
    
    // Externe Spielvariablen
    private Einstellungen einstellungen;
    
    /**
     * Standardkonstruktor.
     * 
     * @param einstellungen
     *            Settingsdatei mit den Einstellungen des Spiels
     */
    public Spielkern(Einstellungen einstellungen, byte startSpieler) {
        this.einstellungen = einstellungen;
        this.wuerfel = new Random();
        
        // Erzeugt das Spielfeld
        this.resetFeld();
        
        this.bank = einstellungen.getSpielOpt().getBankChips();
        
        // Berechnet die Gesamtzahl der Chips
        einstellungen.getSpielOpt().setGesamtChips(
                this.bank + this.countChips());
        
        // Erzeugt die Spielerkonten und fuellt sie auf
        this.konto = new int[einstellungen.getSpielOpt().getKi().length];
        this.chipsToSet = new byte[einstellungen.getSpielOpt().getKi().length];
        this.skip = new boolean[einstellungen.getSpielOpt().getKi().length];
        this.maxKettenreaktion = new int[einstellungen.getSpielOpt().getKi().length];
        for (int i = 0; i < this.konto.length; i++) {
            this.konto[i] = einstellungen.getSpielOpt().getStartChips();
            this.skip[i] = false;
            this.chipsToSet[i] = einstellungen.getSpielOpt().getMaxChipsToSet();
            einstellungen.getSpielOpt().setGesamtChips(
                    einstellungen.getSpielOpt().getGesamtChips()
                            + einstellungen.getSpielOpt().getStartChips());
        }
        
        // Starten eines neuen Spieles in der Statistik
        einstellungen.getStatistik().neuesSpiel(
                ArrOps.arrayCopy(this.spielbrett), einstellungen);
        
        // Setzt den aktiven Spieler auf den ersten Spieler
        this.aktiverSpieler = startSpieler;
        this.runde = 1;
        this.calcChipsToSet();
        
        // Einstellungen sperrem
        einstellungen.startSpiel(true);
    }
    
    /**
     * Erstellt ein Spiel aus gespeicherten Daten eines anderen Spieles.
     * 
     * @param einstellungen
     *            Spieleinstellungen
     * @param konto
     *            Kontostaende
     * @param aktiverSpieler
     *            aktiver Spieler
     * @param brett
     *            Spielbrett
     * @param bank
     *            Anzahl der Chips in der Bank
     * @param skip
     *            Liste mit zu ueberspringenden Spielern
     * @param maxkettenreaktion
     *            Liste mit der groessten Kettenreaktion des jeweiligen Spielers
     */
    public Spielkern(Einstellungen einstellungen, int[] konto,
            byte aktiverSpieler, byte[][] brett, int bank, boolean[] skip,
            int[] maxkettenreaktion) {
        this.wuerfel = new Random();
        this.einstellungen = einstellungen;
        this.konto = konto;
        this.aktiverSpieler = aktiverSpieler;
        this.spielbrett = brett;
        this.bank = bank;
        this.skip = skip;
        this.maxKettenreaktion = maxkettenreaktion;
        this.chipsToSet = new byte[konto.length];
        for (int j = 0; j < maxkettenreaktion.length; j++)
            this.chipsToSet[j] = einstellungen.getSpielOpt().getMaxChipsToSet();
        
        // Starten eines neuen Spieles in der Statistik
        einstellungen.getStatistik().neuesSpiel(
                ArrOps.arrayCopy(this.spielbrett), einstellungen);
        this.calcChipsToSet();
        
        // Einstellungen sperrem
        einstellungen.startSpiel(true);
    }
    
    /**
     * Berechnet die Auswirkungen auf das Spiel durch das setzen der Chips und
     * gibt die Veraenderungen fuer die Animation zurueck.
     * 
     * @param stapel
     *            Array mit zu legenden Chips vom Typ Point
     * @return Chips mit allen nachfolgenden Aenderungen
     */
    public LinkedList<ReaktorPoint> beendeRunde(Point[] stapel) {
        LinkedList<ReaktorPoint> aenderungen = null;
        
        // Stapel bearbeiten
        stapel = this.kuerzeStapel(stapel);
        
        // Leere Stapel abfangen
        if (stapel != null) {
            aenderungen = new LinkedList<ReaktorPoint>();
            
            // ************* Kontrollwerte und Kontrollschleife *************//
            int secureGewinn = 0;
            int secureReaktionen = 0;
            int secureRausGefallen = 0;
            int secureAusBank = 0;
            byte[][] kontrollBrett = new byte[this.spielbrett.length][this.spielbrett[0].length];
            String debugInformation = "";
            if (this.einstellungen.getProgOpt().isDebug()) {
                SecureReaktor secureReaktor = new SecureReaktor(
                        this.einstellungen.getSpielOpt().getMod());
                debugInformation += "Initialfeld: \n"
                        + ArrOps.feldToString(this.spielbrett) + "\n"
                        + ArrOps.chipsToString(stapel) + "\n";
                kontrollBrett = ArrOps.arrayCopy(this.spielbrett);
                int tmpBank = this.bank;
                for (int i = 0; i < stapel.length; i++)
                    if (stapel[i] != null) {
                        kontrollBrett = secureReaktor.reaktion(kontrollBrett,
                                stapel[i], this.einstellungen.getGrafOpt()
                                        .useBF());
                        secureAusBank += secureReaktor.getAusBank();
                        if (secureReaktor.getAusBank() <= tmpBank) {
                            secureGewinn += secureReaktor.getAusBank();
                            tmpBank -= secureReaktor.getAusBank();
                        } else {
                            secureGewinn += tmpBank;
                            tmpBank = 0;
                        }
                        secureGewinn += secureReaktor.getGewinn();
                        secureRausGefallen += secureReaktor.getGewinn();
                        secureReaktionen += secureReaktor.getReaktionen();
                    } else if (i == this.einstellungen.getSpielOpt()
                            .getMaxChipsToSet())
                        break;
            }
            // **************************************************************//
            
            // Neues Brett berechnen
            this.spielbrett = this.einstellungen
                    .getSpielOpt()
                    .getReaktor()
                    .reaktion(this.spielbrett, stapel,
                            this.einstellungen.getGrafOpt().useBF(), this.bank);
            int gewinn = 0;
            
            // Man bekommt nicht mehr aus der Bank, als drin ist
            if (this.einstellungen.getSpielOpt().getReaktor().getAusBank() <= this.bank) {
                gewinn += this.einstellungen.getSpielOpt().getReaktor()
                        .getAusBank();
                this.bank -= this.einstellungen.getSpielOpt().getReaktor()
                        .getAusBank();
            } else {
                gewinn += this.bank;
                this.bank = 0;
            }
            
            // Reingewinne hinzurechnen (Chips, die vom Brett fallen)
            int rausGefallen = this.einstellungen.getSpielOpt().getReaktor()
                    .getExtraGewinn();
            gewinn += rausGefallen;
            
            // Fuer Statistik verlorene Chips zaehlen
            this.einstellungen.getStatistik().beendeZug(rausGefallen, gewinn);
            
            // Reaktionen abspeichern - fuer Statistik
            int reaktionen = this.einstellungen.getSpielOpt().getReaktor()
                    .getReaktionen();
            
            // ******** Vergleiche zwischem altem und neuem Reaktor *********//
            if (this.einstellungen.getProgOpt().isDebug()) {
                debugInformation += "securefeld: \n"
                        + ArrOps.feldToString(kontrollBrett)
                        + "\n reaktorfeld: \n"
                        + ArrOps.feldToString(this.spielbrett);
                
                boolean fehler = false;
                for (int x = 0; x < kontrollBrett.length; x++) {
                    for (int y = 0; y < kontrollBrett[0].length; y++)
                        if (this.spielbrett[x][y] != kontrollBrett[x][y]) {
                            BugReport.recordBug(new IllegalArgumentException(
                                    "Spielbretter ungleich! \n"
                                            + debugInformation));
                            fehler = true;
                            break;
                        }
                    if (fehler)
                        break;
                }
                if (gewinn != secureGewinn)
                    BugReport.recordBug(new IllegalArgumentException(
                            "Gewinne ungleich: sec = " + secureGewinn
                                    + "; neu = " + gewinn + "\n"
                                    + debugInformation));
                
                if (rausGefallen != secureRausGefallen)
                    BugReport.recordBug(new IllegalArgumentException(
                            "Anzahl herausgefallener Chips ungleich: sec = "
                                    + secureRausGefallen + "; neu = "
                                    + rausGefallen + "\n" + debugInformation));
                
                if (reaktionen != secureReaktionen)
                    BugReport.recordBug(new IllegalArgumentException(
                            "Reaktionenanzahl ungleich: sec = "
                                    + secureReaktionen + "; neu = "
                                    + reaktionen + "\n" + debugInformation));
                
                if (this.einstellungen.getSpielOpt().getReaktor().getAusBank() != secureAusBank)
                    BugReport.recordBug(new IllegalArgumentException(
                            "Reaktionenanzahl ungleich: sec = "
                                    + secureAusBank
                                    + "; neu = "
                                    + this.einstellungen.getSpielOpt()
                                            .getReaktor().getAusBank() + "\n"
                                    + debugInformation));
            }
            // **************************************************************//
            
            // Chip wird vom Konto entfernt
            this.konto[this.aktiverSpieler] -= stapel.length;
            
            // Gewinn hinzurechnen
            this.konto[this.aktiverSpieler] += gewinn;
            
            if (this.einstellungen.getProgOpt().isDebug())
                // Konsistenz der Chipsanzahl pruefen
                if (this.countChips() != this.getChips())
                    BugReport.recordBug(new IllegalArgumentException(
                            "Es sind Chips w�hrend des Spieles verloren gegangen: gezaehlt = "
                                    + this.countChips() + "; rechnerisch = "
                                    + this.getChips()));
            
            aenderungen.add(this.einstellungen.getSpielOpt().getReaktor()
                    .getAenderung());
            
            // Groesste Kettenreaktion speichern
            if (reaktionen > this.maxKettenreaktion[this.aktiverSpieler])
                this.maxKettenreaktion[this.aktiverSpieler] = reaktionen;
        }
        
        this.naechsterSpieler();
        
        // Markiert das Spiel nach der Aenderung als nicht gespeichert
        this.einstellungen.getSpielOpt().setSaved(false);
        return aenderungen;
    }
    
    /**
     * Berechnet fuer jeden Spieler, wie viele Spielsteine er setzen darf.
     */
    private void calcChipsToSet() {
        // Wuerfeln
        if (this.einstellungen.getSpielOpt().isWuerfeln())
            this.chipsToSet[this.aktiverSpieler] = (byte) (1 + this.wuerfel
                    .nextInt((this.einstellungen.getSpielOpt().getWuerfelEnd() - this.einstellungen
                            .getSpielOpt().getWuerfelStart()) + 1));
        
        // Einschraenkung nur nutzen, wenn Limitierung gewuenscht.
        if (this.einstellungen.getSpielOpt().isChipsLimit())
            if (this.konto[this.aktiverSpieler] < this.chipsToSet[this.aktiverSpieler])
                this.chipsToSet[this.aktiverSpieler] = (byte) this.konto[this.aktiverSpieler];
    }
    
    /**
     * Zaehlt, wie viele Chips auf dem Spielfeld liegen.
     * 
     * @return Anzahl der Chips
     */
    private int countChips() {
        int chips = 0;
        for (byte[] element : this.spielbrett)
            for (int y = 0; y < this.spielbrett[0].length; y++)
                if (element[y] != -1)
                    chips += element[y];
        return chips;
    }
    
    /**
     * Wird beim schliessen des Spiels aufgerufen. Gibt die Einstellungen wieder
     * frei und setzt die Bank auf 0 zum Schutz vor Manipulation.
     */
    public void gameClose() {
        this.bank = 0;
        this.einstellungen.startSpiel(false);
    }
    
    /**
     * Gibt den aktiven Spieler zurueck.
     * 
     * @return Nummer des aktiven Spielers
     */
    public byte getAktiverSpieler() {
        return this.aktiverSpieler;
    }
    
    /**
     * Gibt die Anzahl der Chips der Bank zurueck.
     * 
     * @return Chips der Bank
     */
    public int getBank() {
        return this.bank;
    }
    
    /**
     * Gibt die Anzahl der Chips wieder, die rechnerisch auf dem Spielfeld
     * liegen sollten.
     * 
     * @return Anzahl der Chips auf dem Spielbrett
     */
    public int getChips() {
        int konten = 0;
        for (int element : this.konto)
            konten += element;
        return this.einstellungen.getSpielOpt().getGesamtChips() - this.bank
                - konten;
    }
    
    /**
     * Gibt zurueck, wie viele Chips der aktuelle Spieler setzen darf.
     * 
     * @return
     */
    public byte getChipsToSet() {
        return this.chipsToSet[this.aktiverSpieler];
    }
    
    /**
     * Gibt eine Kopie der Konten zurueck.
     * 
     * @return Kopie der Konten
     */
    public int[] getKonto() {
        return ArrOps.arrayCopy(this.konto);
    }
    
    /**
     * Gibt eine Kopie der Liste mit den groessten Kettenreaktionen der Spieler
     * zurueck.
     * 
     * @return Liste mit den jeweils groessten Kettenreaktionen
     */
    public int[] getMaxKettenreaktion() {
        return ArrOps.arrayCopy(this.maxKettenreaktion);
    }
    
    /**
     * Gibt die aktuelle Runde zurueck.
     * 
     * @return Runde
     */
    public int getRunde() {
        return this.runde;
    }
    
    /**
     * Gibt eine die Liste der zu ueberpringenden Spieler zurueck.
     * 
     * @return Liste mit zu ueberspringenden Spielern
     */
    public boolean[] getSkip() {
        return this.skip;
    }
    
    /**
     * Gibt eine Kopie des Spielfeldes zurueck.
     * 
     * @return Kopie des Spielfeldes
     */
    public byte[][] getSpielbrett() {
        return ArrOps.arrayCopy(this.spielbrett);
    }
    
    /**
     * Gibt die Anzahl der Spieler zurueck.
     * 
     * @return Anzahl der Spieler
     */
    public int getSpieler() {
        return this.konto.length;
    }
    
    /**
     * Ermittelt, ob das Spiel vorbei ist, wenn die Bank keine Chips mehr hat
     * oder nur noch ein Spieler mehr als 0 Chips hat.
     * 
     * @return Wahrheitswert
     */
    public boolean isOver() {
        // Zaehlt, wie viele Spieler uebersprungen werden
        int skips = 0;
        for (boolean element : this.skip)
            if (element)
                skips++;
        return ((this.bank <= 0) || (skips >= (this.skip.length - 1)));
    }
    
    /**
     * Gibt einen Stapel ohne leere Elemente zurueck.
     * 
     * @param stapel
     *            Stapel, der evtl. leere Elemente enthaelt
     * @return Stapel ohne leere Elemente, oder Null, wenn alle leer waren
     */
    private Point[] kuerzeStapel(Point[] stapel) {
        if (stapel != null) {
            LinkedList<Point> neu = new LinkedList<Point>();
            for (Point point : stapel)
                if (point != null)
                    neu.add(point);
            if (neu.size() > 0) {
                Point[] result = new Point[neu.size()];
                for (int i = 0; i < result.length; i++)
                    result[i] = neu.get(i);
                return result;
            } else
                return null;
        } else
            return null;
    }
    
    /**
     * Aktiviert den naechsten Spieler
     */
    private void naechsterSpieler() {
        // Gibt es ein Kontolimit, werden Spieler ohne Chips markiert
        if (this.einstellungen.getSpielOpt().isChipsLimit())
            for (int i = 0; i < this.konto.length; i++)
                this.skip[i] = (this.konto[i] == 0);
        
        if (!this.isOver()) {
            // Zaehler wird um 1 erhoeht
            this.aktiverSpieler++;
            
            // Vom letzten zum ersten Spieler springen
            if (this.aktiverSpieler >= this.konto.length)
                this.aktiverSpieler = 0;
            
            // Wird der Spieler uebersprungen, naechster Spieler
            if (this.skip[this.aktiverSpieler])
                this.naechsterSpieler();
            
            this.calcChipsToSet();
            this.runde++;
        }
    }
    
    /**
     * Besetzt das Feld mit den Startwerten
     */
    private void resetFeld() {
        switch (this.einstellungen.getSpielOpt().getMapModus()) {
            case 0:
                this.spielbrett = this.einstellungen.getSpielOpt().getMod()
                        .generateDefaultMap();
                break;
            case 1:
                this.spielbrett = this.einstellungen.getSpielOpt().getMod()
                        .generateRandomMap();
            default:
                break;
        }
    }
    
}
