package de.vdwals.kettenreaktion.spiel;

import java.awt.Point;
import java.util.LinkedList;

import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.spiel.mod.Mod;
import de.vdwals.kettenreaktion.spiel.mod.Quadrat;
import de.vdwals.kettenreaktion.statistik.Statistik;

/**
 * Klasse zum ausloesen einer Reaktion. Errechnet das neue Brett, sowie Anzahl
 * der Reaktionen, Anzahl der gewonnenen Chips und Anzahl der Chips aus der
 * Bank.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Reaktor {
    /**
     * Klasse zum Speichern der Aenderungen waehrend der Reaktion und zum
     * Speichern des neuen Feldwertes, der sich innerhalb einer Reaktionskette
     * ergab.
     * 
     * @author Dennis van der Wals
     */
    public class ReaktorPoint extends Point {
        
        /**
         * Default SerialID
         */
        private static final long        serialVersionUID = 1L;
        private byte                     change, neuWert;
        private LinkedList<ReaktorPoint> folgende;
        private ReaktorPoint             vorgaender;
        
        /**
         * Erzeugt einen neuen Punkt und speichert den Wert +1 als Aenderung.
         * 
         * @param p
         *            Punkt, an dem die Aenderung stattfindet.
         * @param neuWert
         *            Speichert den neuen Wert des Feldes.
         */
        private ReaktorPoint(Point p, byte neuWert) {
            super(p);
            this.neuWert = neuWert;
        }
        
        /**
         * Erzeugt einen neuen Punkt und speichert den Wert +1 als Aenderung.
         * 
         * @param p
         *            Punkt, an dem die Aenderung stattfindet.
         * @param vorgaenger
         *            Quellpunkt der Reaktion, die hierher fuehrte
         */
        private ReaktorPoint(Point p, ReaktorPoint vorgaenger) {
            super(p);
            this.change = 1;
            this.folgende = new LinkedList<ReaktorPoint>();
            this.vorgaender = vorgaenger;
        }
        
        /**
         * Fuegt einen Punkt an den Koordinaten als Nachfolger an, wenn er im
         * Feld ist.
         * 
         * @param x
         *            x-Koordinate
         * @param y
         *            y-Koordinate
         */
        private void add(int x, int y) {
            Point p = new Point(x, y);
            if (Reaktor.this.isInFeld(p))
                this.folgende.add(new ReaktorPoint(p, this));
        }
        
        /**
         * Fuegt einen Punkt als Nachfolger an, wenn er im Feld ist.
         * 
         * @param p
         *            Nachfolgender Punkt
         */
        private void add(ReaktorPoint p) {
            if (Reaktor.this.isInFeld(p))
                this.folgende.add(p);
        }
        
        /**
         * Gibt zurueck, welche Aenderungen vorgenommen wurden.
         * 
         * @return Aenderung
         */
        public int getChange() {
            return this.change;
        }
        
        /**
         * Gibt eine LinkedList mit allen folgenden Punkten zurueck
         * 
         * @return Alle folgenden Aenderungen
         */
        public LinkedList<ReaktorPoint> getFolgende() {
            return this.folgende;
        }
        
        /**
         * @return the neuWert
         */
        public byte getNeuWert() {
            return this.neuWert;
        }
        
        /**
         * Gibt den Wert des Feldes zurueck.
         * 
         * @return Wert des Feldes
         */
        public byte getValue() {
            return Reaktor.this.neuesBrett[this.x][this.y];
        }
        
        /**
         * Gibt zurueck, ob der Chip von einem Spieler gesetzt wurde.
         * 
         * @return Wahrheitswert
         */
        public boolean isGesetzt() {
            return (this.vorgaender == null);
        }
        
        /**
         * Laesst den Punkt reagieren und gibt den neuen Punkt mit reduzierter
         * Punktzahl zurueck.
         * 
         * @return neuen Punkt mit neuer Aenderung
         */
        private ReaktorPoint reagiere() {
            
            // Neuen Punkt erzeugen
            ReaktorPoint p = new ReaktorPoint(new Point(this.x, this.y), this);
            // QA Fuenferregel hier wichtig?
            if (Reaktor.this.fuenferRegel)
                p.change = (byte) -this.getValue();
            else
                p.change = (byte) -Reaktor.this.mod.getChipsFuerReaktion();
            
            // Nachbarn ermitteln
            Point[] borders = Reaktor.this.mod.getNachbarn(p);
            for (Point border : borders)
                this.add(border.x, border.y);
            this.add(p);
            return p;
        }
        
        /**
         * @param neuWert
         *            the neuWert to set
         */
        public void setNeuWert(byte neuWert) {
            this.neuWert = neuWert;
        }
        
    }
    
    /**
     * Klasse zum Ablegen der Koordinaten, welche eine Reaktionskette bilden und
     * speichern des Reaktionsablaufgewinns.
     * 
     * @author Dennis van der Wals
     */
    private class WaySearchPoint extends Point {
        /**
         * Default Serial-ID.
         */
        private static final long serialVersionUID = 2978772149298883734L;
        
        private WaySearchPoint    next;
        private int               gewinn;
        
        /**
         * Standardkonstruktor.
         * 
         * @param x
         *            x-Koordinate
         * @param y
         *            y-Koordinate
         * @param gewinn
         *            Gewinn der Reaktion
         */
        protected WaySearchPoint(Point p, int gewinn) {
            super(p);
            this.gewinn = gewinn;
        }
        
        /**
         * Gibt den gesamten Gewinn der Reaktionsreihenfolge zurueck.
         * 
         * @return gesamter Gewinn der Reaktionsreihenfolge
         */
        protected int getGesamtGewinn() {
            if (this.next != null)
                return this.gewinn + this.next.getGesamtGewinn();
            return this.gewinn;
        }
    }
    
    // DEBUG
    /**
     * Testfunktion
     */
    public static void main(String[] args) {
        Reaktor r = new Reaktor(new Quadrat(), new Statistik());
        r.fuenferRegel = true;
        r.bankRegel = true;
        byte[][] spielbrett = { { 3, 3, 3, 0, 0, 0 }, { 2, 2, 3, 0, 3, 0 },
                { 3, 3, 3, 0, 3, 0 }, { 0, 0, 3, 0, 2, 0 },
                { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 } };
        Point[] steine = { new Point(2, 2), new Point(1, 4), new Point(1, 0) };
        LinkedList<Point> t = new LinkedList<Point>();
        for (Point point : steine)
            t.add(point);
        int bank = 3;
        r.neuesBrett = ArrOps.arrayCopy(spielbrett);
        WaySearchPoint test = r.getBestWay(t, spielbrett, bank);
        
        while (test != null) {
            System.out.println("(" + test.x + "|" + test.y + ")");
            test = test.next;
        }
    }
    
    // Spielvariablen
    private int          gewinn, reaktionen, ausBank;
    private byte         neuesBrett[][];
    private ReaktorPoint aenderung;
    private boolean      fuenferRegel, bankRegel;
    
    // Externe Programme
    private Mod          mod;
    private Statistik    statistik;
    
    /**
     * Standardkonstruktor.
     * 
     * @param mod
     *            Modifikation
     * @param statistik
     *            Spielstatistik
     */
    public Reaktor(Mod mod, Statistik statistik) {
        this.mod = mod;
        this.statistik = statistik;
    }
    
    /**
     * Zaehlt, wie viele der uebergebenen Chips aus dem Spielfeld "fallen".
     * 
     * @param borders
     *            zu ueberpruefende Chips, meist Reaktionschips
     * @return Anzahl der Chips, welche aus dem Spielfeld fallen
     */
    private int countChipsGoOut(Point[] borders) {
        int counter = 0;
        for (int i = 0; i < borders.length; i++)
            if (!this.isInFeld(borders[i]))
                counter++;
        return counter;
    }
    
    /**
     * Gibt den Kopf des Aenderungsbaumes der letzten Reaktion zurueck.
     * 
     * @return Kopf des Aenderungsbaumes
     */
    public ReaktorPoint getAenderung() {
        return this.aenderung;
    }
    
    /**
     * Gibt zurueck, wie viele Chips nach der letzten Reaktion aus der Bank
     * ausgezahlt werden muessen. Entspricht der Anzahl der Reaktionen, die
     * nicht am Rand stattfanden, also bei der kein Chip verloren ging.
     * 
     * @return Anzahl der Chips, die aus der Bank entfernt werden muessen
     */
    public int getAusBank() {
        return this.ausBank;
    }
    
    /**
     * Methode zum ermitteln des besten Reaktionsweges fuer ein uebergebenes
     * Spielbrett und einem Array mit Initialisierungssteinen.
     * 
     * @param initialSteine
     *            Array aus Punkten
     * @param spielbrett
     *            Spielbrett
     * @param inBank
     *            Anzahl der Steine der Bank
     * @return WaySearchPoint - Reaktionsablaufplan
     */
    private WaySearchPoint getBestWay(LinkedList<Point> initialSteine,
            byte[][] spielbrett, int inBank) {
        LinkedList<ReaktorPoint> geandertStapel = new LinkedList<ReaktorPoint>();
        LinkedList<Point> reaktionsStapel = new LinkedList<Point>();
        
        // Erste Listen erstellen
        for (Point point : initialSteine) {
            
            // Aenderung eintragen
            ReaktorPoint p = new ReaktorPoint(point,
                    (byte) (spielbrett[point.x][point.y] + 1));
            if (geandertStapel.contains(p)) {
                p.setNeuWert((byte) (geandertStapel.get(
                        geandertStapel.indexOf(p)).getNeuWert() + 1));
                geandertStapel.get(geandertStapel.indexOf(p)).setNeuWert(
                        p.getNeuWert());
            } else
                geandertStapel.add(p);
            
            // Moegliche Reaktion eintragen
            if ((p.getNeuWert() >= this.mod.getChipsFuerReaktion())
                    && !reaktionsStapel.contains(point))
                reaktionsStapel.add(point);
        }
        int max = 0;
        WaySearchPoint best = null;
        
        for (Point aktuelleReaktion : reaktionsStapel) {
            /*
             * Kopiere alle zu uebergebenen Listen
             */
            LinkedList<ReaktorPoint> geaenderteStapelNext = new LinkedList<ReaktorPoint>();
            /*
             * Hardclone, da die Punkte von Schleife zu Schleife erhalten
             * bleiben, sich aber die internen Werte aendern.
             */
            for (ReaktorPoint changed : geandertStapel)
                geaenderteStapelNext.add(new ReaktorPoint(changed, changed
                        .getNeuWert()));
            LinkedList<Point> potentialstapelNext = new LinkedList<Point>();
            /*
             * Softclone, da die internen Werte irrelevant sind, nur die
             * Elemente der Liste sind wichtig.
             */
            potentialstapelNext.addAll(reaktionsStapel);
            
            WaySearchPoint kandidat = this.getBestWay(
                    geaenderteStapelNext,
                    potentialstapelNext,
                    spielbrett,
                    new ReaktorPoint(aktuelleReaktion, geandertStapel.get(
                            geandertStapel.indexOf(aktuelleReaktion))
                            .getNeuWert()), inBank);
            /*
             * Ist der Gewinn groesser, als der bisher erwartete, speichere
             * diesen Gewinn und den Punkt als Nachfolger.
             */
            if (kandidat.getGesamtGewinn() > max) {
                max = kandidat.getGesamtGewinn();
                best = kandidat;
            }
        }
        return best;
    }
    
    /**
     * Ermittelt den besten Reaktionsweg aus einer Liste von potentiellen
     * Reaktionskernen, aktuellen Feldwerten und der Anzahl der verfuegbaren
     * Banksteine.
     * 
     * @param geaenderteStapel
     *            Liste mit Feldern, deren Wert in vorigen Reaktionen geandert
     *            wurde und sich vom Spielfeldwert unterscheidet.
     * @param potentialstapel
     *            Stapel mit Feldern, bei denen die Reaktion fortgesetzt werden
     *            kann.
     * @param spielbrett
     *            Das Spielbrett mit initialwerten (also ohne Aenderungen durch
     *            fruehere Reaktionen).
     * @param aktuelleReaktion
     *            Die Koordinaten des aktuellen Reaktionskerns.
     * @param inBank
     *            Anzahl der Steine, die noch in der Bank liegen.
     * @return WaySearchPoint mit eingetragenem, bestem Nachfolger.
     */
    private WaySearchPoint getBestWay(
            LinkedList<ReaktorPoint> geaenderteStapel,
            LinkedList<Point> potentialstapel, byte[][] spielbrett,
            Point aktuelleReaktion, int inBank) {
        
        // Neuen Suchpunkt erstellen.
        WaySearchPoint aktuell = new WaySearchPoint(aktuelleReaktion, 0);
        int index = geaenderteStapel.indexOf(aktuelleReaktion);
        
        // Nachbarn bestimmen
        Point[] nachbarn = this.mod.getNachbarn(aktuelleReaktion);
        
        /*
         * Gewinn fuer aktuelle Reaktion bestimmen.
         */
        // Ueberprueft, ob wir in einer Ecke sind fuer Extrachips
        aktuell.gewinn = this.countChipsGoOut(nachbarn);
        
        /*
         * Reduziert die Chips der Bank um 1 wenn die Reaktion nicht am
         * Spielfeldrand stattfand und setzt den Gewinn auf 1
         */
        if (aktuell.gewinn == 0) {
            inBank--;
            aktuell.gewinn = 1;
        }
        
        /*
         * Ueberprueft, ob mehr als fuer die Reaktion notwendige Chips auf dem
         * Feld lagen, wenn 5er-Regel aktiv
         */
        if (this.fuenferRegel) {
            aktuell.gewinn += geaenderteStapel.get(index).getNeuWert()
                    - this.mod.getChipsFuerReaktion();
            geaenderteStapel.get(index).setNeuWert((byte) 0);
        } else
            // Chips abziehen
            geaenderteStapel.get(index).setNeuWert(
                    (byte) (geaenderteStapel.get(index).getNeuWert() - this.mod
                            .getChipsFuerReaktion()));
        
        /*
         * Folgen der aktuellen Reaktion ermitteln.
         */
        for (Point point : nachbarn)
            // Ueberpruefe, ob Nachbar im Feld ist.
            if (this.isInFeld(point)) {
                
                // Bestimme den Wert des Feldes
                byte wert = (byte) (spielbrett[point.x][point.y] + 1);
                
                if (geaenderteStapel.contains(point)) {
                    
                    /*
                     * Wurde das Feld im Verlauf frueherer Reaktionen schon
                     * veraendert, dann aktuallisiere den Aenderungswert...
                     */
                    wert = (byte) (geaenderteStapel.get(
                            geaenderteStapel.indexOf(point)).getNeuWert() + 1);
                    geaenderteStapel.remove(point);
                    
                    /*
                     * Neuen Wert nur eintragen, wenn er vom Spielfeldwert
                     * abweicht.
                     */
                    if (wert != spielbrett[point.x][point.y])
                        geaenderteStapel.add(new ReaktorPoint(point, wert));
                } else
                    // ... oder trage das Feld neu als geaendert ein.
                    geaenderteStapel.add(new ReaktorPoint(point, wert));
                
                // Bestimme ob der Stapel reagiert
                if (wert >= this.mod.getChipsFuerReaktion())
                    potentialstapel.add(point);
            }
        
        /*
         * Alle Nachfolgereaktionen durchgehen und Nachfolgegewinne ermitteln,
         * wenn Bankregel aktiv und Bedingung erfuellt, oder Bankregel inaktiv.
         */
        if (!this.bankRegel || (inBank > 0)) {
            
            int max = 0;
            // Gehe alle ausbleibenden Reaktionen durch und bestimme ihren Wert
            for (Point naechsteReaktion : potentialstapel) {
                
                // while (!potentialstapel.isEmpty()) {
                // Point naechsteReaktion = potentialstapel.pop();
                
                /*
                 * Kopiere alle zu uebergebenen Listen
                 */
                LinkedList<ReaktorPoint> geaenderteStapelNext = new LinkedList<ReaktorPoint>();
                /*
                 * Hardclone, da die Punkte von Schleife zu Schleife erhalten
                 * bleiben, sich aber die internen Werte aendern.
                 */
                for (ReaktorPoint changed : geaenderteStapel)
                    geaenderteStapelNext.add(new ReaktorPoint(changed, changed
                            .getNeuWert()));
                LinkedList<Point> potentialstapelNext = new LinkedList<Point>();
                /*
                 * Softclone, da die internen Werte irrelevant sind, nur die
                 * Elemente der Liste sind wichtig.
                 */
                potentialstapelNext.addAll(potentialstapel);
                potentialstapelNext.remove(naechsteReaktion);
                
                WaySearchPoint naechste = this
                        .getBestWay(
                                geaenderteStapelNext,
                                potentialstapelNext,
                                spielbrett,
                                new ReaktorPoint(
                                        naechsteReaktion,
                                        (geaenderteStapelNext
                                                .contains(naechsteReaktion)) ? geaenderteStapelNext
                                                .get(geaenderteStapelNext
                                                        .indexOf(naechsteReaktion))
                                                .getNeuWert()
                                                : spielbrett[naechsteReaktion.x][naechsteReaktion.y]),
                                inBank);
                /*
                 * Ist der Gewinn groesser, als der bisher erwartete, speichere
                 * diesen Gewinn und den Punkt als Nachfolger.
                 */
                if (naechste.getGesamtGewinn() > max) {
                    max = naechste.getGesamtGewinn();
                    aktuell.next = naechste;
                }
            }
        }
        
        return aktuell;
    }
    
    /**
     * Gibt den Gewinn der letzten Reaktion zurueck. Als Gewinn werden alle
     * Chips gezaehlt, die aus dem Spielfeld fallen.
     * 
     * @return Gewinn der letzten Reaktion
     */
    public int getExtraGewinn() {
        return this.gewinn;
    }
    
    /**
     * Gibt eine Kopie des Spielfelds zurueck, welches nach der letzten Reaktion
     * entstand.
     * 
     * @return Ausreagiertes Spielfeld mit hinterlegtem Chip
     */
    public byte[][] getNeuesBrett() {
        return ArrOps.arrayCopy(this.neuesBrett);
    }
    
    /**
     * Gibt die Anzahl der Reaktionen zurueck, die waehrend der letzten
     * Kettenreaktion stattfanden.
     * 
     * @return Anzahl der letzten Reaktionen
     */
    public int getReaktionen() {
        return this.reaktionen;
    }
    
    /**
     * Gibt zurueck, ob der Punkt p innerhalb ger Grenzen des Spielfeldes liegt.
     * 
     * @param p
     *            Punkt
     * @return Wahrheitswert
     */
    private boolean isInFeld(Point p) {
        if ((p.x < 0) || (p.x >= this.neuesBrett.length) || (p.y < 0)
                || (p.y >= this.neuesBrett[0].length))
            return false;
        return (this.neuesBrett[p.x][p.y] != -1);
    }
    
    /**
     * Berechnet die Kettenreaktion einer Eingabe, speichert/ueberschreibt
     * Anzahl der Reaktionen, Anzahl der gewonnenen Chips und Anzahl der Chips
     * aus der Bank.
     * 
     * @param brett
     *            Aktuelles Spielbrett
     * @param chips
     *            gelegte Chips
     * @param bf
     *            Soll der Breitensuchealgorithmus benutzt werden
     * @param inBank
     *            Wie viele Chips leigen noch in der Bank
     * @return neues Spielfeld
     */
    public byte[][] reaktion(byte[][] brett, Point[] chip, boolean bf,
            int inBank) {
        // Initialisiert die Variablen
        this.gewinn = this.reaktionen = this.ausBank = 0;
        this.neuesBrett = ArrOps.arrayCopy(brett);
        
        this.statistik.neuerZug(ArrOps.arrayCopy(brett));
        
        // Beste Reaktionsreihenfolge suchen, wenn noetig
        // TODO Regeln implementieren
        if (this.bankRegel || this.fuenferRegel)
            ;
        
        // Phantompunkt fuer Datenstruktur
        this.aenderung = new ReaktorPoint(chip[0], null);
        this.aenderung.change = 0;
        
        // Punkte der Reihe nach einfuegen
        for (Point point : chip)
            this.aenderung.add(new ReaktorPoint(point, this.aenderung));
        LinkedList<ReaktorPoint> stapel = new LinkedList<ReaktorPoint>();
        
        if (bf)
            return this.reaktionBF(stapel, inBank);
        
        return this.reaktionDF(stapel, inBank);
    }
    
    /**
     * Berechnet die Kettenreaktion einer Eingabe nach dem
     * Breitensuchealgorithmus, speichert/ueberschreibt Anzahl der Reaktionen,
     * Anzahl der gewonnenen Chips und Anzahl der Chips aus der Bank.
     * 
     * @param stapel
     *            Liste mit abzuarbeitenden Punkten
     * @return resultierendes Spielfeld
     */
    private byte[][] reaktionBF(LinkedList<ReaktorPoint> stapel, int inBank) {
        // Initialsteine in Stapel eintragen
        for (ReaktorPoint reaktorPoint : this.aenderung.getFolgende())
            stapel.offer(reaktorPoint);
        while (!stapel.isEmpty()) {
            ReaktorPoint p = stapel.poll();
            
            // Ueberpruft, ob eine Reaktion stattfindet
            if (p.getValue() >= this.mod.getChipsFuerReaktion()) {
                this.statistik.reagiere(p);
                
                Point[] nachbarn = this.mod.getNachbarn(p);
                
                // Erhoeht den Zaehler um 1
                this.reaktionen++;
                
                // Ueberprueft, ob wir in einer Ecke sind fuer Extrachips
                int tmpGewinn = this.countChipsGoOut(nachbarn);
                
                /*
                 * Reduziert die Chips der Bank um 1 wenn die Reaktion nicht am
                 * Spielfeldrand stattfand
                 */
                if (tmpGewinn > 0)
                    this.gewinn += tmpGewinn;
                else
                    this.ausBank++;
                
                /*
                 * Die Chips abziehen und den Folgepunkt mit weniger Chips
                 * speichern
                 */
                ReaktorPoint tmp = p.reagiere();
                this.neuesBrett[p.x][p.y] -= this.mod.getChipsFuerReaktion();
                stapel.addAll(p.getFolgende());
                stapel.remove(tmp);
                
            } else {
                
                // Legt einen Chip auf das Feld
                this.neuesBrett[p.x][p.y]++;
                
                /*
                 * Sonderbedingungen: Nur Stapel aendern, wenn keine
                 * Sonderbedingung gewaehlt.
                 */
                // Wenn Reaktionausloeser wieder zur Ueberpruefung anmelden
                if ((p.getValue() >= this.mod.getChipsFuerReaktion()))
                    stapel.offer(p);
            }
        }
        return this.neuesBrett;
    }
    
    /**
     * Berechnet die Kettenreaktion einer Eingabe nach dem
     * Tiefensuchealgorithmus, speichert/ueberschreibt Anzahl der Reaktionen,
     * Anzahl der gewonnenen Chips und Anzahl der Chips aus der Bank.
     * 
     * @param stapel
     *            Liste mit abzuarbeitenden Chips
     * @return resultierendes Spielfeld
     */
    private byte[][] reaktionDF(LinkedList<ReaktorPoint> stapel, int inBank) {
        for (ReaktorPoint reaktorPoint : this.aenderung.getFolgende())
            stapel.push(reaktorPoint);
        while (!stapel.isEmpty()) {
            
            ReaktorPoint p = stapel.pop();
            
            // Ueberpruft, ob eine Reaktion stattfindet
            if (p.getValue() >= this.mod.getChipsFuerReaktion()) {
                this.statistik.reagiere(p);
                
                Point[] nachbarn = this.mod.getNachbarn(p);
                
                // Erhoeht den Zaehler um 1
                this.reaktionen++;
                
                // Ueberprueft, ob wir in einer Ecke sind fuer Extrachips
                int tmpGewinn = this.countChipsGoOut(nachbarn);
                
                /*
                 * Reduziert die Chips der Bank um 1 wenn die Reaktion nicht am
                 * Spielfeldrand stattfand
                 */
                if (tmpGewinn > 0)
                    this.gewinn += tmpGewinn;
                else
                    this.ausBank++;
                
                /*
                 * Die Chips abziehen und den Folgepunkt mit weniger Chips
                 * speichern
                 */
                this.neuesBrett[p.x][p.y] -= this.mod.getChipsFuerReaktion();
                ReaktorPoint tmp = p.reagiere();
                for (ReaktorPoint reaktorPoint : p.getFolgende())
                    stapel.push(reaktorPoint);
                stapel.remove(tmp);
                
            } else {
                
                // Wert um einen Chip erhoehen
                this.neuesBrett[p.x][p.y]++;
                
                // Wenn Reaktionsausloeser wieder zur Ueberpruefung anmelden
                if ((p.getValue() >= this.mod.getChipsFuerReaktion()))
                    stapel.push(p);
            }
        }
        return this.neuesBrett;
    }
    
    /**
     * Berechnet die Kettenreaktion einer Eingabe, speichert/ueberschreibt
     * Anzahl der Reaktionen, Anzahl der gewonnenen Chips und Anzahl der Chips
     * aus der Bank. Besonders schnell, da keine unnoetigen Aufzeichnungen
     * gemacht werden.
     * 
     * @param brett
     *            Spielbrett - bleibt unveraendert, da es kopiert wird
     * @param chip
     *            Spielstein
     * @return resultierendes Spielfeld
     */
    public byte[][] reaktionKI(byte[][] brett, Point chip) {
        // Initialisiert die Variablen
        // TODO Bestway integrieren
        this.gewinn = this.reaktionen = this.ausBank = 0;
        this.neuesBrett = ArrOps.arrayCopy(brett);
        LinkedList<Point> stapel = new LinkedList<Point>();
        stapel.offer(chip);
        
        while (!stapel.isEmpty()) {
            
            Point p = stapel.poll();
            
            // Ueberprueft, ob es im Feld liegt
            if (this.isInFeld(p))
                // Ueberpruft, ob eine Reaktion stattfindet
                if (this.neuesBrett[p.x][p.y] >= this.mod
                        .getChipsFuerReaktion()) {
                    Point[] nachbarn = this.mod.getNachbarn(p);
                    
                    // Erhoeht den Zaehler um 1
                    this.reaktionen++;
                    
                    // Ueberprueft, ob wir in einer Ecke sind fuer Extrachips
                    int tmp = this.countChipsGoOut(nachbarn);
                    
                    /*
                     * Reduziert die Chips der Bank um 1 wenn die Reaktion nicht
                     * am Spielfeldrand stattfand
                     */
                    if (tmp > 0)
                        this.gewinn += tmp;
                    else
                        this.ausBank++;
                    
                    // Erstellt die Chips, welche Reagieren
                    for (Point element : nachbarn)
                        stapel.offer(new Point(element.x, element.y));
                    
                    this.neuesBrett[p.x][p.y] += -this.mod
                            .getChipsFuerReaktion();
                    
                } else {
                    
                    // Wenn Reaktionsausloeser zur Ueberpruefung einreihen
                    if (this.neuesBrett[p.x][p.y] == (this.mod
                            .getChipsFuerReaktion() - 1))
                        stapel.offer(p);
                    
                    // Wert um einen Chip erhoehen
                    this.neuesBrett[p.x][p.y] += 1;
                }
        }
        return this.neuesBrett;
    }
    
    /**
     * Setzt die Sonderregeln des Spieles
     * 
     * @param fiveRule
     *            5er-Regel
     * @param bankRule
     *            Bank-Regel
     */
    public void setMode(boolean fiveRule, boolean bankRule) {
        this.fuenferRegel = fiveRule;
        this.bankRegel = bankRule;
    }
    
}
