package de.vdwals.kettenreaktion.optionen.gui;

import javax.swing.JPanel;

import de.vdwals.kettenreaktion.gui.Einstellungen;

/**
 * Oberklasse fuer alle Optionsfenster.
 * 
 * @author Dennis van der Wals
 * 
 */
public abstract class OptionGUI extends JPanel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    protected Einstellungen   mainGUI;
    
    public OptionGUI(Einstellungen mainGUI) {
        this.mainGUI = mainGUI;
    }
    
    abstract public void build();
    
    @Override
    abstract public void repaint();
    
    abstract public void save();
}
