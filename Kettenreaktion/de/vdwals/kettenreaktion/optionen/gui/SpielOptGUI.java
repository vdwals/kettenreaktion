package de.vdwals.kettenreaktion.optionen.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.regex.Pattern;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.vdwals.kettenreaktion.gui.Einstellungen;
import de.vdwals.kettenreaktion.optionen.SpielOptionen;

/**
 * Klasse zur Anzeige der Spieloptionen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class SpielOptGUI extends OptionGUI implements ChangeListener {
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = -3540094846558630703L;
    
    private SpielOptionen     optionen;
    
    // Komponenten
    private JCheckBox         chipsLimit, wuerfelnCheckBox, fiveRule, bankRule;
    private JTextField        maxChipsToSet, bankChips, startChips,
            wuerfelnVon, wuerfelnBis;
    private JComboBox<?>         mapModusSelection, modSelection;
    
    public SpielOptGUI(Einstellungen mainGUI, SpielOptionen optionen) {
        super(mainGUI);
        this.optionen = optionen;
        this.build();
    }
    
    @Override
    public void build() {
        this.setLayout(new BorderLayout(5, 5));
        
        JPanel mainPanel = new JPanel(new GridLayout(8, 2, 5, 5));
        JPanel wuerfelPanel = new JPanel(new GridLayout(1, 4, 5, 5));
        
        wuerfelPanel.add(new JLabel("Von: "));
        wuerfelPanel.add(this.getWuerfelnVon());
        wuerfelPanel.add(new JLabel(" bis: "));
        wuerfelPanel.add(this.getWuerfelnBis());
        
        mainPanel.add(new JLabel("Spielmod:"));
        mainPanel.add(this.getModSelection());
        mainPanel.add(new JLabel("Maximale Chips:"));
        mainPanel.add(this.getMaxChipsToSet());
        mainPanel.add(new JLabel("Startchips:"));
        mainPanel.add(this.getStartChips());
        mainPanel.add(new JLabel("Bankchips:"));
        mainPanel.add(this.getBankChips());
        mainPanel.add(new JLabel("Spielbrettf�llmodus:"));
        mainPanel.add(this.getMapModusSelection());
        mainPanel.add(this.getFiveRule());
        mainPanel.add(this.getBankRule());
        mainPanel.add(this.getChipsLimit());
        mainPanel.add(this.getWuerfelnCheckBox());
        
        this.add(mainPanel, BorderLayout.CENTER);
        this.add(wuerfelPanel, BorderLayout.SOUTH);
    }
    
    /**
     * Gibt ein Textfeld zur Eingabe der Chips in der Bank zurueck.
     * 
     * @return Textfeld zur Eingabe der Chips in der Bank
     */
    private JTextField getBankChips() {
        if (this.bankChips == null) {
            this.bankChips = new JTextField();
            if (this.optionen != null)
                this.bankChips.setText("" + this.optionen.getBankChips());
            this.bankChips
                    .setToolTipText("Legt fest, wie viele Chips in der Bank liegen");
            this.bankChips.addCaretListener(this.mainGUI);
        }
        return this.bankChips;
    }
    
    /**
     * @return
     */
    private JCheckBox getBankRule() {
        if (this.bankRule == null) {
            this.bankRule = new JCheckBox("Bank-Regel");
            this.bankRule
                    .setToolTipText("<html>Sollen die Reaktionen w�hrend des Zuges anhalten,<br>wenn die Bank leer ist</html>");
            this.bankRule.addChangeListener(this.mainGUI);
            if (this.optionen != null)
                this.bankRule.setSelected(this.optionen.isBankRule());
        }
        return this.bankRule;
    }
    
    /**
     * Gibt eine Auswahlbox fuer ein Chiplimit zurueck.
     * 
     * @return Auswahlbox fuer ein Chiplimit
     */
    private JCheckBox getChipsLimit() {
        if (this.chipsLimit == null) {
            this.chipsLimit = new JCheckBox("Kein Chiplimit");
            this.chipsLimit
                    .setToolTipText("<html>Bestimmt, ob Spieler immer die maximale<br>Anzahl an Chips legen darf</html>");
            this.chipsLimit.addChangeListener(this.mainGUI);
            if (this.optionen != null)
                this.chipsLimit.setSelected(this.optionen.isChipsLimit());
        }
        return this.chipsLimit;
    }
    
    /**
     * @return
     */
    private JCheckBox getFiveRule() {
        if (this.fiveRule == null) {
            this.fiveRule = new JCheckBox("5er-Regel");
            this.fiveRule
                    .setToolTipText("<html>Geben Stapel mit mehr Chips als f�r die Reaktion ben�tigt<br>mehr Chips, wenn sie reagieren</html>");
            this.fiveRule.addChangeListener(this.mainGUI);
            if (this.optionen != null)
                this.fiveRule.setSelected(this.optionen.isFiveRule());
        }
        return this.fiveRule;
    }
    
    /**
     * @return the mapModusSelection
     */
    private JComboBox<?> getMapModusSelection() {
        if (this.mapModusSelection == null) {
            String[] modi = { "Standard", "Zuf�llig" };
            this.mapModusSelection = new JComboBox<Object>(modi);
            this.mapModusSelection
                    .setToolTipText("Auswahl des Spielbrettauff�llmodus");
            this.mapModusSelection.addActionListener(this.mainGUI);
            if (this.optionen != null)
                this.mapModusSelection.setSelectedIndex(this.optionen
                        .getMapModus());
        }
        return this.mapModusSelection;
    }
    
    /**
     * Gibt eine Textfeld zur Eingabe der maximal legbaren Chips pro Runde
     * zurueck.
     * 
     * @return Textfeld zur Eingabe der maximal legbaren Chips
     */
    private JTextField getMaxChipsToSet() {
        if (this.maxChipsToSet == null) {
            this.maxChipsToSet = new JTextField(""
                    + this.optionen.getMaxChipsToSet());
            this.maxChipsToSet
                    .setToolTipText("Legt fest, wie viele Chips maximal gelegt werden d\u00FCrfen");
            this.maxChipsToSet.addCaretListener(this.mainGUI);
            if (this.optionen != null)
                this.maxChipsToSet.setEnabled(!this.optionen.isWuerfeln());
        }
        return this.maxChipsToSet;
    }
    
    /**
     * Erzeugt die Auswahlbox fuer die Spielmodi.
     * 
     * @return Auswahlbox fuer die Spielmodi
     */
    private JComboBox<?> getModSelection() {
        if (this.modSelection == null) {
            String[] modi = new String[this.optionen.getMods().length];
            for (int i = 0; i < modi.length; i++)
                modi[i] = this.optionen.getMods()[i].toString();
            this.modSelection = new JComboBox<Object>(modi);
            this.modSelection.setToolTipText("Auswahl der Spielmodifikation");
            this.modSelection.addActionListener(this.mainGUI);
            if (this.optionen != null)
                this.modSelection.setSelectedIndex(this.optionen.getModIndex());
        }
        return this.modSelection;
    }
    
    /**
     * Gibt ein Textfeld zur Eingabe der Startchipanzahl zurueck.
     * 
     * @return Textfeld zur Eingabe der Startchipanzahl
     */
    private JTextField getStartChips() {
        if (this.startChips == null) {
            this.startChips = new JTextField();
            if (this.optionen != null)
                this.startChips.setText("" + this.optionen.getStartChips());
            this.startChips
                    .setToolTipText("<html>Legt fest, wie viele Chips den Spielern beim<br>Spielstart zur Verf\u00FCgung stehen</html>");
            this.startChips.addCaretListener(this.mainGUI);
        }
        return this.startChips;
    }
    
    /**
     * @return the wuerfelnBis
     */
    private JTextField getWuerfelnBis() {
        if (this.wuerfelnBis == null) {
            this.wuerfelnBis = new JTextField(""
                    + this.optionen.getWuerfelEnd());
            this.wuerfelnBis.setToolTipText("Maximale zu w�rfelnde Zahl");
            this.wuerfelnBis.addCaretListener(this.mainGUI);
            if (this.optionen != null)
                this.wuerfelnBis.setEnabled(this.optionen.isWuerfeln());
        }
        return this.wuerfelnBis;
    }
    
    /**
     * Gibt eine Auswahlbox fuer die 5er-Regel zurueck.
     * 
     * @return Auswahlbox fuer die 5er-Regel
     */
    private JCheckBox getWuerfelnCheckBox() {
        if (this.wuerfelnCheckBox == null) {
            this.wuerfelnCheckBox = new JCheckBox("W�rfeln");
            this.wuerfelnCheckBox
                    .setToolTipText("Aktivieren um die zu maximale zu setzende Chipzahl zu erw�rfeln");
            this.wuerfelnCheckBox.addChangeListener(this.mainGUI);
            this.wuerfelnCheckBox.addChangeListener(this);
            if (this.optionen != null)
                this.wuerfelnCheckBox.setSelected(this.optionen.isWuerfeln());
        }
        return this.wuerfelnCheckBox;
    }
    
    /**
     * @return the wuerfelnVon
     */
    private JTextField getWuerfelnVon() {
        if (this.wuerfelnVon == null) {
            this.wuerfelnVon = new JTextField(""
                    + this.optionen.getWuerfelStart());
            this.wuerfelnVon.setToolTipText("Minimale zu w�rfelnde Zahl");
            this.wuerfelnVon.addCaretListener(this.mainGUI);
            if (this.optionen != null)
                this.wuerfelnVon.setEnabled(this.optionen.isWuerfeln());
        }
        return this.wuerfelnVon;
    }
    
    @Override
    public void repaint() {
        if (this.optionen != null) {
            this.getModSelection()
                    .setSelectedIndex(this.optionen.getModIndex());
            this.getMapModusSelection().setSelectedIndex(
                    this.optionen.getMapModus());
            this.getWuerfelnCheckBox().setSelected(this.optionen.isWuerfeln());
            this.getFiveRule().setSelected(this.optionen.isFiveRule());
            this.getChipsLimit().setSelected(this.optionen.isChipsLimit());
            this.getBankRule().setSelected(this.optionen.isBankRule());
            this.getBankChips().setText("" + this.optionen.getBankChips());
            this.getWuerfelnVon().setText("" + this.optionen.getWuerfelStart());
            this.getMaxChipsToSet().setText(
                    "" + this.optionen.getMaxChipsToSet());
            this.getStartChips().setText("" + this.optionen.getStartChips());
            this.getWuerfelnBis().setText("" + this.optionen.getWuerfelEnd());
            this.getWuerfelnBis().setEnabled(this.optionen.isWuerfeln());
            this.getWuerfelnVon().setEnabled(this.optionen.isWuerfeln());
        }
    }
    
    @Override
    public void save() {
        // Modaenderung durchfuehren
        if (this.optionen.getModIndex() != this.getModSelection()
                .getSelectedIndex())
            this.optionen.setModIndex((byte) this.getModSelection()
                    .getSelectedIndex());
        
        this.optionen.setFiveRule(this.getFiveRule().isSelected());
        this.optionen.setBankRule(this.getBankRule().isSelected());
        
        // Sonderregeln setzen
        this.optionen.getReaktor().setMode(this.optionen.isFiveRule(),
                this.optionen.isBankRule());
        
        // Alle Daten auslesen
        if (Pattern.matches("\\d+", this.getMaxChipsToSet().getText()))
            this.optionen.setMaxChipsToSet((byte) Integer.parseInt(this
                    .getMaxChipsToSet().getText()));
        
        if (Pattern.matches("\\d+", this.getBankChips().getText()))
            this.optionen.setBankChips(Integer.parseInt(this.getBankChips()
                    .getText()));
        
        if (Pattern.matches("\\d+", this.getStartChips().getText()))
            this.optionen.setStartChips((byte) Integer.parseInt(this
                    .getStartChips().getText()));
        
        if (Pattern.matches("\\d+", this.getWuerfelnVon().getText()))
            this.optionen.setWuerfelStart((byte) Integer.parseInt(this
                    .getWuerfelnVon().getText()));
        
        if (Pattern.matches("\\d+", this.getWuerfelnBis().getText()))
            this.optionen.setWuerfelEnd((byte) Integer.parseInt(this
                    .getWuerfelnBis().getText()));
        
        // Checkbuttonauswahl an Variablen uebertragen
        this.optionen.setWuerfeln(this.getWuerfelnCheckBox().isSelected());
        this.optionen.setMapModus((byte) this.getMapModusSelection()
                .getSelectedIndex());
        this.optionen.setFiveRule(this.getFiveRule().isSelected());
        this.optionen.setBankRule(this.getBankRule().isSelected());
    }
    
    @Override
    public void stateChanged(ChangeEvent arg0) {
        this.optionen.setWuerfeln(this.getWuerfelnCheckBox().isSelected());
        this.getWuerfelnBis().setEnabled(this.optionen.isWuerfeln());
        this.getWuerfelnVon().setEnabled(this.optionen.isWuerfeln());
        this.repaint();
    }
    
}
