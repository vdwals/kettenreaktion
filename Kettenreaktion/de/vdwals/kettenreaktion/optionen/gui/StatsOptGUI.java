package de.vdwals.kettenreaktion.optionen.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.vdwals.kettenreaktion.gui.Einstellungen;
import de.vdwals.kettenreaktion.optionen.StatistikOptionen;

public class StatsOptGUI extends OptionGUI implements ChangeListener {
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = 1L;
    
    // Komponenten
    private JCheckBox         livePanel;
    
    private StatistikOptionen optionen;
    
    public StatsOptGUI(Einstellungen mainGUI, StatistikOptionen optionen) {
        super(mainGUI);
        this.optionen = optionen;
        this.build();
    }
    
    @Override
    public void build() {
        this.setLayout(new BorderLayout(5, 5));
        
        JPanel north = new JPanel(new BorderLayout(5, 5));
        JPanel checkBox = new JPanel(new GridLayout(1, 2, 5, 5));
        
        checkBox.add(new JLabel("Statistikgraph w�hrend Spiel anzeigen:"));
        checkBox.add(this.getLivePanel());
        
        north.add(checkBox, BorderLayout.NORTH);
        
        this.add(north, BorderLayout.WEST);
    }
    
    private JCheckBox getLivePanel() {
        if (this.livePanel == null) {
            this.livePanel = new JCheckBox();
            this.livePanel.addChangeListener(this);
            if (this.optionen != null)
                this.livePanel.setSelected(this.optionen.isLivePanel());
        }
        return this.livePanel;
    }
    
    @Override
    public void repaint() {
        if (this.optionen != null)
            this.getLivePanel().setSelected(this.optionen.isLivePanel());
    }
    
    @Override
    public void save() {
        this.optionen.setLivePanel(this.getLivePanel().isSelected());
    }
    
    @Override
    public void stateChanged(ChangeEvent arg0) {
        this.optionen.setLivePanel(this.getLivePanel().isSelected());
    }
    
}
