package de.vdwals.kettenreaktion.optionen.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import de.vdwals.kettenreaktion.gui.Einstellungen;
import de.vdwals.kettenreaktion.optionen.GrafikOption;

/**
 * Klasse zum Anzeigen der Anzeigeoptionen
 * 
 * @author Dennis van der Wals
 * 
 */
public class GrafikOptGUI extends OptionGUI implements ChangeListener,
        ActionListener, PopupMenuListener {
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = 1452010700469916160L;
    
    // Komponenten
    private JCheckBox         showAnimation;
    private JRadioButton[]    auswahl;
    private ButtonGroup       bg;
    private JSlider           speed;
    private JLabel            speedLabel;
    private JButton           aktuelleFarbe;
    private JComboBox<String> farbwaehler;
    
    private GrafikOption      optionen;
    
    /**
     * Standardkonstruktor
     * 
     * @param mainGUI
     *            Hauptfenster
     * @param optionen
     *            Animationsoptionen
     */
    public GrafikOptGUI(Einstellungen mainGUI, GrafikOption optionen) {
        super(mainGUI);
        this.optionen = optionen;
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getSource() == this.getAktuelleFarbe()) {
            this.optionen.getFarben()[this.getFarbwaehler().getSelectedIndex()] = JColorChooser
                    .showDialog(null, "Farbwahl",
                            this.optionen.getFarben()[this.getFarbwaehler()
                                    .getSelectedIndex()]);
            this.getAktuelleFarbe().setBackground(
                    this.optionen.getFarben()[this.getFarbwaehler()
                            .getSelectedIndex()]);
        }
    }
    
    @Override
    public void build() {
        this.setLayout(new BorderLayout(5, 5));
        
        JPanel west = new JPanel(new BorderLayout(5, 5));
        
        JPanel hauptPanel = new JPanel(new GridLayout(4, 1, 5, 5));
        JPanel radioButtons = new JPanel(new GridLayout(1, 3, 5, 5));
        for (int i = 0; i < this.getAuswahl().length; i++) {
            radioButtons.add(this.getAuswahl()[i]);
            this.bg.add(this.getAuswahl()[i]);
        }
        
        JPanel radioButtonsWest = new JPanel(new BorderLayout(5, 5));
        radioButtonsWest.add(radioButtons, BorderLayout.WEST);
        
        hauptPanel.add(this.getShowAnimation());
        hauptPanel.add(radioButtonsWest);
        hauptPanel.add(this.getSpeedLabel());
        hauptPanel.add(this.getSpeed());
        
        JPanel farbPanel = new JPanel(new GridLayout(0, 2, 5, 5));
        farbPanel.add(this.getFarbwaehler());
        farbPanel.add(this.getAktuelleFarbe());
        
        farbPanel.setBorder(new TitledBorder(
                BorderFactory.createEtchedBorder(), "Farbe"));
        hauptPanel.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), "Animation"));
        
        west.add(hauptPanel, BorderLayout.CENTER);
        west.add(farbPanel, BorderLayout.SOUTH);
        
        this.add(west, BorderLayout.NORTH);
    }
    
    private String[] farbnamen() {
        String[] farbnamen = new String[this.optionen.getFarben().length];
        farbnamen[0] = "Hintergrund";
        farbnamen[1] = "Pin 1";
        farbnamen[2] = "Pin 2";
        farbnamen[3] = "Kein Feld";
        for (int i = 0; i < (farbnamen.length - 4); i++)
            farbnamen[4 + i] = "Chip " + (i + 1);
        
        return farbnamen;
    }
    
    /**
     * @return the aktuelleFarbe
     */
    private JButton getAktuelleFarbe() {
        if (this.aktuelleFarbe == null) {
            this.aktuelleFarbe = new JButton();
            this.aktuelleFarbe.setBackground(this.optionen.getFarbe(this
                    .getFarbwaehler().getSelectedIndex()));
        }
        return this.aktuelleFarbe;
    }
    
    /**
     * Gibt zurueck, welche Animation ausgewaehlt wurde.
     * 
     * @return Index der gewaehlten Animation
     */
    private int getAnimationSelection() {
        for (int i = 0; i < this.auswahl.length; i++)
            if (this.auswahl[i].isSelected())
                return i;
        return -1;
    }
    
    /**
     * Gibt die Animationsauswahlbuttons zurueck und erstellt eine Buttongroup.
     * Fuegt die Buttons jedoch noch nicht der Buttongroup hinzu.
     * 
     * @return JRadioButton Array mit eintraegen fuer jede Animation
     */
    private JRadioButton[] getAuswahl() {
        if (this.auswahl == null) {
            this.auswahl = new JRadioButton[3];
            this.auswahl[0] = new JRadioButton("Depth");
            this.auswahl[1] = new JRadioButton("Breadth");
            this.auswahl[2] = new JRadioButton("Wave");
            this.bg = new ButtonGroup();
            for (JRadioButton element : this.auswahl)
                element.addChangeListener(this);
            if (this.optionen != null)
                this.auswahl[this.optionen.getAnimation()].setSelected(true);
        }
        return this.auswahl;
    }
    
    /**
     * @return the farbwaehler
     */
    private JComboBox<String> getFarbwaehler() {
        if (this.farbwaehler == null) {
            this.farbwaehler = new JComboBox<String>(this.farbnamen());
            this.farbwaehler.setToolTipText("Zu �ndernde Farbe w�hlen.");
            this.farbwaehler.addActionListener(this);
            this.farbwaehler.addPopupMenuListener(this);
        }
        return this.farbwaehler;
    }
    
    /**
     * Gibt eine Auswahlbox zum zeigen der Animation zurueck.
     * 
     * @return Auswahlbox zum zeigen der Animation
     */
    private JCheckBox getShowAnimation() {
        if (this.showAnimation == null) {
            this.showAnimation = new JCheckBox("Zeige Animation");
            this.showAnimation
                    .setToolTipText("Bestimmt, ob eine Animation angezeigt werden soll");
            this.showAnimation.addChangeListener(this.mainGUI);
            if (this.optionen != null)
                this.showAnimation.setSelected(this.optionen.isShowA());
        }
        return this.showAnimation;
    }
    
    /**
     * Gibt einen Animationsgeschwindigkeitsregler zurueck.
     * 
     * @return Animationsgeschwindigkeitsregler
     */
    private JSlider getSpeed() {
        if (this.speed == null) {
            this.speed = new JSlider(0, 20, 5);
            this.speed.setPaintTicks(true);
            this.speed.setMinorTickSpacing(1);
            this.speed.setMajorTickSpacing(5);
            this.speed.addChangeListener(this);
            if (this.optionen != null)
                this.speed
                        .setValue((int) ((double) this.optionen.getSleep() / 1000));
        }
        return this.speed;
    }
    
    /**
     * Gibt ein Label fuer die Animationsgeschwindigkeit zurueck.
     * 
     * @return Label fuer die Animationsgeschwindigkeit
     */
    private JLabel getSpeedLabel() {
        if (this.speedLabel == null) {
            this.speedLabel = new JLabel();
            if (this.optionen != null)
                this.speedLabel.setText("Geschwindigkeit: "
                        + ((double) this.optionen.getSleep() / 1000) + " s");
        }
        return this.speedLabel;
    }
    
    @Override
    public void popupMenuCanceled(PopupMenuEvent arg0) {
    }
    
    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent arg0) {
        this.getAktuelleFarbe().setBackground(
                this.optionen.getFarben()[this.getFarbwaehler()
                        .getSelectedIndex()]);
        this.paintImmediately(this.getFarbwaehler().getBounds());
    }
    
    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent arg0) {
    }
    
    @Override
    public void repaint() {
        if (this.optionen != null) {
            // Buttons neu einfaerben
            this.getShowAnimation().setSelected(this.optionen.isShowA());
            this.getAuswahl()[this.optionen.getAnimation()].setSelected(true);
            this.getSpeed().setValue(this.optionen.getSleep() / 100);
            this.getAktuelleFarbe()
                    .setBackground(
                            this.optionen.getFarbe(this.farbwaehler
                                    .getSelectedIndex()));
        }
    }
    
    @Override
    public void save() {
        this.optionen.setShowA(this.getShowAnimation().isSelected());
        this.optionen.setAnimation((byte) this.getAnimationSelection());
        this.optionen.setSleep(this.getSpeed().getValue() * 100);
    }
    
    @Override
    public void stateChanged(ChangeEvent e) {
        
        if (e.getSource() == this.getSpeed()) {
            this.getSpeedLabel()
                    .setText(
                            "Geschwindigkeit:"
                                    + ((double) this.getSpeed().getValue() / 10)
                                    + " s");
            this.paintImmediately(this.getSpeedLabel().getBounds());
        } else
            for (byte i = 0; i < this.getAuswahl().length; i++)
                if (e.getSource() == this.getAuswahl()[i])
                    this.optionen.setAnimation(i);
    }
}
