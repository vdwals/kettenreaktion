package de.vdwals.kettenreaktion.optionen.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.vdwals.kettenreaktion.gui.Einstellungen;
import de.vdwals.kettenreaktion.gui.language.Messages;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.optionen.ProgrammOptionen;

/**
 * Klasse zur Darstellung der Programmoptionen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class ProgrammOptGUI extends OptionGUI implements ActionListener {
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = 3130343330211191695L;
    // Komponenten
    private JCheckBox         autoUpdate, debug;
    private JButton           lookForUpdate;
    private JComboBox<?>         langSelection;
    
    private ProgrammOptionen  optionen;
    
    /**
     * Standardkonstruktor.
     * 
     * @param mainGUI
     *            Hauptfenster
     * @param optionen
     *            Programmoptionen
     */
    public ProgrammOptGUI(Einstellungen mainGUI, ProgrammOptionen optionen) {
        super(mainGUI);
        this.optionen = optionen;
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
        
        // Sprache auswaehlen
        try {
            Messages.lang(this.optionen.getLang()[this.getLangSelection()
                    .getSelectedIndex()]);
        } catch (IOException e1) {
            BugReport.recordBug(e1);
        }
    }
    
    @Override
    public void build() {
        JPanel border = new JPanel(new BorderLayout(5, 5));
        JPanel panel = new JPanel(new GridLayout(3, 2, 5, 5));
        
        panel.add(this.getAutoUpdate());
        panel.add(this.getDebug());
        panel.add(this.getLookForUpdate());
        panel.add(new JLabel());
        panel.add(new JLabel("Sprache:"));
        panel.add(this.getLangSelection());
        
        border.add(panel, BorderLayout.WEST);
        
        this.setLayout(new BorderLayout(5, 5));
        this.add(border, BorderLayout.NORTH);
    }
    
    /**
     * Gibt eine Auswahlbox fuer automatische Updates zurueck.
     * 
     * @return Auswahlbox fuer automatische Updates
     */
    private JCheckBox getAutoUpdate() {
        if (this.autoUpdate == null) {
            this.autoUpdate = new JCheckBox("Automatisches Update");
            this.autoUpdate
                    .setToolTipText("Sollen Updates automatisch heruntergeladen werden");
            this.autoUpdate.addChangeListener(this.mainGUI);
            if (this.optionen != null)
                this.autoUpdate.setSelected(this.optionen.isAutoUpdate());
        }
        return this.autoUpdate;
    }
    
    /**
     * Erstellt die Debugcheckbox und gibt sie zurueck.
     * 
     * @return Debugcheckbox
     */
    private JCheckBox getDebug() {
        if (this.debug == null) {
            this.debug = new JCheckBox("Debug-Aufzeichnung");
            this.debug.addChangeListener(this.mainGUI);
            this.debug
                    .setToolTipText("Sammelt weite Informationen zum debugen.");
            if (this.optionen != null)
                this.debug.setSelected(this.optionen.isDebug());
        }
        return this.debug;
    }
    
    /**
     * Erzeugt die Auswahlbox fuer die Sprachen.
     * 
     * @return Auswahlbox fuer Sprachen
     */
    private JComboBox<?> getLangSelection() {
        if (this.langSelection == null) {
            String[] sprachen = { "Deutsch", "Englisch", "Schwedisch" };
            this.langSelection = new JComboBox<Object>(sprachen);
            this.langSelection.setToolTipText("Auswahl der Sprache");
            this.langSelection.addActionListener(this.mainGUI);
            this.langSelection.addActionListener(this);
            // TODO Sprache reaktivieren
            this.langSelection.setEnabled(false);
        }
        return this.langSelection;
    }
    
    /**
     * Gibt einen Button zum suchen nach einem Update zurueck.
     * 
     * @return Button zum suchen nach einem Update
     */
    private JButton getLookForUpdate() {
        if (this.lookForUpdate == null) {
            this.lookForUpdate = new JButton("Nach Update suchen");
            this.lookForUpdate.addActionListener(this.optionen);
            this.lookForUpdate
                    .setToolTipText("<html>Erzwingt eine Pr\u00FCfung, ob eine aktuellere<br>Version des Programms verf\u00FCgbar ist</html>");
        }
        return this.lookForUpdate;
    }
    
    @Override
    public void repaint() {
        if (this.optionen != null) {
            this.getAutoUpdate().setSelected(this.optionen.isAutoUpdate());
            this.getDebug().setSelected(this.optionen.isDebug());
        }
    }
    
    @Override
    public void save() {
        
        // TODO Reaktivieren
        // this.optionen.setLangIndex(this.getLangSelection().getSelectedIndex());
        
        // Sprache auswaehlen
        try {
            Messages.lang(this.optionen.getLanguage());
        } catch (IOException e1) {
            BugReport.recordBug(e1);
        }
        
        this.optionen.setAutoUpdate(this.getAutoUpdate().isSelected());
        this.optionen.setDebug(this.getDebug().isSelected());
    }
    
}
