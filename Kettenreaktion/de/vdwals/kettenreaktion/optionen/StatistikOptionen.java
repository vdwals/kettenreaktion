package de.vdwals.kettenreaktion.optionen;

public class StatistikOptionen extends Optionen {
    private boolean livePanel;
    
    public StatistikOptionen() {
        this.toDefault();
    }
    
    public StatistikOptionen(boolean livePanel) {
        this.livePanel = livePanel;
    }
    
    /**
     * @return the livePanel
     */
    public boolean isLivePanel() {
        return this.livePanel;
    }
    
    /**
     * @param livePanel
     *            the livePanel to set
     */
    public void setLivePanel(boolean livePanel) {
        this.livePanel = livePanel;
    }
    
    @Override
    public void toDefault() {
        this.livePanel = false;
    }
    
}
