package de.vdwals.kettenreaktion.optionen;

import java.awt.Color;

import de.vdwals.kettenreaktion.spiel.gui.pfadalgorithmen.BreadthFirst;
import de.vdwals.kettenreaktion.spiel.gui.pfadalgorithmen.DepthFirst;
import de.vdwals.kettenreaktion.spiel.gui.pfadalgorithmen.PfadSuche;
import de.vdwals.kettenreaktion.spiel.gui.pfadalgorithmen.Wave;

/**
 * Klasse zum Speichern der Anzeigeoptionen
 * 
 * @author Dennis van der Wals
 * 
 */
public class GrafikOption extends Optionen {
    private Color[]     farben;
    private int         sleep;
    private byte        animation;
    private boolean     showA;
    private PfadSuche[] pfadsuche;
    
    /**
     * Standardkonstruktor.
     */
    public GrafikOption(Color[] farben, int sleep, byte animation,
            boolean showAnimation, int maxChips) {
        this.farben = farben;
        this.sleep = sleep;
        this.animation = animation;
        this.showA = showAnimation;
        
        this.setPfadsuche(new PfadSuche[3]);
        this.getPfadsuche()[0] = new DepthFirst();
        this.getPfadsuche()[1] = new BreadthFirst();
        this.getPfadsuche()[2] = new Wave();
    }
    
    public GrafikOption(int maxChips) {
        this.setPfadsuche(new PfadSuche[3]);
        this.getPfadsuche()[0] = new DepthFirst();
        this.getPfadsuche()[1] = new BreadthFirst();
        this.getPfadsuche()[2] = new Wave();
        this.farben = new Color[maxChips];
        
        this.toDefault();
    }
    
    /**
     * @return the animation
     */
    public byte getAnimation() {
        return this.animation;
    }
    
    /**
     * Gibt die Farbe zurueck.
     * 
     * @param i
     *            0 = Hintergurnd, 1 = Pin1, 2 = Pin2, 3 = NoField, 4 = 1.
     *            Chip...
     * @return Farbe
     */
    public Color getFarbe(int i) {
        return this.getFarben()[i];
    }
    
    /**
     * @return the farben
     */
    public Color[] getFarben() {
        return this.farben;
    }
    
    /**
     * @return the pfadsuche
     */
    public PfadSuche[] getPfadsuche() {
        return this.pfadsuche;
    }
    
    /**
     * Gibt den Aenderungsbaumalgorithmus zurueck.
     * 
     * @return Aenderungsbaumalgorithmus
     */
    public PfadSuche getPfadsucheAlg() {
        return this.pfadsuche[this.animation];
    }
    
    /**
     * Gibt zurueck, wie lange die Animation nach Legen eines Chips warten soll,
     * bis zum legen des naechsten Chips.
     * 
     * @return Zeit zwischen Animationsschritten in Millisekunden
     */
    public int getSleep() {
        return this.sleep;
    }
    
    /**
     * @return the showA
     */
    public boolean isShowA() {
        return this.showA;
    }
    
    /**
     * @param animation
     *            the animation to set
     */
    public void setAnimation(byte animation) {
        this.animation = animation;
    }
    
    /**
     * @param farben
     *            the farben to set
     */
    public void setFarben(Color[] farben) {
        this.farben = farben;
    }
    
    /**
     * @param pfadsuche
     *            the pfadsuche to set
     */
    public void setPfadsuche(PfadSuche[] pfadsuche) {
        this.pfadsuche = pfadsuche;
    }
    
    /**
     * @param showA
     *            the showA to set
     */
    public void setShowA(boolean showA) {
        this.showA = showA;
    }
    
    /**
     * @param sleep
     *            the sleep to set
     */
    public void setSleep(int sleep) {
        this.sleep = sleep;
    }
    
    @Override
    public void toDefault() {
        this.setSleep(500);
        this.animation = 0;
        
        this.getFarben()[0] = new Color(51, 51, 51);
        this.getFarben()[1] = new Color(0, 0, 100);
        this.getFarben()[2] = Color.CYAN;
        this.getFarben()[3] = new Color(212, 212, 252);
        this.getFarben()[4] = new Color(237, 237, 0);
        for (int i = 5; i < this.getFarben().length; i++)
            this.getFarben()[i] = this.getFarben()[i - 1].darker();
        
        this.setShowA(true);
    }
    
    public boolean useBF() {
        return this.animation != 0;
    }
    
}
