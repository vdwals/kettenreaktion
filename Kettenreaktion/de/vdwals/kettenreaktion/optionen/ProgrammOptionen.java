package de.vdwals.kettenreaktion.optionen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import de.vdwals.kettenreaktion.gui.help.Help;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.implement.Updater;

/**
 * Klasse zum Speichern der Programmoptionen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class ProgrammOptionen extends Optionen implements ActionListener {
    private boolean  autoUpdate, debug;
    private String[] lang;
    private Updater  update;
    private Help     hilfe;
    private int      langIndex;
    
    public ProgrammOptionen() {
        this.setLang(new String[3]);
        this.getLang()[0] = "de";
        this.getLang()[1] = "en";
        this.getLang()[2] = "sv";
        
        this.update = new Updater();
        try {
            this.hilfe = new Help();
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
        
        this.toDefault();
    }
    
    /**
     * Standardkonstruktor.
     */
    public ProgrammOptionen(boolean autoUpdate, boolean debug) {
        this.setLang(new String[3]);
        this.getLang()[0] = "de";
        this.getLang()[1] = "en";
        this.getLang()[2] = "sv";
        
        this.autoUpdate = autoUpdate;
        this.debug = debug;
        
        // DEBUG
        this.langIndex = 0;
        
        this.update = new Updater();
        try {
            this.hilfe = new Help();
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
        try {
            this.update.getUpdate();
        } catch (IOException e1) {
            BugReport.recordBug(e1);
        }
    }
    
    /**
     * @return the hilfe
     */
    public Help getHilfe() {
        return this.hilfe;
    }
    
    /**
     * @return the lang
     */
    public String[] getLang() {
        return this.lang;
    }
    
    /**
     * @return the langIndex
     */
    public int getLangIndex() {
        return this.langIndex;
    }
    
    public String getLanguage() {
        return this.lang[this.langIndex];
    }
    
    /**
     * @return the update
     */
    public Updater getUpdate() {
        return this.update;
    }
    
    /**
     * @return the autoUpdate
     */
    public boolean isAutoUpdate() {
        return this.autoUpdate;
    }
    
    /**
     * @return the debug
     */
    public boolean isDebug() {
        return this.debug;
    }
    
    /**
     * @param autoUpdate
     *            the autoUpdate to set
     */
    public void setAutoUpdate(boolean autoUpdate) {
        this.autoUpdate = autoUpdate;
    }
    
    /**
     * @param debug
     *            the debug to set
     */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
    
    /**
     * @param hilfe
     *            the hilfe to set
     */
    public void setHilfe(Help hilfe) {
        this.hilfe = hilfe;
    }
    
    /**
     * @param lang
     *            the lang to set
     */
    public void setLang(String[] lang) {
        this.lang = lang;
    }
    
    /**
     * @param langIndex
     *            the langIndex to set
     */
    public void setLangIndex(int langIndex) {
        this.langIndex = langIndex;
    }
    
    /**
     * @param update
     *            the update to set
     */
    public void setUpdate(Updater update) {
        this.update = update;
    }
    
    @Override
    public void toDefault() {
        this.setAutoUpdate(true);
        this.setDebug(true);
        this.langIndex = 0;
    }
    
}
