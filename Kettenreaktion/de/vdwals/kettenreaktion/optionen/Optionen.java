package de.vdwals.kettenreaktion.optionen;

/**
 * Abstrakte Oberklasse fuer Optionen
 * 
 * @author Dennis van der Wals
 * 
 */
public abstract class Optionen {
    
    public abstract void toDefault();
    
}
