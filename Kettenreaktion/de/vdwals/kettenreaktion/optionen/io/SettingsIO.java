package de.vdwals.kettenreaktion.optionen.io;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JOptionPane;

import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.io.FileHandler;
import de.vdwals.kettenreaktion.optionen.GrafikOption;
import de.vdwals.kettenreaktion.optionen.Optionen;
import de.vdwals.kettenreaktion.optionen.ProgrammOptionen;
import de.vdwals.kettenreaktion.optionen.SpielOptionen;
import de.vdwals.kettenreaktion.optionen.StatistikOptionen;

/**
 * Klasse zum Speichern der Spieleinstellungen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class SettingsIO extends FileHandler {
    private final byte DB_VERSION  = 7;
    private final int  MAX_DB_SIZE = 1024;
    private File       settingsFile;
    private Optionen[] optionen;
    
    /**
     * Standardkonstruktor.
     */
    public SettingsIO() {
        super();
        File ort = new File("data");
        ort.mkdir();
        this.fileExtension = ".ini";
        this.settingsFile = new File("data/settings" + this.fileExtension);
        try {
            this.settingsFile.createNewFile();
        } catch (IOException e) {
            BugReport.recordBug(e);
            JOptionPane.showMessageDialog(null,
                    "Einstellungen konnten nicht angelegt werden.");
        }
    }
    
    @Override
    public byte[] generateData() {
        byte[] output = new byte[this.MAX_DB_SIZE];
        byte counter = 0;
        output[counter++] = this.DB_VERSION;
        
        // Programmblock
        boolean[] bits = new boolean[this.MAX_BIT];
        byte counter4boolean = 0;
        bits[counter4boolean++] = ((ProgrammOptionen) this.optionen[0])
                .isAutoUpdate();
        bits[counter4boolean++] = ((ProgrammOptionen) this.optionen[0])
                .isDebug();
        output[counter++] = ArrOps.toByte(bits);
        
        // Spielblock
        output[counter++] = ((SpielOptionen) this.optionen[1]).getStartChips();
        output[counter++] = ((SpielOptionen) this.optionen[1])
                .getMaxChipsToSet();
        byte[] tmp = ArrOps.IntToByteArray(((SpielOptionen) this.optionen[1])
                .getBankChips());
        for (int i = 0; i < this.INTEGER_LAENGE; i++)
            output[counter++] = tmp[i];
        output[counter++] = ((SpielOptionen) this.optionen[1])
                .getWuerfelStart();
        output[counter++] = ((SpielOptionen) this.optionen[1]).getWuerfelEnd();
        bits = new boolean[this.MAX_BIT];
        counter4boolean = 0;
        bits[counter4boolean++] = ((SpielOptionen) this.optionen[1])
                .isWuerfeln();
        bits[counter4boolean++] = ((SpielOptionen) this.optionen[1])
                .isChipsLimit();
        bits[counter4boolean++] = ((SpielOptionen) this.optionen[1])
                .isFiveRule();
        bits[counter4boolean++] = ((SpielOptionen) this.optionen[1])
                .isBankRule();
        boolean[] modusBits = ArrOps.toBits(
                ((SpielOptionen) this.optionen[1]).getModIndex(),
                this.BIT_BREITE);
        for (int i = 0; i < this.BIT_BREITE; i++)
            bits[counter4boolean++] = modusBits[i];
        modusBits = ArrOps.toBits(
                ((SpielOptionen) this.optionen[1]).getMapModus(),
                this.BIT_BREITE);
        for (int i = 0; i < this.BIT_BREITE; i++)
            bits[counter4boolean++] = modusBits[i];
        output[counter++] = ArrOps.toByte(bits);
        
        // Grafikblock
        tmp = ArrOps.IntToByteArray(((GrafikOption) this.optionen[2])
                .getSleep());
        for (int i = 0; i < this.INTEGER_LAENGE; i++)
            output[counter++] = tmp[i];
        bits = new boolean[this.MAX_BIT];
        counter4boolean = 0;
        modusBits = ArrOps.toBits(
                ((GrafikOption) this.optionen[2]).getAnimation(),
                this.BIT_BREITE);
        for (int i = 0; i < this.BIT_BREITE; i++)
            bits[counter4boolean++] = modusBits[i];
        bits[counter4boolean++] = ((GrafikOption) this.optionen[2]).isShowA();
        output[counter++] = ArrOps.toByte(bits);
        
        for (Color color : ((GrafikOption) this.optionen[2]).getFarben()) {
            tmp = ArrOps.IntToByteArray(color.getRGB());
            for (int i = 0; i < this.INTEGER_LAENGE; i++)
                output[counter++] = tmp[i];
        }
        return ArrOps.getPartOfArray(output, 0, counter);
    }
    
    /**
     * Enthaelt alle Spieloptionsklassen. 0:Programmoptionen; 1:Spieloptionen;
     * 2:Statistikoptionen; 3:Grafikoptionen
     * 
     * @return the optionen
     */
    public Optionen[] getOptionen() {
        return this.optionen;
    }
    
    @Override
    public void load() throws IOException {
        // Datei einlesen
        byte[] input = new byte[(int) this.settingsFile.length()];
        FileInputStream fis = new FileInputStream(this.settingsFile);
        fis.read(input);
        fis.close();
        
        // Fuer jeden eintrag eine Klasse erstellen und speichern:
        byte counter = 0;
        byte version = input[counter++];
        this.optionen = new Optionen[4];
        byte optionenCounter = 0;
        if (version == this.DB_VERSION) {
            
            // Programmblock
            boolean[] tmp = ArrOps.toBits(input[counter++], 2);
            byte counter4boolean = 0;
            boolean autoUpdate = tmp[counter4boolean++];
            boolean debug = tmp[counter4boolean++];
            this.optionen[optionenCounter++] = new ProgrammOptionen(autoUpdate,
                    debug);
            
            // Spielblock
            byte startChips = input[counter++];
            byte chipsToSet = input[counter++];
            int bankChips = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(input,
                    counter, counter += this.INTEGER_LAENGE));
            byte wuerfelStart = input[counter++];
            byte wuerfelEnd = input[counter++];
            tmp = ArrOps.toBits(input[counter++], 8);
            counter4boolean = 0;
            boolean wuerfeln = tmp[counter4boolean++];
            boolean chipLimit = tmp[counter4boolean++];
            boolean fiveRule = tmp[counter4boolean++];
            boolean bankRule = tmp[counter4boolean++];
            byte modus = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                    counter4boolean, counter4boolean += this.BIT_BREITE));
            byte mapModus = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                    counter4boolean, counter4boolean += this.BIT_BREITE));
            this.optionen[optionenCounter++] = new SpielOptionen(startChips,
                    chipsToSet, bankChips, wuerfelStart, wuerfelEnd, wuerfeln,
                    chipLimit, fiveRule, bankRule, modus, mapModus);
            
            // Statistikblock
            tmp = ArrOps.toBits(input[counter++], 1);
            counter4boolean = 0;
            boolean livePanel = tmp[counter4boolean++];
            this.optionen[optionenCounter++] = new StatistikOptionen(livePanel);
            
            // Grafikblock
            int sleep = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(input,
                    counter, counter += this.INTEGER_LAENGE));
            tmp = ArrOps.toBits(input[counter++], 8);
            counter4boolean = 0;
            byte animation = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                    counter4boolean, counter4boolean += this.BIT_BREITE));
            boolean showAnimation = tmp[counter4boolean++];
            Color[] colors = new Color[(input.length - counter)
                    / this.INTEGER_LAENGE];
            for (int i = 0; i < colors.length; i++)
                colors[i] = new Color(ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE)));
            
            int[] maxChips = new int[((SpielOptionen) this.optionen[1])
                    .getMods().length];
            for (int i = 0; i < maxChips.length; i++)
                maxChips[i] = ((SpielOptionen) this.optionen[1]).getMods()[i]
                        .getMaxChips();
            
            this.optionen[optionenCounter++] = new GrafikOption(colors, sleep,
                    animation, showAnimation,
                    4 + maxChips[ArrOps.getMax(maxChips)]);
        } else
            throw new IOException("Unbekannte DB_Version: " + version);
    }
    
    @Override
    public void save() {
        
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(this.settingsFile);
            fos.write(this.generateData());
            fos.close();
        } catch (FileNotFoundException e) {
            BugReport.recordBug(e);
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
    }
    
    /**
     * @param optionen
     *            the optionen to set
     */
    public void setOptionen(Optionen[] optionen) {
        this.optionen = optionen;
    }
    
}
