package de.vdwals.kettenreaktion.optionen;

import java.util.Random;

import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.spiel.Reaktor;
import de.vdwals.kettenreaktion.spiel.ki.Strategie;
import de.vdwals.kettenreaktion.spiel.mod.Hexagon;
import de.vdwals.kettenreaktion.spiel.mod.Mod;
import de.vdwals.kettenreaktion.spiel.mod.Quadrat;
import de.vdwals.kettenreaktion.spiel.mod.Triangel;
import de.vdwals.kettenreaktion.statistik.Statistik;

/**
 * Klasse zum Speichern der Spieloptionen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class SpielOptionen extends Optionen {
    private int         gesamtChips, bankChips, turnierPunkte[][];
    private byte        startChips, maxSpieler, maxChipsToSet,
            originalPosition[], modIndex, wuerfelStart, wuerfelEnd, mapModus;
    private boolean     random, saved, wuerfeln, fiveRule, bankRule,
            chipsLimit;
    private Random      peter;
    
    private Reaktor     reaktor;
    private Mod[]       mod;
    private Statistik   statistik;
    private String[]    namen;
    private Strategie[] ki;
    
    public SpielOptionen() {
        // Werte initialisieren
        this.namen = new String[0];
        this.ki = new Strategie[0];
        this.originalPosition = new byte[0];
        
        this.setMods(new Mod[3]);
        this.getMods()[0] = new Triangel();
        this.getMods()[1] = new Quadrat();
        this.getMods()[2] = new Hexagon();
        
        this.statistik = new Statistik();
        this.reaktor = new Reaktor(this.getMod(), this.getStatistik());
        this.maxSpieler = 4;
        
        this.toDefault();
    }
    
    /**
     * Standardkonstruktor.
     * 
     * @param startChips
     * @param chipsToSet
     * @param bankChips
     * @param wuerfelStart
     * @param wuerfelEnd
     * @param wuerfeln
     * @param chipLimit
     * @param fiveRule
     * @param bankRule
     * @param modusIndex
     * @param mapModus
     */
    public SpielOptionen(byte startChips, byte chipsToSet, int bankChips,
            byte wuerfelStart, byte wuerfelEnd, boolean wuerfeln,
            boolean chipLimit, boolean fiveRule, boolean bankRule,
            byte modusIndex, byte mapModus) {
        this.startChips = startChips;
        this.maxChipsToSet = chipsToSet;
        this.bankChips = bankChips;
        this.wuerfelStart = wuerfelStart;
        this.wuerfelEnd = wuerfelEnd;
        this.wuerfeln = wuerfeln;
        this.chipsLimit = chipLimit;
        this.fiveRule = fiveRule;
        this.bankRule = bankRule;
        this.modIndex = modusIndex;
        this.mapModus = mapModus;
        this.maxSpieler = 4;
        
        // Werte initialisieren
        this.namen = new String[0];
        this.ki = new Strategie[0];
        this.originalPosition = new byte[0];
        
        this.setMods(new Mod[3]);
        this.getMods()[0] = new Triangel();
        this.getMods()[1] = new Quadrat();
        this.getMods()[2] = new Hexagon();
        
        this.statistik = new Statistik();
        this.reaktor = new Reaktor(this.getMod(), this.getStatistik());
    }
    
    /**
     * @return the bankChips
     */
    public int getBankChips() {
        return this.bankChips;
    }
    
    /**
     * @return the gesamtChips
     */
    public int getGesamtChips() {
        return this.gesamtChips;
    }
    
    /**
     * Gibt die Karte des Spiels zurueck.
     * 
     * @return Karte des Spiels
     */
    public boolean[][] getKarte() {
        return this.getMod().getKarte();
    }
    
    /**
     * @return the ki
     */
    public Strategie[] getKi() {
        return this.ki;
    }
    
    /**
     * @return the mapModus
     */
    public byte getMapModus() {
        return this.mapModus;
    }
    
    /**
     * @return the maxChipsToSet
     */
    public byte getMaxChipsToSet() {
        return this.maxChipsToSet;
    }
    
    /**
     * @return the maxSpieler
     */
    public byte getMaxSpieler() {
        return this.maxSpieler;
    }
    
    /**
     * Gibt den aktuellen Mod zurueck.
     * 
     * @return aktueller Mod
     */
    public Mod getMod() {
        return this.getMods()[this.modIndex];
    }
    
    /**
     * @return the modus
     */
    public byte getModIndex() {
        return this.modIndex;
    }
    
    /**
     * @return the mod
     */
    public Mod[] getMods() {
        return this.mod;
    }
    
    /**
     * @return the namen
     */
    public String[] getNamen() {
        return this.namen;
    }
    
    /**
     * @return the originalPosition
     */
    public byte[] getOriginalPosition() {
        return this.originalPosition;
    }
    
    /**
     * @return the reaktor
     */
    public Reaktor getReaktor() {
        return this.reaktor;
    }
    
    /**
     * @return the startChips
     */
    public byte getStartChips() {
        return this.startChips;
    }
    
    /**
     * @return the statistik
     */
    public Statistik getStatistik() {
        return this.statistik;
    }
    
    /**
     * @return the turnierPunkte
     */
    public int[][] getTurnierPunkte() {
        return this.turnierPunkte;
    }
    
    /**
     * @return the wuerfelEnd
     */
    public byte getWuerfelEnd() {
        return this.wuerfelEnd;
    }
    
    /**
     * @return the wuerfelStart
     */
    public byte getWuerfelStart() {
        return this.wuerfelStart;
    }
    
    /**
     * @return the bankRule
     */
    public boolean isBankRule() {
        return this.bankRule;
    }
    
    /**
     * @return the chipsLimit
     */
    public boolean isChipsLimit() {
        return this.chipsLimit;
    }
    
    /**
     * @return the fiveRule
     */
    public boolean isFiveRule() {
        return this.fiveRule;
    }
    
    /**
     * @return the random
     */
    public boolean isRandom() {
        return this.random;
    }
    
    /**
     * @return the saved
     */
    public boolean isSaved() {
        return this.saved;
    }
    
    /**
     * @return the wuerfeln
     */
    public boolean isWuerfeln() {
        return this.wuerfeln;
    }
    
    /**
     * @param bankChips
     *            the bankChips to set
     */
    public void setBankChips(int bankChips) {
        this.bankChips = bankChips;
    }
    
    /**
     * @param bankRule
     *            the bankRule to set
     */
    public void setBankRule(boolean bankRule) {
        this.bankRule = bankRule;
    }
    
    /**
     * @param chipsLimit
     *            the chipsLimit to set
     */
    public void setChipsLimit(boolean chipsLimit) {
        this.chipsLimit = chipsLimit;
    }
    
    /**
     * @param fiveRule
     *            the fiveRule to set
     */
    public void setFiveRule(boolean fiveRule) {
        this.fiveRule = fiveRule;
    }
    
    /**
     * @param gesamtChips
     *            the gesamtChips to set
     */
    public void setGesamtChips(int gesamtChips) {
        this.gesamtChips = gesamtChips;
    }
    
    /**
     * Legt die Karte des Spieles fest.
     * 
     * @param karte
     *            Karte des Spiels
     */
    public void setKarte(boolean[][] karte) {
        for (Mod modus : this.getMods())
            modus.setKarte(ArrOps.arrayCopy(karte));
    }
    
    /**
     * Setzt die Spieler fest. Wenn KIs uebergeben wurden, werden ihnen
     * automatisch die Einstellungen uebertragen.
     * 
     * @param ki
     *            Liste mit KIs
     */
    public void setKi(Strategie[] ki) {
        this.ki = ki;
        this.originalPosition = new byte[ki.length];
        for (byte i = 0; i < ki.length; i++) {
            this.originalPosition[i] = i;
            if (ki[i] != null)
                ki[i].setReaktor(this.reaktor);
        }
    }
    
    /**
     * @param mapModus
     *            the mapModus to set
     */
    public void setMapModus(byte mapModus) {
        this.mapModus = mapModus;
    }
    
    /**
     * @param maxChipsToSet
     *            the maxChipsToSet to set
     */
    public void setMaxChipsToSet(byte maxChipsToSet) {
        this.maxChipsToSet = maxChipsToSet;
    }
    
    /**
     * @param maxSpieler
     *            the maxSpieler to set
     */
    public void setMaxSpieler(byte maxSpieler) {
        this.maxSpieler = maxSpieler;
    }
    
    /**
     * @param modIndex
     *            the modus to set
     */
    public void setModIndex(byte modIndex) {
        this.modIndex = modIndex;
        this.reaktor = new Reaktor(this.getMod(), this.statistik);
        // Karte resetten
        this.setKarte(this.getMod().getStandardKarte());
    }
    
    /**
     * @param mod
     *            the mod to set
     */
    public void setMods(Mod[] mod) {
        this.mod = mod;
    }
    
    /**
     * @param namen
     *            the namen to set
     */
    public void setNamen(String[] namen) {
        this.namen = namen;
    }
    
    /**
     * @param originalPosition
     *            the originalPosition to set
     */
    public void setOriginalPosition(byte[] originalPosition) {
        this.originalPosition = originalPosition;
    }
    
    /**
     * @param random
     *            the random to set
     */
    public void setRandom(boolean random) {
        this.random = random;
    }
    
    /**
     * @param reaktor
     *            the reaktor to set
     */
    public void setReaktor(Reaktor reaktor) {
        this.reaktor = reaktor;
    }
    
    /**
     * @param saved
     *            the saved to set
     */
    public void setSaved(boolean saved) {
        this.saved = saved;
    }
    
    /**
     * @param startChips
     *            the startChips to set
     */
    public void setStartChips(byte startChips) {
        this.startChips = startChips;
    }
    
    /**
     * @param statistik
     *            the statistik to set
     */
    public void setStatistik(Statistik statistik) {
        this.statistik = statistik;
    }
    
    /**
     * @param turnierPunkte
     *            the turnierPunkte to set
     */
    public void setTurnierPunkte(int[][] turnierPunkte) {
        this.turnierPunkte = turnierPunkte;
    }
    
    /**
     * @param wuerfelEnd
     *            the wuerfelEnd to set
     */
    public void setWuerfelEnd(byte wuerfelEnd) {
        this.wuerfelEnd = wuerfelEnd;
    }
    
    /**
     * @param wuerfeln
     *            the wuerfeln to set
     */
    public void setWuerfeln(boolean wuerfeln) {
        this.wuerfeln = wuerfeln;
    }
    
    /**
     * @param wuerfelStart
     *            the wuerfelStart to set
     */
    public void setWuerfelStart(byte wuerfelStart) {
        this.wuerfelStart = wuerfelStart;
    }
    
    /**
     * Mischt die Reihenfolge der KIs und merkt sich die neue Position
     */
    public void shuffle() {
        String tmpN;
        Strategie tmp;
        int rand;
        byte tmpi;
        for (int j = 0; j < 10; j++)
            for (int i = 0; i < this.ki.length; i++) {
                rand = this.peter.nextInt(this.ki.length);
                // Namen neu Ordnen
                tmpN = this.namen[i];
                this.namen[i] = this.namen[rand];
                this.namen[rand] = tmpN;
                
                // KIs neu Ordnen
                tmp = this.ki[i];
                this.ki[i] = this.ki[rand];
                this.ki[rand] = tmp;
                
                // Alte Position rand, neue Position i
                tmpi = this.originalPosition[rand];
                this.originalPosition[rand] = this.originalPosition[i];
                this.originalPosition[i] = tmpi;
            }
    }
    
    @Override
    public void toDefault() {
        this.startChips = 7;
        this.maxChipsToSet = 3;
        this.bankChips = 106;
        this.wuerfelEnd = 6;
        this.wuerfelStart = 1;
        this.modIndex = 1;
        this.mapModus = 0;
        this.reaktor = new Reaktor(this.getMod(), this.statistik);
        this.wuerfeln = false;
        this.setFiveRule(true);
        this.setBankRule(true);
        this.setChipsLimit(true);
    }
    
}
