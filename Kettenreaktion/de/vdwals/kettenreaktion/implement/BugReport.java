package de.vdwals.kettenreaktion.implement;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

/**
 * Klasse zum Aufnehmen und Abspeichern von Fehlermeldungen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class BugReport {
    private static File report = new File("stack.rep");
    
    /**
     * Speichert alle relevanten Daten zu einer Exception und versucht diese an
     * den Server zu uebermitteln.
     * 
     * @param e
     *            Exception
     */
    public static void recordBug(Exception e) {
        
        // Fehlermeldung erzeugen
        StringBuilder error = new StringBuilder();
        error.append("**************Neuer Fehler**************\n");
        error.append(new SimpleDateFormat().format(Calendar.getInstance()
                .getTime()) + "\n");
        error.append("Fehler: \t" + e.toString() + "\n");
        error.append("Nachricht: \t" + e.getMessage() + "\n");
        error.append("Ausloeser: \t" + e.getCause() + "\n");
        error.append("-------Details-------\n");
        
        // Stack hinzufuegen
        int counter = 1;
        for (StackTraceElement element : e.getStackTrace()) {
            error.append("Element: \t" + (counter++) + "\n");
            error.append("Datei: \t\t" + element.getFileName() + "\n");
            error.append("Klasse: \t" + element.getClassName() + "\n");
            error.append("Methode: \t" + element.getMethodName() + "\n");
            error.append("Zeile: \t\t" + element.getLineNumber() + "\n");
            error.append("-------\n");
        }
        
        try {
            
            // Fehlermeldung hochladen
            upload(new Scanner(error.toString()));
            
            /*
             * Wenn eine Fehlermeldungsdatei existiert und Fehler beinhaltet,
             * lade diese auch hoch
             */
            if (!report.createNewFile() && (report.length() > 0))
                upload();
            
        } catch (Exception e2) {
            
            // Wenn Server nicht erreichbar, Fehler in Datei speichern
            try {
                
                report.createNewFile();
                FileWriter fw = new FileWriter(report, true);
                fw.append(error.toString());
                fw.close();
                
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        
    }
    
    /**
     * Laedt die Fehlerdatei hoch
     * 
     * @return erfolgreicher Upload, oder nicht
     */
    private static boolean upload() {
        try {
            // Lade Datei hoch
            upload(new Scanner(report));
            
            // Loesche den Inhalt der Datei
            FileWriter fw = new FileWriter(report, false);
            fw.write("");
            fw.close();
            
            // Loesche die Datei
            report.delete();
            
            // Gib Erfolgsmeldung zurueck.
            return true;
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Uebermittelt den Dateiinhalt an den Server
     * 
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws MalformedURLException
     */
    private static void upload(Scanner scan) throws MalformedURLException,
            UnsupportedEncodingException, IOException {
        
        // Zeile fuer Zeile versenden
        while (scan.hasNextLine())
            new URL(Meta.BUG_ADD + "?app=" + Meta.APP_NAME + "&text="
                    + URLEncoder.encode(scan.nextLine(), "UTF-8")).openStream();
        scan.close();
    }
}
