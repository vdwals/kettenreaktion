package de.vdwals.kettenreaktion.implement;

public class Meta {
    public final static String  SERVER        = "http://example.com/apps/";
    public final static String  VERSIONSDATEI = "version";
    public final static String  SUB_ADD       = "kettenreaktion";
    public final static String  APP_NAME      = "Reaktor";
    public final static Version v             = new Version(1, 0, 100,
                                                      Version.RELEASE);
    public final static String  BUG_ADD       = SERVER + "bugreport/catch.php";
    public final static String  FDB_ADD       = SERVER + "feedback/catch.php";
    public final static int     UPLOAD_LIMIT  = 50;
    /**
     * Reihenfolge mit Feedbackkategorieauswahl synchron halten!
     */
    public final static String  KAT_SHORT[]   = { "k", "l", "v", "f" };
    public static final String  EMAIL_PATTERN = "^([a-zA-Z0-9])+([\\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\\.[a-zA-Z0-9_-]+)+";
    
}
