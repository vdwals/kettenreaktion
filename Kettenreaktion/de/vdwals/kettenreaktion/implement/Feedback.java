package de.vdwals.kettenreaktion.implement;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

public class Feedback extends JFrame implements ActionListener {
    private final String      KATEGORIEN[]     = { "Kritik", "Lob",
            "Vorschlag", "Fehlermeldung"      };
    private final String[]    option           = { "Ok" };
    
    // Komponenten
    private JComboBox<?>      kategorie;
    private JTextArea         message;
    private JScrollPane       messageBox;
    private JTextField        name, mail;
    private JButton           send, abort;
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = -366079006225611820L;
    
    public Feedback() {
        
        super("Feedback");
        
        // Layoutmanager setzen
        this.setLayout(new BorderLayout(5, 5));
        JPanel top = new JPanel(new GridLayout(2, 1, 5, 5));
        JPanel firstLine = new JPanel(new GridLayout(1, 2, 5, 5));
        JPanel name = new JPanel(new BorderLayout(5, 5));
        JPanel mail = new JPanel(new BorderLayout(5, 5));
        JPanel bottom = new JPanel(new BorderLayout());
        JPanel buttons = new JPanel(new GridLayout(1, 2, 5, 5));
        
        // Panels fuellen
        name.add(new JLabel("Name"), BorderLayout.WEST);
        name.add(this.getNamen(), BorderLayout.CENTER);
        
        firstLine.add(name);
        firstLine.add(this.getKategorie());
        
        mail.add(new JLabel("E-Mail"), BorderLayout.WEST);
        mail.add(this.getMail(), BorderLayout.CENTER);
        
        top.add(firstLine);
        top.add(mail);
        
        buttons.add(this.getSend());
        buttons.add(this.getAbort());
        
        bottom.add(buttons, BorderLayout.EAST);
        
        this.add(top, BorderLayout.NORTH);
        this.add(this.getMessageBox(), BorderLayout.CENTER);
        this.add(bottom, BorderLayout.SOUTH);
        
        this.pack();
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
        
        if (arg0.getSource() == this.getAbort())
            // Fenster schlie�en
            this.dispose();
        else if (arg0.getSource() == this.getSend())
            // E-Mail validieren
            if (Pattern.matches(Meta.EMAIL_PATTERN, this.getMail().getText())) {
                StringBuilder nachricht = new StringBuilder();
                
                nachricht.append("************Neuer Eintrag************\n");
                nachricht.append(new SimpleDateFormat().format(Calendar
                        .getInstance().getTime()) + "\n");
                nachricht.append("Von: " + this.getNamen().getText() + "\n");
                nachricht.append("Mail: " + this.getMail().getText() + "\n");
                
                Scanner scan = new Scanner(this.getMessage().getText());
                
                // Nachricht in kleinere Zeilen zerlegen, falls noetig
                while (scan.hasNextLine()) {
                    String zeile = scan.nextLine();
                    
                    while (zeile.length() > Meta.UPLOAD_LIMIT) {
                        nachricht.append(zeile.substring(0, Meta.UPLOAD_LIMIT)
                                + "\n");
                        zeile = zeile.substring(Meta.UPLOAD_LIMIT);
                    }
                    nachricht.append(zeile);
                }

                scan.close();
                scan = new Scanner(nachricht.toString());
                
                String show = "Nachricht gesendet";
                
                // Zeile fuer Zeile versenden
                while (scan.hasNextLine())
                    try {
                        new URL(Meta.FDB_ADD
                                + "?app="
                                + Meta.APP_NAME
                                + "&cat="
                                + Meta.KAT_SHORT[this.getKategorie()
                                        .getSelectedIndex()] + "&text="
                                + URLEncoder.encode(scan.nextLine(), "UTF-8"))
                                .openStream();
                    } catch (Exception e) {
                        e.printStackTrace();
                        show = "Nachricht konnte nicht gesendet werden.";
                    }
                
                scan.close();
                
                JOptionPane.showOptionDialog(null, show, "Feedback",
                        JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                        this.option, this.option[0]);
                this.dispose();
            } else
                JOptionPane.showOptionDialog(null, "Ungueltige E-Mail Adresse",
                        "Feedback", JOptionPane.OK_OPTION,
                        JOptionPane.PLAIN_MESSAGE, null, this.option,
                        this.option[0]);
    }
    
    private JButton getAbort() {
        if (this.abort == null) {
            this.abort = new JButton("Abbrechen");
            this.abort.addActionListener(this);
        }
        return this.abort;
    }
    
    /**
     * Gibt die Kategorieauswahl zurueck.
     * 
     * @return Kategorieauswahl
     */
    private JComboBox<?> getKategorie() {
        if (this.kategorie == null)
            this.kategorie = new JComboBox<Object>(this.KATEGORIEN);
        return this.kategorie;
    }
    
    private JTextField getMail() {
        if (this.mail == null)
            this.mail = new JTextField(35);
        return this.mail;
    }
    
    /**
     * Gibt das Texteingabefeld zurueck.
     * 
     * @return Texteingabefeld
     */
    private JTextArea getMessage() {
        if (this.message == null) {
            this.message = new JTextArea(10, 50);
            this.message.setLineWrap(true);
            this.message.setWrapStyleWord(true);
        }
        return this.message;
    }
    
    /**
     * Gibt das Texteingabefeld mit Scrollbalken zurueck.
     * 
     * @return Texteingabefeld mit Scrollbalken
     */
    private JScrollPane getMessageBox() {
        if (this.messageBox == null) {
            this.messageBox = new JScrollPane(this.getMessage());
            this.messageBox
                    .setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        }
        return this.messageBox;
    }
    
    private JTextField getNamen() {
        if (this.name == null)
            this.name = new JTextField(20);
        return this.name;
    }
    
    private JButton getSend() {
        if (this.send == null) {
            this.send = new JButton("Senden");
            this.send.addActionListener(this);
        }
        return this.send;
    }
}
