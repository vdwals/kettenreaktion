package de.vdwals.kettenreaktion.implement;

import java.awt.Point;
import java.util.Arrays;

/**
 * Klasse fuer Standardfunktionen von Arrays
 * 
 * @author Dennis van der Wals
 * 
 */
public class ArrOps {
    
    /**
     * Erstellt ein neues Array und fuellt es mit den Werten aus dem Original
     * 
     * @param original
     *            zu kopierendes Array
     * @return Wertgenaue Kopie
     */
    public static boolean[][] arrayCopy(boolean[][] original) {
        boolean[][] copy = new boolean[original.length][original[0].length];
        for (int i = 0; i < copy.length; i++)
            for (int j = 0; j < copy[0].length; j++)
                copy[i][j] = original[i][j];
        return copy;
    }
    
    /**
     * Erstellt ein neues Array und fuellt es mit den Werten aus dem Original
     * 
     * @param original
     *            zu kopierendes Array
     * @return Wertgenaue Kopie
     */
    public static byte[][] arrayCopy(byte[][] original) {
        byte[][] copy = new byte[original.length][original[0].length];
        for (int i = 0; i < copy.length; i++)
            for (int j = 0; j < copy[0].length; j++)
                copy[i][j] = original[i][j];
        return copy;
    }
    
    /**
     * Erstellt ein neues Array und fuellt es mit den Werten aus dem Original
     * 
     * @param original
     *            zu kopierendes Array
     * @return Wertgenaue Kopie
     */
    public static double[] arrayCopy(double[] original) {
        double[] copy = new double[original.length];
        for (int i = 0; i < copy.length; i++)
            copy[i] = original[i];
        return copy;
    }
    
    /**
     * Erstellt ein neues Array und fuellt es mit den Werten aus dem Original
     * 
     * @param original
     *            zu kopierendes Array
     * @return Wertgenaue Kopie
     */
    public static int[] arrayCopy(int[] original) {
        int[] copy = new int[original.length];
        for (int i = 0; i < copy.length; i++)
            copy[i] = original[i];
        return copy;
    }
    
    /**
     * Gibt das uebergebene Array als String zurueck.
     * 
     * @param array
     *            Array
     * @return Array als String
     */
    public static String arrayToString(int[] array) {
        StringBuilder sb = new StringBuilder();
        for (int element : array)
            sb.append(element + " ");
        return sb.toString();
    }
    
    /**
     * Konvertiert ein Bytearray der Laenge 17 in ein Double
     * 
     * @param buffer
     *            bytearray
     * @return Integer
     */
    public static double ByteArrayToDouble(byte[] buffer) {
        return Double.parseDouble(new String(buffer));
    }
    
    /**
     * Konvertiert ein Bytearray der Laenge 4 in ein Integer
     * 
     * @param buffer
     *            bytearray
     * @return Integer
     */
    public static int ByteArrayToInt(byte[] buffer) {
        int value = (0xFF & buffer[0]) << 24;
        value |= (0xFF & buffer[1]) << 16;
        value |= (0xFF & buffer[2]) << 8;
        value |= (0xFF & buffer[3]);
        
        return value;
    }
    
    /**
     * Gibt die Stringrepreasentation einer Liste von Punkten zurueck.
     * 
     * @param chips
     *            Liste von Punkten
     * @return Punkte als String
     */
    public static String chipsToString(Point[] chips) {
        StringBuilder sb = new StringBuilder();
        for (Point chip : chips)
            if (chip != null)
                sb.append("(" + chip.x + "|" + chip.y + ")" + "\t");
            else
                sb.append("( | )\t");
        sb.trimToSize();
        return sb.toString();
    }
    
    /**
     * Konvertiert ein Souble in ein Bytearray der Laenge 17
     * 
     * @param val
     *            Integer
     * @return bytearray
     */
    public static byte[] DoubleToByteArray(double val) {
        return String.valueOf(val).getBytes();
    }
    
    /**
     * Gibt eine Stringrepraesentation des Spielbrettes zurueck.
     * 
     * @param brett
     *            Spielbrett
     * @return Spielbrett als String
     */
    public static String feldToString(byte[][] brett) {
        StringBuilder sb = new StringBuilder();
        for (int y = 0; y < brett[0].length; y++) {
            for (byte[] element : brett)
                sb.append(element[y] + "\t");
            sb.append("\n");
        }
        sb.trimToSize();
        return sb.toString();
    }
    
    /**
     * Gibt den Index mit dem hoechsten Wert zurueck.
     * 
     * @param array
     *            zu durchsuchendes Array
     * @return index mit hoechstem Wert
     */
    public static int getMax(int[] array) {
        int max = 0;
        for (int i = 1; i < array.length; i++)
            if (array[i] > array[max])
                max = i;
        return max;
    }
    
    /**
     * Berechnet Indizes des groessten und des Kleinsten Wertes eines Arrays und
     * gibt sie in einem Array zurueck.
     * 
     * @param array
     *            zu durchsuchendes Array
     * @return Array mit 0 = Index des Minimums; 1 = Index des Maximums
     */
    public static int[] getMaxMin(double[] array) {
        int[] min_max = new int[2];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < array[min_max[0]])
                min_max[0] = i;
            if (array[i] > array[min_max[1]])
                min_max[1] = i;
        }
        return min_max;
    }
    
    /**
     * Gibt einen Teil eines Bytearrays zurueck.
     * 
     * @param komplett
     *            Komplettes Array
     * @param start
     *            Startindex
     * @param ende
     *            Endindex
     * @return Teilarray
     */
    public static boolean[] getPartOfArray(boolean[] komplett, int start,
            int ende) {
        if ((start < 0) || (ende > komplett.length)) {
            BugReport.recordBug(new IllegalArgumentException(
                    "Zugriff ausserhalb des Arrays: size = " + komplett.length
                            + "; s = " + start + "; e = " + ende));
            return null;
        } else {
            boolean[] teilArray = new boolean[ende - start];
            for (int i = 0; i < teilArray.length; i++)
                teilArray[i] = komplett[start++];
            return teilArray;
        }
    }
    
    /**
     * Gibt einen Teil eines Bytearrays zurueck.
     * 
     * @param komplett
     *            Komplettes Array
     * @param start
     *            Startindex
     * @param ende
     *            Endindex
     * @return Teilarray
     */
    public static byte[] getPartOfArray(byte[] komplett, int start, int ende) {
        if ((start < 0) || (ende > komplett.length)) {
            BugReport.recordBug(new IllegalArgumentException(
                    "Zugriff ausserhalb des Arrays: size = " + komplett.length
                            + "; s = " + start + "; e = " + ende));
            return null;
        } else {
            byte[] teilArray = new byte[ende - start];
            for (int i = 0; i < teilArray.length; i++)
                teilArray[i] = komplett[start++];
            return teilArray;
        }
    }
    
    /**
     * Ermittelt den Hashwert einer Karte.
     * 
     * @param karte
     *            Karte
     * @return Hashwert
     */
    public static long hash(byte[][] karte) {
        return Arrays.deepHashCode(karte);
    }
    
    /**
     * Sortiert eine Array mittels InsertionSort und gibt ein Array mit den
     * Indizes des entsprechenden Wertes aus dem uebergebenem Array in
     * aufwaertssortierter Reihenfolge zurueck. Aus { 2, 3, 5, 4, 1 } -> {4, 0,
     * 1, 3, 2}
     * 
     * @param Zahlenreihe
     *            unsortierte Zahlenreihe
     * @return Array mit den Sortierindizes
     */
    public static int[] indexSort(double[] Zahlenreihe) {
        int[] reihenfolge = new int[Zahlenreihe.length];
        for (int i = 0; i < reihenfolge.length; i++)
            reihenfolge[i] = i;
        for (int i = 1; i < Zahlenreihe.length; i++) {
            double temp = Zahlenreihe[i];
            int tempReihenfolge = reihenfolge[i];
            int j = i - 1;
            while ((j >= 0) && (Zahlenreihe[j] > temp)) {
                Zahlenreihe[j + 1] = Zahlenreihe[j];
                reihenfolge[j + 1] = reihenfolge[j];
                j--;
            }
            Zahlenreihe[j + 1] = temp;
            reihenfolge[j + 1] = tempReihenfolge;
        }
        return reihenfolge;
    }
    
    /**
     * Sortiert eine Array mittels InsertionSort und gibt ein Array mit den
     * Indizes des entsprechenden Wertes aus dem uebergebenem Array in
     * aufwaertssortierter Reihenfolge zurueck. Aus { 2, 3, 5, 4, 1 } -> {4, 0,
     * 1, 3, 2}
     * 
     * @param Zahlenreihe
     *            unsortierte Zahlenreihe
     * @return Array mit den Sortierindizes
     */
    public static int[] indexSort(int[] Zahlenreihe) {
        int[] reihenfolge = new int[Zahlenreihe.length];
        for (int i = 0; i < reihenfolge.length; i++)
            reihenfolge[i] = i;
        for (int i = 1; i < Zahlenreihe.length; i++) {
            int temp = Zahlenreihe[i];
            int tempReihenfolge = reihenfolge[i];
            int j = i - 1;
            while ((j >= 0) && (Zahlenreihe[j] > temp)) {
                Zahlenreihe[j + 1] = Zahlenreihe[j];
                reihenfolge[j + 1] = reihenfolge[j];
                j--;
            }
            Zahlenreihe[j + 1] = temp;
            reihenfolge[j + 1] = tempReihenfolge;
        }
        return reihenfolge;
    }
    
    /**
     * Konvertiert ein Integer in ein Bytearray der Laenge 4
     * 
     * @param val
     *            Integer
     * @return bytearray
     */
    public static byte[] IntToByteArray(int val) {
        byte[] buffer = new byte[4];
        
        buffer[0] = (byte) (val >>> 24);
        buffer[1] = (byte) (val >>> 16);
        buffer[2] = (byte) (val >>> 8);
        buffer[3] = (byte) val;
        
        return buffer;
    }
    
    private static boolean testBit(byte n, byte pos) {
        byte mask = (byte) (1 << pos);
        
        return (n & mask) == mask;
        // alternativ: return (n & 1<<pos) != 0;
    }
    
    /**
     * Erzeugt aus einem Byte ein Array aus Bits
     * 
     * @param b
     *            byte
     * @return Array aus Bits vom Typ boolean
     */
    public static boolean[] toBits(byte b, int laenge) {
        boolean[] bits = new boolean[laenge];
        // byte counter = 0;
        // while (b != 0) {
        // bits[counter++] = (b % 2 == 1 || b % 2 == -1);
        // b /= 2;
        // }
        for (byte i = 0; i < bits.length; i++)
            bits[i] = testBit(b, i);
        return bits;
    }
    
    /**
     * Erzeugt ein Byte aus einer Liste aus Bits.
     * 
     * @param bits
     *            Liste aus Bits vom Typ boolean.
     * @return byte
     */
    public static byte toByte(boolean[] bits) {
        byte b = 0;
        for (int i = 0; i < bits.length; i++)
            if (bits[i])
                b += Math.pow(2, i);
        return b;
    }
}
