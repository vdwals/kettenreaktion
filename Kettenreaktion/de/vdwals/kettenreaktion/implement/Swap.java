package de.vdwals.kettenreaktion.implement;

public class Swap {
    
    /**
     * Tauscht zwei Zahlen inerhalb eines Arrays
     * 
     * @param zahlenreihe
     *            Array
     * @param links
     *            linke Zahl
     * @param rechts
     *            rechte Zahl
     */
    public static void swap(int[] zahlenreihe, int links, int rechts) {
        int temp = zahlenreihe[links];
        zahlenreihe[links] = zahlenreihe[rechts];
        zahlenreihe[rechts] = temp;
    }
    
}
