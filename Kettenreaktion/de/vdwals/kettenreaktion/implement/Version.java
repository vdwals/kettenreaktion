package de.vdwals.kettenreaktion.implement;

import java.util.Scanner;

/**
 * Versionsklasse
 * 
 * @author Dennis van der Wals
 * 
 */
public class Version implements Comparable<Version> {
    public static final int     ALPHA = 0, BETA = 1, RELEASE = 2;
    private int                 major, fix, build, release;
    // Filter fuer Versionsstil
    private final static String regex = "(\\d+)(\\.)(\\d+)(\\.)(\\d+)(\\s)((alpha?)|(beta?)|())";
    
    /**
     * Konstruktor.
     * 
     * @param major
     *            Majorreleaseversionsnummer
     * @param fix
     *            Bugfixversionsnummer
     * @param build
     *            Buildverionsnummer
     * @param release
     *            Releasestatus
     */
    public Version(int major, int fix, int build, int release) {
        this.major = major;
        this.fix = fix;
        this.build = build;
        this.release = release;
    }
    
    /**
     * Erstellt eine Version aus einem String
     * 
     * @param version
     */
    public Version(String version) {
        if (!version.matches(regex))
            throw new IllegalArgumentException(
                    "Ungueltige Versionsnummer eingegeben");
        
        Scanner scan = new Scanner(version);
        scan.useDelimiter("(\\.)|(\\s)");
        
        this.major = Integer.parseInt(scan.next());
        this.fix = Integer.parseInt(scan.next());
        this.build = Integer.parseInt(scan.next());
        try {
            this.release = this.getRelease(scan.next());
        } catch (Exception e) {
            this.release = 2;
        }
        scan.close();
    }
    
    @Override
    public int compareTo(Version arg0) {
        if ((arg0.major > this.major) || (arg0.fix > this.fix)
                || (arg0.build > this.build) || (arg0.release > this.release))
            return -1;
        else if ((arg0.major < this.major) || (arg0.fix < this.fix)
                || (arg0.build < this.build) || (arg0.release < this.release))
            return 1;
        else
            return 0;
    }
    
    /**
     * Wandelt die Releasestatusnummer in eine Bezeichnung um
     * 
     * @param release
     *            Releasestatusnummer
     * @return alpha; beta;
     */
    private String getRelease(int release) {
        switch (release) {
            case 0:
                return "alpha";
            case 1:
                return "beta";
            case 2:
                return "";
            default:
                break;
        }
        return "";
    }
    
    /**
     * Wandelt die Releasebezeichnung in eine Statusnummer um.
     * 
     * @param release
     *            Releasebezeichnung
     * @return 0 = alpha; 1 = beta; 2
     */
    private int getRelease(String release) {
        switch (release) {
            case "alpha":
                return 0;
            case "beta":
                return 1;
            case "":
                return 2;
            default:
                break;
        }
        return 2;
    }
    
    @Override
    public String toString() {
        return this.major + "." + this.fix + "." + this.build + " "
                + this.getRelease(this.release);
    }
    
}
