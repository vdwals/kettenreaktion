package de.vdwals.kettenreaktion.statistik;

import java.awt.Color;
import java.util.LinkedList;

import de.vdwals.kettenreaktion.statistik.gui.LivePanel;
import de.vdwals.kettenreaktion.statistik.gui.implement.simpleGraph.DataSet;
import de.vdwals.kettenreaktion.statistik.gui.implement.simpleGraph.DataSetStyle;

/**
 * Klasse zum Aufnehmen statistischer Daten eines Spieles
 * 
 * @author Dennis van der Wals
 * 
 */
public class Spiel {
    private LivePanel       livepanel;
    public Thread           t;
    
    // Eigenwerte
    private int             bankChips, gesamtChips;
    private byte            spieler, startChips, chipsToSet;
    private LinkedList<Zug> rundenListe;
    private Zug             aktuelleRunde;
    
    // Shrink-Werte beendeter Spiele
    private int             stapelHoehen[], ausloesungProHoehe[],
            rausgefallen[], reaktionen[], clusterGreosse[],
            ausloesungProClustergroesse[], gewonnen[], summe[], runden;
    private DataSet[][]     datensaezte;
    
    /**
     * Standardkonstruktor. Speichert die Spielumgebung und eine Liste aller
     * Runden.
     * 
     * @param spieler
     *            Anzahl der Spieler
     * @param startChips
     *            Menge der Startchips
     * @param bankChips
     *            Menge der Bankschips
     * @param chipsToSet
     *            Anzahl der zu setzenden Chips
     * @param gesamtChips
     *            Menge aller Chips
     */
    public Spiel(byte spieler, byte startChips, int bankChips, byte chipsToSet,
            int gesamtChips, boolean livePanel) {
        this.spieler = spieler;
        this.startChips = startChips;
        this.bankChips = bankChips;
        this.chipsToSet = chipsToSet;
        this.gesamtChips = gesamtChips;
        
        this.rundenListe = new LinkedList<Zug>();
        
        if (livePanel)
            this.getLivePanel();
    }
    
    /**
     * Fuegt eine neue Runde hinzu.
     * 
     * @param neu
     *            neue Runde
     * @param debug
     *            Sollen Debuginformationen ausgegeben werden
     */
    protected void add(Zug neu, boolean debug) {
        if (debug && (this.getAktuelleRunde() != null)) {
            Zug last = this.getAktuelleRunde();
            System.out.println("Runde: " + this.rundenListe.size());
            System.out.println("Summe der Felder: " + last.getSumme());
            System.out.println("Stapel: ");
            for (int i = 0; i < last.getStapelHoehen().length; i++)
                System.out.println("Hoehe " + i + " = "
                        + last.getStapelHoehen()[i] + "; auslösung = "
                        + last.getAusloesungProHoehe()[i]);
            System.out.println("Cluster: ");
            for (int i = 0; i < last.getClusterGreosse().length; i++)
                System.out.println("Groesse " + i + " = "
                        + last.getClusterGreosse()[i] + "; auslösung = "
                        + last.getAusloesungProClustergroesse()[i]);
            System.out.println("Herausgefallen: " + last.getRausgefallen());
            System.out.println("Gewonnen: " + last.getGewonnen());
            System.out.println("");
        }
        this.aktuelleRunde = neu;
        this.rundenListe.add(neu);
    }
    
    /**
     * Beendet ein Spiel.
     */
    public void beendeSpiel() {
        if (this.rundenListe.size() > 0) {
            this.runden = this.rundenListe.size();
            this.stapelHoehen = new int[this.rundenListe.getFirst()
                    .getStapelHoehen().length];
            this.ausloesungProHoehe = new int[this.rundenListe.getFirst()
                    .getStapelHoehen().length];
            this.rausgefallen = new int[this.runden];
            this.reaktionen = new int[this.runden];
            this.gewonnen = new int[this.runden];
            this.summe = new int[this.runden];
            
            int maxCluster = 0;
            for (Zug runde : this.rundenListe)
                // Groessten Cluster bestimmen
                if (runde.getClusterGreosse().length > maxCluster)
                    maxCluster = runde.getClusterGreosse().length;
            this.clusterGreosse = new int[maxCluster];
            this.ausloesungProClustergroesse = new int[maxCluster];
            
            for (int i = 0; i < this.runden; i++) {
                Zug tmp = this.rundenListe.get(i);
                
                for (int j = 0; j < tmp.getStapelHoehen().length; j++) {
                    this.stapelHoehen[j] += tmp.getStapelHoehen()[j];
                    this.ausloesungProHoehe[j] += tmp.getAusloesungProHoehe()[j];
                }
                
                this.rausgefallen[i] = tmp.getRausgefallen();
                this.reaktionen[i] = tmp.getReaktionen();
                this.gewonnen[i] = tmp.getGewonnen();
                this.summe[i] = tmp.getSumme();
                
                for (int j = 0; j < tmp.getClusterGreosse().length; j++) {
                    this.clusterGreosse[j] += tmp.getClusterGreosse()[j];
                    this.ausloesungProClustergroesse[j] += tmp
                            .getAusloesungProClustergroesse()[j];
                }
            }
        }
        
    }
    
    /**
     * Gibt die aktuelle Runde zurueck.
     * 
     * @return runde
     */
    public Zug getAktuelleRunde() {
        return this.aktuelleRunde;
    }
    
    /**
     * @return the bankChips
     */
    protected int getBankChips() {
        return this.bankChips;
    }
    
    /**
     * @return the chipsToSet
     */
    protected int getChipsToSet() {
        return this.chipsToSet;
    }
    
    /**
     * @return the gesamtChips
     */
    protected int getGesamtChips() {
        return this.gesamtChips;
    }
    
    public LivePanel getLivePanel() {
        if (this.livepanel == null) {
            String[] titel = { "Chipzahlen", "Stapelhoehe",
                    "Ausloesungen pro Stapelhoehe", "Clustergroesse",
                    "Ausloesungen pro Clustergroesse" };
            this.datensaezte = new DataSet[5][];
            this.datensaezte[0] = new DataSet[4];
            for (int i = 1; i < 3; i++)
                this.datensaezte[i] = new DataSet[6];
            for (int i = 3; i < 5; i++)
                this.datensaezte[i] = new DataSet[20];
            DataSetStyle[] stile = this.getStile();
            this.datensaezte[0][0] = new DataSet("Dichte", stile[0]);
            this.datensaezte[0][1] = new DataSet("Gewonnene Chips", stile[1]);
            this.datensaezte[0][2] = new DataSet("Herausgefallene Chips",
                    stile[2]);
            this.datensaezte[0][3] = new DataSet("Reaktionen", stile[3]);
            for (int i = 0; i < this.datensaezte[1].length; i++) {
                this.datensaezte[1][i] = new DataSet("Stapel der Hoehe" + i,
                        stile[i]);
                this.datensaezte[2][i] = new DataSet(
                        "Ausloesungen von Stapeln der Hoehe" + i, stile[i]);
            }
            for (int i = 0; i < this.datensaezte[3].length; i++) {
                this.datensaezte[3][i] = new DataSet("Cluster der Groesse" + i,
                        stile[i]);
                this.datensaezte[4][i] = new DataSet(
                        "Ausloesungen von Clustern der Groesse" + i, stile[i]);
            }
            this.livepanel = new LivePanel("Statistiken", this.datensaezte,
                    titel);
            this.livepanel.build();
            this.livepanel.setVisible(true);
        }
        return this.livepanel;
    }
    
    /**
     * @return the runden
     */
    public LinkedList<Zug> getRunden() {
        return this.rundenListe;
    }
    
    /**
     * @return the spieler
     */
    protected int getSpieler() {
        return this.spieler;
    }
    
    /**
     * @return the startChips
     */
    protected int getStartChips() {
        return this.startChips;
    }
    
    /**
     * Erzeugt DataSetStile
     * 
     * @return
     */
    private DataSetStyle[] getStile() {
        DataSetStyle[] stile = new DataSetStyle[20];
        stile[0] = new DataSetStyle();
        stile[1] = new DataSetStyle(Color.RED);
        stile[2] = new DataSetStyle(Color.BLUE);
        stile[3] = new DataSetStyle(Color.GREEN);
        stile[4] = new DataSetStyle(Color.YELLOW);
        stile[5] = new DataSetStyle(Color.CYAN);
        stile[6] = new DataSetStyle(Color.DARK_GRAY);
        stile[7] = new DataSetStyle(Color.GRAY);
        stile[8] = new DataSetStyle(Color.LIGHT_GRAY);
        stile[9] = new DataSetStyle(Color.MAGENTA);
        stile[10] = new DataSetStyle(Color.ORANGE);
        stile[11] = new DataSetStyle(Color.PINK);
        stile[12] = new DataSetStyle(Color.RED.darker());
        stile[13] = new DataSetStyle(Color.BLUE.darker());
        stile[14] = new DataSetStyle(Color.GREEN.darker());
        stile[15] = new DataSetStyle(Color.YELLOW.darker());
        stile[16] = new DataSetStyle(Color.CYAN.darker());
        stile[17] = new DataSetStyle(Color.MAGENTA.darker());
        stile[18] = new DataSetStyle(Color.ORANGE.darker());
        stile[19] = new DataSetStyle(Color.PINK.darker());
        return stile;
    }
    
    /**
     * Updatet das LivePanel.
     */
    public void updateLivePanel(final Zug runde) {
        this.t = new Thread(new Runnable() {
            
            @Override
            public void run() {
                int r = Spiel.this.rundenListe.size() - 1;
                Spiel.this.datensaezte[0][0].addData(r, runde.getDichte());
                Spiel.this.datensaezte[0][1].addData(r, runde.getGewonnen());
                Spiel.this.datensaezte[0][2].addData(r, runde.getRausgefallen());
                Spiel.this.datensaezte[0][3].addData(r, runde.getReaktionen());
                for (int i = 0; i < runde.getStapelHoehen().length; i++)
                    Spiel.this.datensaezte[1][i].addData(r,
                            runde.getStapelHoehen()[i]);
                for (int i = 0; i < runde.getAusloesungProHoehe().length; i++)
                    Spiel.this.datensaezte[2][i].addData(r,
                            runde.getAusloesungProHoehe()[i]);
                for (int i = 0; i < runde.getClusterGreosse().length; i++)
                    Spiel.this.datensaezte[3][i].addData(r,
                            runde.getClusterGreosse()[i]);
                for (int i = 0; i < runde.getAusloesungProClustergroesse().length; i++)
                    Spiel.this.datensaezte[4][i].addData(r,
                            runde.getAusloesungProClustergroesse()[i]);
                Spiel.this.livepanel.graphenBorderPanel.paintImmediately(0, 0,
                        Spiel.this.livepanel.graphenBorderPanel.getWidth(),
                        Spiel.this.livepanel.graphenBorderPanel.getHeight());
            }
        });
        this.t.start();
    }
    
}
