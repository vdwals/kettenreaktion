package de.vdwals.kettenreaktion.statistik;

import java.util.LinkedList;

import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.spiel.mod.Mod;

/**
 * Klasse zum aufnehmen und kategorisieren statistischer Daten.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Karte {
    // Eigenwerte
    private byte[][]          karte;
    private Mod               mod;
    private LinkedList<Spiel> spiele;
    
    // Shrink-Werte aus beendeten Spielen
    @SuppressWarnings("unused")
    private int               stapelHoehen[], ausloesungProHoehe[],
            rausgefallen[], reaktionen[], clusterGreosse[],
            ausloesungProClustergroesse[], gewonnen[], summe[], runden;
    
    /**
     * Standardkonstruktor.
     * 
     * @param karte
     *            Spielbrett
     * @param mod
     *            Modifikation
     */
    public Karte(byte[][] karte, Mod mod) {
        this.karte = ArrOps.arrayCopy(karte);
        this.mod = mod;
        this.spiele = new LinkedList<Spiel>();
    }
    
    /**
     * Fuegt der Karte ein Spiel hinzu.
     * 
     * @param spiel
     *            neues Spiel
     */
    protected void add(Spiel spiel) {
        this.spiele.add(spiel);
    }
    
    /**
     * Ueberprueft, ob die uebergebene Karte der eigenen gleicht.
     * 
     * @param vergleich
     *            Vergleichskarte
     * @return Wahrheitswert
     */
    public boolean equals(Karte vergleich) {
        for (int x = 0; x < vergleich.karte.length; x++)
            for (int y = 0; y < vergleich.karte[0].length; y++)
                if (this.karte[x][y] != vergleich.karte[x][y])
                    return false;
        return this.mod.toString().equals(vergleich.mod.toString());
    }
    
    /**
     * Gibt das aktuelle Spiel zurueck.
     * 
     * @return aktuelles Spiel
     */
    protected Spiel getAktuellesSpiel() {
        return this.spiele.getLast();
    }
    
    protected byte[] karteToByte() {
        // TODO Grenzwert bestimmen
        byte[] output = new byte[10];
        int counter = 0;
        
        // Kartendimensionen schreiben
        output[counter++] = (byte) this.karte.length;
        output[counter++] = (byte) this.karte[0].length;
        
        // Karte schreiben
        for (byte[] spalt : this.karte)
            for (byte feld : spalt)
                output[counter++] = feld;
        
        return output;
    }
    
    /**
     * Fuegt dem aktuellem Spiel eine neue Runde hinzu.
     * 
     * @param spielbrett
     *            Spielbrett
     * @param debug
     *            Sollen Debuginformationen gesammelt werden
     */
    protected void neuerZug(byte[][] spielbrett, boolean debug) {
        this.getAktuellesSpiel().add(new Zug(spielbrett, this.mod, debug),
                debug);
    }
}
