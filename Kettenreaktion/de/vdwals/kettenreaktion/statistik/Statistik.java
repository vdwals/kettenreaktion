package de.vdwals.kettenreaktion.statistik;

import java.awt.Point;
import java.io.IOException;
import java.util.LinkedList;

import javax.xml.bind.annotation.XmlRootElement;

import de.vdwals.kettenreaktion.gui.Einstellungen;
import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.optionen.ProgrammOptionen;
import de.vdwals.kettenreaktion.spiel.ki.Strategie;
import de.vdwals.kettenreaktion.statistik.io.StatistikIO;

/**
 * Klasse zum aufzeichnen und Verwalten diverser Statistiken fuer einen
 * Spielverlauf.
 * 
 * @author Dennis van der Wals
 * 
 */
@XmlRootElement
public class Statistik {
    private LinkedList<Karte>   karten;
    private LinkedList<Spieler> spieler;
    private Karte               aktuelleKarte;
    private ProgrammOptionen    einstellungen;
    private StatistikIO         io;
    
    /**
     * Erzeugt eine neue Statistik
     */
    public Statistik() {
        this.karten = new LinkedList<Karte>();
        this.spieler = new LinkedList<Spieler>();
        this.io = new StatistikIO();
        
        try {
            this.karten = this.io.getKarten();
            this.spieler = this.io.getSpieler();
        } catch (Exception e) {
            BugReport.recordBug(e);
        }
    }
    
    /**
     * Aktuallisiert die Statistiken nach einem Spiel
     * 
     * @param durchGespielt
     *            Wurde das Spiel durchgespielt, oder abgebrochen (false)
     * @param konto
     *            Spielerkonten
     * @param namen
     *            Spielernamen
     * @param maxKettenreaktionen
     *            groesste Kettenreaktion der Spieler
     * @param kis
     *            KI-Liste
     */
    public void beendeSpiel(boolean durchGespielt, int[] konto, String[] namen,
            int[] maxKettenreaktionen, Strategie[] kis) {
        
        // Spielerstatistik nur aufnehmen, wenn Spiel durchgespielt wurde
        if (durchGespielt) {
            LinkedList<Spieler> neueStatistiken = new LinkedList<Spieler>();
            int gewinner = ArrOps.getMax(konto);
            boolean ki = false;
            
            // Jeden Spieler aufnehmen
            for (int i = 0; i < kis.length; i++)
                // KIs ueberspringen
                if (kis[i] != null)
                    ki = true;
                else
                    neueStatistiken.add(new Spieler(namen[i], (byte) 1,
                            (byte) ((ki) ? 1 : 0), (byte) ((i == gewinner) ? 1
                                    : 0), (byte) (((i == gewinner) && ki) ? 1
                                    : 0), konto[i], maxKettenreaktionen[i]));
            
            LinkedList<Spieler> toAdd = new LinkedList<Spieler>();
            toAdd.addAll(neueStatistiken);
            
            // Neue mit alten vergleichen
            for (Spieler neu : neueStatistiken) {
                
                // Spiel gegen KI eintragen
                if (ki) {
                    neu.setSpieleGegenKi((byte) 1);
                    if (neu.getGewonnen() == 1)
                        neu.setGewonnenGegenKi((byte) 1);
                }
                
                for (Spieler alt : this.spieler)
                    // Wenn schon vorhanden, Daten aktualisieren
                    if (alt.equals(neu)) {
                        alt.setGewonnen((byte) (alt.getGewonnen() + neu
                                .getGewonnen()));
                        alt.setGewonnenGegenKi((byte) (alt.getGewonnenGegenKi() + neu
                                .getGewonnenGegenKi()));
                        if (alt.getHighReaction() < neu.getHighReaction())
                            alt.setHighReaction(neu.getHighReaction());
                        if (alt.getHighScore() < neu.getHighScore())
                            alt.setHighScore(neu.getHighScore());
                        alt.setSpiele((byte) (alt.getSpiele() + neu.getSpiele()));
                        alt.setSpieleGegenKi((byte) (alt.getSpieleGegenKi() + neu
                                .getSpieleGegenKi()));
                        
                        // Aus neuer Liste loeschen
                        toAdd.remove(neu);
                        
                        break;
                    }
            }
            
            this.spieler.addAll(toAdd);
            
            // sortieren
            java.util.Collections.sort(this.spieler);
        }
        this.aktuelleKarte.getAktuellesSpiel().beendeSpiel();
    }
    
    /**
     * Fuegt der aktuellen Runde des aktuellen Spieles die Anzahl der verlorenen
     * Chips hinzu.
     * 
     * @param rausgefallen
     *            Anzahl der herausgefallenen Chips
     * @param gewonnen
     *            Anzahl der gewonnenen Chips
     */
    public void beendeZug(int rausgefallen, int gewonnen) {
        this.getAktuellesSpiel().getAktuelleRunde()
                .beendeZug(rausgefallen, gewonnen);
    }
    
    /**
     * Gibt die Statistikklasse des aktuellen Spieles zurueck.
     * 
     * @return
     */
    public Spiel getAktuellesSpiel() {
        return this.aktuelleKarte.getAktuellesSpiel();
    }
    
    /**
     * Fuegt dem aktuellem Spiel eine neue Runde hinzu. Ermittelt Anzahl der
     * Stapel je Hoehe, Anzahl und Groesse der Cluster und den Mittelwert des
     * Spielfeldes.
     * 
     * @param spielbrett
     *            Spielbrett
     */
    public void neuerZug(byte[][] spielbrett) {
        this.aktuelleKarte.neuerZug(spielbrett, this.einstellungen.isDebug());
    }
    
    /**
     * Erzeugt Statistik fuer ein neues Spiel. Speichert die Spielumgebung und
     * eine Liste aller Runden.
     * 
     * @param karte
     *            Spielbrett
     * @param einstellungen
     *            Spieleinstellungen
     */
    public void neuesSpiel(byte[][] karte, Einstellungen einstellungen) {
        this.aktuelleKarte = new Karte(karte, einstellungen.getSpielOpt()
                .getMod());
        if (this.karten == null)
            this.karten = new LinkedList<Karte>();
        
        if (!this.karten.contains(this.aktuelleKarte))
            this.karten.add(this.aktuelleKarte);
        
        this.aktuelleKarte.add(new Spiel((byte) einstellungen.getName()
                .length(), einstellungen.getSpielOpt().getStartChips(),
                einstellungen.getSpielOpt().getBankChips(), einstellungen
                        .getSpielOpt().getMaxChipsToSet(), einstellungen
                        .getSpielOpt().getGesamtChips(), einstellungen
                        .getStatistikOpt().isLivePanel()));
        this.einstellungen = einstellungen.getProgOpt();
    }
    
    /**
     * Registriet die Reaktion an Stelle p in der aktuellen Runde im aktuellem
     * Spiel. Ermittelt die groesse des reagierten Clusters und die hoehe des
     * reagierten Stapel.
     * 
     * @param p
     *            Feld der Reaktion
     */
    public void reagiere(Point p) {
        this.aktuelleKarte.getAktuellesSpiel().getAktuelleRunde().reaktion(p);
    }
    
    /**
     * Speicher die Statistiken.
     */
    public void save() {
        this.io.setSpieler(this.spieler);
        this.io.setKarten(this.karten);
        try {
            this.io.save();
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
    }
    
}
