package de.vdwals.kettenreaktion.statistik;

/**
 * Klasse zum Speichern einer Spielerstatistik.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Spieler implements Comparable<Spieler> {
    private String name;
    private int    spiele, spieleGegenKi, gewonnen, gewonnenGegenKi, highScore,
            highReaction;
    
    /**
     * Konstruktor erwartet Werte fuer die Statistik.
     * 
     * @param name
     *            Spielername
     * @param spiele
     *            Anzahl gespielter Spiele
     * @param spieleGegenKi
     *            Anzahl gespielet Spiele gegen KI
     * @param gewonnen
     *            Anzahl gewonnener Spiele
     * @param gewonnenGegenKi
     *            Anzahl gewonnener Spiele gegen eine KI
     * @param highScore
     *            Hoechste erhaltene Punktzahl
     * @param highReaction
     *            Hoechste erreichte Kettenreaktion
     */
    public Spieler(String name, int spiele, int spieleGegenKi, int gewonnen,
            int gewonnenGegenKi, int highScore, int highReaction) {
        this.name = name;
        this.spiele = spiele;
        this.spieleGegenKi = spieleGegenKi;
        this.gewonnen = gewonnen;
        this.gewonnenGegenKi = gewonnenGegenKi;
        this.highScore = highScore;
        this.highReaction = highReaction;
    }
    
    @Override
    public int compareTo(Spieler o) {
        if (this.highScore > o.highScore)
            return -1;
        if (this.highScore < o.highScore)
            return 1;
        return 0;
    }
    
    /**
     * Gibt zurueck, ob zwei Statistiken den selben Spieler beteffen.
     * 
     * @param b
     *            zu vergleichende Statistik
     * @return Wahrheitswert
     */
    public boolean equals(Spieler b) {
        return this.name.equals(b.getName());
    }
    
    /**
     * Gibt die Anzahl gewonnener Spiele zurueck.
     * 
     * @return Anzahl gewonnener Spiele
     */
    public int getGewonnen() {
        return this.gewonnen;
    }
    
    /**
     * Gibt die Anzahl gewonnener Spiele gegen eine KI zurueck.
     * 
     * @return Anzahl gewonnener Spiele gegen eine KI
     */
    public int getGewonnenGegenKi() {
        return this.gewonnenGegenKi;
    }
    
    /**
     * Gibt die groesste erreichte Kettenreaktion zurueck.
     * 
     * @return groesste erreichte Kettenreaktion
     */
    public int getHighReaction() {
        return this.highReaction;
    }
    
    /**
     * Gibt die hoechste erreichte Punktzahl zurueck.
     * 
     * @return hoechste erreichte Punktzahl
     */
    public int getHighScore() {
        return this.highScore;
    }
    
    /**
     * Gibt den Namen des Spieler zurueck.
     * 
     * @return Namen des Spieler
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Gibt die Anzahl gespielter Spiele zurueck.
     * 
     * @return Anzahl gespielter Spiele
     */
    public int getSpiele() {
        return this.spiele;
    }
    
    /**
     * Gibt die Anzahl gespielter Spiele gegen eine KI zurueck.
     * 
     * @return Anzahl gespielter Spiele gegen eine KI
     */
    public int getSpieleGegenKi() {
        return this.spieleGegenKi;
    }
    
    /**
     * Setzt die Anzahl gewonnener Spiele.
     * 
     * @param spiele
     *            Anzahl gewonnener Spiele
     */
    public void setGewonnen(byte gewonnen) {
        this.gewonnen = gewonnen;
    }
    
    /**
     * Setzt die Anzahl gewonnener Spiele gegen eine KI.
     * 
     * @param spiele
     *            Anzahl gewonnener Spiele gegen eine KI
     */
    public void setGewonnenGegenKi(byte gewonnenGegenKi) {
        this.gewonnenGegenKi = gewonnenGegenKi;
    }
    
    /**
     * Setzt die groesste erreichte Kettenreaktion.
     * 
     * @param highScore
     *            groesste erreichte Kettenreaktion
     */
    public void setHighReaction(int highReaction) {
        this.highReaction = highReaction;
    }
    
    /**
     * Setzt die hoechste erreichte Punktzahl.
     * 
     * @param highScore
     *            hoechste erreichte Punktzahl
     */
    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }
    
    /**
     * Setzt die Anzahl gespielter Spiele.
     * 
     * @param spiele
     *            Anzahl gespielter Spiele
     */
    public void setSpiele(byte spiele) {
        this.spiele = spiele;
    }
    
    /**
     * Setzt die Anzahl gespielter Spiele gegen eine KI.
     * 
     * @param spiele
     *            Anzahl gespielter Spiele gegen eine KI
     */
    public void setSpieleGegenKi(byte spieleGegenKi) {
        this.spieleGegenKi = spieleGegenKi;
    }
    
}
