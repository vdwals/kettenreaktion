package de.vdwals.kettenreaktion.statistik;

import java.awt.Point;
import java.util.LinkedList;

import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.spiel.mod.Mod;

/**
 * Klasse zum sammeln statistischer Daten pro Runde.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Zug {
    // Statistisch interessante Werte:
    /**
     * Array mit der Menge der Stapel der jeweiligen durch die Arrayposition
     * bestimmten Hoehe.
     */
    private int                           stapelHoehen[];
    /**
     * Array mit der Menge der Reaktionen der jeweiligen durch die Arrayposition
     * bestimmten Stapelhoehe.
     */
    private int                           ausloesungProHoehe[];
    /**
     * Anzahl der Chips, die in dieser Runde vom Spielbrett vielen.
     */
    private int                           rausgefallen;
    /**
     * Anzahl der Reaktionen in dieser Runde.
     */
    private int                           reaktionen;
    /**
     * Anzahl der Cluster der jeweiligen durch die Arrayposition bestimmten
     * Clustergroesse.
     */
    private int                           clusterGreosse[];
    /**
     * Anzahl der ausgeloesten Cluster der jeweiligen durch die Arrayposition
     * bestimmten Clustergroesse.
     */
    private int                           ausloesungProClustergroesse[];
    /**
     * Anzahl der in dieser Runde gewonnenen Chips.
     */
    private int                           gewonnen;
    /**
     * Menge alles Chips auf dem Feld bei Rundenbegin.
     */
    private int                           summe;
    /**
     * Spielbrett bei Rundenbegin.
     */
    private byte                          spielbrett[][];
    /**
     * Liste aller Cluster.
     */
    private LinkedList<LinkedList<Point>> allCluster;
    
    /**
     * Standardkonstruktor. Ermittelt Anzahl der Stapel je Hoehe, Anzahl und
     * Groesse der Cluster und den Mittelwert des Spielfeldes.
     * 
     * @param spielbrett
     *            Spielbrett
     * @param mod
     *            Modifikation
     * @param debug
     *            Sollen Debuginformationen gesammelt werden
     */
    protected Zug(byte[][] spielbrett, Mod mod, boolean debug) {
        this.summe = 0;
        this.stapelHoehen = new int[mod.getChipsFuerReaktion()];
        this.ausloesungProHoehe = new int[mod.getChipsFuerReaktion()];
        this.spielbrett = spielbrett;
        this.allCluster = new LinkedList<LinkedList<Point>>();
        
        // Clustergroesse ermitteln
        int max = 0;
        byte[][] tmp = ArrOps.arrayCopy(spielbrett);
        
        // Liste aller moeglichen Clusterpunkte erstellen
        for (int x = 0; x < spielbrett.length; x++)
            for (int y = 0; y < spielbrett[0].length; y++)
                // Gueltige Felder pruefen
                if (spielbrett[x][y] != -1) {
                    // Chipzaehler erhoehen
                    this.summe += spielbrett[x][y];
                    // Stapelhoehenzaehler erhoehen
                    this.stapelHoehen[spielbrett[x][y]]++;
                    
                    // Potentielle und noch nicht besuchte Felder genauer
                    // betrachten
                    if (tmp[x][y] == (mod.getChipsFuerReaktion() - 1)) {
                        LinkedList<Point> cluster = new LinkedList<Point>();
                        Point p = new Point(x, y);
                        
                        // Punkt den Clustern hinzufuegen
                        cluster.add(p);
                        
                        // Feld als besucht markieren
                        tmp[x][y] = -1;
                        
                        // Liste fuer Cascade vorbereiten
                        LinkedList<Point> stapel = new LinkedList<Point>();
                        stapel.add(p);
                        while (!stapel.isEmpty()) {
                            
                            Point[] nachbarn = mod.getNachbarn(stapel.poll());
                            for (Point point : nachbarn)
                                /*
                                 * Nachbarn in Liste aufnehmen, wenn Sie
                                 * reaktion fortfuehren koennen
                                 */
                                if (this.isInFeld(point, tmp)
                                        && (tmp[point.x][point.y] == (mod
                                                .getChipsFuerReaktion() - 1))) {
                                    cluster.add(point);
                                    stapel.add(point);
                                    
                                    // Als gesehen markieren
                                    tmp[point.x][point.y] = -1;
                                }
                        }
                        
                        // Groesse des groessten Clusters bestimmen.
                        if (max < cluster.size())
                            max = cluster.size();
                        
                        // Cluster der Liste hinzufuegen.
                        this.allCluster.add(cluster);
                    }
                }
        
        // Anzahl der Cluster jeder groesse notieren
        this.clusterGreosse = new int[max + 1];
        for (LinkedList<Point> cluster : this.allCluster)
            this.clusterGreosse[cluster.size()]++;
        this.ausloesungProClustergroesse = new int[max + 1];
        
        // ********************** Debug ************************//
        if (debug) {
            int hoeheMax = 0;
            StringBuilder debugInformation = new StringBuilder();
            for (int i = 0; i < this.clusterGreosse.length; i++) {
                hoeheMax += this.clusterGreosse[i] * i;
                debugInformation.append("Groesse " + i + ": "
                        + this.clusterGreosse[i] + "\n");
            }
            if (this.stapelHoehen[this.stapelHoehen.length - 1] != hoeheMax)
                BugReport
                        .recordBug(new Exception(
                                "Cluster und Stapelh�hen stimmen nicht �berein:\n"
                                        + debugInformation
                                        + "Gesamt: "
                                        + hoeheMax
                                        + "; Stapelmenge: "
                                        + this.stapelHoehen[this.stapelHoehen.length - 1]));
        }
    }
    
    /**
     * Setzt den Wert der Anzahl der herausgefallenen Chips.
     * 
     * @param rausgefallen
     *            Anzahl der herausgefallenen Chips
     * @param gewonnen
     *            Anzahl der gewonnenen Chips
     */
    protected void beendeZug(int rausgefallen, int gewonnen) {
        this.rausgefallen = rausgefallen;
        this.gewonnen = gewonnen;
    }
    
    /**
     * @return the allCluster
     */
    protected LinkedList<LinkedList<Point>> getAllCluster() {
        return this.allCluster;
    }
    
    /**
     * @return the ausloesungProClustergroesse
     */
    public int[] getAusloesungProClustergroesse() {
        return this.ausloesungProClustergroesse;
    }
    
    /**
     * @return the ausloesungProHoehe
     */
    public int[] getAusloesungProHoehe() {
        return this.ausloesungProHoehe;
    }
    
    /**
     * @return the clusterGreosse
     */
    public int[] getClusterGreosse() {
        return this.clusterGreosse;
    }
    
    public double getDichte() {
        return this.summe
                / ((double) this.spielbrett.length * (double) this.spielbrett[0].length);
    }
    
    /**
     * @return the gewonnen
     */
    public int getGewonnen() {
        return this.gewonnen;
    }
    
    /**
     * @return the rausgefallen
     */
    public int getRausgefallen() {
        return this.rausgefallen;
    }
    
    /**
     * @return the reaktionen
     */
    public int getReaktionen() {
        return this.reaktionen;
    }
    
    /**
     * @return the spielbrett
     */
    protected byte[][] getSpielbrett() {
        return this.spielbrett;
    }
    
    /**
     * @return the stapelHoehen
     */
    public int[] getStapelHoehen() {
        return this.stapelHoehen;
    }
    
    /**
     * @return the mittelwert
     */
    public int getSumme() {
        return this.summe;
    }
    
    /**
     * Gibt zurueck, ob der Punkt p innerhalb ger Grenzen des Spielfeldes liegt.
     * 
     * @param p
     *            Punkt
     * @return Wahrheitswert
     */
    private boolean isInFeld(Point p, byte[][] spielbrett) {
        if ((p.x < 0) || (p.x >= spielbrett.length) || (p.y < 0)
                || (p.y >= spielbrett[0].length))
            return false;
        return (spielbrett[p.x][p.y] != -1);
    }
    
    /**
     * Registriert eine Reaktion auf dem Spielfeld in der aktuellen Runde.
     * Ermittelt die groesse des reagierten Clusters und die hoehe des
     * reagierten Stapel.
     * 
     * @param p
     *            Feld, das reagiert
     */
    protected void reaktion(Point p) {
        this.reaktionen++;
        this.ausloesungProHoehe[this.spielbrett[p.x][p.y]]++;
        
        // Ueberpruefe, zu welchem Cluster p gehoert:
        for (LinkedList<Point> cluster : this.allCluster)
            if (cluster.contains(p)) {
                // Erhoehe Werte und loesche gefundenen Cluster aus der Liste
                this.ausloesungProClustergroesse[cluster.size()]++;
                this.allCluster.remove(cluster);
                break;
            }
    }
}
