package de.vdwals.kettenreaktion.statistik.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.io.FileHandler;
import de.vdwals.kettenreaktion.statistik.Karte;
import de.vdwals.kettenreaktion.statistik.Spieler;

/**
 * Klasse zur Dateikommunikation der Statistik.
 * 
 * @author Dennis van der Wals
 */
public class StatistikIO extends FileHandler {
    private LinkedList<Spieler> spieler;
    private LinkedList<Karte>   karten;
    private File                statistikFile;
    
    /**
     * Standardkonstruktor.
     */
    public StatistikIO() {
        super();
        File ort = new File("data");
        ort.mkdir();
        this.fileExtension = ".ksf"; //$NON-NLS-1$
        this.statistikFile = new File("data/stats" + this.fileExtension); //$NON-NLS-1$
        this.spieler = new LinkedList<Spieler>();
        try {
            if (!this.statistikFile.createNewFile())
                this.load();
        } catch (Exception e) {
            BugReport.recordBug(e);
        }
    }
    
    @Override
    public byte[] generateData() {
        // TODO Auto-generated method stub
        return null;
    }
    
    /**
     * Gibt die Karten zurueck.
     * 
     * @return
     */
    public LinkedList<Karte> getKarten() {
        return this.karten;
    }
    
    /**
     * Gibt die Statistiken zurueck.
     * 
     * @return Statistiken
     */
    public LinkedList<Spieler> getSpieler() {
        return this.spieler;
    }
    
    @Override
    public void load() throws Exception {
        this.spieler = new LinkedList<Spieler>();
        this.setKarten(new LinkedList<Karte>());
        
        // Datei einlesen
        byte[] input = new byte[(int) this.statistikFile.length()];
        FileInputStream fis = new FileInputStream(this.statistikFile);
        fis.read(input);
        fis.close();
        
        if (input.length >= 1) {
            
            // Fuer jeden eintrag eine Klasse erstellen und speichern:
            byte counter = 0;
            byte version = input[counter++];
            if (version == this.DB_VERSION) {
                
                @SuppressWarnings("unused")
                int anzahlDerSpiele = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
                
                while (input[counter] != -1) {
                    
                    byte[] tmpName = new byte[input.length - counter];
                    byte tmpCounter = 0;
                    while (input[counter] != 0)
                        tmpName[tmpCounter++] = input[counter++];
                    counter++;
                    String name = new String(ArrOps.getPartOfArray(tmpName, 0,
                            tmpCounter));
                    
                    int spiele = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(
                            input, counter, counter += this.INTEGER_LAENGE));
                    int gewonnen = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(
                            input, counter, counter += this.INTEGER_LAENGE));
                    int gegenKi = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(
                            input, counter, counter += this.INTEGER_LAENGE));
                    int gewonnenGegenKi = ArrOps.ByteArrayToInt(ArrOps
                            .getPartOfArray(input, counter,
                                    counter += this.INTEGER_LAENGE));
                    int highScore = ArrOps.ByteArrayToInt(ArrOps
                            .getPartOfArray(input, counter,
                                    counter += this.INTEGER_LAENGE));
                    int highScoreReaction = ArrOps.ByteArrayToInt(ArrOps
                            .getPartOfArray(input, counter,
                                    counter += this.INTEGER_LAENGE));
                    
                    this.spieler.add(new Spieler(name, spiele, gegenKi,
                            gewonnen, gewonnenGegenKi, highScore,
                            highScoreReaction));
                    
                }
            }
        }
    }
    
    @Override
    public void save() throws IOException {
        byte[] output = new byte[1048576];
        int counter = 0;
        
        output[counter++] = this.DB_VERSION;
        
        for (byte i : ArrOps.IntToByteArray(0))
            output[counter++] = i;
        
        for (Spieler statistik : this.spieler) {
            
            for (byte bs : statistik.getName().getBytes())
                output[counter++] = bs;
            output[counter++] = 0;
            
            for (byte i : ArrOps.IntToByteArray(statistik.getSpiele()))
                output[counter++] = i;
            
            for (byte i : ArrOps.IntToByteArray(statistik.getGewonnen()))
                output[counter++] = i;
            
            for (byte i : ArrOps.IntToByteArray(statistik.getSpieleGegenKi()))
                output[counter++] = i;
            
            for (byte i : ArrOps.IntToByteArray(statistik.getGewonnenGegenKi()))
                output[counter++] = i;
            
            for (byte i : ArrOps.IntToByteArray(statistik.getHighScore()))
                output[counter++] = i;
            
            for (byte i : ArrOps.IntToByteArray(statistik.getHighReaction()))
                output[counter++] = i;
            
            // TODO Laengenueberpruefung einbinden
        }
        output[counter++] = -1;
        
        FileOutputStream fos = new FileOutputStream(this.statistikFile);
        fos.write(ArrOps.getPartOfArray(output, 0, counter));
        fos.close();
    }
    
    /**
     * Setzt die Karten.
     * 
     * @param karten
     */
    public void setKarten(LinkedList<Karte> karten) {
        this.karten = karten;
    }
    
    /**
     * Setzt die Spieler.
     * 
     * @param spieler
     */
    public void setSpieler(LinkedList<Spieler> statistiken) {
        this.spieler = statistiken;
    }
    
}
