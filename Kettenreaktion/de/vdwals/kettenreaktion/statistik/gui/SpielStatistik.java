package de.vdwals.kettenreaktion.statistik.gui;

import javax.swing.JPanel;

/**
 * Klasse zur Darstellung einer Spielstatistik.
 * 
 * @author Dennis van der Wals
 * 
 */
public class SpielStatistik extends JPanel {
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = 2287078314638224758L;
    
}
