package de.vdwals.kettenreaktion.statistik.gui.implement.simpleGraph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.LineMetrics;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;

import javax.swing.JPanel;

/**
 * Klasse zum Plotten eines Graphen mit verschiedenen Einstellungen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Graph extends JPanel {
    
    /**
     * Default Serial-ID
     */
    private static final long   serialVersionUID = 1L;
    
    // Darstellungseigenschaften
    private final int           pixelAbstand     = 10;
    /**
     * Darstellungsgrenzen: 0 = untere Grenze; 1 = obere Grenze; 2 = linke
     * Grenze; 3 = rechte Grenze
     */
    private double              grenzen[];
    private int                 anzahlHistogramme;
    private double              scaleX, scaleY;
    private String              xAxis, yAxis, title;
    private boolean             yHelpLine, xHelpLine, manuelleGrenze[];
    
    // Zu plottende Werte
    private LinkedList<DataSet> datensaetze;
    
    /**
     * Standardkonstruktor.
     */
    public Graph(String title) {
        super();
        this.setDoubleBuffered(true);
        this.xAxis = "x-Achse";
        this.yAxis = "y-Achse";
        this.datensaetze = new LinkedList<DataSet>();
        this.grenzen = new double[4];
        this.manuelleGrenze = new boolean[4];
        this.setTitle(title);
    }
    
    /**
     * Standardkonstruktor. Erwartet Ploteinstellungen.
     * 
     * @param xAxis
     *            x-Achsen-Beschriftung
     * @param yAxis
     *            y-Achsen-Beschriftung
     */
    public Graph(String title, String xAxis, String yAxis) {
        super();
        this.setDoubleBuffered(true);
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.datensaetze = new LinkedList<DataSet>();
        this.grenzen = new double[4];
        this.manuelleGrenze = new boolean[4];
        this.setTitle(title);
    }
    
    /**
     * Fuegt eine Wertereihe zum Zeichnen hinzu.
     * 
     * @param x
     *            x-Werte
     * @param y
     *            y-Werte
     * @param c
     *            Graphfarbe - Schwarz, wenn nichts uebergeben
     */
    public void addWerte(DataSet daten) {
        this.datensaetze.add(daten);
        this.recalcDimensionen();
    }
    
    /**
     * Berechnet die Histogrammbloecke, die zu zeichnen sind.
     * 
     * @param daten
     *            Zu zeichnende Werte
     * @param bloeckeGesamt
     *            Anzahl aller Datensaetze mit Histogrammstil.
     * @param blockNummer
     *            Anzahl aller vorangehender Datensaetze mit Histogrammstil.
     * @return LinkedList<Rectangle2D>
     */
    private LinkedList<Rectangle2D> calcBlocks(DataSet daten,
            int bloeckeGesamt, int blockNummer) {
        LinkedList<Rectangle2D> bloecke = new LinkedList<Rectangle2D>();
        
        return bloecke;
    }
    
    /**
     * Berechnet die zu zeichnenen Kreuze.
     * 
     * @param daten
     *            Zu zeichnende Werte
     * @return LinkedList<Line2D>
     */
    private LinkedList<Line2D> calcCross(DataSet daten) {
        LinkedList<Line2D> kreuze = new LinkedList<Line2D>();
        
        return kreuze;
    }
    
    /**
     * Berechnet die zu zeichnenden Punkte.
     * 
     * @param daten
     *            Zu zeichnende Werte
     * @return LinkedList<Ellipse2D>
     */
    private LinkedList<Ellipse2D> calcDots(DataSet daten) {
        LinkedList<Ellipse2D> punkte = new LinkedList<Ellipse2D>();
        for (int i = daten.getStartPunkt(); i < daten.getEndPunkt(); i++) {
            Point2D wert = new Point2D.Double(daten.getX()[i], daten.getY()[i]);
            
            // Ist der Punkt darstellbar auf der Oberflaeche
            if (this.getSichtbareGraphflaeche().contains(wert)) {
                wert = this.graphCoordToPanelCoord(wert);
                punkte.add(new Ellipse2D.Double(wert.getX() - 2,
                        wert.getY() - 2, 4, 4));
            }
        }
        return punkte;
    }
    
    /**
     * Berechnet die zu zeichnenden Linien.
     * 
     * @param daten
     *            Zu zeichnende Werte
     * 
     * @return LinkedList<Line2D> die gezeichnet werden muessen.
     */
    private LinkedList<Line2D> calcLines(DataSet daten) {
        LinkedList<Line2D> linien = new LinkedList<Line2D>();
        
        // Ersten Punkt bestimmen, der im Graphen liegt.
        int i = daten.getStartPunkt();
        while (daten.getX()[i] < this.grenzen[2])
            i++;
        
        // Punkt fuer weiterarbeit speichern
        daten.setStartPunkt(i);
        
        // Alle Punkte durchgehen, bis man den Graphen verlaesst
        while ((i < daten.getX().length)
                && (daten.getX()[i] <= this.grenzen[3])) {
            Point2D startpunkt = new Point2D.Double(daten.getX()[i - 1],
                    daten.getY()[i - 1]);
            Point2D endpunkt = new Point2D.Double(daten.getX()[i],
                    daten.getY()[i]);
            Line2D linie = new Line2D.Double(startpunkt, endpunkt);
            
            // Ueberpruefen, ob von der Linie etwas sichtbar waere
            if (linie.intersects(this.getSichtbareGraphflaeche())) {
                
                // Falls noetig, Schnittpunkt mit dem linken Rand bestimmen.
                if (linie.intersectsLine(this.grenzen[2], this.grenzen[0],
                        this.grenzen[2], this.grenzen[1])) {
                    Line2D tmp = new Line2D.Double(this.grenzen[2],
                            this.grenzen[0], this.grenzen[2], this.grenzen[1]);
                    
                    Point2D neu = this.intersectLines(tmp, linie);
                    if (!startpunkt.equals(neu))
                        startpunkt = neu;
                    
                    linie.setLine(startpunkt, endpunkt);
                } else if (linie.intersectsLine(this.grenzen[3],
                        this.grenzen[0], this.grenzen[3], this.grenzen[1])) {
                    /*
                     * sonst pruefen, ob rechter Rand geschnitten wird und Punkt
                     * bestimmen.
                     */
                    Line2D tmp = new Line2D.Double(this.grenzen[3],
                            this.grenzen[0], this.grenzen[3], this.grenzen[1]);
                    
                    Point2D neu = this.intersectLines(tmp, linie);
                    if (!endpunkt.equals(neu))
                        endpunkt = neu;
                    
                    linie.setLine(startpunkt, endpunkt);
                }
                
                // Falls noetig, Schnittpunkt mit oberen Rand bestimmen
                if (linie.intersectsLine(this.grenzen[2], this.grenzen[1],
                        this.grenzen[3], this.grenzen[1])) {
                    Line2D tmp = new Line2D.Double(this.grenzen[2],
                            this.grenzen[1], this.grenzen[3], this.grenzen[1]);
                    
                    if (startpunkt.getY() > this.grenzen[1])
                        // Kommt von Oben
                        startpunkt = this.intersectLines(tmp, linie);
                    else if (endpunkt.getY() > this.grenzen[1])
                        // oder geht nacht Oben
                        endpunkt = this.intersectLines(tmp, linie);
                    
                    linie.setLine(startpunkt, endpunkt);
                    
                } else if (linie.intersectsLine(this.grenzen[2],
                        this.grenzen[0], this.grenzen[3], this.grenzen[0])) {
                    /*
                     * sonst pruefen, ob unterer Rand geschnitten wird und Punkt
                     * bestimmen.
                     */
                    Line2D tmp = new Line2D.Double(this.grenzen[2],
                            this.grenzen[0], this.grenzen[3], this.grenzen[0]);
                    
                    if (endpunkt.getY() < this.grenzen[0])
                        // Kommt von Oben
                        endpunkt = this.intersectLines(tmp, linie);
                    else if (startpunkt.getY() < this.grenzen[0])
                        // oder geht nacht Oben
                        startpunkt = this.intersectLines(tmp, linie);
                    
                    linie.setLine(startpunkt, endpunkt);
                }
                
                // Koordinaten umrechnen
                linie = new Line2D.Double(this.graphCoordToPanelCoord(linie
                        .getP1()), this.graphCoordToPanelCoord(linie.getP2()));
                
                linien.add(linie);
            }
            
            // Punkt fuer weiterarbeit speichern
            daten.setEndPunkt(++i);
        }
        
        return linien;
    }
    
    /**
     * Berechnet die zu zeichnenen Dreiecke.
     * 
     * @param daten
     *            Zu zeichnende Werte
     * @return LinkedList<Line2D>
     */
    private LinkedList<Line2D> calcTri(DataSet daten) {
        LinkedList<Line2D> dreiecke = new LinkedList<Line2D>();
        
        return dreiecke;
    }
    
    /**
     * Entfernt die Wertepaare i aus dem Graphen.
     * 
     * @param i
     *            Index des zu entfernenden Wertepaares
     */
    public void delWerte(int i) {
        if ((i >= 0) && (i < this.datensaetze.size())) {
            this.datensaetze.remove(i);
            this.recalcDimensionen();
        }
    }
    
    /**
     * @return the datensaetze
     */
    public LinkedList<DataSet> getDatensaetze() {
        return this.datensaetze;
    }
    
    /**
     * Gibt ein Rechteck zurueck, welches die sichtbare Flaeche des Graphen
     * repraesentiert.
     * 
     * @return Rectangle2D, in dem jedes sichtbare Objekt liegt.
     */
    private Rectangle2D getSichtbareGraphflaeche() {
        return new Rectangle2D.Double(this.grenzen[2], this.grenzen[0],
                this.grenzen[3] - this.grenzen[2], this.grenzen[1]
                        - this.grenzen[0]);
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return this.title;
    }
    
    /**
     * @return the xAxis
     */
    public String getxAxis() {
        return this.xAxis;
    }
    
    /**
     * @return the xMax
     */
    public double getxMax() {
        return this.grenzen[3];
    }
    
    /**
     * @return the xMin
     */
    public double getxMin() {
        return this.grenzen[2];
    }
    
    /**
     * @return the yAxis
     */
    public String getyAxis() {
        return this.yAxis;
    }
    
    /**
     * @return the yMax
     */
    public double getyMax() {
        return this.grenzen[1];
    }
    
    /**
     * @return the yMin
     */
    public double getyMin() {
        return this.grenzen[0];
    }
    
    /**
     * Berechnet einen neuen Punkt mit den Panelkoordinaten aus den bekannten
     * Grenzen der Darstellung und den Koordinaten im Graphen.
     * 
     * @param graphPunkt
     *            Umzurechnender Punkt.
     * @return neuer Punkt mit direkt zu zeichnenden Koordinaten
     */
    private Point2D graphCoordToPanelCoord(Point2D graphPunkt) {
        // Anwenden der Skallierung
        double xNeu = graphPunkt.getX() * this.scaleX;
        double yNeu = graphPunkt.getY() * this.scaleY;
        
        // Nullpunktverschiebung der x-Achse
        xNeu += this.pixelAbstand;
        // Nullpunktverschiebung der y-Achse;
        yNeu += this.pixelAbstand;
        
        // Verschiebung wenn Graphenursprung != 0
        xNeu -= this.grenzen[2];
        yNeu -= this.grenzen[0];
        
        /*
         * Negierung der y-Achse, damit dies wie gewohnt unten und nicht oben
         * ist.
         */
        yNeu = this.getHeight() - yNeu;
        
        return new Point2D.Double(xNeu, yNeu);
    }
    
    /**
     * Gibt den Schnittpunkt zweier Linien zurueck.
     * 
     * @param achse
     * @param schnittlinie
     * @return Point2D Schnittpunkt
     */
    public Point2D.Double intersectLines(Line2D achse, Line2D schnittlinie) {
        
        double xSchnitt = (((achse.getX1() * achse.getY2()) - (achse.getY1() * achse
                .getX2())) * (schnittlinie.getX1() - schnittlinie.getX2()))
                - ((achse.getX1() - achse.getX2()) * ((schnittlinie.getX1() * schnittlinie
                        .getY2()) - (schnittlinie.getY1() * schnittlinie
                        .getX2())));
        double ySchnitt = (((achse.getX1() * achse.getY2()) - (achse.getY1() * achse
                .getX2())) * (schnittlinie.getY1() - schnittlinie.getY2()))
                - ((achse.getY1() - achse.getY2()) * ((schnittlinie.getX1() * schnittlinie
                        .getY2()) - (schnittlinie.getY1() * schnittlinie
                        .getX2())));
        
        double n = ((achse.getX1() - achse.getX2()) * (schnittlinie.getY1() - schnittlinie
                .getY2()))
                - ((achse.getY1() - achse.getY2()) * (schnittlinie.getX1() - schnittlinie
                        .getX2()));
        
        return new Point2D.Double(xSchnitt / n, ySchnitt / n);
    }
    
    /**
     * @return the manuelXMax
     */
    public boolean isManuelXMax() {
        return this.manuelleGrenze[3];
    }
    
    /**
     * @return the manuelxMin
     */
    public boolean isManuelxMin() {
        return this.manuelleGrenze[2];
    }
    
    /**
     * @return the manuelYMax
     */
    public boolean isManuelYMax() {
        return this.manuelleGrenze[1];
    }
    
    /**
     * @return the manuelYMin
     */
    public boolean isManuelYMin() {
        return this.manuelleGrenze[0];
    }
    
    /**
     * @return the xHelpLine
     */
    public boolean isxHelpLine() {
        return this.xHelpLine;
    }
    
    /**
     * @return the yHelpLine
     */
    public boolean isyHelpLine() {
        return this.yHelpLine;
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        this.recalcDimensionen();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        
        // Faerbe Hintergrund
        g2.setColor(Color.WHITE);
        g2.fillRect(this.pixelAbstand, this.pixelAbstand, this.getWidth()
                - this.pixelAbstand, this.getHeight() - this.pixelAbstand);
        
        g2.setColor(Color.BLACK);
        
        // Zeichne y-Achse.
        g2.draw(new Line2D.Double(this.pixelAbstand, this.pixelAbstand,
                this.pixelAbstand, this.getHeight() - this.pixelAbstand));
        
        // Zeichne x-Achse.
        g2.draw(new Line2D.Double(this.pixelAbstand, this.getHeight()
                - this.pixelAbstand, this.getWidth() - this.pixelAbstand, this
                .getHeight() - this.pixelAbstand));
        
        // Schreibe Achsenbeschriftung.
        LineMetrics lineM = g2.getFont().getLineMetrics("0",
                g2.getFontRenderContext());
        double buchstabenHoehe = lineM.getAscent() + lineM.getDescent();
        
        // y-Achsen Beschriftung.
        double yPosition = this.pixelAbstand
                + (((this.getHeight() - (2 * this.pixelAbstand)) - (this.yAxis
                        .length() * buchstabenHoehe)) / 2) + lineM.getAscent();
        double xPosition = 0;
        for (int i = 0; i < this.yAxis.length(); i++) {
            String buchstabe = String.valueOf(this.yAxis.charAt(i));
            double buchstabenBreite = (float) g2.getFont()
                    .getStringBounds(buchstabe, g2.getFontRenderContext())
                    .getWidth();
            xPosition = (this.pixelAbstand - buchstabenBreite) / 2;
            g2.drawString(buchstabe, (int) xPosition, (int) yPosition);
            yPosition += buchstabenHoehe;
        }
        
        // x-Achsen Beschriftung.
        yPosition = (this.getHeight() - this.pixelAbstand)
                + ((this.pixelAbstand - buchstabenHoehe) / 2)
                + lineM.getAscent();
        double wortbreite = (float) g2.getFont()
                .getStringBounds(this.xAxis, g2.getFontRenderContext())
                .getWidth();
        xPosition = (this.getWidth() - wortbreite) / 2;
        g2.drawString(this.xAxis, (int) xPosition, (int) yPosition);
        
        // Graphentitel
        yPosition = ((this.pixelAbstand - buchstabenHoehe) / 2)
                + lineM.getAscent();
        g2.drawString(this.title, (int) xPosition, (int) yPosition);
        
        // skallierung berechnen
        this.scaleY = (this.getHeight() - (2 * this.pixelAbstand))
                / (this.grenzen[1] - this.grenzen[0]);
        this.scaleX = (this.getWidth() - (2 * this.pixelAbstand))
                / (this.grenzen[3] - this.grenzen[2]);
        
        int aktuellesHistogramm = 0;
        
        // Datensaetze zeichnen
        for (DataSet datensatz : this.datensaetze) {
            g2.setPaint(datensatz.getStyle().getFarbe());
            
            // Linien einzeichnen
            switch (datensatz.getStyle().getLineStyle()) {
            
                case DataSetStyle.DIAGRAMM_TYP_LINE:
                    LinkedList<Line2D> linien = this.calcLines(datensatz);
                    for (Line2D line2d : linien)
                        g2.draw(line2d);
                    break;
                
                case DataSetStyle.DIAGRAMM_TYP_HISTOGRAM:
                    LinkedList<Rectangle2D> bloecke = this.calcBlocks(
                            datensatz, this.anzahlHistogramme,
                            aktuellesHistogramm++);
                    for (Rectangle2D rectangle2d : bloecke)
                        g2.fill(rectangle2d);
                default:
                    break;
            }
            
            // Punkte zeichnen
            switch (datensatz.getStyle().getPointStyle()) {
            
                case DataSetStyle.POINT_STYLE_DOT:
                    LinkedList<Ellipse2D> punkte = this.calcDots(datensatz);
                    for (Ellipse2D ellipse2d : punkte)
                        g2.fill(ellipse2d);
                    break;
                
                case DataSetStyle.POINT_STYLE_CROSS:
                    LinkedList<Line2D> kreuze = this.calcCross(datensatz);
                    for (Line2D line2d : kreuze)
                        g2.draw(line2d);
                    break;
                
                case DataSetStyle.POINT_STYLE_TRIANGLE:
                    LinkedList<Line2D> dreiecke = this.calcTri(datensatz);
                    for (Line2D line2d : dreiecke)
                        g2.draw(line2d);
                    break;
                default:
                    break;
            }
        }
        
        // for (int j = 0; j < this.datensaetze.size(); j++) {
        // double[] xWerte = this.datensaetze.get(j).getX();
        // double[] yWerte = this.datensaetze.get(j).getY();
        // // Zeichne Linien.
        // double xInc = (double) (getWidth() - (2 * this.pixelAbstand))
        // / (yWerte.length - 1);
        // double scale = (getHeight() - (2 * this.pixelAbstand))
        // / getMax(yWerte);
        // g.setPaint(Color.green.darker());
        // for (int i = 0; i < (yWerte.length - 1); i++) {
        // double x1 = this.pixelAbstand + (i * xInc);
        // double y1 = getHeight() - this.pixelAbstand
        // - (scale * yWerte[i]);
        // double x2 = this.pixelAbstand + ((i + 1) * xInc);
        // double y2 = getHeight() - this.pixelAbstand
        // - (scale * yWerte[i + 1]);
        // g.draw(new Line2D.Double(x1, y1, x2, y2));
        // }
        // // Mark data points.
        // g.setPaint(Color.red);
        // for (int i = 0; i < yWerte.length; i++) {
        // double x = this.pixelAbstand + (i * xInc);
        // double y = getHeight() - this.pixelAbstand
        // - (scale * yWerte[i]);
        // g.fill(new Ellipse2D.Double(x - 2, y - 2, 4, 4));
        // }
        // }
    }
    
    /**
     * Berechnet Darstellungsrelevante Eigenschaften neu.
     */
    private void recalcDimensionen() {
        // Grenzen bestimmen
        for (DataSet datensatz : this.datensaetze) {
            double[] grenzen = datensatz.getGrenzen();
            for (int i = 0; i < grenzen.length; i++)
                if (!this.manuelleGrenze[i])
                    if ((i % 2) == 0)
                        this.grenzen[i] = (this.grenzen[i] < grenzen[i]) ? this.grenzen[i]
                                : grenzen[i];
                    else
                        /*
                         * Grenzen nach Oben und nach Rechts um 10% erhoehen,
                         * damit auch Daten auf den Achsen sichtbar sind
                         */
                        this.grenzen[i] = (this.grenzen[i] > grenzen[i]) ? this.grenzen[i]
                                : grenzen[i] * 1.1;
        }
        
        // Histogramme zaehlen
        this.anzahlHistogramme = 0;
        
        for (DataSet datensatz : this.datensaetze)
            if (datensatz.getStyle().getLineStyle() == DataSetStyle.DIAGRAMM_TYP_HISTOGRAM)
                this.anzahlHistogramme++;
    }
    
    /**
     * @param datensaetze
     *            the datensaetze to set
     */
    public void setDatensaetze(LinkedList<DataSet> datensaetze) {
        this.datensaetze = datensaetze;
    }
    
    /**
     * @param manuelXMax
     *            the manuelXMax to set
     */
    public void setManuelXMax(boolean manuelXMax) {
        this.manuelleGrenze[3] = manuelXMax;
    }
    
    /**
     * @param manuelxMin
     *            the manuelxMin to set
     */
    public void setManuelxMin(boolean manuelxMin) {
        this.manuelleGrenze[2] = manuelxMin;
    }
    
    /**
     * @param manuelYMax
     *            the manuelYMax to set
     */
    public void setManuelYMax(boolean manuelYMax) {
        this.manuelleGrenze[1] = manuelYMax;
    }
    
    /**
     * @param manuelYMin
     *            the manuelYMin to set
     */
    public void setManuelYMin(boolean manuelYMin) {
        this.manuelleGrenze[0] = manuelYMin;
    }
    
    /**
     * @param title
     *            the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    /**
     * @param xAxis
     *            the xAxis to set
     */
    public void setxAxis(String xAxis) {
        this.xAxis = xAxis;
    }
    
    /**
     * @param xHelpLine
     *            the xHelpLine to set
     */
    public void setxHelpLine(boolean xHelpLine) {
        this.xHelpLine = xHelpLine;
    }
    
    /**
     * @param xMax
     *            the xMax to set
     */
    public void setxMax(int xMax) {
        this.grenzen[3] = xMax;
        this.setManuelXMax(true);
    }
    
    /**
     * @param xMin
     *            the xMin to set
     */
    public void setxMin(int xMin) {
        this.grenzen[2] = xMin;
        this.setManuelxMin(true);
    }
    
    /**
     * @param yAxis
     *            the yAxis to set
     */
    public void setyAxis(String yAxis) {
        this.yAxis = yAxis;
    }
    
    /**
     * @param yHelpLine
     *            the yHelpLine to set
     */
    public void setyHelpLine(boolean yHelpLine) {
        this.yHelpLine = yHelpLine;
    }
    
    /**
     * @param yMax
     *            the yMax to set
     */
    public void setyMax(int yMax) {
        this.grenzen[1] = yMax;
        this.setManuelYMax(true);
    }
    
    /**
     * @param yMin
     *            the yMin to set
     */
    public void setyMin(int yMin) {
        this.grenzen[0] = yMin;
        this.setManuelYMin(true);
    }
    
}
