package de.vdwals.kettenreaktion.statistik.gui.implement.simpleGraph;

import java.awt.Color;

/**
 * Stil des zu plottenden Datensatzes
 * 
 * @author Dennis van der Wals
 * 
 */
public class DataSetStyle {
    public static final int DIAGRAMM_TYP_HISTOGRAM = 1, DIAGRAMM_TYP_LINE = 0,
            DIAGRAMM_TYP_POINTS = 2, POINT_STYLE_DOT = 0,
            POINT_STYLE_CROSS = 1, POINT_STYLE_TRIANGLE = 2;
    private int             lineStyle, pointStyle;
    private Color           farbe;
    
    /**
     * Standardkonstruktor. Diagrammtyp: Linien, Punkttyp: Punkte, Farbe:
     * Schwarz
     */
    public DataSetStyle() {
        this.lineStyle = DIAGRAMM_TYP_LINE;
        this.pointStyle = POINT_STYLE_DOT;
        this.farbe = Color.BLACK;
    }
    
    /**
     * Standardkonstruktor. Diagrammtyp: Linien, Punkttyp: Punkte
     */
    public DataSetStyle(Color c) {
        this.lineStyle = DIAGRAMM_TYP_LINE;
        this.pointStyle = POINT_STYLE_DOT;
        this.farbe = c;
    }
    
    /**
     * @return the farbe
     */
    public Color getFarbe() {
        return this.farbe;
    }
    
    /**
     * @return the lineStyle
     */
    public int getLineStyle() {
        return this.lineStyle;
    }
    
    /**
     * @return the pointStyle
     */
    public int getPointStyle() {
        return this.pointStyle;
    }
    
    /**
     * @param farbe
     *            the farbe to set
     */
    public void setFarbe(Color farbe) {
        this.farbe = farbe;
    }
    
    /**
     * @param lineStyle
     *            the lineStyle to set
     */
    public void setLineStyle(int lineStyle) {
        if ((lineStyle >= 0) && (lineStyle <= 2))
            this.lineStyle = lineStyle;
    }
    
    /**
     * @param pointStyle
     *            the pointStyle to set
     */
    public void setPointStyle(int pointStyle) {
        if ((pointStyle >= 0) && (pointStyle <= 2))
            this.pointStyle = pointStyle;
    }
}
