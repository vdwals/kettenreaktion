package de.vdwals.kettenreaktion.statistik.gui.implement.simpleGraph;

import de.vdwals.kettenreaktion.implement.ArrOps;

/**
 * Klasse fuer einen zu setzenden Datensatz.
 * 
 * @author Dennis van der Wals
 * 
 */
public class DataSet {
    private double       x[], y[], grenzen[];
    private int          startPunkt, endPunkt;
    private DataSetStyle style;
    private String       name;
    
    /**
     * Erzeugt einen neuen Datensatz im Standardstil.
     * 
     * @param x
     *            x-Werte
     * @param y
     *            y-Werte
     * @param name
     *            Name des Datensatzes
     */
    public DataSet(double[] x, double[] y, String name) {
        this.setData(x, y);
        this.style = new DataSetStyle();
        this.name = name;
        this.startPunkt = 1;
        this.endPunkt = x.length - 1;
        
    }
    
    /**
     * Erzeugt einen neuen Datensatz im uebergebenen Stil.
     * 
     * @param x
     *            x-Werte
     * @param y
     *            y-Werte
     * @param name
     *            Name des Datensatzes
     * @param style
     *            Stil des Datensatzes
     */
    public DataSet(double[] x, double[] y, String name, DataSetStyle style) {
        this.setData(x, y);
        this.style = style;
        this.name = name;
        this.startPunkt = 1;
        this.endPunkt = x.length - 1;
    }
    
    /**
     * Erzeugt einen neuen Datensatz im Standardstil.
     * 
     * @param name
     *            Name des Datensatzes
     */
    public DataSet(String name) {
        this.setData(new double[2], new double[2]);
        this.style = new DataSetStyle();
        this.name = name;
        this.startPunkt = 1;
        this.endPunkt = this.x.length - 1;
    }
    
    /**
     * Erzeugt einen neuen Datensatz im Standardstil.
     * 
     * @param name
     *            Name des Datensatzes
     * @param style
     *            Stil des Datensatzes
     */
    public DataSet(String name, DataSetStyle stil) {
        this.setData(new double[2], new double[2]);
        this.style = stil;
        this.name = name;
        this.startPunkt = 1;
        this.endPunkt = this.x.length - 1;
    }
    
    /**
     * Fuegt dem Datensatz ein Wertepaar an
     * 
     * @param x
     *            x-Wert
     * @param y
     *            y-Wert
     */
    public void addData(double x, double y) {
        double[] xNeu = new double[this.x.length + 1];
        double[] yNeu = new double[this.y.length + 1];
        for (int i = 0; i < this.x.length; i++) {
            xNeu[i] = this.x[i];
            yNeu[i] = this.y[i];
        }
        xNeu[xNeu.length - 1] = x;
        yNeu[yNeu.length - 1] = y;
        this.x = xNeu;
        this.y = yNeu;
        if (this.grenzen != null) {
            if (x > this.grenzen[3])
                this.grenzen[3] = x;
            if (y > this.grenzen[1])
                this.grenzen[1] = y;
        } else
            this.grenzen = this.getGrenzen();
    }
    
    /**
     * Gibt den zuletzt festgestellten letzten Darstellbaren Punkt zurueck.
     * 
     * @return the endPunkt
     */
    public int getEndPunkt() {
        return this.endPunkt;
    }
    
    /**
     * Berechnet die Grenzen zur Darstellung der Daten.
     * 
     * @return Integerarray mit 0 = yMin; 1 = yMax; 2 = xMin; 3 = xMax
     */
    public double[] getGrenzen() {
        if (this.grenzen == null) {
            this.sortDataX();
            this.grenzen = new double[4];
            int[] extrem = ArrOps.getMaxMin(this.y);
            this.grenzen[0] = this.y[extrem[0]];
            this.grenzen[1] = this.y[extrem[1]];
            this.grenzen[2] = this.x[0];
            this.grenzen[3] = this.x[this.x.length - 1];
        }
        return this.grenzen;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Gibt den zuletzt festgestellten ersten Darstellbaren Punkt zurueck.
     * 
     * @return the startPunkt
     */
    public int getStartPunkt() {
        return this.startPunkt;
    }
    
    /**
     * @return the style
     */
    public DataSetStyle getStyle() {
        return this.style;
    }
    
    /**
     * @return the x
     */
    public double[] getX() {
        return this.x;
    }
    
    /**
     * @return the y
     */
    public double[] getY() {
        return this.y;
    }
    
    /**
     * Setzt die uebergebenen Daten, sofern sie Daten enthalten und gleich lang
     * sind.
     * 
     * @param x
     *            x-Werte
     * @param y
     *            y-Werte
     */
    public void setData(double[] x, double[] y) {
        if (x.length <= 1)
            throw new IllegalArgumentException(
                    "Es muessen mindestens 2 Wertepaare uebergeben werden!");
        else if (x.length != y.length)
            throw new IllegalArgumentException(
                    "Die x- und y-Werte muessen Zahlenmaessig uebereinstimmen!");
        else {
            this.x = x;
            this.y = y;
            this.grenzen = null;
            this.grenzen = this.getGrenzen();
        }
    }
    
    /**
     * Setzt den festgestellten letzten Darstellbaren Punkt zurueck.
     * 
     * @param endPunkt
     *            the endPunkt to set
     */
    public void setEndPunkt(int endPunkt) {
        this.endPunkt = endPunkt;
    }
    
    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Setzt den festgestellten ersten Darstellbaren Punkt zurueck.
     * 
     * @param startPunkt
     *            the startPunkt to set
     */
    public void setStartPunkt(int startPunkt) {
        this.startPunkt = startPunkt;
    }
    
    /**
     * @param style
     *            the style to set
     */
    public void setStyle(DataSetStyle style) {
        this.style = style;
    }
    
    /**
     * @param x
     *            the x to set
     */
    public void setX(double[] x) {
        this.setData(x, this.y);
    }
    
    /**
     * @param y
     *            the y to set
     */
    public void setY(double[] y) {
        this.setData(this.x, y);
    }
    
    /**
     * Sortiert den Datensatz nach dem uebergebenem Reihenfolgearray, wobei
     * reihenfolge[0] den Index der Zahl enthaelt, die an 0. Stelle stehen soll.
     * 
     * @param reihenfolge
     */
    private void sortData(int[] reihenfolge) {
        double[] neuX = new double[this.x.length];
        double[] neuY = new double[this.y.length];
        for (int i = 0; i < reihenfolge.length; i++) {
            neuX[i] = this.x[reihenfolge[i]];
            neuY[i] = this.y[reihenfolge[i]];
        }
        this.x = neuX;
        this.y = neuY;
    }
    
    /**
     * Sortiert den Datensatz unter Erhaltung der Paare nach x-Werten
     */
    public void sortDataX() {
        this.sortData(ArrOps.indexSort(this.x));
    }
    
    /**
     * Sortiert den Datensatz unter Erhaltung der Paare nach y-Werten
     */
    public void sortDataY() {
        this.sortData(ArrOps.indexSort(this.y));
    }
}
