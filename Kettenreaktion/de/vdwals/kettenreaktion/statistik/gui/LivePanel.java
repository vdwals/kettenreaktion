package de.vdwals.kettenreaktion.statistik.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.vdwals.kettenreaktion.statistik.gui.implement.simpleGraph.DataSet;
import de.vdwals.kettenreaktion.statistik.gui.implement.simpleGraph.Graph;

/**
 * Klasse zum Darstellen einer Reihe von Graphen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class LivePanel extends JFrame {
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = 1731148188962307433L;
    
    // DEBUG
    public static void main(String[] args) throws InterruptedException {
        
        // zufaellige Datensaetze erzeugen.
        int datensatzMenge = (int) (Math.random() * 10);
        datensatzMenge = 3;
        DataSet[][] graphenDatensaetze = new DataSet[datensatzMenge][];
        String[] titel = new String[datensatzMenge];
        
        for (int i = 0; i < graphenDatensaetze.length; i++) {
            
            int mengeDerDatensaetzeProGraph = (int) (Math.random() * 10);
            graphenDatensaetze[i] = new DataSet[mengeDerDatensaetzeProGraph];
            titel[i] = "" + i;
            
            for (int j = 0; j < graphenDatensaetze[i].length; j++) {
                
                int anzahlDerDaten = (int) (Math.random() * 10) + 2;
                double[] x = new double[anzahlDerDaten];
                double[] y = new double[anzahlDerDaten];
                
                for (int k = 0; k < x.length; k++) {
                    x[k] = k;
                    y[k] = Math.random() * 100;
                }
                graphenDatensaetze[i][j] = new DataSet(x, y, "");
            }
        }
        System.out.println(datensatzMenge);
        System.out.println(Integer.toHexString(Color.BLACK.getRGB()).substring(
                2, 8));
        System.out.println(Integer.toHexString(Color.GREEN.getRGB()));
        System.out.println(Integer.toHexString(Color.RED.getRGB()));
        
        LivePanel lp = new LivePanel("Test", graphenDatensaetze, titel);
        
        lp.build();
        lp.setVisible(true);
        
        while (true) {
            Thread.sleep(1000);
            for (DataSet[] tmpDatensaetze : graphenDatensaetze)
                for (DataSet dataSet : tmpDatensaetze) {
                    double x = dataSet.getX().length;
                    double y = Math.random() * 100;
                    dataSet.addData(x, y);
                }
            lp.repaint();
        }
    }
    
    private Graph  graphen[];
    private String title;
    
    public JPanel  graphenBorderPanel;
    
    /**
     * Standardkonstruktor.
     * 
     * @param title
     *            Fenstertitel
     * @param datensaetze
     *            2-Dimensionales Datensatzarray, 1-Dimension gibt die Anzahl
     *            der Graphen an, 2-Dimension die Anzahl der Datensaetze im
     *            jeweiligem Graphen.
     * @param datenTitel
     *            Titel der einzelnen Graphen
     */
    public LivePanel(String title, DataSet[][] datensaetze, String[] datenTitel) {
        this.title = title;
        this.graphen = new Graph[datensaetze.length];
        for (int i = 0; i < datensaetze.length; i++) {
            this.graphen[i] = new Graph(datenTitel[i]);
            for (int j = 0; j < datensaetze[i].length; j++)
                this.graphen[i].addWerte(datensaetze[i][j]);
        }
    }
    
    /**
     * Erzeugt das Fenster.
     */
    public void build() {
        this.setLayout(new BorderLayout());
        JLabel titel = new JLabel(this.title);
        
        this.add(titel, BorderLayout.NORTH);
        
        this.graphenBorderPanel = new JPanel(new GridLayout(0, 2));
        
        for (Graph element : this.graphen) {
            StringBuilder tooltip = new StringBuilder("<html><body><h1>"
                    + element.getTitle() + "</h1><table>");
            for (DataSet datensatz : element.getDatensaetze())
                tooltip.append("<tr><td width='20' height='10' bgcolor='#"
                        + Integer.toHexString(
                                datensatz.getStyle().getFarbe().getRGB())
                                .substring(2, 8) + "'></td><td>"
                        + datensatz.getName() + "</td></tr>");
            tooltip.append("</table></body></html>");
            element.setToolTipText(tooltip.toString());
        }
        this.graphenBorderPanel.add(this.graphen[0]);
        this.graphenBorderPanel.add(new JLabel());
        for (int i = 1; i < this.graphen.length; i++)
            this.graphenBorderPanel.add(this.graphen[i]);
        
        this.add(this.graphenBorderPanel, BorderLayout.CENTER);
        this.setSize(400, 600);
    }
    
    @Override
    public void repaint() {
        for (Graph element : this.graphen) {
            element.setPreferredSize(new Dimension(200, 200));
            element.repaint();
        }
        super.repaint();
    }
}
