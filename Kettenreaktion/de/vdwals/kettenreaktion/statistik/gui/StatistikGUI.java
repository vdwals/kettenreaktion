package de.vdwals.kettenreaktion.statistik.gui;

import java.awt.GridLayout;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.LinkedList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JTable;

import de.vdwals.kettenreaktion.gui.language.Messages;
import de.vdwals.kettenreaktion.implement.ArrOps;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.spiel.ki.Strategie;
import de.vdwals.kettenreaktion.statistik.Spieler;
import de.vdwals.kettenreaktion.statistik.io.StatistikIO;

/**
 * Klasse zum verwalten der Spielerstatistiken.
 * 
 * @author Dennis van der Wals
 * 
 */
public class StatistikGUI extends JFrame {
    /**
     * Default Serial_ID
     */
    private static final long   serialVersionUID = -3949975177968013823L;
    
    private LinkedList<Spieler> spielerStatistiken;
    private StatistikIO         io;
    private String[]            title;
    private JTable              tabelle;
    private boolean             changed;
    
    /**
     * Erzeugt einen neuen Manager.
     * 
     * @throws IOException
     *             Exception, falls keine Statistikdatei vorhanden ist
     */
    public StatistikGUI() {
        this.io = new StatistikIO();
        this.spielerStatistiken = this.io.getSpieler();
        
        this.title = new String[7];
        this.title[0] = Messages.getString("StatistikManager.0"); //$NON-NLS-1$
        this.title[1] = Messages.getString("StatistikManager.1"); //$NON-NLS-1$
        this.title[2] = Messages.getString("StatistikManager.2"); //$NON-NLS-1$
        this.title[3] = Messages.getString("StatistikManager.3"); //$NON-NLS-1$
        this.title[4] = Messages.getString("StatistikManager.4"); //$NON-NLS-1$
        this.title[5] = Messages.getString("StatistikManager.5"); //$NON-NLS-1$
        this.title[6] = Messages.getString("StatistikManager.6"); //$NON-NLS-1$
        
        this.changed = true;
        
        this.setLayout(new GridLayout());
        this.setResizable(false);
        
        // In die Mitte positionieren
        this.setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - this
                        .getWidth()) / 2, (Toolkit.getDefaultToolkit()
                        .getScreenSize().height - this.getHeight()) / 2);
        
        // Icon setzen
        try {
            this.setIconImage(ImageIO.read(this.getClass().getResource(
                    "images/radioaktiv.xrt")));
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
    }
    
    /**
     * Oeffnet das Fenster und gibt die Statistiken aus.
     */
    private JTable getTabelle() {
        if (this.tabelle == null)
            this.tabelle = new JTable();
        
        if (this.spielerStatistiken.size() > 0) {
            String[][] inhalt = new String[this.spielerStatistiken.size() + 1][this.title.length];
            
            // Titelzeile einfuegen
            inhalt[0] = this.title;
            
            // Statistik auslesen
            for (int i = 0; i < this.spielerStatistiken.size(); i++) {
                inhalt[i + 1][0] = this.spielerStatistiken.get(i).getName();
                inhalt[i + 1][1] = "" + this.spielerStatistiken.get(i).getSpiele(); //$NON-NLS-1$
                inhalt[i + 1][2] = "" + this.spielerStatistiken.get(i).getGewonnen(); //$NON-NLS-1$
                inhalt[i + 1][3] = "" + this.spielerStatistiken.get(i).getSpieleGegenKi(); //$NON-NLS-1$
                inhalt[i + 1][4] = "" + this.spielerStatistiken.get(i).getGewonnenGegenKi(); //$NON-NLS-1$
                inhalt[i + 1][5] = "" + this.spielerStatistiken.get(i).getHighScore(); //$NON-NLS-1$
                inhalt[i + 1][6] = "" + this.spielerStatistiken.get(i).getHighReaction(); //$NON-NLS-1$
            }
            
            this.tabelle = new JTable(inhalt, this.title);
        }
        return this.tabelle;
    }
    
    /**
     * Stellt das Fenster dar.
     */
    public void openWindow() {
        if (this.changed) {
            if (this.tabelle != null)
                this.remove(this.tabelle);
            this.add(this.getTabelle());
            this.pack();
            this.changed = false;
        }
        this.setVisible(true);
    }
    
    /**
     * Aktuallisiert die Statistiken nach einem Spiel
     * 
     * @param konto
     *            Spielerkonten
     * @param namen
     *            Spielernamen
     * @param maxKettenreaktionen
     *            groesste Kettenreaktion der Spieler
     * @param kis
     *            KI-Liste
     */
    public void SpielBeendet(int[] konto, String[] namen,
            int[] maxKettenreaktionen, Strategie[] kis) {
        LinkedList<Spieler> neueStatistiken = new LinkedList<Spieler>();
        int gewinner = ArrOps.getMax(konto);
        boolean ki = false;
        
        // Jeden Spieler aufnehmen
        for (int i = 0; i < kis.length; i++)
            // KIs ueberspringen
            if (kis[i] != null)
                ki = true;
            else
                neueStatistiken.add(new Spieler(namen[i], (byte) 1,
                        (byte) ((ki) ? 1 : 0),
                        (byte) ((i == gewinner) ? 1 : 0),
                        (byte) (((i == gewinner) && ki) ? 1 : 0), konto[i],
                        maxKettenreaktionen[i]));
        
        LinkedList<Spieler> toAdd = new LinkedList<Spieler>();
        toAdd.addAll(neueStatistiken);
        
        // Neue mit alten vergleichen
        for (Spieler neu : neueStatistiken) {
            
            // Spiel gegen KI eintragen
            if (ki) {
                neu.setSpieleGegenKi((byte) 1);
                if (neu.getGewonnen() == 1)
                    neu.setGewonnenGegenKi((byte) 1);
            }
            
            for (Spieler alt : this.spielerStatistiken)
                // Wenn schon vorhanden, Daten aktualisieren
                if (alt.equals(neu)) {
                    alt.setGewonnen((byte) (alt.getGewonnen() + neu
                            .getGewonnen()));
                    alt.setGewonnenGegenKi((byte) (alt.getGewonnenGegenKi() + neu
                            .getGewonnenGegenKi()));
                    if (alt.getHighReaction() < neu.getHighReaction())
                        alt.setHighReaction(neu.getHighReaction());
                    if (alt.getHighScore() < neu.getHighScore())
                        alt.setHighScore(neu.getHighScore());
                    alt.setSpiele((byte) (alt.getSpiele() + neu.getSpiele()));
                    alt.setSpieleGegenKi((byte) (alt.getSpieleGegenKi() + neu
                            .getSpieleGegenKi()));
                    
                    // Aus neuer Liste loeschen
                    toAdd.remove(neu);
                    
                    break;
                }
        }
        
        this.spielerStatistiken.addAll(toAdd);
        
        // sortieren
        java.util.Collections.sort(this.spielerStatistiken);
        
        this.changed = true;
    }
}
