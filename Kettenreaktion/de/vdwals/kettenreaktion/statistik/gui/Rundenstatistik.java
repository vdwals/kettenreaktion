package de.vdwals.kettenreaktion.statistik.gui;

import javax.swing.JPanel;

/**
 * Klasse zur Darstellung der Rundenstatistiken.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Rundenstatistik extends JPanel {
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = -4608251048841849552L;
    
}
