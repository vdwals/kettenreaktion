package de.vdwals.kettenreaktion.statistik.gui;

import javax.swing.JPanel;

/**
 * Klasse zur Darstellung von Spielerstatistiken.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Spieler extends JPanel {
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = -4812373805251929764L;
    
}
