package de.vdwals.kettenreaktion.editor.gui;

import de.vdwals.kettenreaktion.editor.io.KarteIOData;
import de.vdwals.kettenreaktion.gui.Fenster;
import de.vdwals.kettenreaktion.implement.BugReport;
import de.vdwals.kettenreaktion.optionen.SpielOptionen;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Klasse zum Erstellen von Karten.
 * 
 * @author Dennis van der Wals
 * 
 */
public class KartenEditor extends Fenster implements MouseListener,
        ActionListener, MouseMotionListener {
    // Komponenten
    private JButton           neueKarte, karteLaden, karteSpeichern, invert,
            standard;
    private KartenVorschau    feld;
    private JMenuBar          menu;
    
    // Spieleinstellungen
    private SpielOptionen     optionen;
    
    // Variablen
    private KarteIOData       karteIO;
    private String            karteName;
    private boolean           feldEinfuegen, feldEntfernen, gesichert;
    private Point             letzteAenderung;
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = -3649543401151013100L;
    
    /**
     * Standardkonstruktor.
     * 
     * @param einstellungen
     *            Spieleinstellungen
     */
    public KartenEditor(SpielOptionen einstellungen) {
        super();
        
        this.optionen = einstellungen;
        this.karteIO = new KarteIOData();
        this.karteName = "Standard";
        this.gesichert = true;
        this.letzteAenderung = new Point(-1, -1);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        this.gesichert = false;
        
        if (e.getSource() == this.getNeueKarte())
            // Neue Karte erstellen
            this.createNewMap();
        else if (e.getSource() == this.getKarteLaden())
            // Karte Laden
            try {
                this.optionen.setKarte(this.karteIO.load());
                this.karteName = this.karteIO.mapName;
                this.gesichert = true;
                
            } catch (Exception e1) {
                BugReport.recordBug(e1);
            }
        else if (e.getSource() == this.getKarteSpeichern())
            // Karte speichern
            this.save();
        else if (e.getSource() == this.getInvert()) {
            // Invertiert die Karte
            for (int x = 0; x < this.optionen.getKarte().length; x++)
                for (int y = 0; y < this.optionen.getKarte()[0].length; y++)
                    if (!this.optionen.getKarte()[x][y])
                        this.optionen.getKarte()[x][y] = true;
                    else
                        this.optionen.getKarte()[x][y] = false;
        } else if (e.getSource() == this.getStandard()) {
            
            boolean[][] karte = this.optionen.getMod().getStandardKarte();
            this.optionen.setKarte(karte);
        }
        
        this.aktualisiere();
    }
    
    /**
     * Aktualisiert die Darstellung.
     */
    public void aktualisiere() {
        this.feld.aktualisieren();
        this.setTitle(this.karteName);
    }
    
    /**
     * Startet das Programm.
     */
    public void build() {
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
        // Layout
        this.setLayout(new GridLayout(0, 1));
        this.setPreferredSize(new Dimension(300, 300));
        this.add(this.getFeld());
        
        this.setJMenuBar(this.getMenu());
        this.setSize(700, 750);
        this.setVisible(true);
        this.setTitle(this.karteName);
        this.aktualisiere();
    }
    
    /**
     * Erstellt eine neue Karte.
     */
    private void createNewMap() {
        // Abfrage der Daten erstellen
        JPanel p = new JPanel(new GridLayout(0, 2, 10, 10));
        JLabel datei = new JLabel("Name: ");
        JLabel zeilen = new JLabel("Zeilen: ");
        JLabel spalten = new JLabel("Spalten: ");
        JTextField name = new JTextField("Neue Karte");
        JTextField cols = new JTextField();
        JTextField rows = new JTextField();
        p.add(datei);
        p.add(name);
        p.add(zeilen);
        p.add(cols);
        p.add(spalten);
        p.add(rows);
        
        String[] options = { "Ok", "Abbrechen" };
        int state = 0;
        
        // Abfrage bis Daten gueltig
        while (!Pattern.matches("\\d+", cols.getText())
                || !Pattern.matches("\\d+", rows.getText())) {
            state = JOptionPane.showOptionDialog(null, p, "Neue Karte",
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, options, options[0]);
            if (state == 1)
                break;
        }
        
        // Karte uebernehmen
        this.optionen
                .setKarte(new boolean[Integer.parseInt(rows.getText())][Integer
                        .parseInt(cols.getText())]);
        this.karteName = name.getText();
        this.aktualisiere();
    }
    
    /**
     * Gibt eine neue Kartenvorschau zurueck.
     * 
     * @return Kartenvorschau
     */
    private KartenVorschau getFeld() {
        if (this.feld == null) {
            this.feld = new KartenVorschau(this.optionen);
            this.feld.addMouseListener(this);
            this.feld.addMouseMotionListener(this);
        }
        return this.feld;
    }
    
    /**
     * Gibt einen Button zum Invertieren der Karte zurueck.
     * 
     * @return Button zum Invertieren der Karte
     */
    private JButton getInvert() {
        if (this.invert == null) {
            this.invert = new JButton("Invertieren");
            this.invert.addActionListener(this);
            this.invert.setToolTipText("Invertiert jedes Feld");
        }
        return this.invert;
    }
    
    /**
     * Gibt einen Button zum Laden einer Karte zurueck.
     * 
     * @return Button zum Laden einer Karte
     */
    private JButton getKarteLaden() {
        if (this.karteLaden == null) {
            this.karteLaden = new JButton("�ffnen");
            this.karteLaden.addActionListener(this);
            this.karteLaden.setToolTipText("�ffnet eine vorhandene Karte");
        }
        return this.karteLaden;
    }
    
    /**
     * Gibt einen Button zum Speichern einer Karte zurueck.
     * 
     * @return Button zum Speichern einer Karte
     */
    private JButton getKarteSpeichern() {
        if (this.karteSpeichern == null) {
            this.karteSpeichern = new JButton("Speichern");
            this.karteSpeichern.addActionListener(this);
            this.karteSpeichern.setToolTipText("Speichert die Karte");
        }
        return this.karteSpeichern;
    }
    
    /**
     * Rechnet die Koordinaten der Maus in ein Spielfeld um.
     * 
     * @param maus
     *            Koordinaten der Maus
     * @return umgerechnete Koordinaten
     */
    private Point getKoordinaten(Point maus) {
        
        return this.optionen.getMod().getKoordinaten(maus.x, maus.y);
    }
    
    /**
     * Gibt eine neue Menubar zurueck.
     * 
     * @return Menubar
     */
    private JMenuBar getMenu() {
        if (this.menu == null) {
            this.menu = new JMenuBar();
            this.menu.add(this.getNeueKarte());
            this.menu.add(this.getKarteLaden());
            this.menu.add(this.getKarteSpeichern());
            this.menu.add(this.getInvert());
            this.menu.add(this.getStandard());
        }
        return this.menu;
    }
    
    /**
     * Gibt einen Button fuer eine neue Karte zurueck.
     * 
     * @return Button fuer eine neue Karte
     */
    private JButton getNeueKarte() {
        if (this.neueKarte == null) {
            this.neueKarte = new JButton("Neu");
            this.neueKarte.addActionListener(this);
            this.neueKarte.setToolTipText("Erstellt eine neue Karte");
        }
        return this.neueKarte;
    }
    
    /**
     * Gibt einen Button zum Laden der Standardkarte zurueck.
     * 
     * @return Button zum Laden der Standardkarte
     */
    private JButton getStandard() {
        if (this.standard == null) {
            this.standard = new JButton("Standardkarte");
            this.standard.addActionListener(this);
            this.standard.setToolTipText("L�dt die Standardkarte des Mods");
        }
        return this.standard;
    }
    
    /**
     * Gibt zurueck, ob die Karte schon gespeichert wurde.
     * 
     * @return Wahrheitswert
     */
    public boolean isSaved() {
        return this.gesichert;
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        
        // Feld als gueltig markieren
        if ((e.getButton() == MouseEvent.BUTTON1) && !this.feldEntfernen)
            this.feldEinfuegen = !this.feldEinfuegen;
        
        // Feld als ungueltig markieren
        if ((e.getButton() == MouseEvent.BUTTON3) && !this.feldEinfuegen)
            this.feldEntfernen = !this.feldEntfernen;
        
        this.gesichert = false;
        
    }
    
    @Override
    public void mouseDragged(MouseEvent e) {
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
        
        if (this.feldEinfuegen) {
            // Koordinaten uebersetzen
            Point p = this.getKoordinaten(e.getPoint());
            
            if (!p.equals(this.letzteAenderung)) {
                this.letzteAenderung = new Point(p);
                
                // Feld als gueltig markieren
                this.optionen.getKarte()[p.x][p.y] = true;
                
                // Darstellung aktuallisieren
                this.aktualisiere();
            }
            
        } else if (this.feldEntfernen) {
            
            // Koordinaten uebersetzen
            Point p = this.getKoordinaten(e.getPoint());
            
            if (!p.equals(this.letzteAenderung)) {
                this.letzteAenderung = new Point(p);
                
                // Feld als ungueltig markieren
                this.optionen.getKarte()[p.x][p.y] = false;
                
                // Darstellung aktuallisieren
                this.aktualisiere();
            }
        }
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    
    /**
     * Speichert die aktuelle Karte.
     */
    public void save() {
        try {
            this.karteIO.save(this.optionen.getKarte(), this.karteName);
            this.gesichert = true;
        } catch (IOException e1) {
            BugReport.recordBug(e1);
        }
    }
    
}
