package de.vdwals.kettenreaktion.editor.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JPanel;

import de.vdwals.kettenreaktion.optionen.SpielOptionen;

/**
 * JPanel zur Preview einer Karte.
 * 
 * @author Dennis van der Wals
 * 
 */
public class KartenVorschau extends JPanel {
    private SpielOptionen     einstellungen;
    
    /**
     * Serial ID
     */
    private static final long serialVersionUID = -3899928851162231201L;
    
    /**
     * Standardkonstruktor.
     * 
     * @param karte
     *            zu bearbeitende Karte
     */
    public KartenVorschau(SpielOptionen einstellungen) {
        super();
        this.einstellungen = einstellungen;
        einstellungen.getMod().setFensterDimension(this.getWidth(),
                this.getHeight());
        this.setDoubleBuffered(true);
    }
    
    /**
     * Uebernimmt die uebergebene Karte und zeichnet sie.
     */
    public void aktualisieren() {
        this.einstellungen.getMod().setFensterDimension(this.getWidth(),
                this.getHeight());
        this.paint(this.getGraphics());
    }
    
    /**
     * Zeichnet die Karte.
     */
    @Override
    public void paint(Graphics g) {
        
        if (this.einstellungen != null) {
            
            // Ist die Hoehe kleiner, als die skallierte Breite?
            if (this.getHeight() < (int) (this.getWidth() * this.einstellungen
                    .getMod().getScale()))
                this.setSize((int) (this.getHeight() / this.einstellungen
                        .getMod().getScale()), this.getHeight());
            else if (this.getWidth() < (int) (this.getHeight() / this.einstellungen
                    .getMod().getScale()))
                this.setSize(this.getWidth(),
                        (int) (this.getWidth() * this.einstellungen.getMod()
                                .getScale()));
            
            // Wurde die Fenstergroesse geaendert?
            if ((this.getHeight() != this.einstellungen.getMod()
                    .getFensterHoehe())
                    || (this.getWidth() != this.einstellungen.getMod()
                            .getFensterBreite()))
                this.einstellungen.getMod().setFensterDimension(
                        this.getWidth(), this.getHeight());
        }
        
        // Hintergrund weiss malen
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        
        // Spielfelder schwarz malen
        g.setColor(Color.BLACK);
        for (int x = 0; x < this.einstellungen.getKarte().length; x++)
            for (int y = 0; y < this.einstellungen.getKarte()[0].length; y++)
                if (this.einstellungen.getKarte()[x][y]) {
                    Polygon p = this.einstellungen.getMod().getPolygon(x, y);
                    g.setColor(Color.BLACK);
                    g.fillPolygon(p);
                    
                    // Umrandung wei� malen
                    g.setColor(Color.WHITE);
                    g.drawPolygon(p);
                }
    }
    
}
