package de.vdwals.kettenreaktion.editor.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import de.vdwals.kettenreaktion.implement.ArrOps;

/**
 * Klasse fuer den In-/Output von Karten.
 * 
 * @author Dennis van der Wals
 * 
 */
public class KarteIOData {
    private final byte DB_VERSION = 1;
    public String      mapName;
    protected String   fileExtension;
    
    /**
     * Standardkonstruktor.
     */
    public KarteIOData() {
        super();
        this.fileExtension = ".map";
    }
    
    /**
     * Laedt eine Karte und gibt diese zurueck.
     * 
     * @param loaded
     *            Kartendatei
     * 
     * @return Karte im Format int[][]
     * @throws IOException
     */
    public boolean[][] load() throws IOException {
        File loaded = this.open(null, false);
        boolean[][] karte = null;
        
        this.mapName = loaded.getName().substring(0,
                loaded.getName().length() - 4);
        
        // Daten laden
        byte[] input = new byte[(int) loaded.length()];
        FileInputStream fis = new FileInputStream(loaded);
        fis.read(input);
        fis.close();
        
        int counter = 0;
        
        byte version = input[counter++];
        if (version == this.DB_VERSION) {
            byte x = input[counter++];
            byte y = input[counter++];
            
            karte = new boolean[x][y];
            
            for (int i = 0; i < y; i++) {
                byte spalte = 0;
                for (int j = 0; j <= (x / 8); j++) {
                    boolean[] tmp = ArrOps.toBits(input[counter++],
                            ((x - (j * x)) > 8) ? 8 : x);
                    for (boolean b : tmp)
                        karte[spalte++][i] = b;
                }
            }
        }
        return karte;
    }
    
    /**
     * Oeffnet eine Datei und gibt diese zurueck.
     * 
     * @return File gewaehlte Datei
     */
    protected File open(String name, boolean save) {
        
        // Erstellt den benoetigten Ordner, falls nicht vorhanden
        File f = new File("maps" + "/");
        if (!f.exists())
            f.mkdir();
        
        JFileChooser fc = new JFileChooser(f);
        fc.setDialogTitle("Datei w�hlen");
        if (name != null)
            fc.setSelectedFile(new File(name + this.fileExtension));
        fc.setFileFilter(new FileFilter() {
            
            @Override
            public boolean accept(File f) {
                return (f.getName().endsWith(KarteIOData.this.fileExtension) || f
                        .isDirectory());
            }
            
            @Override
            public String getDescription() {
                return "*" + KarteIOData.this.fileExtension;
            }
        });
        int state = 0;
        if (save)
            state = fc.showSaveDialog(null);
        else
            state = fc.showOpenDialog(null);
        if (state == JFileChooser.APPROVE_OPTION)
            return fc.getSelectedFile();
        return null;
    }
    
    /**
     * Speichert eine Karte in einer Datei
     * 
     * @param karte
     *            karte im Format int[][]
     * @throws IOException
     */
    public void save(boolean[][] karte, String name) throws IOException {
        
        // Dateiendung anhaengen
        if (name.endsWith(this.fileExtension))
            name = name.substring(0, name.length() - 4);
        
        // Datei erzeugen
        File save = this.open(name, true);
        
        // Ausgabeinformationen erzeugen
        byte[] output = new byte[karte.length + karte[0].length + 3];
        int counter = 0;
        
        output[counter++] = this.DB_VERSION;
        output[counter++] = (byte) karte.length;
        output[counter++] = (byte) karte[0].length;
        // Karte schreiben
        for (int i = 0; i < karte[0].length; i++)
            for (int j = 0; j <= (karte.length / 8); j++) {
                boolean[] tmp = new boolean[((karte.length - (j * karte.length)) > 8) ? 8
                        : karte.length];
                for (int k = 0; k < tmp.length; k++)
                    tmp[k] = karte[k + (j * 8)][i];
                output[counter++] = ArrOps.toByte(tmp);
            }
        
        // Datei schreiben
        FileOutputStream fos = new FileOutputStream(save);
        fos.write(ArrOps.getPartOfArray(output, 0, counter));
        fos.close();
    }
}
