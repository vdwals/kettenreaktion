/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package core.simulation;

import java.awt.Point;
import java.util.LinkedList;

/**
 * Strategie die den bestmoeglichen Zug bestimmt.
 * 
 * @author Dennis van der Wals
 * 
 */
public class GreedyOld extends Strategie {
    /**
     * Klasse um einen Punkt mit seinen Nachfolgern sowie den Gewinn mit den der
     * Nachfolgern zu speichern
     * 
     * @author Dennis van der Wals
     * 
     */
    private class GreedyPoint extends Point {
        /**
         * Default SerialID
         */
        private static final long      serialVersionUID = 1L;
        public LinkedList<GreedyPoint> equiFolgende;
        public int                     gewinn, nachfolgeGewinn;

        /**
         * Standardkonstruktor, wie ein Punkt.
         * 
         * @param x
         *            x-Koordinate
         * @param y
         *            y-Koordinate
         */
        public GreedyPoint(int x, int y) {
            super(x, y);
            this.equiFolgende = new LinkedList<GreedyPoint>();
            this.gewinn = 0;
            this.nachfolgeGewinn = 0;
        }

        /**
         * Fuegt einen Punkt der Liste hinzu und speichert dessen Gewinn ab
         * 
         * @param g
         *            Nachfolgender Punkt
         */
        public void add(GreedyPoint g) {
            this.equiFolgende.add(g);
            this.nachfolgeGewinn = g.getGewinn();
        }

        /**
         * Loescht die Liste der Nachfolger und setzt den Gewinn zurueck.
         */
        public void clear() {
            this.equiFolgende.clear();
            this.nachfolgeGewinn = 0;
        }

        /**
         * Gibt den Gewinn der Zuege zurueck.
         * 
         * @return Gewinn der Zuege
         */
        public int getGewinn() {
            return this.gewinn + this.nachfolgeGewinn;
        }

        /**
         * Gibt aus der Liste der Nachfolger mit maximalem Gewinn einen
         * zufaelligen zurueck.
         * 
         * @return Zufaelliger Nachfolger
         */
        public GreedyPoint getNachfolger(boolean random) {
            int index = 0;
            if (random)
                index = Math.round((float) Math.random()
                        * (this.equiFolgende.size() - 1));
            return (this.equiFolgende.size() > 0) ? this.equiFolgende
                    .get(index) : null;
        }
    }

    private boolean random;

    /**
     * Standardkonstruktor.
     * 
     * @param zufall
     *            Bestimmt, ob ein Zufallsmodus genutzt werden soll.
     */
    public GreedyOld(boolean zufall) {
        super((zufall) ? "KI-GreedyOld+" : "KI-GreedyOld"); //$NON-NLS-1$ //$NON-NLS-2$
        this.random = zufall;
    }

    @Override
    public Point[] getChips(byte[][] Brett) {
        GreedyPoint chip0 = new GreedyPoint(0, 0);
        for (int x1 = 0; x1 < Brett.length; x1++)
            for (int y1 = 0; y1 < Brett[0].length; y1++) {
                GreedyPoint chip1 = new GreedyPoint(x1, y1);
                byte[][] brett1 = reaktion(Brett, chip1);
                chip1.gewinn = getGewinn() + getAusBank();
                /*
                 * Findet die effizientesten Punkte fuer den zweiten und dritten
                 * Chip
                 */
                for (int x2 = 0; x2 < brett1.length; x2++)
                    for (int y2 = 0; y2 < brett1[0].length; y2++) {
                        // Neuer Punkt wird angelegt und das Brett berechnet
                        GreedyPoint chip2 = new GreedyPoint(x2, y2);
                        byte[][] brett2 = reaktion(brett1, chip2);
                        chip2.gewinn = getGewinn() + getAusBank();
                        // Findet den effizientesten Punkt fuer den dritten Chip
                        for (int x3 = 0; x3 < brett2.length; x3++)
                            for (int y3 = 0; y3 < brett2[0].length; y3++) {
                                GreedyPoint chip3 = new GreedyPoint(x3, y3);
                                reaktion(brett2, chip3);
                                chip3.gewinn = getGewinn() + getAusBank();
                                /*
                                 * Ist der Punkt besser als seine Vorgaenger,
                                 * wird er genommen
                                 */
                                if (chip3.getGewinn() >= chip2.nachfolgeGewinn) {
                                    if (chip3.getGewinn() > chip2.nachfolgeGewinn)
                                        chip2.clear();
                                    chip2.add(chip3);
                                }
                            }
                        /*
                         * Ist der Punkt mit seinem besten Nachfolger besser als
                         * seine Vorgaenger, wird er genommen.
                         */
                        if (chip2.getGewinn() >= chip1.nachfolgeGewinn) {
                            if (chip2.getGewinn() > chip1.nachfolgeGewinn)
                                chip1.clear();
                            chip1.add(chip2);
                        }
                    }
                /*
                 * Ist der Punkt mit seinen besten Nachfolgern besser als seine
                 * Vorgaenger, wird er genommen.
                 */
                if (chip1.getGewinn() >= chip0.nachfolgeGewinn) {
                    if (chip1.getGewinn() > chip0.nachfolgeGewinn)
                        chip0.clear();
                    chip0.add(chip1);
                }
            }
        GreedyPoint lastChip = chip0;
        Point[] chips = new Point[this.chipsToSet];
        for (int i = 0; i < this.chipsToSet; i++) {
            lastChip = lastChip.getNachfolger(this.random);
            chips[i] = lastChip;
        }
        return chips;
    }

}
