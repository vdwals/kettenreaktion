/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package spiel.io;

import implement.ArrOps;
import implement.BugReport;
import io.FileHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JOptionPane;

/**
 * Klasse zum Speichern der Spieleinstellungen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class SettingsIO_v6 extends FileHandler {
    /**
     */
    private final byte DB_VERSION  = 6;
    /**
     */
    private final byte S_BIT_SEQ   = 4;
    /**
     */
    private final int  MAX_DB_SIZE = 1024;
    /**
     */
    private File       settingsFile;
    /**
     */
    private byte       maxSpieler;
    /**
     */
    private byte       startChips;
    /**
     */
    private byte       modus;
    /**
     */
    private byte       chipsToSet;
    /**
     */
    private byte       animation;
    /**
     */
    private byte       wuerfelStart;
    /**
     */
    private byte       wuerfelEnd;
    /**
     */
    private byte       mapModus;
    /**
     */
    private int        bankChips;
    /**
     */
    private int        sleep;
    /**
     */
    private int        colors[];
    /**
     */
    private boolean    showA;
    /**
     */
    private boolean    autoUpdate;
    /**
     */
    private boolean    debug;
    /**
     */
    private boolean    wuerfeln;
    /**
     */
    private boolean    fiveRule;
    /**
     */
    private boolean    bankRule;

    /**
     * Standardkonstruktor.
     */
    public SettingsIO_v6() {
        super();
        File ort = new File("data");
        ort.mkdir();
        this.fileExtension = ".ini";
        this.settingsFile = new File("data/settings" + this.fileExtension);
        try {
            this.settingsFile.createNewFile();
        } catch (IOException e) {
            BugReport.recordBug(e);
            JOptionPane.showMessageDialog(null,
                    "Einstellungen konnten nicht angelegt werden.");
        }
    }

    /**
     * @return
     */
    public byte getAnimation() {
        return this.animation;
    }

    /**
     * @return
     */
    public int getBankChips() {
        return this.bankChips;
    }

    /**
     * @return
     */
    public byte getChipsToSet() {
        return this.chipsToSet;
    }

    /**
     * Gibt die Farben: 0 = Hintergrund; 1 = Pin1; 2 = Pin2; 3 = Kein Feld; 4 =
     * Chip 1... zurueck
     * 
     * @return Array mit Farbwerten
     */
    public int[] getColors() {
        return this.colors;
    }

    /**
     * @return the mapModus
     */
    public byte getMapModus() {
        return this.mapModus;
    }

    /**
     * @return
     */
    public byte getMaxSpieler() {
        return this.maxSpieler;
    }

    /**
     * @return
     */
    public byte getModus() {
        return this.modus;
    }

    /**
     * @return
     */
    public int getSleep() {
        return this.sleep;
    }

    /**
     * @return
     */
    public byte getStartChips() {
        return this.startChips;
    }

    /**
     * @return
     */
    public byte getWuerfelEnd() {
        return this.wuerfelEnd;
    }

    /**
     * @return
     */
    public byte getWuerfelStart() {
        return this.wuerfelStart;
    }

    /**
     * @return
     */
    public boolean isAutoUpdate() {
        return this.autoUpdate;
    }

    /**
     * @return the bankRule
     */
    public boolean isBankRule() {
        return this.bankRule;
    }

    /**
     * @return
     */
    public boolean isDebug() {
        return this.debug;
    }

    /**
     * @return the fiveRule
     */
    public boolean isFiveRule() {
        return this.fiveRule;
    }

    /**
     * @return
     */
    public boolean isShowA() {
        return this.showA;
    }

    /**
     * @return
     */
    public boolean isWuerfeln() {
        return this.wuerfeln;
    }

    @Override
    public void load() throws IOException {
        // Datei einlesen
        byte[] input = new byte[(int) this.settingsFile.length()];
        FileInputStream fis = new FileInputStream(this.settingsFile);
        fis.read(input);
        fis.close();

        // Fuer jeden eintrag eine Klasse erstellen und speichern:
        byte counter = 0;
        byte version = input[counter++];
        if (version <= this.DB_VERSION) {

            this.maxSpieler = input[counter++];
            this.startChips = input[counter++];
            this.chipsToSet = input[counter++];
            this.bankChips = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(input,
                    counter, counter += this.INTEGER_LAENGE));
            this.sleep = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(input,
                    counter, counter += this.INTEGER_LAENGE));

            // Aenderungen in dem Byte, dass booleans speichert
            if (version == 1) {
                // Version 1
                boolean[] tmp = ArrOps.toBits(input[counter++], 6);
                byte counter4boolean = 0;
                this.autoUpdate = tmp[counter4boolean++];
                this.showA = tmp[counter4boolean++];
                this.modus = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                        counter4boolean, counter4boolean += this.BIT_BREITE));
                this.animation = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                        counter4boolean, counter4boolean += this.BIT_BREITE));

                // Neu ab Version 2:
                this.debug = true;

            } else if (version <= 3) {
                // Bis Version 3
                boolean[] tmp = ArrOps.toBits(input[counter++], 7);
                byte counter4boolean = 0;
                this.autoUpdate = tmp[counter4boolean++];
                this.showA = tmp[counter4boolean++];
                this.debug = tmp[counter4boolean++];
                this.modus = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                        counter4boolean, counter4boolean += this.BIT_BREITE));
                this.animation = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                        counter4boolean, counter4boolean += this.BIT_BREITE));

            } else {
                // Ab Version 4
                boolean[] tmp = ArrOps.toBits(input[counter++], this.MAX_BIT);
                byte counter4boolean = 0;
                this.autoUpdate = tmp[counter4boolean++];
                this.showA = tmp[counter4boolean++];
                this.debug = tmp[counter4boolean++];
                // Neu ab Version 4:
                this.wuerfeln = tmp[counter4boolean++];
                this.modus = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                        counter4boolean, counter4boolean += this.BIT_BREITE));
                this.animation = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                        counter4boolean, counter4boolean += this.BIT_BREITE));

            }

            // Neu ab Version 4:
            if (version <= 3) {
                // Bis Version 3
                this.wuerfeln = false;
                this.wuerfelStart = 1;
                this.wuerfelEnd = 6;
            } else {
                // Ab Version 4
                this.wuerfelStart = input[counter++];
                this.wuerfelEnd = input[counter++];
            }

            // Neu ab Version 5:
            if (version <= 4)
                // Bis Version 4
                this.mapModus = 0;
            else if (version <= 5)
                // Bis Version 5
                this.mapModus = input[counter++];
            else {
                // Neu ab Version 6:
                boolean[] tmp = ArrOps.toBits(input[counter++], this.S_BIT_SEQ);
                byte counter4boolean = 0;
                this.fiveRule = tmp[counter4boolean++];
                this.bankRule = tmp[counter4boolean++];
                this.mapModus = ArrOps.toByte(ArrOps.getPartOfArray(tmp,
                        counter4boolean, counter4boolean += this.BIT_BREITE));
            }

            // Farben auslesen
            if (version < 3) {
                this.colors = new int[10];
                byte colorCounter = 0;
                this.colors[colorCounter++] = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
                this.colors[colorCounter++] = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
                this.colors[colorCounter++] = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
                this.colors[colorCounter = 4] = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
                this.colors[colorCounter++] = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
                this.colors[colorCounter++] = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
                this.colors[colorCounter++] = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
                this.colors[colorCounter++] = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
                this.colors[colorCounter++] = ArrOps.ByteArrayToInt(ArrOps
                        .getPartOfArray(input, counter,
                                counter += this.INTEGER_LAENGE));
                this.colors[3] = ArrOps.ByteArrayToInt(ArrOps.getPartOfArray(
                        input, counter, counter += this.INTEGER_LAENGE));
            } else {

                // Neu ab Version 3:
                this.colors = new int[(input.length - counter)
                        / this.INTEGER_LAENGE];
                for (int i = 0; i < this.colors.length; i++)
                    this.colors[i] = ArrOps.ByteArrayToInt(ArrOps
                            .getPartOfArray(input, counter,
                                    counter += this.INTEGER_LAENGE));
            }
        } else
            throw new IOException("Unbekannte DB_Version: " + version);
    }

    @Override
    public void save() {
        byte[] output = new byte[this.MAX_DB_SIZE];
        byte counter = 0;

        output[counter++] = this.DB_VERSION;
        output[counter++] = this.maxSpieler;
        output[counter++] = this.startChips;
        output[counter++] = this.chipsToSet;
        byte[] tmp = ArrOps.IntToByteArray(this.bankChips);
        for (int i = 0; i < this.INTEGER_LAENGE; i++)
            output[counter++] = tmp[i];
        tmp = ArrOps.IntToByteArray(this.sleep);
        for (int i = 0; i < this.INTEGER_LAENGE; i++)
            output[counter++] = tmp[i];

        // Bits
        boolean[] bits = new boolean[this.MAX_BIT];
        byte counter4boolean = 0;
        bits[counter4boolean++] = this.autoUpdate;
        bits[counter4boolean++] = this.showA;
        bits[counter4boolean++] = this.debug;
        bits[counter4boolean++] = this.wuerfeln;
        boolean[] modusBits = ArrOps.toBits(this.modus, this.BIT_BREITE);
        for (int i = 0; i < this.BIT_BREITE; i++)
            bits[counter4boolean++] = modusBits[i];
        modusBits = ArrOps.toBits(this.animation, this.BIT_BREITE);
        for (int i = 0; i < this.BIT_BREITE; i++)
            bits[counter4boolean++] = modusBits[i];
        output[counter++] = ArrOps.toByte(bits);

        // Wuerfelgrenzen
        output[counter++] = this.wuerfelStart;
        output[counter++] = this.wuerfelEnd;

        // Zweite Bitmenge
        bits = new boolean[this.MAX_BIT];
        counter4boolean = 0;
        bits[counter4boolean++] = this.fiveRule;
        bits[counter4boolean++] = this.bankRule;
        modusBits = ArrOps.toBits(this.mapModus, this.BIT_BREITE);
        for (int i = 0; i < this.BIT_BREITE; i++)
            bits[counter4boolean++] = modusBits[i];
        output[counter++] = ArrOps.toByte(bits);

        for (int color : this.colors) {
            tmp = ArrOps.IntToByteArray(color);
            for (int i = 0; i < this.INTEGER_LAENGE; i++)
                output[counter++] = tmp[i];
        }

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(this.settingsFile);
            fos.write(ArrOps.getPartOfArray(output, 0, counter));
            fos.close();
        } catch (FileNotFoundException e) {
            BugReport.recordBug(e);
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
    }

    /**
     * @param animation
     */
    public void setAnimation(byte animation) {
        this.animation = animation;
    }

    /**
     * @param autoUpdate
     */
    public void setAutoUpdate(boolean autoUpdate) {
        this.autoUpdate = autoUpdate;
    }

    /**
     * @param bankChips
     */
    public void setBankChips(int bankChips) {
        this.bankChips = bankChips;
    }

    /**
     * @param bankRule
     *            the bankRule to set
     */
    public void setBankRule(boolean bankRule) {
        this.bankRule = bankRule;
    }

    /**
     * @param chipsToSet
     */
    public void setChipsToSet(byte chipsToSet) {
        this.chipsToSet = chipsToSet;
    }

    /**
     * Setzt die Farben: 0 = Hintergrund; 1 = Pin1; 2 = Pin2; 3 = Kein Feld; 4 =
     * Chip 1...
     * 
     * @param colors
     */
    public void setColors(int[] colors) {
        this.colors = colors;
    }

    /**
     * @param debug
     */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    /**
     * @param fiveRule
     *            the fiveRule to set
     */
    public void setFiveRule(boolean fiveRule) {
        this.fiveRule = fiveRule;
    }

    /**
     * @param mapModus
     *            the mapModus to set
     */
    public void setMapModus(byte mapModus) {
        this.mapModus = mapModus;
    }

    /**
     * @param maxSpieler
     */
    public void setMaxSpieler(byte maxSpieler) {
        this.maxSpieler = maxSpieler;
    }

    /**
     * @param modus
     */
    public void setModus(byte modus) {
        this.modus = modus;
    }

    /**
     * @param showA
     */
    public void setShowA(boolean showA) {
        this.showA = showA;
    }

    /**
     * @param sleep
     */
    public void setSleep(int sleep) {
        this.sleep = sleep;
    }

    /**
     * @param startChips
     */
    public void setStartChips(byte startChips) {
        this.startChips = startChips;
    }

    /**
     * @param wuerfelEnd
     */
    public void setWuerfelEnd(byte wuerfelEnd) {
        this.wuerfelEnd = wuerfelEnd;
    }

    /**
     * @param wuerfeln
     */
    public void setWuerfeln(boolean wuerfeln) {
        this.wuerfeln = wuerfeln;
    }

    /**
     * @param wuerfelStart
     */
    public void setWuerfelStart(byte wuerfelStart) {
        this.wuerfelStart = wuerfelStart;
    }

}
