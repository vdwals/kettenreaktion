package gui;

import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import simulation.Strategie;
import core.ArrayOperation;
import core.Game;
import core.Settings;

/**
 * Klasse zur Simulation eine KI-Kampfes
 * 
 * @author Dennis van der Wals
 * 
 */
public class Simulation extends JFrame {
	/**
	 * Default SerialID
	 */
	private static final long serialVersionUID = 1L;
	private int[] originalPosition;

	// Komponenten
	private JProgressBar[] gewonnen;
	private JProgressBar gesamt;
	private JLabel[] namen, zeiten;
	private JLabel remain, elapsed, requierd;
	private JToggleButton stop;

	// Einstellungen
	private Settings einstellungen;
	private boolean pause;
	private Thread run;

	/**
	 * Standardkonstruktor
	 * 
	 * @param ki
	 */
	public Simulation(Settings einstellungen) {
		super("Simulation");
		this.einstellungen = einstellungen;
		originalPosition = new int[einstellungen.getKi().length];
		for (int i = 0; i < einstellungen.getKi().length; i++) {
			originalPosition[i] = i;
		}
		String s = "";

		try {
			s = JOptionPane.showInputDialog("Anzahl der Simulationen");
		} catch (Exception e) {
		}

		// Falsche Eingaben abfangen
		if (!Pattern.matches("\\d+", s)) {
			einstellungen.setLoops(100);
		} else {
			einstellungen.setLoops(Integer.parseInt(s));
			if (einstellungen.getLoops() <= 0)
				einstellungen.setLoops(100);
		}
	}

	/**
	 * Beendet die Aufzeichnung der KIs
	 */
	public void closeRecord() {
		for (int j = 0; j < einstellungen.getKi().length; j++) {
			einstellungen.getKi()[j].save();
		}
	}

	/**
	 * Gibt einen Fortschrittsbalken fuer die gesamte Simulation zurueck.
	 * 
	 * @return Fortschrittsbalken fuer die gesamte Simulation
	 */
	public JProgressBar getAnzeige() {
		if (gesamt == null) {
			gesamt = new JProgressBar(0, einstellungen.getLoops());
			gesamt.setOrientation(SwingConstants.HORIZONTAL);
			gesamt.setStringPainted(true);
			gesamt.setToolTipText("Simulierte Spiele: " + gesamt.getValue()
					+ "/" + einstellungen.getLoops());
		}
		return gesamt;
	}

	/**
	 * Gibt ein Label fuer die vergangene Simulationszeit zurueck
	 * 
	 * @return Label fuer die vergangene Simulationszeit
	 */
	public JLabel getElapsed() {
		if (elapsed == null) {
			elapsed = new JLabel("--:--");
			elapsed.setToolTipText("Verstrichene Zeit");
		}
		return elapsed;
	}

	/**
	 * Gibt eine Liste mit Fortschrittsbalken fuer die Anzahl der gewonnenen
	 * Spiele jeder KI zurueck.
	 * 
	 * @return Fortschrittsbalken fuer die Anzahl der gewonnenen Spiele jeder KI
	 */
	public JProgressBar[] getGewonnen() {
		if (gewonnen == null) {
			gewonnen = new JProgressBar[einstellungen.getKi().length];
			for (int i = 0; i < gewonnen.length; i++) {
				gewonnen[i] = new JProgressBar(0, einstellungen.getLoops());
				gewonnen[i].setStringPainted(true);
				gewonnen[i].setToolTipText("Gewonnene Spiele: "
						+ gewonnen[i].getValue() + "/"
						+ einstellungen.getLoops());
			}
		}
		return gewonnen;
	}

	/**
	 * Gibt eine Liste mit KI-Namen zurueck.
	 * 
	 * @return Liste mit KI-Namen
	 */
	public JLabel[] getNamen() {
		if (namen == null) {
			namen = new JLabel[einstellungen.getKi().length];
			for (int i = 0; i < namen.length; i++) {
				namen[i] = new JLabel(einstellungen.getKi()[i].toString());
			}
		}
		return namen;
	}

	/**
	 * Gibt ein Label fuer die verbleibende Simulationszeit zurueck
	 * 
	 * @return Label fuer die verbleibende Simulationszeit
	 */
	public JLabel getRemain() {
		if (remain == null) {
			remain = new JLabel("--:--");
			remain.setToolTipText("Erwartete verbleibende Zeit");
		}
		return remain;
	}

	/**
	 * Gibt ein Label fuer die benoetigte Simulationszeit zurueck
	 * 
	 * @return Label fuer die benoetigte Simulationszeit
	 */
	public JLabel getRequierd() {
		if (requierd == null) {
			requierd = new JLabel("--:--");
			requierd.setToolTipText("Erwartete Gesamtlaufzeit");
		}
		return requierd;
	}

	/**
	 * Gibt einen Button zum pausieren des Threads zurueck.
	 * 
	 * @return Button zum pausieren des Threads
	 */
	public JToggleButton getStop() {
		if (stop == null) {
			stop = new JToggleButton("Pause");
			stop.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					// Pausieren
					if (stop.isSelected()) {
						if (run.isAlive()) {
							pause = true;
						}
						stop.setText("Weiter");

					} else {

						// Pause aufheben
						if (run.isAlive()) {
							pause = false;
						}
						stop.setText("Pause");
					}
				}
			});
		}
		return stop;
	}

	/**
	 * Gibt eine Liste mit durchschnittlichen Berechnungszeiten der KIs zurueck.
	 * 
	 * @return Liste mit durchschnittlichen Berechnungszeiten der KIs
	 */
	public JLabel[] getZeiten() {
		if (zeiten == null) {
			zeiten = new JLabel[einstellungen.getKi().length];
			for (int i = 0; i < zeiten.length; i++) {
				zeiten[i] = new JLabel();
			}
		}
		return zeiten;
	}

	private String round(long nanos) {
		String out = "";
		double time = 0.0;
		if (nanos > 1000) {
			time = ((double) nanos / 1000);
			out = "\u03BCs";
		}

		if (time > 1000) {
			time = time / 1000;
			out = "ms";
		}

		out = String.format("%01.4f %s", time, out);
		return out;
	}

	/**
	 * Mischt die Reihenfolge der KIs und merkt sich die neue Position
	 */
	private void shuffle() {
		Strategie tmp;
		int rand;
		int tmpi;
		Random r = new Random();
		Strategie[] ki = einstellungen.getKi();
		for (int i = 0; i < ki.length; i++) {
			rand = r.nextInt(ki.length);
			tmp = ki[i];
			ki[i] = ki[rand];
			ki[rand] = tmp;
			// Alte Position rand, neue Position i
			tmpi = originalPosition[rand];
			originalPosition[rand] = originalPosition[i];
			originalPosition[i] = tmpi;
		}
	}

	/**
	 * Startet die Simulation
	 */
	public void simulieren() {
		run = new Thread(new Runnable() {

			@Override
			public void run() {
				// Berechnungszeit aufzeichnung
				long[] time = new long[einstellungen.getKi().length];
				long simStart = System.nanoTime();

				/*
				 * Zaehler, damit die Zeit nur bei jedem Prozent aktualisiert
				 * wird
				 */
				double aktualisiereZeit = 0.01;

				// Durchlaeufe
				for (int i = 0; i < einstellungen.getLoops(); i++) {

					// Neues Spiel erstellen
					Game spiel = new Game(einstellungen);

					// Reihenfolge durchmischen
					if (einstellungen.isRandom())
						shuffle();

					// Rundenzaehler
					int[] counter = new int[einstellungen.getKi().length];
					long[] timeRound = new long[einstellungen.getKi().length];

					while (!spiel.isOver()) {
						int[] konto = spiel.getKonto();
						int spieler = spiel.getAktiverSpieler();

						/*
						 * Ueberprueft, ob die KIs weniger als 3 Chips auf dem
						 * Konto haben falls eine Beschraenkung vorliegt
						 */
						if (einstellungen.getStartChips() > 0
								&& konto[spieler] < 3)
							einstellungen.getKi()[spieler]
									.setChipsToSet(konto[spieler]);

						// Start der Zeitmessung
						long start = System.nanoTime();

						// Chips abfragen
						Point[] chips = einstellungen.getKi()[spieler]
								.getChips(spiel.getBoard());

						// Zeitmessung beenden
						timeRound[spieler] += System.nanoTime() - start;

						// Runde beenden
						spiel.beendeRunde(chips);

						// Rundenzaehler erhoehen
						counter[spieler]++;
					}

					// Speichern der KI-Berechnungen und der Durchschnittszeit
					for (int j = 0; j < einstellungen.getKi().length; j++) {
						einstellungen.getKi()[j].save();

						/*
						 * Gesamte Berechnungszeit durch die Anzahl der Zuege
						 * teilen
						 */
						time[originalPosition[j]] += timeRound[j] / counter[j];
					}
					int gewinner = originalPosition[ArrayOperation.getMax(spiel
							.getKonto())];

					// Progressbar aktualisieren
					gewonnen[gewinner].setValue(gewonnen[gewinner].getValue() + 1);
					gesamt.setValue(gesamt.getValue() + 1);

					// Tooltipps aktualisieren
					gewonnen[gewinner].setToolTipText("Gewonnene Spiele: "
							+ gewonnen[gewinner].getValue() + "/"
							+ einstellungen.getLoops());
					gesamt.setToolTipText("Simulierte Spiele: "
							+ gesamt.getValue() + "/"
							+ einstellungen.getLoops());

					// Aktualisiere nach jedem Prozent die Zeit
					if (gesamt.getPercentComplete() >= aktualisiereZeit) {

						// Vergangene Zeit berechnen
						long elapsed = System.nanoTime() - simStart;

						// Benoetigte Zeit hochrechnen
						long requiered = elapsed * einstellungen.getLoops()
								/ (i + 1);

						// Anzeige aktualisieren
						Simulation.this.elapsed.setText(toTime(elapsed));
						remain.setText(toTime(requiered - elapsed));
						Simulation.this.requierd.setText(toTime(requiered));

						// Schwellwert zur Aktualisierung um 1 Prozent erhoehen
						aktualisiereZeit = gesamt.getPercentComplete() + 0.01;
					}

					// Wenn Thread pausiert:
					if (pause) {

						// Durchschnitt der Zeit bilden und plotten
						for (int j = 0; j < time.length; j++) {
							// zeiten[j].setText((((time[j] / i) == 0) ? "<1"
							// : (time[j] / i)) + " ms");
							zeiten[j].setText(round(time[j] / i));
						}

						// Speicher aufraeumen
						System.gc();

						// Endlosschleife solange Thread pausiert ist
						while (pause) {
							try {
								Thread.sleep(1000);

								// Pausezeit wird aus Zeitberechnung entfernt
								simStart += 1000000000;
							} catch (InterruptedException e) {
							}
						}
					}
				}

				// Durchschnitt der Zeit bilden und plotten
				for (int j = 0; j < time.length; j++) {
					// zeiten[j].setText((((time[j] / einstellungen.getLoops())
					// == 0) ? "<1"
					// : (time[j] / einstellungen.getLoops()))
					// + " ms");
					zeiten[j].setText(round(time[j] / einstellungen.getLoops()));
				}

				// Speicher leeren
				System.gc();
			}
		});
		run.start();
	}

	/**
	 * Erzeugt die graphische Oberflaeche
	 */
	public void start() {
		setLayout(new GridLayout(0, 1, 10, 10));

		// Fortschrittsbalken fuer den ganzen Verlauf
		add(getAnzeige());

		// Informationen ueber die Laufzeit
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(1, 3, 10, 0));
		p.add(getElapsed());
		p.add(getRemain());
		p.add(getRequierd());
		add(p);

		// Hinzufuegen der KIs
		for (int i = 0; i < einstellungen.getKi().length; i++) {
			p = new JPanel();
			p.setLayout(new GridLayout(1, 3, 10, 0));
			p.add(getNamen()[i]);
			p.add(getGewonnen()[i]);
			p.add(getZeiten()[i]);
			add(p);
		}

		// Pausebutton hinzufuegen
		p = new JPanel();
		p.setLayout(new GridLayout(1, 3, 10, 0));
		p.add(new JLabel());
		p.add(new JLabel());
		p.add(getStop());
		add(p);
		pack();
		setLocation(
				(Toolkit.getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
				(Toolkit.getDefaultToolkit().getScreenSize().height - getHeight()) / 2);
		setVisible(true);
		simulieren();
	}

	/**
	 * Verkuerzter Zugriff auf new SimpleDateFormat("mm:ss").format(new
	 * Date(millis))
	 * 
	 * @param millis
	 * @return mm:ss
	 */
	private String toTime(long nanos) {
		long millis = nanos / 1000000;
		return new SimpleDateFormat("mm:ss").format(new Date(millis));
	}
}
