/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package io;

import gui.Messages;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * Klasse zum Speichern der Spieleinstellungen.
 * 
 * @author Dennis van der Wals
 * 
 */
public class SettingsIO extends FileHandler {
    private File settingsFile;
    byte         maxSpieler, startChips, modus, chipsToSet;
    private int  bankChips, sleep, background, pin1, pin2, first, second,
            third, fourth, fifth, sixth, noField, animation;
    private boolean showA, autoUpdate;

    /**
     * Standardkonstruktor.
     */
    public SettingsIO() {
        super();
        this.fileExtension = ".ini"; //$NON-NLS-1$
        this.settingsFile = new File("data" + this.fileExtension); //$NON-NLS-1$
        if (!this.settingsFile.exists())
            try {
                this.settingsFile.createNewFile();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null,
                        Messages.getString("SettingsIO.2")); //$NON-NLS-1$
            }
    }

    /*
     * Getter fuer alle Werte.
     */

    public int getAnimation() {
        return this.animation;
    }

    public int getBackground() {
        return this.background;
    }

    public int getBankChips() {
        return this.bankChips;
    }

    public byte getChipsToSet() {
        return this.chipsToSet;
    }

    public int getFifth() {
        return this.fifth;
    }

    public int getFirstChipColor() {
        return this.first;
    }

    public int getFourth() {
        return this.fourth;
    }

    public byte getMaxSpieler() {
        return this.maxSpieler;
    }

    public int getNoField() {
        return this.noField;
    }

    public int getPin1() {
        return this.pin1;
    }

    public int getPin2() {
        return this.pin2;
    }

    public int getSecond() {
        return this.second;
    }

    public int getSixth() {
        return this.sixth;
    }

    public int getSleep() {
        return this.sleep;
    }

    public byte getStartChips() {
        return this.startChips;
    }

    public int getThird() {
        return this.third;
    }

    public boolean isAutoUpdate() {
        return this.autoUpdate;
    }

    public boolean isShowA() {
        return this.showA;
    }

    @Override
    public void load() throws IOException {
        // Datei einlesen
        Scanner scan = new Scanner(
                this.verschluesselung.decrypt(this.settingsFile));

        // Fuer jeden eintrag eine Klasse erstellen und speichern:
        this.maxSpieler = (byte) scan.nextInt();
        this.startChips = (byte) scan.nextInt();
        this.chipsToSet = (byte) scan.nextInt();
        this.bankChips = scan.nextInt();
        this.sleep = scan.nextInt();
        this.autoUpdate = scan.nextBoolean();
        this.background = scan.nextInt();
        this.pin1 = scan.nextInt();
        this.pin2 = scan.nextInt();
        this.first = scan.nextInt();
        this.second = scan.nextInt();
        this.third = scan.nextInt();
        this.fourth = scan.nextInt();
        this.fifth = scan.nextInt();
        this.sixth = scan.nextInt();
        this.noField = scan.nextInt();
        this.animation = scan.nextInt();
        this.showA = scan.nextBoolean();
        this.modus = (byte) scan.nextInt();
    }

    @Override
    public void save() throws IOException {
        BufferedWriter bw = new BufferedWriter(
                new FileWriter(this.settingsFile));

        bw.newLine();

        bw.write(toString(this.maxSpieler));
        bw.newLine();

        bw.write(toString(this.startChips));
        bw.newLine();

        bw.write(toString(this.chipsToSet));
        bw.newLine();

        bw.write(toString(this.bankChips));
        bw.newLine();

        bw.write(toString(this.sleep));
        bw.newLine();

        bw.write("" + this.autoUpdate); //$NON-NLS-1$
        bw.newLine();

        bw.write(toString(this.background));
        bw.newLine();
        bw.write(toString(this.pin1));
        bw.newLine();
        bw.write(toString(this.pin2));
        bw.newLine();
        bw.write(toString(this.first));
        bw.newLine();
        bw.write(toString(this.second));
        bw.newLine();
        bw.write(toString(this.third));
        bw.newLine();
        bw.write(toString(this.fourth));
        bw.newLine();
        bw.write(toString(this.fifth));
        bw.newLine();
        bw.write(toString(this.sixth));
        bw.newLine();
        bw.write(toString(this.noField));
        bw.newLine();

        bw.write(toString(this.animation));
        bw.newLine();

        bw.write("" + this.showA); //$NON-NLS-1$
        bw.newLine();

        bw.write(toString(this.modus));
        bw.newLine();

        bw.close();

        this.verschluesselung.encrypt(this.settingsFile);
    }

    /*
     * Setter fuer alle Werte.
     */

    public void setAnimation(int animation) {
        this.animation = animation;
    }

    public void setAutoUpdate(boolean autoUpdate) {
        this.autoUpdate = autoUpdate;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public void setBankChips(int bankChips) {
        this.bankChips = bankChips;
    }

    public void setChipsToSet(byte chipsToSet) {
        this.chipsToSet = chipsToSet;
    }

    public void setFifth(int fifth) {
        this.fifth = fifth;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public void setFourth(int fourth) {
        this.fourth = fourth;
    }

    public void setMaxSpieler(byte maxSpieler) {
        this.maxSpieler = maxSpieler;
    }

    public void setNoField(int noField) {
        this.noField = noField;
    }

    public void setPin1(int pin1) {
        this.pin1 = pin1;
    }

    public void setPin2(int pin2) {
        this.pin2 = pin2;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public void setShowA(boolean showA) {
        this.showA = showA;
    }

    public void setSixth(int sixth) {
        this.sixth = sixth;
    }

    public void setSleep(int sleep) {
        this.sleep = sleep;
    }

    public void setStartChips(byte startChips) {
        this.startChips = startChips;
    }

    public void setThird(int third) {
        this.third = third;
    }

    private String toString(int i) {
        return "" + i;
    }

    public void setModus(byte modus) {
        this.modus = modus;
    }

    public byte getModus() {
        return this.modus;
    }

}
