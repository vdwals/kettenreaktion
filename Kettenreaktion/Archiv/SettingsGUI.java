/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package gui;

import gui.language.Messages;
import gui.spiel.pfadalgorithmen.BreadthFirst;
import gui.spiel.pfadalgorithmen.DepthFirst;
import gui.spiel.pfadalgorithmen.PfadSuche;
import gui.spiel.pfadalgorithmen.Wave;
import implement.ArrOps;
import implement.BugReport;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import core.Settings;
import core.mod.Hexagon;
import core.mod.Mod;
import core.mod.Quadrat;
import core.mod.Triangel;

public class SettingsGUI extends JFrame implements ActionListener,
        ChangeListener, CaretListener {

    /**
     * Default SerialID
     */
    private static final long serialVersionUID = 1L;

    // GUI-Komponenten
    private JCheckBox         autoUpdateCheckBox, chipsLimit, wuerfelnCheckBox,
            showAnimation, debugCheckBox;
    private JTextField        maxSpielerField, maxChipsToSetField,
            bankChipsField, startChipsField, wuerfelnVon, wuerfelnBis;
    private JButton           lookForUpdate, toDefault, set, cancel,
            farbenButton[], ok;
    private JSlider           speed;
    private JLabel            speedLabel;
    private JRadioButton[]    auswahl;
    private ButtonGroup       bg;
    private JComboBox         langSelection, modSelection;

    // Spielvariablen
    private Settings          einstellungen;

    // Konstanten
    private final PfadSuche[] animation        = { new DepthFirst(),
            new BreadthFirst(), new Wave()    };
    private final Mod[]       mod              = { new Triangel(),
            new Quadrat(), new Hexagon()      };
    private final String[]    sprachen         = {
            Messages.getString("Settings.1"), Messages.getString("Settings.6"),
            Messages.getString("Settings.12") };
    private final String[]    lang             = { "de", "en", "sv" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

    /**
     * Standardkonstruktor mit Standardwerten.
     * 
     * @throws IOException
     */
    public SettingsGUI(Settings einstellungen) {
        // Ausfuehrungsprogramme initialisieren
        this.einstellungen = einstellungen;

        // Temporaere Werte erzeugen
        int selectedAnimation = 0;

        build(selectedAnimation);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == getLookForUpdate())
            // Versuche Update zu starten
            try {
                this.einstellungen.getUpdate().getUpdate();
            } catch (IOException e1) {
                BugReport.recordBug(e1);
            }
        else if (e.getSource() == getToDefault())

            // Standardwerte setzen
            this.einstellungen.toDefault();

        else if (e.getSource() == getUebernehmen())

            // Werte speichern
            this.einstellungen.save();

        else if (e.getSource() == getCancel())

            // Fenster schlie�en
            dispose();

        else if (e.getSource() == getOk()) {

            // Speichern und Fenster schlie�en
            this.einstellungen.save();
            dispose();

        } else {
            getUebernehmen().setEnabled(true);
            // Farbe aendern
            for (int i = 0; i < getFarbenButton().length; i++)

                // Ueberpruefe jeden Button
                if (getFarbenButton()[i] == e.getSource()) {
                    this.einstellungen.getFarben()[i] = JColorChooser
                            .showDialog(
                                    null,
                                    Messages.getString("Settings.3"), this.einstellungen.getFarben()[i]); //$NON-NLS-1$
                    getFarbenButton()[i].setBackground(this.einstellungen
                            .getFarben()[i]);
                    break;
                }
        }
    }

    /**
     * Erzeugt die Ansicht.
     * 
     * @param animation
     *            Index der gewaehlten animation
     */
    private void build(int animation) {

        // Sprache identifizieren
        for (int i = 0; i < this.lang.length; i++)
            if (this.lang[i].equals(Messages.getLang())) {
                getLangSelection().setSelectedIndex(i);
                break;
            }

        // Fenster erzeugen
        setLayout(new BorderLayout(5, 5));

        // Hilfspanel erzeugen
        JPanel center = new JPanel(new GridLayout(1, 2, 5, 5));
        JPanel leftCenter = new JPanel(new GridLayout(5, 2, 5, 5));
        JPanel rightCenter = new JPanel(new BorderLayout(5, 5));
        JPanel rightCenterTop = new JPanel(new GridLayout(4, 2, 5, 5));
        JPanel rightCenterBottom = new JPanel(new GridLayout(4, 1, 5, 5));
        JPanel rightCenterBottomRadioButton = new JPanel(new GridLayout(1, 3,
                5, 5));
        JPanel colorChooser = new JPanel(new GridLayout(2, 0, 5, 5));
        JPanel all = new JPanel(new BorderLayout(5, 5));
        JPanel bottom = new JPanel(new BorderLayout(5, 5));
        JPanel extraButtons = new JPanel(new GridLayout(1, 2, 5, 5));

        // Rahmen setzen:
        colorChooser.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), Messages.getString("Settings.42"))); //$NON-NLS-1$
        leftCenter.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), Messages.getString("Settings.43"))); //$NON-NLS-1$
        rightCenterTop.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), Messages.getString("Settings.44"))); //$NON-NLS-1$
        rightCenterBottom.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), Messages.getString("Settings.45"))); //$NON-NLS-1$

        // Panel fuellen, von innen nach aussen
        for (int i = 0; i < getAuswahl().length; i++) {
            rightCenterBottomRadioButton.add(getAuswahl()[i]);
            this.bg.add(getAuswahl()[i]);
        }
        rightCenterBottom.add(getShowAnimation());
        rightCenterBottom.add(rightCenterBottomRadioButton);
        rightCenterBottom.add(getSpeedLabel());
        rightCenterBottom.add(getSpeed());

        rightCenterTop.add(getAutoUpdate());
        rightCenterTop.add(getDebugCheckBox());
        rightCenterTop.add(getLookForUpdate());
        rightCenterTop.add(getToDefault());
        rightCenterTop.add(new JLabel(Messages.getString("Settings.28"))); //$NON-NLS-1$
        rightCenterTop.add(getLangSelection());
        rightCenterTop.add(new JLabel("Spielmodus: "));
        rightCenterTop.add(getModSelection());

        rightCenter.add(rightCenterTop, BorderLayout.NORTH);
        rightCenter.add(rightCenterBottom, BorderLayout.CENTER);

        leftCenter.add(new JLabel(Messages.getString("Settings.46"))); //$NON-NLS-1$
        leftCenter.add(getMaxSpielerField());
        leftCenter.add(new JLabel(Messages.getString("Settings.47"))); //$NON-NLS-1$
        leftCenter.add(getMaxChipsToSetField());
        leftCenter.add(new JLabel(Messages.getString("Settings.48"))); //$NON-NLS-1$
        leftCenter.add(getStartChipsField());
        leftCenter.add(new JLabel(Messages.getString("Settings.49"))); //$NON-NLS-1$
        leftCenter.add(getBankChipsField());
        leftCenter.add(getWuerfelnCheckBox());
        leftCenter.add(getChipsLimit());

        center.add(leftCenter);
        center.add(rightCenter);

        for (int i = 0; i < getFarbenButton().length; i++)
            colorChooser.add(getFarbenButton()[i]);
        colorChooser.add(new JLabel(Messages.getString("Settings.50"))); //$NON-NLS-1$
        colorChooser.add(new JLabel(Messages.getString("Settings.51"))); //$NON-NLS-1$
        colorChooser.add(new JLabel(Messages.getString("Settings.52"))); //$NON-NLS-1$
        colorChooser.add(new JLabel(Messages.getString("Settings.53"))); //$NON-NLS-1$
        for (int i = 0; i < (this.einstellungen.getFarben().length - 4); i++)
            colorChooser.add(new JLabel("Chip " + i));

        extraButtons.add(getOk());
        extraButtons.add(getUebernehmen());
        extraButtons.add(getCancel());

        bottom.add(new JLabel(), BorderLayout.CENTER);
        bottom.add(extraButtons, BorderLayout.EAST);

        all.add(center, BorderLayout.CENTER);
        all.add(colorChooser, BorderLayout.SOUTH);

        add(all, BorderLayout.CENTER);
        add(bottom, BorderLayout.SOUTH);

        pack();

        // In die Mitte positionieren
        setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
                (Toolkit.getDefaultToolkit().getScreenSize().height - getHeight()) / 2);

        // Icon setzen
        try {
            setIconImage(ImageIO.read(getClass().getResource(
                    "../gui/images/radioaktiv.xrt")));
        } catch (IOException e) {
            BugReport.recordBug(e);
        }

        // Selektionen uebertragen
        getAutoUpdate().setSelected(this.einstellungen.isAutoUpdate());
        getShowAnimation().setSelected(this.einstellungen.isAnimation());
        getDebugCheckBox().setSelected(this.einstellungen.isDebug());
        getAuswahl()[animation].setSelected(true);
        getModSelection().setSelectedIndex(this.einstellungen.getModus());
        getSpeed().setValue(this.einstellungen.getSleep() / 100);
    }

    @Override
    public void caretUpdate(CaretEvent e) {
        getUebernehmen().setEnabled(true);
    }

    /**
     * Gibt den Aenderungsbaumalgorithmus zurueck.
     * 
     * @return Aenderungsbaumalgorithmus
     */
    public PfadSuche getAnimation() {
        for (int i = 0; i < this.auswahl.length; i++)
            if (this.auswahl[i].isSelected())
                return this.animation[i];
        return null;
    }

    /**
     * Gibt die Animationsauswahlbuttons zurueck und erstellt eine Buttongroup.
     * Fuegt die Buttons jedoch noch nicht der Buttongroup hinzu.
     * 
     * @return JRadioButton Array mit eintraegen fuer jede Animation
     */
    private JRadioButton[] getAuswahl() {
        if (this.auswahl == null) {
            this.auswahl = new JRadioButton[3];
            this.auswahl[0] = new JRadioButton(Messages.getString("Settings.7")); //$NON-NLS-1$
            this.auswahl[1] = new JRadioButton(Messages.getString("Settings.8")); //$NON-NLS-1$
            this.auswahl[2] = new JRadioButton(Messages.getString("Settings.9")); //$NON-NLS-1$
            this.bg = new ButtonGroup();
            for (JRadioButton element : this.auswahl)
                element.addChangeListener(this);
        }
        return this.auswahl;
    }

    /**
     * Gibt eine Auswahlbox fuer automatische Updates zurueck.
     * 
     * @return Auswahlbox fuer automatische Updates
     */
    public JCheckBox getAutoUpdate() {
        if (this.autoUpdateCheckBox == null) {
            this.autoUpdateCheckBox = new JCheckBox(
                    Messages.getString("Settings.10")); //$NON-NLS-1$
            this.autoUpdateCheckBox.setToolTipText(Messages
                    .getString("Settings.11")); //$NON-NLS-1$
            this.autoUpdateCheckBox.addChangeListener(this);
        }
        return this.autoUpdateCheckBox;
    }

    /**
     * Gibt ein Textfeld zur Eingabe der Chips in der Bank zurueck.
     * 
     * @return Textfeld zur Eingabe der Chips in der Bank
     */
    public JTextField getBankChipsField() {
        if (this.bankChipsField == null) {
            this.bankChipsField = new JTextField(
                    "" + this.einstellungen.getBankChips()); //$NON-NLS-1$
            this.bankChipsField.setToolTipText(Messages
                    .getString("Settings.13")); //$NON-NLS-1$
            this.bankChipsField.addCaretListener(this);
        }
        return this.bankChipsField;
    }

    private JButton getCancel() {
        if (this.cancel == null) {
            this.cancel = new JButton(Messages.getString("Creator.43")); //$NON-NLS-1$
            this.cancel.addActionListener(this);
        }
        return this.cancel;
    }

    /**
     * Gibt eine Auswahlbox fuer ein Chiplimit zurueck.
     * 
     * @return Auswahlbox fuer ein Chiplimit
     */
    private JCheckBox getChipsLimit() {
        if (this.chipsLimit == null) {
            this.chipsLimit = new JCheckBox(Messages.getString("Settings.14")); //$NON-NLS-1$
            this.chipsLimit.setToolTipText(Messages.getString("Settings.15")); //$NON-NLS-1$
            this.chipsLimit.addChangeListener(this);
        }
        return this.chipsLimit;
    }

    /**
     * Erstellt die Debugcheckbox und gibt sie zurueck.
     * 
     * @return Debugcheckbox
     */
    public JCheckBox getDebugCheckBox() {
        if (this.debugCheckBox == null) {
            this.debugCheckBox = new JCheckBox("Debug-Aufzeichnung");
            this.debugCheckBox.addChangeListener(this);
            this.debugCheckBox
                    .setToolTipText("Sammelt weite Informationen zum debugen.");
        }
        return this.debugCheckBox;
    }

    /**
     * Gibt ein Array mit Buttons fuer jede Farbe zurueck.
     * 
     * @return Array mit Buttons fuer jede Farbe
     */
    private JButton[] getFarbenButton() {
        if (this.farbenButton == null) {
            this.farbenButton = new JButton[this.einstellungen.getFarben().length];

            for (int i = 0; i < this.farbenButton.length; i++) {
                this.farbenButton[i] = new JButton();
                this.farbenButton[i].addActionListener(this);
                this.farbenButton[i].setBackground(this.einstellungen
                        .getFarbe(i));
            }
        }
        return this.farbenButton;
    }

    /**
     * Erzeugt die Auswahlbox fuer die Sprachen.
     * 
     * @return Auswahlbox fuer Sprachen
     */
    public JComboBox getLangSelection() {
        if (this.langSelection == null) {
            this.langSelection = new JComboBox(this.sprachen);
            this.langSelection.setToolTipText("Auswahl der Sprache");
            this.langSelection.addActionListener(this);
        }
        return this.langSelection;
    }

    /**
     * Gibt einen Button zum suchen nach einem Update zurueck.
     * 
     * @return Button zum suchen nach einem Update
     */
    private JButton getLookForUpdate() {
        if (this.lookForUpdate == null) {
            this.lookForUpdate = new JButton(Messages.getString("Settings.18")); //$NON-NLS-1$
            this.lookForUpdate.addActionListener(this);
            this.lookForUpdate
                    .setToolTipText(Messages.getString("Settings.19")); //$NON-NLS-1$
        }
        return this.lookForUpdate;
    }

    /**
     * Gibt eine Textfeld zur Eingabe der maximal legbaren Chips pro Runde
     * zurueck.
     * 
     * @return Textfeld zur Eingabe der maximal legbaren Chips
     */
    public JTextField getMaxChipsToSetField() {
        if (this.maxChipsToSetField == null) {
            this.maxChipsToSetField = new JTextField(
                    "" + this.einstellungen.getMaxChipsToSet()); //$NON-NLS-1$
            this.maxChipsToSetField.setToolTipText(Messages
                    .getString("Settings.21")); //$NON-NLS-1$
            this.maxChipsToSetField.addCaretListener(this);
        }
        return this.maxChipsToSetField;
    }

    /**
     * Gibt eine Textfeld zur Eingabe der maximalen Spieleranzahl zurueck.
     * 
     * @return Textfeld zur Eingabe der maximalen Spieleranzahl
     */
    public JTextField getMaxSpielerField() {
        if (this.maxSpielerField == null) {
            this.maxSpielerField = new JTextField(
                    "" + this.einstellungen.getMaxSpieler()); //$NON-NLS-1$
            this.maxSpielerField.setToolTipText(Messages
                    .getString("Settings.23")); //$NON-NLS-1$
            this.maxSpielerField.addCaretListener(this);
        }
        return this.maxSpielerField;
    }

    /**
     * Erzeugt die Auswahlbox fuer die Spielmodi.
     * 
     * @return Auswahlbox fuer die Spielmodi
     */
    public JComboBox getModSelection() {
        if (this.modSelection == null) {
            String[] modi = new String[this.mod.length];
            for (int i = 0; i < modi.length; i++)
                modi[i] = this.mod[i].toString();
            this.modSelection = new JComboBox(modi);
            this.modSelection.setToolTipText("Auswahl der Spielmodifikation");
            this.modSelection.addActionListener(this);
        }
        return this.modSelection;
    }

    /**
     * Erstellt den Ok-Button und gibt ihn zurueck.
     * 
     * @return OK-Button
     */
    private JButton getOk() {
        if (this.ok == null) {
            this.ok = new JButton("OK");
            this.ok.addActionListener(this);
            this.ok.setToolTipText("Speichert die Einstellungen und schlie�t das Fenster");
        }
        return this.ok;
    }

    /**
     * Gibt eine Auswahlbox zum zeigen der Animation zurueck.
     * 
     * @return Auswahlbox zum zeigen der Animation
     */
    public JCheckBox getShowAnimation() {
        if (this.showAnimation == null) {
            this.showAnimation = new JCheckBox(
                    Messages.getString("Settings.24")); //$NON-NLS-1$
            this.showAnimation
                    .setToolTipText(Messages.getString("Settings.25")); //$NON-NLS-1$
            this.showAnimation.addChangeListener(this);
        }
        return this.showAnimation;
    }

    /**
     * Gibt einen Animationsgeschwindigkeitsregler zurueck.
     * 
     * @return Animationsgeschwindigkeitsregler
     */
    private JSlider getSpeed() {
        if (this.speed == null) {
            this.speed = new JSlider(0, 20, 5);
            this.speed.setPaintTicks(true);
            this.speed.setMinorTickSpacing(1);
            this.speed.setMajorTickSpacing(5);
            this.speed.addChangeListener(this);
        }
        return this.speed;
    }

    /**
     * Gibt ein Label fuer die Animationsgeschwindigkeit zurueck.
     * 
     * @return Label fuer die Animationsgeschwindigkeit
     */
    private JLabel getSpeedLabel() {
        if (this.speedLabel == null)
            this.speedLabel = new JLabel(
                    Messages.getString("Settings.0") + (double) this.einstellungen.getSleep() / 1000 //$NON-NLS-1$
                            + Messages.getString("Settings.27")); //$NON-NLS-1$
        return this.speedLabel;
    }

    /**
     * Gibt ein Textfeld zur Eingabe der Startchipanzahl zurueck.
     * 
     * @return Textfeld zur Eingabe der Startchipanzahl
     */
    public JTextField getStartChipsField() {
        if (this.startChipsField == null) {
            this.startChipsField = new JTextField(
                    "" + this.einstellungen.getStartChips()); //$NON-NLS-1$
            this.startChipsField.setToolTipText(Messages
                    .getString("Settings.31")); //$NON-NLS-1$
            this.startChipsField.addCaretListener(this);
        }
        return this.startChipsField;
    }

    /**
     * Gibt einen Button zum Laden der Standardeinstellungen zurueck.
     * 
     * @return Button zum Laden der Standardeinstellungen
     */
    private JButton getToDefault() {
        if (this.toDefault == null) {
            this.toDefault = new JButton(Messages.getString("MapEditor.0")); //$NON-NLS-1$
            this.toDefault.addActionListener(this);
            this.toDefault.addChangeListener(this);
            this.toDefault.setToolTipText(Messages.getString("Settings.33")); //$NON-NLS-1$
        }
        return this.toDefault;
    }

    public JButton getUebernehmen() {
        if (this.set == null) {
            this.set = new JButton(Messages.getString("Settings.4")); //$NON-NLS-1$
            this.set.addActionListener(this);
            this.set.setToolTipText(Messages.getString("Settings.5")); //$NON-NLS-1$
            this.set.setEnabled(false);
        }
        return this.set;
    }

    /**
     * @return the wuerfelnBis
     */
    private JTextField getWuerfelnBis() {
        if (this.wuerfelnBis == null) {
            this.wuerfelnBis = new JTextField(
                    "" + this.einstellungen.getWuerfelEnd()); //$NON-NLS-1$
            this.wuerfelnBis.setToolTipText("Maximale zu w�rfelnde Zahl");
            this.wuerfelnBis.addCaretListener(this);
        }
        return this.wuerfelnBis;
    }

    /**
     * Gibt eine Auswahlbox fuer die 5er-Regel zurueck.
     * 
     * @return Auswahlbox fuer die 5er-Regel
     */
    public JCheckBox getWuerfelnCheckBox() {
        if (this.wuerfelnCheckBox == null) {
            this.wuerfelnCheckBox = new JCheckBox("W�rfeln");
            this.wuerfelnCheckBox
                    .setToolTipText("Aktivieren um die zu maximale zu setzende Chipzahl zu erw�rfeln");
            this.wuerfelnCheckBox.addChangeListener(this);
        }
        return this.wuerfelnCheckBox;
    }

    /**
     * @return the wuerfelnVon
     */
    private JTextField getWuerfelnVon() {
        if (this.wuerfelnVon == null) {
            this.wuerfelnVon = new JTextField(
                    "" + this.einstellungen.getWuerfelStart()); //$NON-NLS-1$
            this.wuerfelnVon.setToolTipText("Minimale zu w�rfelnde Zahl");
            this.wuerfelnVon.addCaretListener(this);
        }
        return this.wuerfelnVon;
    }

    /**
     * Gibt zurueck, ob die zu legenden Chips limitiert werden sollen.
     * 
     * @return Wahrheitswert
     */
    public boolean isChipsLimit() {
        return !this.chipsLimit.isSelected();
    }

    @Override
    public void repaint() {
        super.repaint();

        // Buttons neu einfaerben
        for (int i = 0; i < getFarbenButton().length; i++)
            getFarbenButton()[i].setBackground(this.einstellungen.getFarbe(i));

        // Auswahl uebertragen
        getMaxChipsToSetField().setText(
                "" + this.einstellungen.getMaxChipsToSet());
        getMaxSpielerField().setText("" + this.einstellungen.getMaxSpieler());
        getBankChipsField().setText("" + this.einstellungen.getBankChips());
        getStartChipsField().setText("" + this.einstellungen.getStartChips());
        getAutoUpdate().setSelected(this.einstellungen.isAutoUpdate());
        getShowAnimation().setSelected(this.einstellungen.isAnimation());
        getDebugCheckBox().setSelected(this.einstellungen.isDebug());
        getSpeed().setValue(this.einstellungen.getSleep() / 100);
    }

    /**
     * Legt die Karte des Spieles fest.
     * 
     * @param karte
     *            Karte des Spiels
     */
    public void setKarte(boolean[][] karte) {
        // if (editable)
        for (Mod modus : this.mod)
            modus.setKarte(ArrOps.arrayCopy(karte));
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        getUebernehmen().setEnabled(true);

        if (e.getSource() == getSpeed()) {
            SettingsGUI.this.einstellungen.setSleep(((JSlider) e.getSource())
                    .getValue() * 100);
            getSpeedLabel()
                    .setText(
                            Messages.getString("Settings.0") + (double) this.einstellungen.getSleep() / 1000 + Messages.getString("Settings.27")); //$NON-NLS-1$ //$NON-NLS-2$
        }

        // Checkbuttonauswahl an Variablen uebertragen
        if (e.getSource() == getWuerfelnCheckBox()) {
            if (getWuerfelnCheckBox().isSelected()) {
                getMaxChipsToSetField().setEnabled(false);
                getWuerfelnBis().setEnabled(true);
                getWuerfelnVon().setEnabled(true);
            } else {
                getMaxChipsToSetField().setEnabled(true);
                getWuerfelnBis().setEnabled(false);
                getWuerfelnVon().setEnabled(false);
            }
            this.einstellungen.setWuerfeln(getWuerfelnCheckBox().isSelected());
        } else if (e.getSource() == getAutoUpdate())
            this.einstellungen.setAutoUpdate(getAutoUpdate().isSelected());
        else if (e.getSource() == getDebugCheckBox())
            this.einstellungen.setDebug(getDebugCheckBox().isSelected());
        else if (e.getSource() == getShowAnimation())
            this.einstellungen.setShowA(getShowAnimation().isSelected());
    }

    /**
     * Gibt zurueck, ob der Breitensuchalgorithmus verwendet werden soll.
     * 
     * @return Wahrheitswert
     */
    public boolean useBF() {
        return !getAuswahl()[0].isSelected();
    }
}
