package gui.spiel;

import gui.spiel.pfadalgorithmen.PfadSuche;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.LinkedList;

import javax.swing.JPanel;

import core.Reaktor.ReaktorPoint;
import core.Settings;

/**
 * Klasse dient der farblichen Anzeige des uebergebenen Spielfeldes.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Board extends JPanel {
	/**
	 * Default Serial
	 */
	private static final long serialVersionUID = 1L;

	// Spieleinstellungen
	private Settings einstellungen;

	// Interne Variablen
	/*
	 * Extravariablen zum einhalten der Seitenverhaeltnisse, da die Funktion zur
	 * Groesseneinstellung aufgerufen wird, bevor ein Feld existiert. Das fuehrt
	 * zum Absturz.
	 */
	private int scaleX;
	private int scaleY;
	private Feld[][] spielbrett;
	public Thread t;

	/**
	 * Standardkonstruktor
	 * 
	 * @param einstellungen
	 *            Spieleinstellungen
	 */
	public Board(Settings einstellungen) {
		this.einstellungen = einstellungen;
		scaleX = scaleY = 1;

		// Gegen das flackern
		// setDoubleBuffered(true);
	}

	@Override
	public void repaint() {
		// Quadratisches Format erhalten
		if (getHeight() * scaleY < getWidth() * scaleX)
			setSize((int) (getHeight() * scaleY / scaleX), getHeight());
		if (getWidth() * scaleX < getHeight() * scaleY)
			setSize(getWidth(), (int) (scaleX * getWidth() / scaleY));
		super.repaint();
	}

	/**
	 * Legt einen Chip auf das Spielfeld, beeinfluss nicht das Spielfeld.
	 * 
	 * @param x
	 *            x-Koordinate
	 * @param y
	 *            y-Koordinate
	 */
	public void addChip(int x, int y) {
		spielbrett[y][x].addChip();
	}

	/**
	 * Entfernt einen Chip vom Spielfeld, beeinfluss nicht das Spielfeld.
	 * 
	 * @param x
	 *            x-Koordinate
	 * @param y
	 *            y-Koordinate
	 */
	public void removeChip(int x, int y) {
		spielbrett[y][x].removeChip();
	}

	/**
	 * Setter fuer das Brett.
	 * 
	 * @param spielbrett
	 *            zu uebernehmendes Spielbrett
	 */
	public void setBrett(int[][] spielbrett) {

		// Wenn ein neues Feld uebergeben wird, altes Ueberschreiben
		if (this.spielbrett == null
				|| this.spielbrett.length != spielbrett[0].length
				|| this.spielbrett[0].length != spielbrett.length) {

			scaleX = spielbrett[0].length;
			scaleY = spielbrett.length;
			this.spielbrett = new Feld[scaleX][scaleY];
			setLayout(new GridLayout(scaleX, scaleY));
			System.gc();
		}

		boolean s = true;
		int counter = 0;
		for (int y = 0; y < scaleX; y++) {
			for (int x = 0; x < scaleY; x++) {

				// Neues Feld anlegen
				if (this.spielbrett[y][x] == null) {

					// Abwechselnd einen oder zwei Chips legen
					if (s) {
						this.spielbrett[y][x] = new Feld(
								einstellungen.getFarbe(1), spielbrett[x][y]);
					} else {
						this.spielbrett[y][x] = new Feld(
								einstellungen.getFarbe(2), spielbrett[x][y]);
					}

					// Komponente hinzufuegen
					add(this.spielbrett[y][x], counter++);

				} else if (this.spielbrett[y][x].value != spielbrett[x][y]) {

					// Wert ueberschreiben, wenn er sich geaendert hat
					this.spielbrett[y][x].setValue(spielbrett[x][y]);
				}
				s = !s;
			}
			s = !s;
		}
		repaint();
	}

	/**
	 * Zeichnet nacheinander die Veraenderungen ein um die Reaktionen so
	 * anschaulich zu machen.
	 * 
	 * @param aenderungen
	 *            Liste mit Reaktionsbaeumen
	 * @param panel
	 *            Panel zur Aktuallisierung der ChipsToPlace-Anzeige
	 */
	public void plottChanges(LinkedList<ReaktorPoint> aenderungen,
			final InfoPanel panel, PfadSuche pfadSuchAlgoithmus) {
		final PfadSuche p = pfadSuchAlgoithmus;
		p.setAenderungsBaum(aenderungen);
		t = new Thread(new Runnable() {

			@Override
			public void run() {
				int chips = einstellungen.getMaxChipsToSet();
				// Alle Aenderungen printen
				while (p.hasNext()) {

					// Minimierung des Flackerns
					int xleft = scaleX;
					int yleft = scaleY;
					int xright = 0;
					int yright = 0;

					for (ReaktorPoint reaktorPoint : p.getNext()) {

						// Aktuallisierungsfeld anpassen
						if (reaktorPoint.x < xleft)
							xleft = reaktorPoint.x;
						if (reaktorPoint.y < yleft)
							yleft = reaktorPoint.y;
						if (reaktorPoint.x > xright)
							xright = reaktorPoint.x;
						if (reaktorPoint.y > yright)
							yright = reaktorPoint.y;

						/*
						 * Wurde der Chip vom Spieler gelegt, wird der Zaehler
						 * verringert
						 */
						if (reaktorPoint.isGesetzt()) {
							panel.getToPlaceLabel().setText("" + --chips); //$NON-NLS-1$
							panel.paintImmediately(panel.getSubPanel()
									.getBounds());
						}

						// Spielbrettanzeige manipulieren
						spielbrett[reaktorPoint.y][reaktorPoint.x]
								.addChips(reaktorPoint.getChange());
					}
					// paint(Board.this.getGraphics());
					paintImmediately(xleft * spielbrett[0][0].getWidth(), yleft
							* spielbrett[0][0].getHeight(), (xright - xleft)
							* spielbrett[0][0].getWidth(), (yright - yleft)
							* spielbrett[0][0].getHeight());

					// Pause
					try {
						Thread.sleep(einstellungen.getSleep());
					} catch (InterruptedException e) {
					}
				}
			}
		});
		t.start();
	}

	/**
	 * Uebernimmt die Spieleinstellungen
	 * 
	 * @param einstellungen
	 *            Spieleinstellungen
	 */
	public void setEinstellungen(Settings einstellungen) {
		this.einstellungen = einstellungen;
	}

	/**
	 * Zaehlt die sichtbaren Chips auf dem Feld
	 * 
	 * @return Anzahl der sichtbaren Chips.
	 */
	public int countChips() {
		int count = 0;
		for (int x = 0; x < spielbrett.length; x++) {
			for (int y = 0; y < spielbrett[x].length; y++) {
				if (spielbrett[x][y].value != -1)
					count += spielbrett[x][y].value;
			}
		}
		return count;
	}

	/**
	 * Unterklasse fuer die Spielfelder. Performance wird erhoeht, da nicht das
	 * ganze Spielfeld neuberechnet und gezeichnet wird.
	 * 
	 * @author Dennis van der Wals
	 * 
	 */
	private class Feld extends JPanel {

		/**
		 * SerialID
		 */
		private static final long serialVersionUID = -8162139790867805387L;
		private int value;
		private Color stack;

		/**
		 * Standardkonstruktor.
		 * 
		 * @param stack
		 *            Farbe des Zentrums
		 * @param value
		 *            Anzahl der Chips
		 */
		private Feld(Color stack, int value) {
			this.value = value;
			this.stack = stack;
			setDoubleBuffered(true);
		}

		@Override
		public void paint(Graphics g) {

			// Hintergrund malen
			if (value == -1) {
				g.setColor(einstellungen.getFarbe(3));
				g.fillRect(0, 0, getWidth(), getHeight());
			} else {
				g.setColor(einstellungen.getFarbe(0));
				g.fillRect(0, 0, getWidth(), getHeight());

				// Skaliervariablen initialisieren
				int xI = 0;
				int yI = 0;
				int breiteI = 0;
				int hoeheI = 0;

				// Chips zeichnen
				for (int i = 0; i < value; i++) {

					// Skalierung berechnen
					xI = (int) (i * (double) getWidth() / 14);
					yI = (int) (i * (double) getHeight() / 14);
					breiteI = (int) (getWidth() * (1 - (double) i / 7));
					hoeheI = (int) (getHeight() * (1 - (double) i / 7));

					// Farbe festlegen
					g.setColor(setColor(i + 1));
					g.fillOval(xI, yI, breiteI, hoeheI);
					g.setColor(Color.BLACK);
					g.drawOval(xI, yI, breiteI, hoeheI);
				}

				// Die Mitte des Feldes faerben
				xI = (int) (6 * (double) getWidth() / 14);
				yI = (int) (6 * (double) getHeight() / 14);
				breiteI = (int) (getWidth() * (1 - (double) 6 / 7));
				hoeheI = (int) (getHeight() * (1 - (double) 6 / 7));
				g.setColor(Color.BLACK);
				g.drawOval(xI, yI, breiteI, hoeheI);
				g.setColor(stack);
				g.fillOval(xI, yI, breiteI, hoeheI);
			}
		}

		/**
		 * Bestimmt die zu malende Farbe anhand der Chipanzahl
		 * 
		 * @param x
		 *            x-Koordinate
		 * @param y
		 *            y-Koordinate
		 * @return Farbe fuer das Feld
		 */
		private Color setColor(int i) {
			return einstellungen.getFarbe(i + 3);
		}

		/**
		 * Legt einen Chip auf das Spielfeld, beeinfluss nicht das Spielfeld.
		 * Wird nur aufgerufen, wenn ein Spieler einen Chip legt
		 */
		private void addChip() {
			if (value != -1) {
				value++;
				repaint();
			}
		}

		/**
		 * Entfernt einen Chip vom Spielfeld, beeinflusst nicht das Spielfeld
		 * Wird nur aufgerufen, wenn ein Spieler einen Chip entfernt
		 */
		private void removeChip() {
			if (value != -1) {
				value--;
				repaint();
			}
		}

		/**
		 * Legt eine bestimmte Anzahl an Chips auf ein Feld, beeinflusst das
		 * Spielfeld nicht
		 * 
		 * @param anzahl
		 */
		private void addChips(int anzahl) {
			if (value != -1)
				value += anzahl;
		}

		/**
		 * Setzt die Anzahl der Chips auf einen bestimmten Wert
		 * 
		 * @param value
		 */
		private void setValue(int value) {
			if (value != -1)
				this.value = value;
		}

	}
}
