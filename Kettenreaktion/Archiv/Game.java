/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package core;

import java.awt.Point;
import java.util.LinkedList;

import core.Reaktor.ReaktorPoint;

/**
 * Kettenreaktionsspiel
 * 
 * @author Dennis van der Wals
 * 
 */
public class Game {
    // Interne Spielvariablen
    private byte      aktiverSpieler, spielbrett[][], chipsToSet[];
    private int       bank, maxKettenreaktion[], konto[];
    private boolean[] skip;

    // Externe Spielvariablen
    private Settings  einstellungen;

    /**
     * Standardkonstruktor.
     * 
     * @param einstellungen
     *            Settingsdatei mit den Einstellungen des Spiels
     */
    public Game(Settings einstellungen) {
        this.einstellungen = einstellungen;

        // Erzeugt das Spielfeld
        resetFeld();

        this.bank = einstellungen.getBankChips();

        // Berechnet die Gesamtzahl der Chips
        einstellungen.setGesamtChips(this.bank + countChips());

        // Erzeugt die Spielerkonten und fuellt sie auf
        this.konto = new int[einstellungen.getKi().length];
        this.chipsToSet = new byte[einstellungen.getKi().length];
        this.skip = new boolean[einstellungen.getKi().length];
        this.maxKettenreaktion = new int[einstellungen.getKi().length];
        for (int i = 0; i < this.konto.length; i++) {
            this.konto[i] = einstellungen.getStartChips();
            this.skip[i] = false;
            this.chipsToSet[i] = einstellungen.getMaxChipsToSet();
            einstellungen.setGesamtChips(einstellungen.getGesamtChips()
                    + einstellungen.getStartChips());
        }

        // Setzt den aktiven Spieler auf den ersten Spieler
        this.aktiverSpieler = 0;
        calcChipsToSet();
        // einstellungen.setEditable(false);
    }

    /**
     * Erstellt ein Spiel aus gespeicherten Daten eines anderen Spieles.
     * 
     * @param einstellungen
     *            Spieleinstellungen
     * @param konto
     *            Kontostaende
     * @param aktiverSpieler
     *            aktiver Spieler
     * @param brett
     *            Spielbrett
     * @param bank
     *            Anzahl der Chips in der Bank
     * @param skip
     *            Liste mit zu ueberspringenden Spielern
     * @param maxkettenreaktion
     *            Liste mit der groessten Kettenreaktion des jeweiligen Spielers
     */
    public Game(Settings einstellungen, int[] konto, byte aktiverSpieler,
            byte[][] brett, int bank, boolean[] skip, int[] maxkettenreaktion) {
        this.einstellungen = einstellungen;
        this.konto = konto;
        this.aktiverSpieler = aktiverSpieler;
        this.spielbrett = brett;
        this.bank = bank;
        this.skip = skip;
        this.maxKettenreaktion = maxkettenreaktion;
        this.chipsToSet = new byte[konto.length];
        for (int j = 0; j < maxkettenreaktion.length; j++)
            this.chipsToSet[j] = einstellungen.getMaxChipsToSet();
        calcChipsToSet();
    }

    /**
     * Berechnet die Auswirkungen auf das Spiel durch das setzen der Chips und
     * gibt die Veraenderungen fuer die Animation zurueck.
     * 
     * @param stapel
     *            Array mit zu legenden Chips vom Typ Point
     * @return Chips mit allen nachfolgenden Aenderungen
     */
    public LinkedList<ReaktorPoint> beendeRunde(Point[] stapel) {
        LinkedList<ReaktorPoint> aenderungen = null;

        // Leere Stapel abfangen
        if (stapel != null) {
            int gewinn = 0;
            int reaktionen = 0;
            aenderungen = new LinkedList<ReaktorPoint>();

            // Jeden Chip legen
            for (int i = 0; i < stapel.length; i++)
                // Nicht genutzte Chips abfangen (Da Konto zu klein)
                if (stapel[i] != null) {

                    // Neues Brett berechnen
                    this.spielbrett = this.einstellungen.getReaktor().reaktion(
                            this.spielbrett, stapel[i],
                            this.einstellungen.useBF(), false);

                    // Man bekommt nicht mehr aus der Bank, als drin ist
                    if (this.einstellungen.getReaktor().getAusBank() <= this.bank) {
                        gewinn += this.einstellungen.getReaktor().getAusBank();
                        this.bank -= this.einstellungen.getReaktor()
                                .getAusBank();
                    } else {
                        gewinn += this.bank;
                        this.bank = 0;
                    }

                    // Reingewinne hinzurechnen (Chips, die vom Brett fallen)
                    gewinn += this.einstellungen.getReaktor().getGewinn();

                    // Reaktionen abspeichern - fuer Statistik
                    reaktionen += this.einstellungen.getReaktor()
                            .getReaktionen();

                    // Speichert alle Veraenderungen auf dem Feld
                    aenderungen.add(this.einstellungen.getReaktor()
                            .getAenderung());

                    // Chip wird vom Konto entfernt
                    this.konto[this.aktiverSpieler]--;

                } else if (i == this.einstellungen.getMaxChipsToSet())
                    /*
                     * Abbruch bei Manipulationsversuch durch Einschleusen von
                     * mehr Chips
                     */
                    break;

            // Groesste Kettenreaktion speichern
            if (reaktionen > this.maxKettenreaktion[this.aktiverSpieler])
                this.maxKettenreaktion[this.aktiverSpieler] = reaktionen;

            // Gewinn hinzurechnen
            this.konto[this.aktiverSpieler] += gewinn;
        }

        // naechsten Spieler aktivieren
        naechsterSpieler();

        // Konsistenz der Chipsanzahl pruefen
        // if (countChips() != getChips())
        //            throw new ArithmeticException(Messages.getString("Game.0")); //$NON-NLS-1$

        // Markiert das Spiel nach der Aenderung als nicht gespeichert
        this.einstellungen.setSaved(false);
        return aenderungen;
    }

    /**
     * Berechnet fuer jeden Spieler, wie viele Spielsteine er setzen darf.
     */
    private void calcChipsToSet() {

        // Einschraenkung nur nutzen, wenn Limitierung gewuenscht.
        if (this.einstellungen.isChipsLimit())
            for (int i = 0; i < this.konto.length; i++)
                if (this.konto[i] < this.einstellungen.getMaxChipsToSet())
                    this.chipsToSet[i] = (byte) this.konto[i];
    }

    /**
     * Zaehlt, wie viele Chips auf dem Spielfeld liegen.
     * 
     * @return Anzahl der Chips
     */
    private int countChips() {
        int chips = 0;
        for (byte[] element : this.spielbrett)
            for (int y = 0; y < this.spielbrett[0].length; y++)
                if (element[y] != -1)
                    chips += element[y];
        return chips;
    }

    /**
     * Wird beim schliessen des Spiels aufgerufen. Gibt die Einstellungen wieder
     * frei und setzt die Bank auf 0 zum Schutz vor Manipulation.
     */
    public void gameClose() {
        // einstellungen.setEditable(true);
        this.bank = 0;
    }

    /**
     * Gibt den aktiven Spieler zurueck.
     * 
     * @return Nummer des aktiven Spielers
     */
    public byte getAktiverSpieler() {
        return this.aktiverSpieler;
    }

    /**
     * Gibt die Anzahl der Chips der Bank zurueck.
     * 
     * @return Chips der Bank
     */
    public int getBank() {
        return this.bank;
    }

    /**
     * Gibt eine Kopie des Spielfeldes zurueck.
     * 
     * @return Kopie des Spielfeldes
     */
    public byte[][] getSpielbrett() {
        return ArrayOperation.arrayCopy(this.spielbrett);
    }

    /**
     * Gibt die Anzahl der Chips wieder, die rechnerisch auf dem Spielfeld
     * liegen sollten.
     * 
     * @return Anzahl der Chips auf dem Spielbrett
     */
    public int getChips() {
        int konten = 0;
        for (int element : this.konto)
            konten += element;
        return this.einstellungen.getGesamtChips() - this.bank - konten;
    }

    /**
     * Gibt zurueck, wie viele Chips der aktuelle Spieler setzen darf.
     * 
     * @return
     */
    public byte getChipsToSet() {
        return this.chipsToSet[this.aktiverSpieler];
    }

    /**
     * Gibt eine Kopie der Konten zurueck.
     * 
     * @return Kopie der Konten
     */
    public int[] getKonto() {
        return ArrayOperation.arrayCopy(this.konto);
    }

    /**
     * Gibt eine Kopie der Liste mit den groessten Kettenreaktionen der Spieler
     * zurueck.
     * 
     * @return Liste mit den jeweils groessten Kettenreaktionen
     */
    public int[] getMaxKettenreaktion() {
        return ArrayOperation.arrayCopy(this.maxKettenreaktion);
    }

    /**
     * Gibt eine die Liste der zu ueberpringenden Spieler zurueck.
     * 
     * @return Liste mit zu ueberspringenden Spielern
     */
    public boolean[] getSkip() {
        return this.skip;
    }

    /**
     * Gibt die Anzahl der Spieler zurueck.
     * 
     * @return Anzahl der Spieler
     */
    public int getSpieler() {
        return this.konto.length;
    }

    /**
     * Ermittelt, ob das Spiel vorbei ist, wenn die Bank keine Chips mehr hat
     * oder nur noch ein Spieler mehr als 0 Chips hat.
     * 
     * @return Wahrheitswert
     */
    public boolean isOver() {
        // Zaehlt, wie viele Spieler uebersprungen werden
        int skips = 0;
        for (boolean element : this.skip)
            if (element)
                skips++;
        return ((this.bank <= 0) || (skips >= this.skip.length - 1));
    }

    /**
     * Aktiviert den naechsten Spieler
     */
    private void naechsterSpieler() {
        // Gibt es ein Kontolimit, werden Spieler ohne Chips markiert
        if (this.einstellungen.isChipsLimit())
            for (int i = 0; i < this.konto.length; i++)
                this.skip[i] = (this.konto[i] == 0);

        if (!isOver()) {
            // Zaehler wird um 1 erhoeht
            this.aktiverSpieler++;

            // Vom letzten zum ersten Spieler springen
            if (this.aktiverSpieler >= this.konto.length)
                this.aktiverSpieler = 0;

            // Wird der Spieler uebersprungen, naechster Spieler
            if (this.skip[this.aktiverSpieler])
                naechsterSpieler();

            calcChipsToSet();
        }
    }

    /**
     * Besetzt das Feld mit den Startwerten
     */
    private void resetFeld() {
        this.spielbrett = this.einstellungen.getMod().generateMap();
    }

}
