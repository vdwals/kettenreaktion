/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package gui.spiel;

import gui.Messages;
import implement.ArrOps;
import implement.BugReport;
import io.FileHandler;
import io.Recorder;
import io.SaveGameIO;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.MouseInputListener;

import core.Reaktor.ReaktorPoint;
import core.Settings;
import core.Spielkern;

/**
 * Oberflaeche fuer Kettenreaktion.
 * 
 * @author Dennis van der Wals
 * 
 */
public class SpielfensterThreaded extends JFrame implements MouseInputListener,
        ActionListener {
    /**
     * Default SerialID
     */
    private static final long   serialVersionUID = 1L;

    // Komponenten
    private Spielbrettinterface spielOberflaeche;
    private InfoPanel           infoPanel;
    private Menu                menu;
    private JButton             next;

    // Interne Variablen
    private Point[]             chips;
    private byte                chipNumber;
    private boolean             first;

    // Ausfuehrungsprogramme
    private Spielkern           spiel;
    private Thread              t;
    private Settings            einstellungen;
    private Recorder            recorder;
    private FileHandler         save;

    /**
     * Standardkonstruktor.
     * 
     * @param spiel
     *            Spiel, das gespielt werden soll
     * @param einstellungen
     *            Einstellungen des Spiels
     */
    public SpielfensterThreaded(Spielkern spiel, Settings einstellungen) {
        super(Messages.getString("Settings.43")); //$NON-NLS-1$
        this.einstellungen = einstellungen;
        this.spiel = spiel;
        first = true;

        // Komponenten initialisieren:
        if (einstellungen.isRecord())
            try {
                this.recorder = new Recorder(Messages.getString("Spiel.1")); //$NON-NLS-1$
            } catch (Exception e) {
                BugReport.recordBug(e);
            }

        this.infoPanel = new InfoPanel(einstellungen.getNamen(), getNext());
        this.save = new SaveGameIO(einstellungen, spiel);
        this.menu = new Menu(this.save, einstellungen);
        this.spielOberflaeche = new Spielbrettinterface(einstellungen);
        this.getSpielOberflaeche().setBrett(spiel.getSpielbrett());
        this.getSpielOberflaeche().addMouseListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == getNext()) {

            // Variablen zuruecksetzen
            this.chips = new Point[this.einstellungen.getMaxChipsToSet()];
            this.chipNumber = 0;

            // Ersten Zug abfangen
            if (first) {

                first = false;
                getNext().setText("Zug beenden");
                getNext().setEnabled(false);

                // KI-Zug initialisieren
                kiZug();

            } else
                // Button entsperren
                getNext().setEnabled(false);

            // Thread starten, wenn noch nicht geschehen
            if (t == null || !t.isAlive())
                starte();
        }
    }

    /**
     * Schliesst das Spiel, die Aufzeichnungen und gibt die Einstellungen frei.
     */
    public void gameClose() {

        // Aufzeichnungen beenden
        try {
            if (this.getRecorder() != null)
                this.getRecorder().close();
            for (int i = 0; i < this.einstellungen.getKi().length; i++)
                if (this.einstellungen.getKi()[i] == null)
                    this.einstellungen.getKi()[i].save();
        } catch (Exception e) {
            BugReport.recordBug(e);
        }

        if (this.spiel.isOver()) {
            int[] konto = this.spiel.getKonto();

            // Statistik aktualisieren
            this.einstellungen.getStatistik().SpielBeendet(
                    this.spiel.getKonto(), this.einstellungen.getNamen(),
                    this.spiel.getMaxKettenreaktion(),
                    this.einstellungen.getKi());

            // Ausgabe der Siegestabelle
            int[] reihenfolge = ArrOps.indexSort(ArrOps.arrayCopy(konto));
            StringBuilder ausgabe = new StringBuilder();
            ausgabe.append("Spiel zuende\n");

            if (this.einstellungen.isDebug()
                    && konto[reihenfolge[0]] > konto[reihenfolge[1]])
                BugReport.recordBug(new Exception("Falsche Reihenfolge: "
                        + ArrOps.arrayToString(konto) + "- r: "
                        + ArrOps.arrayToString(reihenfolge)));

            for (int i = 0; i < reihenfolge.length; i++) {
                ausgabe.append((i + 1)
                        + ". - "
                        + this.einstellungen.getNamen()[reihenfolge[reihenfolge.length
                                - 1 - i]] + "\t: "
                        + konto[reihenfolge[reihenfolge.length - 1 - i]] + "\n");
            }

            JOptionPane.showMessageDialog(null, ausgabe.toString());

            getSpielOberflaeche().setEnabled(false);
            getInfoPanel().setEnabled(false);
        }

        // Einstellungen freigeben
        this.spiel.gameClose();
    }

    /**
     * Erzeugt den Button fuer das Beenden des Zuges.
     * 
     * @return Button zum Beenden des Zuges
     */
    public JButton getNext() {
        if (this.next == null) {
            // Ist der erste Spieler KI oder Mensch?
            if (this.einstellungen.getKi()[0] != null)
                this.next = new JButton("Spiel starten");
            else {
                this.next = new JButton("Zug Beenden");
                first = false;
            }
            this.next.setToolTipText("Beendet den Zug");
            this.next.addActionListener(this);
        }
        return this.next;
    }

    /**
     * Gibt zurueck, ob das Spiel zuende gespielt wurde.
     * 
     * @return Wahrheitswert
     */
    public boolean isOver() {
        return this.spiel.isOver();
    }

    /*
     * Ungenutzte Methoden (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */

    /**
     * Gibt zurueck, ob das Spiel zuende gespielt oder gespeichert wurde.
     * 
     * @return Wahrheitswert
     */
    public boolean isSaved() {
        return this.spiel.isOver() || this.einstellungen.isSaved();
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {
        if (!this.spiel.isOver()
                && (this.einstellungen.getKi()[this.spiel.getAktiverSpieler()] == null)) {

            // Chip gelegt oder entfernt
            if ((arg0.getButton() == MouseEvent.BUTTON1)
                    && (this.chipNumber < this.spiel.getChipsToSet())) {

                // Ermittelt das gewaehlte Feld
                Point chip = this.einstellungen.getMod().getKoordinaten(
                        arg0.getPoint().x, arg0.getPoint().y);

                // Speichert den Chip in einem Array
                this.chips[this.chipNumber++] = chip;

                // Auf das Spielfeld legen
                this.getSpielOberflaeche().addChip(chip.x, chip.y);
            } else if ((arg0.getButton() == MouseEvent.BUTTON3)
                    && (this.chipNumber > 0)) {

                // Letzten Chip nehmen und Zaehler reduzieren
                Point chip = this.chips[--this.chipNumber];
                this.chips[this.chipNumber] = null;

                // Vom Spielfeld nehmen
                this.getSpielOberflaeche().removeChip(chip.x, chip.y);

            }

            // Anzeige auffrischen:
            this.getInfoPanel().recalc(this.spiel.getAktiverSpieler(),
                    this.spiel.getKonto(), this.spiel.getBank(),
                    (byte) (this.spiel.getChipsToSet() - this.chipNumber),
                    this.spiel.getRunde());

        }
    }

    @Override
    public void mouseDragged(MouseEvent arg0) {
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
    }

    @Override
    public void mouseMoved(MouseEvent arg0) {
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
    }

    /**
     * Speichert das Spiel
     */
    public void save() {
        try {
            this.save.save();
        } catch (IOException e) {
            BugReport.recordBug(e);
        }
    }

    /**
     * Programm starten
     */
    public void build() {

        setJMenuBar(this.menu);
        setLayout(new BorderLayout(5, 5));

        add(this.getSpielOberflaeche(), BorderLayout.CENTER);
        add(this.getInfoPanel(), BorderLayout.WEST);

        setSize(600, 500);

        // In die Mitte positionieren
        setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
                (Toolkit.getDefaultToolkit().getScreenSize().height - getHeight()) / 2);

        setVisible(true);

        // Anzeige aktualisieren
        this.getSpielOberflaeche().repaint();
        this.getInfoPanel().recalc(this.spiel.getAktiverSpieler(),
                this.spiel.getKonto(), this.spiel.getBank(),
                (byte) (this.spiel.getChipsToSet() - this.chipNumber),
                this.spiel.getRunde());
    }

    public Spielbrettinterface getSpielOberflaeche() {
        return spielOberflaeche;
    }

    public InfoPanel getInfoPanel() {
        return infoPanel;
    }

    public Recorder getRecorder() {
        return recorder;
    }

    public Settings getEinstellungen() {
        return this.einstellungen;
    }

    /**
     * Fuehrt den Zug der KI durch.
     */
    private void kiZug() {
        // Warteicon aktivieren
        this.getInfoPanel().getSanduhr().setVisible(true);

        // Uebermittelt der KI wie viele Steine sie setzen darf,
        this.getEinstellungen().getKi()[this.spiel.getAktiverSpieler()]
                .setChipsToSet(this.spiel.getChipsToSet());

        // Fragt ab, welche Steine die KI setzen will
        this.chips = this.getEinstellungen().getKi()[this.spiel
                .getAktiverSpieler()].getChips(this.spiel.getSpielbrett());

    }

    private void starte() {
        t = new Thread(new Runnable() {

            @Override
            public void run() {
                System.out.println("Ich starte");
                while (!spiel.isOver()) {
                    System.out.println("Neue Runde");
                    // Runde speichern
                    if (getRecorder() != null)
                        try {
                            getRecorder().writeStatus(
                                    getEinstellungen().getNamen(),
                                    spiel.getKonto(), spiel.getBank(),
                                    spiel.getAktiverSpieler(),
                                    spiel.getSpielbrett(), chips);
                        } catch (Exception e) {
                            BugReport.recordBug(e);
                        }

                    // naechste Runde starten und Veraenderungen speichern
                    LinkedList<ReaktorPoint> aenderungen = spiel
                            .beendeRunde(chips);

                    // Aenderungen printen:
                    printAenderungen(aenderungen);
                    Thread.yield();

                    // Chipzahl Konsistenz pruefen
                    if (getEinstellungen().isDebug())
                        if (spiel.getChips() != getSpielOberflaeche()
                                .countVisibleChips())
                            BugReport.recordBug(new IllegalArgumentException(
                                    "Chips auf dem Feld = "
                                            + spiel.getChips()
                                            + "; Chips zu sehen = "
                                            + getSpielOberflaeche()
                                                    .countVisibleChips()));

                    if (!spiel.isOver())
                        if (getEinstellungen().getKi()[spiel
                                .getAktiverSpieler()] != null) {

                            kiZug();

                        } else {
                            // Menschzug
                            getNext().setEnabled(true);
                        }

                    while (getNext().isEnabled()) {
                        try {
                            System.out.println("Ich mache eine Pause");
                            Thread.sleep(1000);
                            Thread.yield();
                        } catch (InterruptedException e) {
                            BugReport.recordBug(e);
                        }
                    }
                }
                gameClose();
            }

            /**
             * Zeichnet alle Aenderungen am Ende des Zuges auf die Oberflaeche.
             * 
             * @param aenderungen
             *            Anderungsbaum
             */
            public void printAenderungen(LinkedList<ReaktorPoint> aenderungen) {
                // Chipanimation
                if (einstellungen.isAnimation()) {
                    getSpielOberflaeche().plottChanges(aenderungen,
                            getInfoPanel(), einstellungen.getAnimation());
                    try {
                        // Auf Animation warten
                        getSpielOberflaeche().t.join();
                    } catch (InterruptedException e) {
                        BugReport.recordBug(e);
                    }
                } else {
                    getSpielOberflaeche().setBrett(spiel.getSpielbrett());
                }

                // Anzeige auffrischen
                getInfoPanel().recalc(spiel.getAktiverSpieler(),
                        spiel.getKonto(), spiel.getBank(),
                        (byte) (spiel.getChipsToSet() - chipNumber),
                        spiel.getRunde());
                getSpielOberflaeche().setBrett(spiel.getSpielbrett());

                // Warteicon entfernen
                getInfoPanel().getSanduhr().setVisible(false);
            }
        });
        t.start();
    }

}
