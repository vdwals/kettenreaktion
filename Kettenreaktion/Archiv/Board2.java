/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package gui.spiel;

import gui.spiel.pfadalgorithmen.PfadSuche;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.LinkedList;

import javax.swing.JPanel;

import core.Reaktor.ReaktorPoint;
import core.Settings;

/**
 * Klasse dient der farblichen Anzeige des uebergebenen Spielfeldes.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Board extends JPanel {
    /**
     * Default Serial
     */
    private static final long serialVersionUID = 1L;

    // Spieleinstellungen
    private Settings          einstellungen;

    // Interne Variablen
    /*
     * Extravariablen zum einhalten der Seitenverhaeltnisse, da die Funktion zur
     * Groesseneinstellung aufgerufen wird, bevor ein Feld existiert. Das fuehrt
     * zum Absturz.
     */
    private int               scaleX, scaleY, spielbrett[][];
    public Thread             t;

    /**
     * Standardkonstruktor
     * 
     * @param einstellungen
     *            Spieleinstellungen
     */
    public Board(Settings einstellungen) {
        this.einstellungen = einstellungen;
        this.scaleX = this.scaleY = 1;

        // Gegen das flackern
        setDoubleBuffered(true);
    }

    /**
     * Legt einen Chip auf das Spielfeld, beeinfluss nicht das Spielfeld.
     * 
     * @param x
     *            x-Koordinate
     * @param y
     *            y-Koordinate
     */
    public void addChip(int x, int y) {
        if (this.spielbrett[x][y] != -1)
            this.spielbrett[x][y]++;
    }

    /**
     * Legt eine bestimmte Anzahl an Chips auf ein Feld, beeinflusst das
     * Spielfeld nicht
     * 
     * @param anzahl
     */
    private void addChips(int x, int y, int anzahl) {
        if (this.spielbrett[x][y] != -1)
            this.spielbrett[x][y] += anzahl;
    }

    /**
     * Zaehlt die sichtbaren Chips auf dem Feld.
     * 
     * @return Anzahl der sichtbaren Chips.
     */
    public int countVisibleChips() {
        int count = 0;
        for (int[] element : this.spielbrett)
            for (int y = 0; y < element.length; y++)
                if (element[y] != -1)
                    count += element[y];
        return count;
    }

    /**
     * Bestimmt die zu malende Farbe anhand der Chipanzahl
     * 
     * @return Farbe fuer das Feld
     */
    private Color getColor(int i) {
        return this.einstellungen.getFarbe(i + 3);
    }

    @Override
    public void paint(Graphics g) {
        boolean s = true;

        int feldBreite = getWidth() / this.scaleX;
        int feldHoehe = getHeight() / this.scaleY;

        g.setColor(this.einstellungen.getFarbe(0));
        g.fillRect(0, 0, getWidth(), getHeight());
        for (int x = 0; x < this.scaleX; x++) {
            for (int y = 0; y < this.scaleY; y++) {
                /*
                 * Mod-Poylgon holen:
                 */
                Polygon feld = einstellungen.getMod()
                        .getPolygon(einstellungen.getKarte(), x, y, getWidth(),
                                getHeight());

                // Umrandung des Feldes zeichnen
                g.setColor(Color.BLACK);
                g.drawPolygon(feld);

                if (this.spielbrett[x][y] == -1) {
                    g.setColor(this.einstellungen.getFarbe(3));
                    g.fillPolygon(feld);
                    // g.fillRect(x * feldBreite, y * feldHoehe, feldBreite,
                    // feldHoehe);

                } else {

                    // g.drawRect(x * feldBreite, y * feldHoehe, feldBreite,
                    // feldHoehe);

                    // Hintergrund malen
                    // g.setColor(einstellungen.getFarbe(0));
                    // g.fillRect(x * feldBreite, y * feldHoehe, feldBreite,
                    // feldHoehe);

                    /*
                     * Chips zeichen.
                     */
                    // Skaliervariablen initialisieren
                    int xI = 0;
                    int yI = 0;
                    int breiteI = 0;
                    int hoeheI = 0;

                    // Skalierfaktor von Mod erhalten
                    double skal = 14;

                    // Chips zeichnen
                    for (int i = 0; i < this.spielbrett[x][y]; i++) {

                        // Skalierung berechnen
                        xI = x * feldBreite
                                + (int) (i * (double) feldBreite / skal);
                        yI = y * feldHoehe
                                + (int) (i * (double) feldHoehe / skal);
                        breiteI = (int) (feldBreite * (1 - (double) i
                                / (skal / 2)));
                        hoeheI = (int) (feldHoehe * (1 - (double) i
                                / (skal / 2)));

                        // Bereich fuellen
                        g.setColor(getColor(i + 1));
                        g.fillOval(xI, yI, breiteI, hoeheI);

                        // Umrandung zeichnen
                        g.setColor(Color.BLACK);
                        g.drawOval(xI, yI, breiteI, hoeheI);
                    }

                    // Die Mitte des Feldes faerben
                    xI = x * feldBreite
                            + (int) (7 * (double) feldBreite / (skal + 2));
                    yI = y * feldHoehe
                            + (int) (7 * (double) feldHoehe / (skal + 2));
                    breiteI = (int) (feldBreite * (1 - (double) 7
                            / ((skal + 2) / 2)));
                    hoeheI = (int) (feldHoehe * (1 - (double) 7
                            / ((skal + 2) / 2)));

                    // Umrandung zeichnen
                    g.setColor(Color.BLACK);
                    g.drawOval(xI, yI, breiteI, hoeheI);

                    // Abwechselnd Mitte einfaerben
                    if (s)
                        g.setColor(this.einstellungen.getFarbe(1));
                    else
                        g.setColor(this.einstellungen.getFarbe(2));
                    g.fillOval(xI, yI, breiteI, hoeheI);
                }
                s = !s;
            }
            s = !s;
        }
    }

    /**
     * Zeichnet nacheinander die Veraenderungen ein um die Reaktionen so
     * anschaulich zu machen.
     * 
     * @param aenderungen
     *            Liste mit Reaktionsbaeumen
     * @param panel
     *            Panel zur Aktuallisierung der ChipsToPlace-Anzeige
     */
    public void plottChanges(LinkedList<ReaktorPoint> aenderungen,
            final InfoPanel panel, PfadSuche pfadSuchAlgoithmus) {
        final PfadSuche p = pfadSuchAlgoithmus;
        p.setAenderungsBaum(aenderungen);
        this.t = new Thread(new Runnable() {

            @Override
            public void run() {
                int chips = Board.this.einstellungen.getMaxChipsToSet();
                // Alle Aenderungen printen
                while (p.hasNext()) {

                    // Minimierung des Flackerns: Aktuallisierungsbereich in die
                    // Mitte setzen.
                    int xlinks = getWidth() / 2;
                    int yoben = getHeight() / 2;
                    int xrechts = xlinks;
                    int yunten = yoben;

                    for (ReaktorPoint reaktorPoint : p.getNext()) {

                        // Aktuallisierungsfeld anpassen
                        if (reaktorPoint.x < xlinks)
                            xlinks = reaktorPoint.x;
                        if (reaktorPoint.y < yoben)
                            yoben = reaktorPoint.y;
                        if (reaktorPoint.x > xrechts)
                            xrechts = reaktorPoint.x;
                        if (reaktorPoint.y > yunten)
                            yunten = reaktorPoint.y;

                        /*
                         * Wurde der Chip vom Spieler gelegt, wird der Zaehler
                         * verringert
                         */
                        if (reaktorPoint.isGesetzt()) {
                            panel.getToPlaceLabel().setText("" + --chips); //$NON-NLS-1$
                            panel.paintImmediately(panel.getSubPanel()
                                    .getBounds());
                        }

                        // Spielbrettanzeige manipulieren
                        addChips(reaktorPoint.x, reaktorPoint.y,
                                reaktorPoint.getChange());
                    }
                    // Ausschnitt neu zeichnen
                    Board.this.paintImmediately(xlinks, yoben,
                            xrechts - xlinks, yunten - yoben);
                    // Alles neu zeichnen
                    // Board.this.paint(Board.this.getGraphics());

                    // Pause
                    try {
                        Thread.sleep(Board.this.einstellungen.getSleep());
                    } catch (InterruptedException e) {
                    }
                }
            }
        });
        this.t.start();
    }

    /**
     * Entfernt einen Chip vom Spielfeld, beeinfluss nicht das Spielfeld.
     * 
     * @param x
     *            x-Koordinate
     * @param y
     *            y-Koordinate
     */
    public void removeChip(int x, int y) {
        if (this.spielbrett[x][y] != -1)
            this.spielbrett[x][y]--;
    }

    @Override
    public void repaint() {
        // Quadratisches Format erhalten
        if (getHeight() * this.scaleY < getWidth() * this.scaleX)
            setSize((getHeight() * this.scaleY / this.scaleX), getHeight());
        if (getWidth() * this.scaleX < getHeight() * this.scaleY)
            setSize(getWidth(), (this.scaleX * getWidth() / this.scaleY));
        super.repaint();
    }

    /**
     * Setter fuer das Brett.
     * 
     * @param spielbrett
     *            zu uebernehmendes Spielbrett
     */
    public void setBrett(int[][] spielbrett) {
        this.scaleX = spielbrett.length;
        this.scaleY = spielbrett[0].length;
        this.spielbrett = spielbrett;

        repaint();
    }

}
