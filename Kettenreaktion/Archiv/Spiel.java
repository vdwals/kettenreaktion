/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package gui.spiel;

import gui.Messages;
import io.FileHandler;
import io.Recorder;
import io.SaveGameIO;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JWindow;
import javax.swing.event.MouseInputListener;

import core.ArrayOperation;
import core.Game;
import core.Reaktor.ReaktorPoint;
import core.Settings;

/**
 * Oberflaeche fuer Kettenreaktion.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Spiel extends JFrame implements MouseInputListener, ActionListener {
    /**
     * Default SerialID
     */
    private static final long serialVersionUID = 1L;

    // Komponenten
    private Board             spielOberflaeche;
    private InfoPanel         infoPanel;
    private Menu              menu;
    private JButton           next;

    // Interne Variablen
    private Game              spiel;
    private Settings          einstellungen;
    private Point[]           chips;
    private int               chipNumber;
    private Recorder          recorder;
    private FileHandler       save;

    /**
     * Standardkonstruktor.
     * 
     * @param spiel
     *            Spiel, das gespielt werden soll
     * @param einstellungen
     *            Einstellungen des Spiels
     */
    public Spiel(Game spiel, Settings einstellungen) {
        super(Messages.getString("Settings.43")); //$NON-NLS-1$
        this.einstellungen = einstellungen;
        this.spiel = spiel;

        // Komponenten initialisieren:
        if (einstellungen.isRecord())
            try {
                this.recorder = new Recorder(Messages.getString("Spiel.1")); //$NON-NLS-1$
            } catch (Exception e) {
            }

        this.infoPanel = new InfoPanel(einstellungen.getNamen());
        this.save = new SaveGameIO(einstellungen, spiel);
        this.menu = new Menu(this.save, einstellungen);
        this.spielOberflaeche = new Board(einstellungen);
        this.spielOberflaeche.setBrett(spiel.getSpielbrett());
        this.spielOberflaeche.addMouseListener(this);

        // Spiel initialisieren
        this.chips = new Point[einstellungen.getMaxChipsToSet()];
        this.chipNumber = 0;
    }

    /**
     * Schliesst das Spiel, die Aufzeichnungen und gibt die Einstellungen frei.
     */
    public void gameClose() {

        // Aufzeichnungen beenden
        try {
            if (this.recorder != null)
                this.recorder.close();
            for (int i = 0; i < this.einstellungen.getKi().length; i++)
                this.einstellungen.getKi()[i].save();
        } catch (Exception e) {
        }

        // Einstellungen freigeben
        this.spiel.gameClose();
    }

    /**
     * Rechnet die Koordinaten der Maus in ein Spielfeld um.
     * 
     * @param maus
     *            Koordinaten der Maus
     * @return umgerechnete Koordinaten
     */
    private Point getFeld(Point maus) {

        // Feld ermitteln
        return this.einstellungen.getMod().getKoordinaten(maus.x, maus.y);
    }

    /**
     * Gibt zurueck, ob das Spiel zuende gespielt wurde.
     * 
     * @return Wahrheitswert
     */
    public boolean isOver() {
        return this.spiel.isOver();
    }

    /**
     * Gibt zurueck, ob das Spiel zuende gespielt oder gespeichert wurde.
     * 
     * @return Wahrheitswert
     */
    public boolean isSaved() {
        return this.spiel.isOver() || this.einstellungen.isSaved();
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {
        if (!this.spiel.isOver()) {

            // Spieler oder KI-Zug?
            if (this.einstellungen.getKi()[this.spiel.getAktiverSpieler()] == null) {

                // Chip gelegt oder entfernt
                if (arg0.getButton() == MouseEvent.BUTTON1) {

                    // Ueberprueft, ob der letzte Stein bereits gelegt wurde
                    if (this.chipNumber == this.spiel.getChipsToSet())
                        nextRound();
                    else {

                        // Ermittelt das gewaehlte Feld
                        Point chip = getFeld(arg0.getPoint());

                        // Speichert den Chip in einem Array
                        this.chips[this.chipNumber++] = chip;

                        // Auf das Spielfeld legen
                        this.spielOberflaeche.addChip(chip.x, chip.y);
                    }

                } else if ((arg0.getButton() == MouseEvent.BUTTON3)
                        && (this.chipNumber > 0)) {

                    // Letzten Chip nehmen und Zaehler reduzieren
                    Point chip = this.chips[--this.chipNumber];
                    this.chips[this.chipNumber] = null;

                    // Vom Spielfeld nehmen
                    this.spielOberflaeche.removeChip(chip.x, chip.y);

                }

                // Anzeige auffrischen:
                this.infoPanel.recalc(this.spiel.getAktiverSpieler(),
                        this.spiel.getKonto(), this.spiel.getBank(),
                        this.spiel.getChipsToSet() - this.chipNumber);

            } else {
                // Uebermittelt der KI wie viele Steine sie setzen darf,
                this.einstellungen.getKi()[this.spiel.getAktiverSpieler()]
                        .setChipsToSet(this.spiel.getChipsToSet());

                // Fragt ab, welche Steine die KI setzen will
                this.chips = this.einstellungen.getKi()[this.spiel
                        .getAktiverSpieler()].getChips(this.spiel
                        .getSpielbrett());

                nextRound();
            }
        } else {

            // Die Aufzeichnung wird beendet
            gameClose();

            // Statistik aktualisieren
            this.einstellungen.getStatistik().SpielBeendet(
                    this.spiel.getKonto(), this.einstellungen.getNamen(),
                    this.spiel.getMaxKettenreaktion(),
                    this.einstellungen.getKi());

            // Ausgabe des Siegers
            JOptionPane.showMessageDialog(
                    null,
                    this.einstellungen.getNamen()[ArrayOperation
                            .getMax(this.spiel.getKonto())]
                            + Messages.getString("Spiel.2") //$NON-NLS-1$
                            + this.einstellungen.getNamen()[ArrayOperation
                                    .getMax(this.spiel.getMaxKettenreaktion())]
                            + Messages.getString("MapIOData.1") //$NON-NLS-1$
                            + this.spiel.getMaxKettenreaktion()[ArrayOperation
                                    .getMax(this.spiel.getMaxKettenreaktion())]
                            + Messages.getString("Spiel.4")); //$NON-NLS-1$
        }
    }

    /*
     * Ungenutzte Methoden (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */

    @Override
    public void mouseDragged(MouseEvent arg0) {
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
    }

    @Override
    public void mouseMoved(MouseEvent arg0) {
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
    }

    /**
     * Aktiviert die naechste Runde des Spiels
     */
    private void nextRound() {

        // Runde speichern
        if (this.recorder != null)
            try {
                this.recorder.writeStatus(this.einstellungen.getNamen(),
                        this.spiel.getKonto(), this.spiel.getBank(),
                        this.spiel.getAktiverSpieler(),
                        this.spiel.getSpielbrett(), this.chips);
            } catch (Exception e) {
            }

        this.spielOberflaeche.setBrett(this.spiel.getSpielbrett());

        // naechste Runde starten und Veraenderungen speichern
        this.spiel.setBf(this.einstellungen.useBF());
        LinkedList<ReaktorPoint> aenderungen = this.spiel
                .beendeRunde(this.chips);

        // Aenderungen printen:
        if (this.einstellungen.isAnimation())
            this.spielOberflaeche.plottChanges(aenderungen, this.infoPanel,
                    this.einstellungen.getAnimation());
        try {
            this.spielOberflaeche.t.join();
        } catch (InterruptedException e) {
        }

        // Variablen zuruecksetzen
        this.chips = new Point[this.einstellungen.getMaxChipsToSet()];
        this.chipNumber = 0;

        // Anzeige auffrischen
        this.infoPanel.recalc(this.spiel.getAktiverSpieler(),
                this.spiel.getKonto(), this.spiel.getBank(),
                this.spiel.getChipsToSet() - this.chipNumber);
        this.spielOberflaeche.setBrett(this.spiel.getSpielbrett());

        // Spieler erinnern:
        showSplash();

        // Chipzahl Konsistenz pruefen
        if (this.spiel.getChips() != this.spielOberflaeche.countVisibleChips())
            System.out.println(Messages.getString("Spiel.5")); //$NON-NLS-1$
    }

    /**
     * Speichert das Spiel
     */
    public void save() {
        try {
            this.save.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Erzeugt einen SplashScreen
     */
    private void showSplash() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                JWindow w = new JWindow();
                w.setLayout(null);
                JLabel reminde = new JLabel(
                        Spiel.this.einstellungen.getNamen()[Spiel.this.spiel
                                .getAktiverSpieler()]);
                w.setSize(200, 100);
                reminde.setBounds(50, 40, 100, 20);
                w.add(reminde);
                w.setAlwaysOnTop(true);
                w.setLocation(
                        (Toolkit.getDefaultToolkit().getScreenSize().width - w
                                .getWidth()) / 2, (Toolkit.getDefaultToolkit()
                                .getScreenSize().height - w.getHeight()) / 2);
                w.setVisible(true);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }
                w.dispose();
                w = null;
            }
        }).start();
    }

    /**
     * Programm starten
     */
    public void start() {

        setJMenuBar(this.menu);
        setLayout(new BorderLayout(5, 5));

        add(this.spielOberflaeche, BorderLayout.CENTER);
        add(this.infoPanel, BorderLayout.WEST);

        setSize(600, 500);

        // In die Mitte positionieren
        setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
                (Toolkit.getDefaultToolkit().getScreenSize().height - getHeight()) / 2);

        setVisible(true);

        // Anzeige aktualisieren
        this.spielOberflaeche.repaint();
        this.infoPanel.recalc(this.spiel.getAktiverSpieler(),
                this.spiel.getKonto(), this.spiel.getBank(),
                this.spiel.getChipsToSet() - this.chipNumber);
    }

    /**
     * Erzeugt den Button fuer das Beenden des Zuges.
     * 
     * @return Button zum Beenden des Zuges
     */
    public JButton getNext() {
        if (next == null) {
            next = new JButton("Zug beenden");
            next.setToolTipText("Beendet den Zug");
            next.addActionListener(this);
        }
        return next;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub

    }

}
