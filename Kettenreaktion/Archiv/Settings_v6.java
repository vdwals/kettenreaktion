/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package spiel;

import gui.help.Help;
import gui.language.Messages;
import implement.ArrOps;
import implement.BugReport;
import implement.Updater;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Random;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import spiel.gui.pfadalgorithmen.BreadthFirst;
import spiel.gui.pfadalgorithmen.DepthFirst;
import spiel.gui.pfadalgorithmen.PfadSuche;
import spiel.gui.pfadalgorithmen.Wave;
import spiel.io.SettingsIO_v6;
import spiel.ki.Strategie;
import spiel.mod.Hexagon;
import spiel.mod.Mod;
import spiel.mod.Quadrat;
import spiel.mod.Triangel;
import statistik.Statistik;

/**
 * Klasse zum speichern der Einstellungen des Spiels.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Settings_v6 extends JFrame implements ActionListener, ChangeListener,
        CaretListener {
    /**
     * Default SerialID
     */
    private static final long serialVersionUID = 1L;

    // GUI-Komponenten
    private JCheckBox         autoUpdateCheckBox, chipsLimit, wuerfelnCheckBox,
            showAnimation, debugCheckBox, fiveRuleCheckBox, bankRuleCheckBox;
    private JTextField        maxSpielerField, maxChipsToSetField,
            bankChipsField, startChipsField, wuerfelnVon, wuerfelnBis;
    private JButton           lookForUpdate, toDefault, set, cancel,
            farbenButton[], ok;
    private JSlider           speed;
    private JLabel            speedLabel;
    private JRadioButton[]    auswahl;
    private ButtonGroup       bg;
    private JComboBox         langSelection, modSelection, mapModusSelection;

    // Spieloptionen
    private int               gesamtChips, bankChips, turnierPunkte[][];
    private byte              startChips, maxSpieler, maxChipsToSet,
            originalPosition[], modus, wuerfelStart, wuerfelEnd, mapModus;
    private String[]          namen;
    private Strategie[]       ki;
    private boolean           random, saved, wuerfeln, fiveRule, bankRule;
    private Reaktor           reaktor;

    // Anzeigeoptionen
    private Color[]           farben;
    private int               sleep;
    private boolean           showA;

    // Simulationsoptionen
    private int               loops;

    // Programmoptionen
    private boolean           autoUpdate, debug;

    // Statistikoptionen
    private boolean           livePanel;

    // Konstanten
    private PfadSuche[]       animation;
    private Statistik         statistik;
    private Mod[]             mod;
    private String[]          lang;

    // Ausfuehrungsprogramme
    private SettingsIO_v6        io;
    private Updater           update;
    private Random            peter;
    private Help              hilfe;

    /**
     * Standardkonstruktor mit Standardwerten.
     * 
     * @throws IOException
     */
    public Settings_v6() {
        // Ausfuehrungsprogramme initialisieren
        this.peter = new Random();
        this.io = new SettingsIO_v6();
        this.setUpdate(new Updater());
        this.statistik = new Statistik();
        try {
            this.hilfe = new Help();
        } catch (IOException e2) {
            BugReport.recordBug(e2);
        }

        // Standardwerte laden:
        this.setAnimationGUI(new PfadSuche[3]);
        this.getAnimationGUI()[0] = new DepthFirst();
        this.getAnimationGUI()[1] = new BreadthFirst();
        this.getAnimationGUI()[2] = new Wave();
        this.setMods(new Mod[3]);
        this.getMods()[0] = new Triangel();
        this.getMods()[1] = new Quadrat();
        this.getMods()[2] = new Hexagon();
        this.setLang(new String[3]);
        this.getLang()[0] = "de";
        this.getLang()[1] = "en";
        this.getLang()[2] = "sv";

        // Temporaere Werte erzeugen
        byte selectedAnimation = 0;

        // Werte initialisieren
        this.namen = new String[0];
        this.ki = new Strategie[0];
        this.originalPosition = new byte[0];

        // Groesste Chipzahl abfragen
        int[] maxChips = new int[this.getMods().length];
        for (int i = 0; i < maxChips.length; i++)
            maxChips[i] = this.getMods()[i].getMaxChips();
        this.setFarben(new Color[4 + maxChips[ArrOps.getMax(maxChips)]]);
        // editable = true;

        try {
            this.io.load();

            // Aus Datei lesen
            this.modus = this.io.getModus();
            this.maxSpieler = this.io.getMaxSpieler();
            this.startChips = this.io.getStartChips();
            this.maxChipsToSet = this.io.getChipsToSet();
            this.bankChips = this.io.getBankChips();
            this.setSleep(this.io.getSleep());
            selectedAnimation = this.io.getAnimation();
            this.wuerfelEnd = this.io.getWuerfelEnd();
            this.wuerfelStart = this.io.getWuerfelStart();
            this.mapModus = this.io.getMapModus();

            this.setShowA(this.io.isShowA());
            this.setAutoUpdate(this.io.isAutoUpdate());
            this.setDebug(this.io.isDebug());
            this.wuerfeln = this.io.isWuerfeln();
            this.setFiveRule(this.io.isFiveRule());
            this.setBankRule(this.io.isBankRule());

            // Farben auslesen
            byte counter = 0;
            for (counter = 0; counter < this.io.getColors().length; counter++)
                this.getFarben()[counter] = new Color(
                        this.io.getColors()[counter]);
            /*
             * Aeltere Versionen verfuegen ueber weniger Farben, den Rest mit
             * der letzten Farbe dunkler werdend auffuellen.
             */
            Color last = this.getFarben()[counter - 1];
            for (byte i = counter; i < this.getFarben().length; i++)
                this.getFarben()[i] = last.darker();

        } catch (Exception e) {
            BugReport.recordBug(e);

            // Standards laden
            toDefault();

            save();
        }

        // Standardkarte erzeugen
        setKarte(this.getMods()[this.modus].getStandardKarte());

        // Reaktor erzeugen
        this.reaktor = new Reaktor(getMod(), this.statistik);

        // Update durchfuehren
        if (this.isAutoUpdate())
            try {
                this.getUpdate().getUpdate();
            } catch (IOException e1) {
                BugReport.recordBug(e1);
            }

        // Entfernen
        this.livePanel = true;
        build(selectedAnimation);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == getLookForUpdate())
            // Versuche Update zu starten
            try {
                this.getUpdate().getUpdate();
            } catch (IOException e1) {
                BugReport.recordBug(e1);
            }
        else if (e.getSource() == getToDefault())

            // Standardwerte setzen
            toDefault();

        else if (e.getSource() == getUebernehmen())

            // Werte speichern
            save();

        else if (e.getSource() == getCancel())

            // Fenster schlie�en
            dispose();

        else if (e.getSource() == getOk()) {

            // Speichern und Fenster schlie�en
            save();
            dispose();

        } else {
            getUebernehmen().setEnabled(true);
            // Farbe aendern
            for (int i = 0; i < getFarbenButton().length; i++)

                // Ueberpruefe jeden Button
                if (getFarbenButton()[i] == e.getSource()) {
                    this.getFarben()[i] = JColorChooser.showDialog(null,
                            "W\u00E4hle neue Farbe", this.getFarben()[i]);
                    getFarbenButton()[i].setBackground(this.getFarben()[i]);
                    break;
                }
        }
    }

    /**
     * Erzeugt die Ansicht.
     * 
     * @param animation
     *            Index der gewaehlten animation
     */
    private void build(byte animation) {

        // Sprache identifizieren
        // TODO reaktivieren
        for (int i = 0; i < this.getLang().length; i++)
            if (this.getLang()[i].equals(Messages.getLang())) {
                getLangSelection().setSelectedIndex(i);
                break;
            }
        getLangSelection().setSelectedIndex(0);

        // Fenster erzeugen
        setLayout(new BorderLayout(5, 5));

        // Subpanel erzeugen
        JPanel center = new JPanel(new GridLayout(1, 2, 5, 5));
        JPanel leftCenter = new JPanel(new BorderLayout(5, 5));
        JPanel leftCenterMain = new JPanel(new GridLayout(7, 2, 5, 5));
        JPanel leftCenterWuerfel = new JPanel(new GridLayout(1, 4, 5, 5));
        JPanel rightCenter = new JPanel(new BorderLayout(5, 5));
        JPanel rightCenterTop = new JPanel(new GridLayout(4, 2, 5, 5));
        JPanel rightCenterBottom = new JPanel(new GridLayout(4, 1, 5, 5));
        JPanel rightCenterBottomRadioButton = new JPanel(new GridLayout(1, 3,
                5, 5));
        JPanel colorChooser = new JPanel(new GridLayout(2, 0, 5, 5));
        JPanel all = new JPanel(new BorderLayout(5, 5));
        JPanel bottom = new JPanel(new BorderLayout(5, 5));
        JPanel extraButtons = new JPanel(new GridLayout(1, 2, 5, 5));

        // Rahmen setzen:
        colorChooser.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), "Farbe"));
        leftCenter.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), "Spiel"));
        rightCenterTop.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), "Programm"));
        rightCenterBottom.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), "Animation"));

        // Panel fuellen, von innen nach aussen
        for (int i = 0; i < getAuswahl().length; i++) {
            rightCenterBottomRadioButton.add(getAuswahl()[i]);
            this.bg.add(getAuswahl()[i]);
        }
        rightCenterBottom.add(getShowAnimation());
        rightCenterBottom.add(rightCenterBottomRadioButton);
        rightCenterBottom.add(getSpeedLabel());
        rightCenterBottom.add(getSpeed());

        rightCenterTop.add(getAutoUpdate());
        rightCenterTop.add(getDebugCheckBox());
        rightCenterTop.add(getLookForUpdate());
        rightCenterTop.add(getToDefault());
        rightCenterTop.add(new JLabel("Sprache:"));
        rightCenterTop.add(getLangSelection());
        rightCenterTop.add(new JLabel("Spielmodus: "));
        rightCenterTop.add(getModSelection());

        rightCenter.add(rightCenterTop, BorderLayout.NORTH);
        rightCenter.add(rightCenterBottom, BorderLayout.CENTER);

        leftCenterWuerfel.add(new JLabel("Von: "));
        leftCenterWuerfel.add(getWuerfelnVon());
        leftCenterWuerfel.add(new JLabel(" bis: "));
        leftCenterWuerfel.add(getWuerfelnBis());

        leftCenterMain.add(new JLabel("Maximale Spieler:"));
        leftCenterMain.add(getMaxSpielerField());
        leftCenterMain.add(new JLabel("Maximale Chips:"));
        leftCenterMain.add(getMaxChipsToSetField());
        leftCenterMain.add(new JLabel("Startchips:"));
        leftCenterMain.add(getStartChipsField());
        leftCenterMain.add(new JLabel("Bankchips:"));
        leftCenterMain.add(getBankChipsField());
        leftCenterMain.add(new JLabel("Spielbrettf�llmodus:"));
        leftCenterMain.add(getMapModusSelection());
        leftCenterMain.add(getFiveRuleCheckBox());
        leftCenterMain.add(getBankRuleCheckBox());
        leftCenterMain.add(getChipsLimit());
        leftCenterMain.add(getWuerfelnCheckBox());

        leftCenter.add(leftCenterMain, BorderLayout.CENTER);
        leftCenter.add(leftCenterWuerfel, BorderLayout.SOUTH);

        center.add(leftCenter);
        center.add(rightCenter);

        for (int i = 0; i < getFarbenButton().length; i++)
            colorChooser.add(getFarbenButton()[i]);
        colorChooser.add(new JLabel("Hintergrund"));
        colorChooser.add(new JLabel("Pin 1"));
        colorChooser.add(new JLabel("Pin 2"));
        colorChooser.add(new JLabel("Kein Feld"));
        for (int i = 0; i < (this.getFarben().length - 4); i++)
            colorChooser.add(new JLabel("Chip " + i));

        extraButtons.add(getOk());
        extraButtons.add(getUebernehmen());
        extraButtons.add(getCancel());

        bottom.add(new JLabel(), BorderLayout.CENTER);
        bottom.add(extraButtons, BorderLayout.EAST);

        all.add(center, BorderLayout.CENTER);
        all.add(colorChooser, BorderLayout.SOUTH);

        add(all, BorderLayout.CENTER);
        add(bottom, BorderLayout.SOUTH);

        pack();

        // In die Mitte positionieren
        setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
                (Toolkit.getDefaultToolkit().getScreenSize().height - getHeight()) / 2);

        // Icon setzen
        try {
            setIconImage(ImageIO.read(ClassLoader
                    .getSystemResource("gui/images/radioaktiv.xrt")));
        } catch (IOException e) {
            BugReport.recordBug(e);
        }

        // Selektionen uebertragen
        getMapModusSelection().setSelectedIndex(this.mapModus);
        getAutoUpdate().setSelected(this.isAutoUpdate());
        getShowAnimation().setSelected(this.isShowA());
        getDebugCheckBox().setSelected(this.isDebug());
        getAuswahl()[animation].setSelected(true);
        getModSelection().setSelectedIndex(this.modus);
        getSpeed().setValue(this.getSleep() / 100);
        getBankRuleCheckBox().setSelected(this.isBankRule());
        getFiveRuleCheckBox().setSelected(this.isFiveRule());
    }

    @Override
    public void caretUpdate(CaretEvent e) {
        getUebernehmen().setEnabled(true);
    }

    /**
     * Gibt den Aenderungsbaumalgorithmus zurueck.
     * 
     * @return Aenderungsbaumalgorithmus
     */
    public PfadSuche getAnimation() {
        for (int i = 0; i < this.auswahl.length; i++)
            if (this.auswahl[i].isSelected())
                return this.getAnimationGUI()[i];
        return null;
    }

    /**
     * @return the animation
     */
    public PfadSuche[] getAnimationGUI() {
        return this.animation;
    }

    /**
     * Gibt zurueck, welche Animation ausgewaehlt wurde.
     * 
     * @return Index der gewaehlten Animation
     */
    private int getAnimationSelection() {
        for (int i = 0; i < this.auswahl.length; i++)
            if (this.auswahl[i].isSelected())
                return i;
        return -1;
    }

    /**
     * Gibt die Animationsauswahlbuttons zurueck und erstellt eine Buttongroup.
     * Fuegt die Buttons jedoch noch nicht der Buttongroup hinzu.
     * 
     * @return JRadioButton Array mit eintraegen fuer jede Animation
     */
    private JRadioButton[] getAuswahl() {
        if (this.auswahl == null) {
            this.auswahl = new JRadioButton[3];
            this.auswahl[0] = new JRadioButton("Depth");
            this.auswahl[1] = new JRadioButton("Breadth");
            this.auswahl[2] = new JRadioButton("Wave");
            this.bg = new ButtonGroup();
            for (JRadioButton element : this.auswahl)
                element.addChangeListener(this);
        }
        return this.auswahl;
    }

    /**
     * Gibt eine Auswahlbox fuer automatische Updates zurueck.
     * 
     * @return Auswahlbox fuer automatische Updates
     */
    private JCheckBox getAutoUpdate() {
        if (this.autoUpdateCheckBox == null) {
            this.autoUpdateCheckBox = new JCheckBox("Automatisches Update");
            this.autoUpdateCheckBox
                    .setToolTipText("Sollen Updates automatisch heruntergeladen werden");
            this.autoUpdateCheckBox.addChangeListener(this);
        }
        return this.autoUpdateCheckBox;
    }

    /**
     * Gibt die Anzahl der Chips in der Bank zurueck.
     * 
     * @return Anzahl der Chips in der Bank
     */
    public int getBankChips() {
        return this.bankChips;
    }

    /**
     * Gibt ein Textfeld zur Eingabe der Chips in der Bank zurueck.
     * 
     * @return Textfeld zur Eingabe der Chips in der Bank
     */
    private JTextField getBankChipsField() {
        if (this.bankChipsField == null) {
            this.bankChipsField = new JTextField("" + this.bankChips);
            this.bankChipsField
                    .setToolTipText("Legt fest, wie viele Chips in der Bank liegen");
            this.bankChipsField.addCaretListener(this);
        }
        return this.bankChipsField;
    }

    /**
     * @return
     */
    private JCheckBox getBankRuleCheckBox() {
        if (this.bankRuleCheckBox == null) {
            this.bankRuleCheckBox = new JCheckBox("Bank-Regel");
            this.bankRuleCheckBox
                    .setToolTipText("<html>Sollen die Reaktionen w�hrend des Zuges anhalten,<br>wenn die Bank leer ist</html>");
            this.bankRuleCheckBox.addChangeListener(this);
        }
        return this.bankRuleCheckBox;
    }

    /**
     * @return
     */
    private JButton getCancel() {
        if (this.cancel == null) {
            this.cancel = new JButton("Abbrechen");
            this.cancel.addActionListener(this);
        }
        return this.cancel;
    }

    /**
     * Gibt eine Auswahlbox fuer ein Chiplimit zurueck.
     * 
     * @return Auswahlbox fuer ein Chiplimit
     */
    private JCheckBox getChipsLimit() {
        if (this.chipsLimit == null) {
            this.chipsLimit = new JCheckBox("Kein Chiplimit");
            this.chipsLimit
                    .setToolTipText("<html>Bestimmt, ob Spieler immer die maximale<br>Anzahl an Chips legen darf</html>");
            this.chipsLimit.addChangeListener(this);
        }
        return this.chipsLimit;
    }

    /**
     * Erstellt die Debugcheckbox und gibt sie zurueck.
     * 
     * @return Debugcheckbox
     */
    private JCheckBox getDebugCheckBox() {
        if (this.debugCheckBox == null) {
            this.debugCheckBox = new JCheckBox("Debug-Aufzeichnung");
            this.debugCheckBox.addChangeListener(this);
            this.debugCheckBox
                    .setToolTipText("Sammelt weite Informationen zum debugen.");
        }
        return this.debugCheckBox;
    }

    /**
     * Gibt die Farbe zurueck.
     * 
     * @param i
     *            0 = Hintergurnd, 1 = Pin1, 2 = Pin2, 3 = NoField, 4 = 1.
     *            Chip...
     * @return Farbe
     */
    public Color getFarbe(int i) {
        return this.getFarben()[i];
    }

    /**
     * @return the farben
     */
    public Color[] getFarben() {
        return this.farben;
    }

    /**
     * Gibt ein Array mit Buttons fuer jede Farbe zurueck.
     * 
     * @return Array mit Buttons fuer jede Farbe
     */
    private JButton[] getFarbenButton() {
        if (this.farbenButton == null) {
            this.farbenButton = new JButton[this.getFarben().length];

            for (int i = 0; i < this.farbenButton.length; i++) {
                this.farbenButton[i] = new JButton();
                this.farbenButton[i].addActionListener(this);
                this.farbenButton[i].setBackground(getFarbe(i));
            }
        }
        return this.farbenButton;
    }

    /**
     * @return
     */
    private JCheckBox getFiveRuleCheckBox() {
        if (this.fiveRuleCheckBox == null) {
            this.fiveRuleCheckBox = new JCheckBox("5er-Regel");
            this.fiveRuleCheckBox
                    .setToolTipText("<html>Geben Stapel mit mehr Chips als f�r die Reaktion ben�tigt<br>mehr Chips, wenn sie reagieren</html>");
            this.fiveRuleCheckBox.addChangeListener(this);
        }
        return this.fiveRuleCheckBox;
    }

    /**
     * Gibt zurueck, wie viele Chips im Spiel sind.
     */
    public int getGesamtChips() {
        return this.gesamtChips;
    }

    /**
     * @return the hilfe
     */
    public Help getHilfe() {
        return this.hilfe;
    }

    /**
     * Gibt die Karte des Spiels zurueck.
     * 
     * @return Karte des Spiels
     */
    public boolean[][] getKarte() {
        return getMod().getKarte();
    }

    /**
     * Gibt die Spieler/KIs zurueck.
     * 
     * @return Spieler/KIs
     */
    public Strategie[] getKi() {
        return this.ki;
    }

    /**
     * @return the lang
     */
    public String[] getLang() {
        return this.lang;
    }

    /**
     * Erzeugt die Auswahlbox fuer die Sprachen.
     * 
     * @return Auswahlbox fuer Sprachen
     */
    private JComboBox getLangSelection() {
        if (this.langSelection == null) {
            String[] sprachen = { "Deutsch", "Englisch", "Schwedisch" };
            this.langSelection = new JComboBox(sprachen);
            this.langSelection.setToolTipText("Auswahl der Sprache");
            this.langSelection.addActionListener(this);
            // TODO Sprache reaktivieren
            this.langSelection.setEnabled(false);
        }
        return this.langSelection;
    }

    /**
     * Gibt einen Button zum suchen nach einem Update zurueck.
     * 
     * @return Button zum suchen nach einem Update
     */
    private JButton getLookForUpdate() {
        if (this.lookForUpdate == null) {
            this.lookForUpdate = new JButton("Nach Update suchen");
            this.lookForUpdate.addActionListener(this);
            this.lookForUpdate
                    .setToolTipText("<html>Erzwingt eine Pr\u00FCfung, ob eine aktuellere<br>Version des Programms verf\u00FCgbar ist</html>");
        }
        return this.lookForUpdate;
    }

    /**
     * Gibt zurueck, wie viele Simulationen durchgefuehrt werden sollen.
     * 
     * @return Anzahl der Simulationen
     */
    public int getLoops() {
        return this.loops;
    }

    /**
     * @return byte
     */
    public byte getMapModus() {
        return this.mapModus;
    }

    /**
     * @return the mapModusSelection
     */
    public JComboBox getMapModusSelection() {
        if (this.mapModusSelection == null) {
            String[] modi = { "Standard", "Zuf�llig" };
            this.mapModusSelection = new JComboBox(modi);
            this.mapModusSelection
                    .setToolTipText("Auswahl des Spielbrettauff�llmodus");
            this.mapModusSelection.addActionListener(this);
        }
        return this.mapModusSelection;
    }

    /**
     * Gibt zurueck, wie viele Chips aktuell gelegt werden duerfen.
     * 
     * @return Anzahl der Chips, die gelegt werden duerfen
     */
    public byte getMaxChipsToSet() {
        return this.maxChipsToSet;
    }

    /**
     * Gibt eine Textfeld zur Eingabe der maximal legbaren Chips pro Runde
     * zurueck.
     * 
     * @return Textfeld zur Eingabe der maximal legbaren Chips
     */
    private JTextField getMaxChipsToSetField() {
        if (this.maxChipsToSetField == null) {
            this.maxChipsToSetField = new JTextField("" + this.maxChipsToSet);
            this.maxChipsToSetField
                    .setToolTipText("Legt fest, wie viele Chips maximal gelegt werden d\u00FCrfen");
            this.maxChipsToSetField.addCaretListener(this);
            this.maxChipsToSetField.setEnabled(!isWuerfeln());
        }
        return this.maxChipsToSetField;
    }

    /**
     * Gibt die maximale Spieleranzahl zurueck.
     * 
     * @return Maximale Anzahl der Spieler
     */
    public int getMaxSpieler() {
        return this.maxSpieler;
    }

    /**
     * Gibt eine Textfeld zur Eingabe der maximalen Spieleranzahl zurueck.
     * 
     * @return Textfeld zur Eingabe der maximalen Spieleranzahl
     */
    private JTextField getMaxSpielerField() {
        if (this.maxSpielerField == null) {
            this.maxSpielerField = new JTextField("" + this.maxSpieler);
            this.maxSpielerField
                    .setToolTipText("Legt fest, wie viele Spieler maximal erlaubt sind");
            this.maxSpielerField.addCaretListener(this);
        }
        return this.maxSpielerField;
    }

    /**
     * Gibt den aktuellen Mod zurueck.
     * 
     * @return aktueller Mod
     */
    public Mod getMod() {
        return this.getMods()[this.modus];
    }

    /**
     * @return the mod
     */
    public Mod[] getMods() {
        return this.mod;
    }

    /**
     * Erzeugt die Auswahlbox fuer die Spielmodi.
     * 
     * @return Auswahlbox fuer die Spielmodi
     */
    private JComboBox getModSelection() {
        if (this.modSelection == null) {
            String[] modi = new String[this.getMods().length];
            for (int i = 0; i < modi.length; i++)
                modi[i] = this.getMods()[i].toString();
            this.modSelection = new JComboBox(modi);
            this.modSelection.setToolTipText("Auswahl der Spielmodifikation");
            this.modSelection.addActionListener(this);
            this.modSelection.setSelectedIndex(this.modus);
        }
        return this.modSelection;
    }

    /**
     * Gibt den Index des aktuellen Mods zurueck.
     * 
     * @return Index des aktuellen Mods
     */
    public int getModus() {
        return this.modus;
    }

    /**
     * Gibt die Namen der Spieler zurueck.
     * 
     * @return Namen der Spieler
     */
    public String[] getNamen() {
        return this.namen;
    }

    /**
     * Erstellt den Ok-Button und gibt ihn zurueck.
     * 
     * @return OK-Button
     */
    private JButton getOk() {
        if (this.ok == null) {
            this.ok = new JButton("OK");
            this.ok.addActionListener(this);
            this.ok.setToolTipText("Speichert die Einstellungen und schlie�t das Fenster");
        }
        return this.ok;
    }

    /**
     * Gibt die Liste mit den originalen Positionen zurueck, falls die
     * Reihenfolge durchgemischt wurde.
     * 
     * @return Liste mit urspruenglichen Positionen
     */
    public byte[] getOriginalPosition() {
        return this.originalPosition;
    }

    /**
     * Gibt den Spielreaktor zurueck.
     * 
     * @return Spielreaktor
     */
    public Reaktor getReaktor() {
        return this.reaktor;
    }

    /**
     * Gibt eine Auswahlbox zum zeigen der Animation zurueck.
     * 
     * @return Auswahlbox zum zeigen der Animation
     */
    private JCheckBox getShowAnimation() {
        if (this.showAnimation == null) {
            this.showAnimation = new JCheckBox("Zeige Animation");
            this.showAnimation
                    .setToolTipText("Bestimmt, ob eine Animation angezeigt werden soll");
            this.showAnimation.addChangeListener(this);
        }
        return this.showAnimation;
    }

    /**
     * Gibt zurueck, wie lange die Animation nach Legen eines Chips warten soll,
     * bis zum legen des naechsten Chips.
     * 
     * @return Zeit zwischen Animationsschritten in Millisekunden
     */
    public int getSleep() {
        return this.sleep;
    }

    /**
     * Gibt einen Animationsgeschwindigkeitsregler zurueck.
     * 
     * @return Animationsgeschwindigkeitsregler
     */
    private JSlider getSpeed() {
        if (this.speed == null) {
            this.speed = new JSlider(0, 20, 5);
            this.speed.setPaintTicks(true);
            this.speed.setMinorTickSpacing(1);
            this.speed.setMajorTickSpacing(5);
            this.speed.addChangeListener(this);
        }
        return this.speed;
    }

    /**
     * Gibt ein Label fuer die Animationsgeschwindigkeit zurueck.
     * 
     * @return Label fuer die Animationsgeschwindigkeit
     */
    private JLabel getSpeedLabel() {
        if (this.speedLabel == null)
            this.speedLabel = new JLabel("Geschwindigkeit: "
                    + ((double) getSleep() / 1000) + " s");
        return this.speedLabel;
    }

    /**
     * Gibt zurueck, mit wie vielen Chips die Spieler starten.
     * 
     * @return Anzahl der Chips der Spieler zu Spielbeginn
     */
    public byte getStartChips() {
        return this.startChips;
    }

    /**
     * Gibt ein Textfeld zur Eingabe der Startchipanzahl zurueck.
     * 
     * @return Textfeld zur Eingabe der Startchipanzahl
     */
    private JTextField getStartChipsField() {
        if (this.startChipsField == null) {
            this.startChipsField = new JTextField("" + this.startChips);
            this.startChipsField
                    .setToolTipText("<html>Legt fest, wie viele Chips den Spielern beim<br>Spielstart zur Verf\u00FCgung stehen</html>");
            this.startChipsField.addCaretListener(this);
        }
        return this.startChipsField;
    }

    /**
     * Gibt den Statistikmanager zurueck.
     * 
     * @return Statistikmanager
     */
    public Statistik getStatistik() {
        return this.statistik;
    }

    /**
     * Gibt einen Button zum Laden der Standardeinstellungen zurueck.
     * 
     * @return Button zum Laden der Standardeinstellungen
     */
    private JButton getToDefault() {
        if (this.toDefault == null) {
            this.toDefault = new JButton("Standard");
            this.toDefault.addActionListener(this);
            this.toDefault.addChangeListener(this);
            this.toDefault
                    .setToolTipText("Stellt die Standardwerte wieder her");
        }
        return this.toDefault;
    }

    /**
     * @return the turnierPunkte
     */
    public int[][] getTurnierPunkte() {
        return this.turnierPunkte;
    }

    private JButton getUebernehmen() {
        if (this.set == null) {
            this.set = new JButton("\u00DCbernehmen");
            this.set.addActionListener(this);
            this.set.setToolTipText("Speichert die Einstellungen");
            this.set.setEnabled(false);
        }
        return this.set;
    }

    /**
     * @return the update
     */
    public Updater getUpdate() {
        return this.update;
    }

    /**
     * @return the wuerfelEnd
     */
    public byte getWuerfelEnd() {
        return this.wuerfelEnd;
    }

    /**
     * @return the wuerfelnBis
     */
    private JTextField getWuerfelnBis() {
        if (this.wuerfelnBis == null) {
            this.wuerfelnBis = new JTextField("" + this.wuerfelEnd);
            this.wuerfelnBis.setToolTipText("Maximale zu w�rfelnde Zahl");
            this.wuerfelnBis.addCaretListener(this);
            this.wuerfelnBis.setEnabled(isWuerfeln());
        }
        return this.wuerfelnBis;
    }

    /**
     * Gibt eine Auswahlbox fuer die 5er-Regel zurueck.
     * 
     * @return Auswahlbox fuer die 5er-Regel
     */
    private JCheckBox getWuerfelnCheckBox() {
        if (this.wuerfelnCheckBox == null) {
            this.wuerfelnCheckBox = new JCheckBox("W�rfeln");
            this.wuerfelnCheckBox
                    .setToolTipText("Aktivieren um die zu maximale zu setzende Chipzahl zu erw�rfeln");
            this.wuerfelnCheckBox.addChangeListener(this);
            this.wuerfelnCheckBox.setSelected(this.wuerfeln);
        }
        return this.wuerfelnCheckBox;
    }

    /**
     * @return the wuerfelnVon
     */
    private JTextField getWuerfelnVon() {
        if (this.wuerfelnVon == null) {
            this.wuerfelnVon = new JTextField("" + this.wuerfelStart);
            this.wuerfelnVon.setToolTipText("Minimale zu w�rfelnde Zahl");
            this.wuerfelnVon.addCaretListener(this);
            this.wuerfelnVon.setEnabled(isWuerfeln());
        }
        return this.wuerfelnVon;
    }

    /**
     * @return the wuerfelStart
     */
    public byte getWuerfelStart() {
        return this.wuerfelStart;
    }

    /**
     * Gibt zurueck, ob eine Animation angezeigt werden soll.
     * 
     * @return Wahrheitswert
     */
    public boolean isAnimation() {
        return this.isShowA();
    }

    /**
     * Gibt zurueck, ob ein automatisches Update ausgefuehrt werden soll.
     * 
     * @return Wahrheitswert
     */
    public boolean isAutoUpdate() {
        return this.autoUpdate;
    }

    /**
     * @return the bankRule
     */
    public boolean isBankRule() {
        return this.bankRule;
    }

    /**
     * Gibt zurueck, ob die zu legenden Chips limitiert werden sollen.
     * 
     * @return Wahrheitswert
     */
    public boolean isChipsLimit() {
        return !this.chipsLimit.isSelected();
    }

    /**
     * Gibt zurueck, ob Debuginformationen gesammelt werden sollen
     * 
     * @return
     */
    public boolean isDebug() {
        return this.debug;
    }

    /**
     * @return the fiveRule
     */
    public boolean isFiveRule() {
        return this.fiveRule;
    }

    /**
     * @return the livePanel
     */
    public boolean isLivePanel() {
        return this.livePanel;
    }

    /**
     * Gibt zurueck, ob eine zufaellige Reihenfolge gewuenscht ist.
     * 
     * @return Wahrheitswert
     */
    public boolean isRandom() {
        return this.random;
    }

    /**
     * Gibt zurueck, ob das Spiel gespeichert wurde.
     * 
     * @return Wahrheitswert
     */
    public boolean isSaved() {
        return this.saved;
    }

    /**
     * @return the showA
     */
    public boolean isShowA() {
        return this.showA;
    }

    /**
     * Gibt zurueck, ob gewuerfelt werden soll.
     * 
     * @return Wahrheitswert
     */
    public boolean isWuerfeln() {
        return this.wuerfeln;
    }

    @Override
    public void repaint() {
        super.repaint();

        // Buttons neu einfaerben
        for (int i = 0; i < getFarbenButton().length; i++)
            getFarbenButton()[i].setBackground(getFarbe(i));

        // Auswahl uebertragen
        getMaxChipsToSetField().setText("" + this.maxChipsToSet);
        getMaxSpielerField().setText("" + this.maxSpieler);
        getBankChipsField().setText("" + this.bankChips);
        getStartChipsField().setText("" + this.startChips);
        getAutoUpdate().setSelected(this.isAutoUpdate());
        getShowAnimation().setSelected(this.isShowA());
        getDebugCheckBox().setSelected(this.isDebug());
        getMapModusSelection().setSelectedIndex(this.mapModus);
        getSpeed().setValue(this.getSleep() / 100);
    }

    /**
     * Liest die Einstellungen aus, uerbtraegt Sie an alle noetigen Komponenten
     * und speichert sie.
     */
    public void save() {

        // Uebernehmenbutton sperren
        getUebernehmen().setEnabled(false);

        // Modaenderung durchfuehren
        if (this.modus != getModSelection().getSelectedIndex()) {
            this.modus = (byte) getModSelection().getSelectedIndex();

            // Reaktor erneuern
            this.reaktor = new Reaktor(this.getMods()[this.modus],
                    this.statistik);

            // Karte resetten
            this.setKarte(this.getMod().getStandardKarte());
        }

        // Sonderregeln setzen
        this.reaktor.setMode(this.isFiveRule(), this.isBankRule());

        // Sprache auswaehlen
        try {
            // TODO Sprache reaktivieren
            Messages.lang(this.getLang()[0]);
            // Messages.lang(this.lang[getLangSelection().getSelectedIndex()]);
        } catch (IOException e1) {
            BugReport.recordBug(e1);
        }

        // Alle Daten auslesen
        if (Pattern.matches("\\d+", getMaxChipsToSetField().getText()))
            this.maxChipsToSet = (byte) Integer
                    .parseInt(getMaxChipsToSetField().getText());

        if (Pattern.matches("\\d+", getBankChipsField().getText()))
            this.bankChips = Integer.parseInt(getBankChipsField().getText());

        if (Pattern.matches("\\d+", getMaxSpielerField().getText()))
            this.maxSpieler = (byte) Integer.parseInt(getMaxSpielerField()
                    .getText());

        if (Pattern.matches("\\d+", getStartChipsField().getText()))
            this.startChips = (byte) Integer.parseInt(getStartChipsField()
                    .getText());

        if (Pattern.matches("\\d+", getWuerfelnVon().getText()))
            this.wuerfelStart = (byte) Integer.parseInt(getWuerfelnVon()
                    .getText());

        if (Pattern.matches("\\d+", getWuerfelnBis().getText()))
            this.wuerfelEnd = (byte) Integer.parseInt(getWuerfelnBis()
                    .getText());

        // Checkbuttonauswahl an Variablen uebertragen
        this.setAutoUpdate(getAutoUpdate().isSelected());
        this.setDebug(getDebugCheckBox().isSelected());
        this.setShowA(getShowAnimation().isSelected());
        this.wuerfeln = getWuerfelnCheckBox().isSelected();
        this.mapModus = (byte) getMapModusSelection().getSelectedIndex();
        this.setFiveRule(getFiveRuleCheckBox().isSelected());
        this.setBankRule(getBankRuleCheckBox().isSelected());

        // Daten uebergeben
        this.io.setModus(this.modus);
        this.io.setAnimation((byte) getAnimationSelection());
        this.io.setMapModus(this.mapModus);
        this.io.setAutoUpdate(this.isAutoUpdate());
        this.io.setBankChips(this.bankChips);
        this.io.setChipsToSet(this.maxChipsToSet);
        this.io.setMaxSpieler(this.maxSpieler);
        this.io.setShowA(this.isShowA());
        this.io.setSleep(this.getSleep());
        this.io.setStartChips(this.startChips);
        this.io.setDebug(this.isDebug());
        this.io.setWuerfelEnd(this.wuerfelEnd);
        this.io.setWuerfelStart(this.wuerfelStart);
        this.io.setWuerfeln(this.wuerfeln);
        this.io.setFiveRule(this.isFiveRule());
        this.io.setBankRule(this.isBankRule());

        // Farben
        int[] colors = new int[this.getFarben().length];
        for (byte i = 0; i < colors.length; i++)
            colors[i] = this.getFarben()[i].getRGB();
        this.io.setColors(colors);

        // Daten speichern
        this.io.save();
    }

    /**
     * @param animation
     *            the animation to set
     */
    public void setAnimationGUI(PfadSuche[] animation) {
        this.animation = animation;
    }

    /**
     * @param autoUpdate
     *            the autoUpdate to set
     */
    public void setAutoUpdate(boolean autoUpdate) {
        this.autoUpdate = autoUpdate;
    }

    /**
     * @param bankRule
     *            the bankRule to set
     */
    public void setBankRule(boolean bankRule) {
        this.bankRule = bankRule;
    }

    /**
     * @param debug
     *            the debug to set
     */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    /**
     * @param farben
     *            the farben to set
     */
    public void setFarben(Color[] farben) {
        this.farben = farben;
    }

    /**
     * @param fiveRule
     *            the fiveRule to set
     */
    public void setFiveRule(boolean fiveRule) {
        this.fiveRule = fiveRule;
    }

    /**
     * Legt fest, wie viele Chips im Spiel sind
     * 
     * @param chipsGesamt
     *            gesamtzahl der Chips im Spiel
     */
    public void setGesamtChips(int chipsGesamt) {
        this.gesamtChips = chipsGesamt;
    }

    /**
     * Legt die Karte des Spieles fest.
     * 
     * @param karte
     *            Karte des Spiels
     */
    public void setKarte(boolean[][] karte) {
        for (Mod modus : this.getMods())
            modus.setKarte(ArrOps.arrayCopy(karte));
    }

    /**
     * Setzt die Spieler fest. Wenn KIs uebergeben wurden, werden ihnen
     * automatisch die Einstellungen uebertragen.
     * 
     * @param ki
     *            Liste mit KIs
     */
    public void setKi(Strategie[] ki) {
        this.ki = ki;
        this.originalPosition = new byte[ki.length];
        for (byte i = 0; i < ki.length; i++) {
            this.originalPosition[i] = i;
            if (ki[i] != null)
                ki[i].setReaktor(this.reaktor);
        }
    }

    /**
     * @param lang
     *            the lang to set
     */
    public void setLang(String[] lang) {
        this.lang = lang;
    }

    /**
     * @param livePanel
     *            the livePanel to set
     */
    public void setLivePanel(boolean livePanel) {
        this.livePanel = livePanel;
    }

    /**
     * Legt fest, wie viele Simulationen durchgefuehrt werden sollen.
     * 
     * @param loops
     *            Anzahl der Simulationen
     */
    public void setLoops(int loops) {
        this.loops = loops;
    }

    /**
     * @param mod
     *            the mod to set
     */
    public void setMods(Mod[] mod) {
        this.mod = mod;
    }

    /**
     * Setzt den aktuellen Mod
     * 
     * @param modus
     *            Index des zu waehlenden Mods
     */
    public void setModus(byte modus) {
        this.modus = modus;
    }

    /**
     * Legt die Namen der Spieler fest.
     * 
     * @param namen
     *            Namen der Spieler
     */
    public void setNamen(String[] namen) {
        // if (editable)
        this.namen = namen;
    }

    /**
     * Legt fest, ob eine zufaellige Reihenfolge gewuenscht ist.
     * 
     * @param random
     *            Wahrheitswert
     */
    public void setRandom(boolean random) {
        // if (editable)
        this.random = random;
    }

    /**
     * Setzt, dass das Spiel gespeichert wurde.
     * 
     * @param saved
     *            Wahrheitswert
     */
    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    /**
     * @param showA
     *            the showA to set
     */
    public void setShowA(boolean showA) {
        this.showA = showA;
    }

    /**
     * @param sleep
     *            the sleep to set
     */
    public void setSleep(int sleep) {
        this.sleep = sleep;
    }

    /**
     * @param startChips
     *            the startChips to set
     */
    public void setStartChips(byte startChips) {
        this.startChips = startChips;
    }

    /**
     * @param turnierPunkte
     *            the turnierPunkte to set
     */
    public void setTurnierPunkte(int[][] turnierPunkte) {
        this.turnierPunkte = turnierPunkte;
    }

    /**
     * @param update
     *            the update to set
     */
    public void setUpdate(Updater update) {
        this.update = update;
    }

    /**
     * @param wuerfelEnd
     *            the wuerfelEnd to set
     */
    public void setWuerfelEnd(byte wuerfelEnd) {
        this.wuerfelEnd = wuerfelEnd;
    }

    /**
     * @param wuerfeln
     *            the wuerfeln to set
     */
    public void setWuerfeln(boolean wuerfeln) {
        this.wuerfeln = wuerfeln;
    }

    /**
     * @param wuerfelStart
     *            the wuerfelStart to set
     */
    public void setWuerfelStart(byte wuerfelStart) {
        this.wuerfelStart = wuerfelStart;
    }

    /**
     * Mischt die Reihenfolge der KIs und merkt sich die neue Position
     */
    public void shuffle() {
        String tmpN;
        Strategie tmp;
        int rand;
        byte tmpi;
        for (int j = 0; j < 10; j++)
            for (int i = 0; i < this.ki.length; i++) {
                rand = this.peter.nextInt(this.ki.length);
                // Namen neu Ordnen
                tmpN = this.namen[i];
                this.namen[i] = this.namen[rand];
                this.namen[rand] = tmpN;

                // KIs neu Ordnen
                tmp = this.ki[i];
                this.ki[i] = this.ki[rand];
                this.ki[rand] = tmp;

                // Alte Position rand, neue Position i
                tmpi = this.originalPosition[rand];
                this.originalPosition[rand] = this.originalPosition[i];
                this.originalPosition[i] = tmpi;
            }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        getUebernehmen().setEnabled(true);

        if (e.getSource() == getSpeed()) {
            Settings_v6.this.setSleep(((JSlider) e.getSource()).getValue() * 100);
            getSpeedLabel().setText(
                    "Geschwindigkeit:" + ((double) getSleep() / 1000) + " s");
        }

        // Checkbuttonauswahl an Variablen uebertragen
        if (e.getSource() == getWuerfelnCheckBox()) {
            if (getWuerfelnCheckBox().isSelected()) {
                getMaxChipsToSetField().setEnabled(false);
                getWuerfelnBis().setEnabled(true);
                getWuerfelnVon().setEnabled(true);
            } else {
                getMaxChipsToSetField().setEnabled(true);
                getWuerfelnBis().setEnabled(false);
                getWuerfelnVon().setEnabled(false);
            }
            this.wuerfeln = getWuerfelnCheckBox().isSelected();
        } else if (e.getSource() == getAutoUpdate())
            this.setAutoUpdate(getAutoUpdate().isSelected());
        else if (e.getSource() == getDebugCheckBox())
            this.setDebug(getDebugCheckBox().isSelected());
        else if (e.getSource() == getShowAnimation())
            this.setShowA(getShowAnimation().isSelected());

    }

    /**
     * Setzt alle Werte auf die Standardwerte zurueck.
     */
    public void toDefault() {
        this.maxSpieler = 4;
        this.startChips = 7;
        this.maxChipsToSet = 3;
        this.bankChips = 106;
        this.setSleep(500);
        this.wuerfelEnd = 6;
        this.wuerfelStart = 1;
        this.modus = 1;
        this.mapModus = 0;
        this.reaktor = new Reaktor(getMod(), this.statistik);
        // TODO false
        this.livePanel = true;
        getAuswahl()[0].setSelected(true);

        this.getFarben()[0] = new Color(51, 51, 51);
        this.getFarben()[1] = new Color(0, 0, 100);
        this.getFarben()[2] = Color.CYAN;
        this.getFarben()[3] = new Color(212, 212, 252);
        this.getFarben()[4] = new Color(237, 237, 0);
        for (int i = 5; i < this.getFarben().length; i++)
            this.getFarben()[i] = this.getFarben()[i - 1].darker();

        this.setShowA(true);
        this.setAutoUpdate(true);
        this.setDebug(true);
        this.wuerfeln = false;
        this.setFiveRule(true);
        this.setBankRule(true);

        repaint();
    }

    /**
     * Gibt zurueck, ob der Breitensuchalgorithmus verwendet werden soll.
     * 
     * @return Wahrheitswert
     */
    public boolean useBF() {
        return !getAuswahl()[0].isSelected();
    }
}
