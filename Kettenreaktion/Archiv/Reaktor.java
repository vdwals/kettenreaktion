package core;

import java.awt.Point;
import java.util.LinkedList;

/**
 * Klasse zum ausloesen einer Reaktion. Errechnet das neue Brett, sowie Anzahl
 * der Reaktionen, Anzahl der gewonnenen Chips und Anzahl der Chips aus der
 * Bank.
 * 
 * @author Dennis van der Wals
 * 
 */
public class ReaktorOld {
	private static final int ROW = 6;
	private int gewinn, reaktionen, ausBank;
	private int[][] neuesBrett;
	private LinkedList<ReaktorPoint> aenderungen;

	/**
	 * Berechnet die Kettenreaktion einer Eingabe, speichert/ueberschreibt
	 * Anzahl der Reaktionen, Anzahl der gewonnenen Chips und Anzahl der Chips
	 * aus der Bank.
	 * 
	 * @param brett
	 *            Spielbrett - bleibt unveraendert, da es kopiert wird
	 * @param chip
	 *            Spielstein
	 * @return resultierendes Spielfeld
	 */
	int[][] reaktion(int[][] brett, Point chip, boolean fromKI) {
		// Initialisiert die Variablen
		gewinn = 0;
		reaktionen = 0;
		ausBank = 0;
		neuesBrett = brett;
		aenderungen = new LinkedList<ReaktorPoint>();
		LinkedList<Point> stapel = new LinkedList<Point>();
		stapel.offer(chip);
		boolean gesetzt = true;
		while (!stapel.isEmpty()) {
			Point p = stapel.poll();
			// Ueberprueft, ob es im Feld liegt
			if (isInFeld(p)) {
				// Ueberpruft, ob eine Reaktion stattfindet
				if (neuesBrett[p.x][p.y] >= 4) {
					// Erhoeht den Zaehler um 1
					reaktionen++;
					/*
					 * Ueberprueft, ob wir in einer Ecke sind fuer einen
					 * Extrachip
					 */
					if (((p.x == 0 || p.x == 5) && (p.y == 0 || p.y == 5))) {
						gewinn++;
					}
					/*
					 * Reduziert die Chips der Bank um 1 wenn die Reaktion nicht
					 * am Spielfeldrand stattfand
					 */
					if (p.x != 0 && p.x != 5 && p.y != 0 && p.y != 5) {
						ausBank++;
					} else {
						gewinn++;
					}
					// Erstellt die Chips, welche Reagieren
					stapel.offer(new Point(p.x - 1, p.y));
					stapel.offer(new Point(p.x, p.y - 1));
					stapel.offer(new Point(p.x + 1, p.y));
					stapel.offer(new Point(p.x, p.y + 1));
					erhoehe(p, -4, gesetzt, fromKI);
				} else {
					if (neuesBrett[p.x][p.y] == 3)
						stapel.offer(p);
					erhoehe(p, 1, gesetzt, fromKI);
				}
			}
			gesetzt = false;
		}
		return neuesBrett;
	}

	/**
	 * Berechnet die Kettenreaktion einer Eingabe, speichert/ueberschreibt
	 * Anzahl der Reaktionen, Anzahl der gewonnenen Chips und Anzahl der Chips
	 * aus der Bank.
	 * 
	 * @param brett
	 *            Spielbrett - bleibt unveraendert, da es kopiert wird
	 * @param chip
	 *            Spielstein
	 * @return resultierendes Spielfeld
	 */
	public int[][] reaktionGame(int[][] brett, Point chip) {
		return reaktion(ArrayOperation.arrayCopy(brett), chip, false);
	}

	/**
	 * Berechnet die Kettenreaktion einer Eingabe, speichert/ueberschreibt
	 * Anzahl der Reaktionen, Anzahl der gewonnenen Chips und Anzahl der Chips
	 * aus der Bank.
	 * 
	 * @param brett
	 *            Spielbrett - wird veraendert
	 * @param chip
	 *            Spielstein
	 * @return resultierendes Spielfeld
	 */
	public int[][] reaktionKI(int[][] brett, Point chip) {
		return reaktion(brett, chip, true);
	}

	/**
	 * Veraendert den Wert des Spielfeldes bei p um value und speichert die
	 * Aenderung
	 * 
	 * @param p
	 * @param change
	 */
	private void erhoehe(Point p, int change, boolean gesetzt, boolean fromKI) {
		neuesBrett[p.x][p.y] += change;
		if (!fromKI)
			aenderungen.add(new ReaktorPoint(p, neuesBrett[p.x][p.y], gesetzt));
	}

	public int getGewinn() {
		return gewinn;
	}

	public int getAusBank() {
		return ausBank;
	}

	public int[][] getNeuesBrett() {
		return ArrayOperation.arrayCopy(neuesBrett);
	}

	public int getReaktionen() {
		return reaktionen;
	}

	public LinkedList<ReaktorPoint> getAenderung() {
		return aenderungen;
	}

	/**
	 * Gibt zurueck, ob der Punkt p innerhalb ger Grenzen des Spielfeldes liegt.
	 * 
	 * @param p
	 *            Punkt
	 * @return Wahrheitswert
	 */
	private boolean isInFeld(Point p) {
		return (p.x >= 0 && p.x < ROW) && (p.y >= 0 && p.y < ROW);
	}

	/**
	 * Klasse zum speichern der Aenderungen waehrend der Reaktion
	 * 
	 * @author Dennis van der Wals
	 * 
	 */
	public class ReaktorPoint extends Point {

		/**
		 * Default SerialID
		 */
		private static final long serialVersionUID = 1L;
		private int value;
		private boolean gesetzt;

		public ReaktorPoint(Point p, int value, boolean gesetzt) {
			super(p);
			this.value = value;
			this.gesetzt = gesetzt;
		}

		public int getValue() {
			return value;
		}

		public boolean isGesetzt() {
			return gesetzt;
		}

	}

}
