/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package gui;

import gui.editor.KartenEditor;
import gui.editor.KartenVorschau;
import gui.language.Messages;
import gui.spiel.Spielfenster;
import implement.BugReport;
import io.KarteIOData;
import io.SaveGameIO;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JWindow;
import javax.swing.UIManager;

import core.simulation.*;
import core.Spielkern;
import core.Settings;

/**
 * Oberflaeche zum erstellen eines Spiels.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Executer extends JFrame implements ActionListener, WindowListener {
    // Komponenten
    private JComboBox         spielerOption;
    private JTextField        spielerName;
    private JButton           spielen, simulieren, karteLaden, editor,
            spielLaden, hinzufuegen, entfernen, settings, statistik;
    private JTable            spielerListe;
    private JCheckBox         record, random;
    private KartenVorschau    mapPreview;
    private JMenuBar          menu;

    // Spieleinstellungen
    private Settings          einstellungen;

    // Ausfuehrungsprogramme
    private Simulation        s;
    private Spielfenster      g;
    private KartenEditor      me;
    private SaveGameIO        save;

    /**
     * Default SerialID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Mainmethode - Startet das Spiel
     * 
     * @param args
     *            keine Funktion
     */
    public static void main(String[] args) {
        Messages.loadLang();
        try {
            Executer c = new Executer();
            c.build();
        } catch (Exception e) {
            BugReport.recordBug(e);
        }
    }

    /**
     * Standardkonstruktor
     */
    public Executer() {
        // Splashscreen zeigen
        showSplash();

        // Design laden
        try {
            UIManager
                    .setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"); //$NON-NLS-1$
        } catch (Exception e1) {
            BugReport.recordBug(e1);
        }

        this.einstellungen = new Settings();
        this.menu = new ExecuterMenu(new SaveGameIO(this.einstellungen, null),
                einstellungen, this);
        // this.einstellungen.getStatistik().addWindowListener(this);
        setTitle(Messages.getString("Creator.1") + this.einstellungen.getVersion()); //$NON-NLS-1$
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        // Spiel oder Simulation starten
        if ((e.getSource() == getSimulieren())
                || (e.getSource() == getSpielStarten())) {

            // Weniger als 2 Spieler abfangen
            if (this.einstellungen.getKi().length < 2)
                JOptionPane.showMessageDialog(null,
                        Messages.getString("Creator.2")); //$NON-NLS-1$
            else if (e.getSource() == getSpielStarten()) {

                // Durchmischen wenn gewuenscht
                if (this.random.isSelected())
                    this.einstellungen.shuffle();

                // Spiel starten
                startGame(new Spielfenster(new Spielkern(this.einstellungen),
                        this.einstellungen));

            } else if ((e.getSource() == this.simulieren)
                    && nurKi(this.einstellungen.getKi())) {

                // Simulation erstellen und starten
                setVisible(false);
                this.s = new Simulation(this.einstellungen);
                this.s.addWindowListener(this);
                this.s.start();

            } else
                // Spieler fuer Simulation abfangen
                JOptionPane.showMessageDialog(null,
                        Messages.getString("Creator.3")); //$NON-NLS-1$

        } else if (e.getSource() == getKarteLaden())
            try {
                // Versuche Karte zu Laden
                KarteIOData mapIO = new KarteIOData();
                boolean[][] karte = mapIO.load();

                if (karte != null) {
                    // Karte uebernehmen
                    this.einstellungen.setKarte(karte);

                    // Ansicht aktualisieren
                    getMapPreview().setKarte(this.einstellungen.getKarte());
                    pack();
                    repaint();
                }

            } catch (Exception a) {
                BugReport.recordBug(a);
                // Info, wenn Karte nicht geladen werden konnte
                JOptionPane.showMessageDialog(this,
                        Messages.getString("Creator.4")); //$NON-NLS-1$
            }
        else if (e.getSource() == getEditor()) {

            if (this.me == null) {

                // Erzeuge MapEditor
                this.me = new KartenEditor(this.einstellungen);
                this.me.start();
                this.me.addWindowListener(this);

            }

            // Holt den MapEditor hervor
            this.me.setVisible(true);

            // Mache Fenster unsichtbar
            setVisible(false);

        } else if (e.getSource() == getSpielLaden()) {

            // Laedt ein Spiel
            if (this.save == null)
                this.save = new SaveGameIO(this.einstellungen, null);

            try {
                this.save.load();

                // Einstellungen uebernehmen
                this.einstellungen = this.save.getEinstellungen();

                // Spiel uebernehmen
                startGame(new Spielfenster(this.save.getSpiel(),
                        this.einstellungen));

            } catch (Exception e1) {
                BugReport.recordBug(e1);
                JOptionPane.showMessageDialog(this,
                        Messages.getString("Creator.5")); //$NON-NLS-1$
            }
        } else if (e.getSource() == getHinzufuegen()) {

            // Spieler hinzufuegen
            addSelection();

            getSpielerName().setText(Messages.getString("StatistikManager.0")); //$NON-NLS-1$

        } else if (e.getSource() == getEntfernen())
            // Loescht einen Spieler
            delete(getSpielerListe().getSelectedRows());
        else if (e.getSource() == getSettings())
            // Oeffnet das Einstellungsfenster
            this.einstellungen.setVisible(true);
        else if (e.getSource() == getStatistik())
            // TODO Statistik fenster �ffnen
            // Oeffnet das Statistikfenster
            // this.einstellungen.getStatistik().openWindow();
            ;
    }

    /**
     * Fuegt einen Spieler der Liste hinzu.
     */
    private void addSelection() {
        // TODO Spielerauswahl aendern.

        // Nur hinzufuegen, wenn noch nicht alle Spieler eingetragen wurden
        if (this.einstellungen.getKi().length < this.einstellungen
                .getMaxSpieler()) {

            Strategie ki = null;
            String name = getSpielerName().getText();
            switch (getSpielerOption().getSelectedIndex()) {

                /*
                 * Erzeugt eine KI fuer 1=KI-Dichte, 2=KI-Dichte+,
                 * 3=KI-Heuristic, 4=KI-Greedy, 5=KI-Greedy+, 6=KI-Border,
                 * 7=KI-Random
                 */
                case 1:
                    ki = new Dichte(false);
                    break;
                case 2:
                    ki = new Dichte(true);
                    break;
                case 3:
                    ki = new Heuristic();
                    break;
                case 4:
                    ki = new Greedy(false);
                    break;
                case 5:
                    ki = new Greedy(true);
                    break;
                case 6:
                    ki = new Border();
                    break;
                case 7:
                    ki = new Zufall();
                    break;
                default:
                    break;
            }

            // KIs Standardnamen zuweisen, wenn nicht anders angegeben
            if ((ki != null)
                    && name.equals(Messages.getString("StatistikManager.0"))) //$NON-NLS-1$
                name = ki.toString();

            // Spieler uebernehmen
            Strategie[] kis = null;
            String[] namen = null;
            if (this.einstellungen.getKi() != null) {
                // Arrays vorbereiten
                kis = new Strategie[this.einstellungen.getKi().length + 1];
                namen = new String[this.einstellungen.getNamen().length + 1];

                // Alte Daten uebernehmen
                for (int i = 0; i < this.einstellungen.getNamen().length; i++) {
                    kis[i] = this.einstellungen.getKi()[i];
                    namen[i] = this.einstellungen.getNamen()[i];
                }

                // Neue Daten hinzufuegen
                kis[this.einstellungen.getKi().length] = ki;
                namen[this.einstellungen.getKi().length] = name;
            } else {

                // Neue Arrays erstellen
                kis = new Strategie[1];
                namen = new String[1];

                kis[0] = ki;
                namen[0] = name;
            }
            // Neue Arrays kopieren
            this.einstellungen.setKi(kis);
            this.einstellungen.setNamen(namen);

            // Anzeige aktualisieren
            getSpielerListe().setValueAt(name, kis.length - 1, 0);
            getSpielerListe().setValueAt(
                    ((ki != null) ? ki.toString()
                            : Messages.getString("StatistikManager.0")), //$NON-NLS-1$
                    kis.length - 1, 1);
            repaint();
        }
    }

    /**
     * Loescht den Spieler aus der Liste.
     * 
     * @param selected
     *            Index des zu loeschenden Spielers
     */
    private void delete(int[] selected) {

        for (int element : selected)
            // Ungueltigen Index abfangen
            if ((this.einstellungen.getKi() != null)
                    && (element < this.einstellungen.getKi().length)
                    && (element >= 0)) {

                // Arrays vorbereiten
                Strategie[] kis = new Strategie[this.einstellungen.getKi().length - 1];
                String[] namen = new String[this.einstellungen.getNamen().length - 1];

                // Alte Daten uebernehmen
                int index = 0;
                for (int i = 0; i < this.einstellungen.getNamen().length; i++)
                    // Ausgewaehltes ueberspringen
                    if (i != element) {
                        kis[index] = this.einstellungen.getKi()[i];
                        namen[index++] = this.einstellungen.getNamen()[i];
                    }

                // Neue Arrays kopieren
                this.einstellungen.setKi(kis);
                this.einstellungen.setNamen(namen);

                // Anzeige aktualisieren
                int i = 0;
                for (i = 0; i < namen.length; i++) {
                    getSpielerListe().setValueAt(namen[i], i, 0);
                    getSpielerListe()
                            .setValueAt(
                                    ((kis[i] == null) ? Messages.getString("StatistikManager.0") //$NON-NLS-1$
                                            : kis[i].toString()), i, 1);
                }
                for (int j = i; j < getSpielerListe().getRowCount(); j++) {
                    getSpielerListe().setValueAt("", j, 0); //$NON-NLS-1$
                    getSpielerListe().setValueAt("", j, 1); //$NON-NLS-1$
                }

                repaint();
            } else if (this.einstellungen.isDebug())
                BugReport.recordBug(new IllegalArgumentException(
                        "Ungueltige Auswahl:" + element));
    }

    /**
     * Erzeugt den Button zum Starten des Editors.
     * 
     * @return Button zum Starten des Editors
     */
    private JButton getEditor() {
        if (this.editor == null) {
            this.editor = new JButton(Messages.getString("Creator.0")); //$NON-NLS-1$
            this.editor.addActionListener(this);
            this.editor.setToolTipText(Messages.getString("Creator.13")); //$NON-NLS-1$
        }
        return this.editor;
    }

    /**
     * Gibt den Button zum Entfernen eines Spielers zurueck.
     * 
     * @return Button zum Entfernen
     */
    private JButton getEntfernen() {
        if (this.entfernen == null) {
            this.entfernen = new JButton("-"); //$NON-NLS-1$
            this.entfernen.setToolTipText(Messages.getString("Creator.15")); //$NON-NLS-1$
            this.entfernen.addActionListener(this);
        }
        return this.entfernen;
    }

    /**
     * Gibt den Button zum Hinzufuegen eines Spielers zurueck.
     * 
     * @return Button zum Hinzufuegen
     */
    private JButton getHinzufuegen() {
        if (this.hinzufuegen == null) {
            this.hinzufuegen = new JButton("+"); //$NON-NLS-1$
            this.hinzufuegen.addActionListener(this);
            this.hinzufuegen.setToolTipText(Messages.getString("Creator.17")); //$NON-NLS-1$
        }
        return this.hinzufuegen;
    }

    /**
     * Erzeugt den Button zum Laden einer Karte.
     * 
     * @return Button zum Laden einer Karte
     */
    private JButton getKarteLaden() {
        if (this.karteLaden == null) {
            this.karteLaden = new JButton(Messages.getString("Creator.18")); //$NON-NLS-1$
            this.karteLaden.addActionListener(this);
            this.karteLaden.setToolTipText(Messages.getString("Creator.19")); //$NON-NLS-1$
        }
        return this.karteLaden;
    }

    /**
     * Erstellt den String Array fuer die Spielerauswahloptionen.
     * 
     * @return String Array fuer die Spielerauswahloptionen
     */
    private String[] getKiOptions() {
        String[] s = {
                Messages.getString("StatistikManager.0"), //$NON-NLS-1$
                Messages.getString("Creator.21"), //$NON-NLS-1$
                Messages.getString("Creator.22"), //$NON-NLS-1$
                Messages.getString("Creator.14"), //$NON-NLS-1$
                Messages.getString("Creator.24"), //$NON-NLS-1$
                Messages.getString("Creator.25"), Messages.getString("Creator.16"), //$NON-NLS-1$//$NON-NLS-2$
                Messages.getString("Creator.26"), //$NON-NLS-1$
        };
        return s;
    }

    /**
     * Erzeugt die MapPreview.
     * 
     * @return MapPreview
     */
    private KartenVorschau getMapPreview() {
        if (this.mapPreview == null)
            this.mapPreview = new KartenVorschau(this.einstellungen);
        return this.mapPreview;
    }

    /**
     * Gibt eine CheckBox zur Auswahl einer zufaelligen Startreihenfolge
     * zurueck.
     * 
     * @return CheckBox zur Auswahl einer zufaelligen Startreihenfolge
     */
    private JCheckBox getRandom() {
        if (this.random == null) {
            this.random = new JCheckBox();
            this.random.setToolTipText(Messages.getString("Creator.27")); //$NON-NLS-1$
        }
        return this.random;
    }

    /**
     * Gibt eine CheckBox zur Auswahl der Aufzeichnung zurueck.
     * 
     * @return CheckBox zur Auswahl der Aufzeichnung
     */
    private JCheckBox getRecord() {
        if (this.record == null) {
            this.record = new JCheckBox();
            this.record.setToolTipText(Messages.getString("Creator.28")); //$NON-NLS-1$
        }
        return this.record;
    }

    /**
     * Erzeugt einen Button zum aufrufen der Spieleinstellungen.
     * 
     * @return Button zum aufrufen der Spieleinstellungen
     */
    private JButton getSettings() {
        if (this.settings == null) {
            this.settings = new JButton(Messages.getString("Creator.29")); //$NON-NLS-1$
            this.settings.addActionListener(this);
            this.settings.setToolTipText(Messages.getString("Creator.30")); //$NON-NLS-1$
        }
        return this.settings;
    }

    /**
     * Gibt den Button zum Starten einer Simulation zurueck.
     * 
     * @return Button zum Starten einer Simulation
     */
    private JButton getSimulieren() {
        if (this.simulieren == null) {
            this.simulieren = new JButton(Messages.getString("Creator.31")); //$NON-NLS-1$
            this.simulieren.addActionListener(this);
            this.simulieren.setToolTipText(Messages.getString("Creator.32")); //$NON-NLS-1$
        }
        return this.simulieren;
    }

    /**
     * Gibt eine Liste mit am Spiel teilnehmenden Spielern zurueck.
     * 
     * @return Liste mit am Spiel teilnehmenden Spielern
     */
    private JTable getSpielerListe() {
        if (this.spielerListe == null) {
            this.spielerListe = new JTable(this.einstellungen.getMaxSpieler(),
                    2);
            this.spielerListe.setToolTipText(Messages.getString("Creator.33")); //$NON-NLS-1$
        }
        return this.spielerListe;
    }

    /**
     * Gibt eine Liste fuer Spielernamen zurueck.
     * 
     * @return Liste fuer Textfelder zur Eingabe von Spielernamen
     */
    private JTextField getSpielerName() {
        if (this.spielerName == null)
            this.spielerName = new JTextField(
                    Messages.getString("StatistikManager.0")); //$NON-NLS-1$
        return this.spielerName;
    }

    /**
     * Erzeugt eine neue Auswahlliste fuer 0=Spieler, 1=KI-Dichte, 2=KI-Dichte+,
     * 3=KI-Random, 4=KI-Greedy, 5=KI-Greedy+, 6=KI-Border, 7=KI-GreedyOld
     * 
     * @return
     */
    private JComboBox getSpielerOption() {
        if (this.spielerOption == null)
            this.spielerOption = new JComboBox(getKiOptions());
        return this.spielerOption;
    }

    /**
     * Gibt einen Button zum Laden eines Spieles zurueck.
     * 
     * @return Button zum Laden eines Spieles
     */
    private JButton getSpielLaden() {
        if (this.spielLaden == null) {
            this.spielLaden = new JButton(Messages.getString("Creator.35")); //$NON-NLS-1$
            this.spielLaden.addActionListener(this);
            this.spielLaden.setToolTipText(Messages.getString("Creator.36")); //$NON-NLS-1$
        }
        return this.spielLaden;
    }

    /**
     * Erzeugt den Button zum Starten eines Spieles zurueck.
     * 
     * @return Button zum Starten eines Spieles
     */
    private JButton getSpielStarten() {
        if (this.spielen == null) {
            this.spielen = new JButton(Messages.getString("Settings.43")); //$NON-NLS-1$
            this.spielen.addActionListener(this);
            this.spielen.setToolTipText(Messages.getString("Creator.38")); //$NON-NLS-1$
        }
        return this.spielen;
    }

    /**
     * Gibt einen Button zum oeffnen der Statistik zurueck.
     * 
     * @return Button zum oeffnen der Statistik
     */
    private JButton getStatistik() {
        if (this.statistik == null) {
            this.statistik = new JButton(Messages.getString("Creator.39")); //$NON-NLS-1$
            this.statistik.addActionListener(this);
            this.statistik.setToolTipText(Messages.getString("Creator.40")); //$NON-NLS-1$
        }
        return this.statistik;
    }

    /**
     * Ueberprueft ob nur KIs gewaehlt wurden.
     * 
     * @param ki
     *            Liste mit KIs
     * @return Wahrheitswert
     */
    private boolean nurKi(Strategie[] ki) {
        for (Strategie element : ki)
            if (element == null)
                return false;
        return true;
    }

    /**
     * Erstellt einen Dialog der Ja/Nein/Abbrechen zur verfuegung stellt
     * 
     * @return Auswahl: 0 = ja, 1 = nein, 2 = abbrechen
     */
    private int saveQuestion() {
        String[] options = {
                Messages.getString("Creator.41"), Messages.getString("Creator.42"), Messages.getString("Creator.43") }; //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
        return JOptionPane.showOptionDialog(
                null,
                Messages.getString("Creator.44"), //$NON-NLS-1$
                Messages.getString("Creator.45"), //$NON-NLS-1$
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                null, options, options[0]);
    }

    /**
     * Erzeugt einen SplashScreen
     */
    private void showSplash() {
        final SplashScreen t = new SplashScreen();
        if (t.getBild() != null)
            new Thread(new Runnable() {

                @Override
                public void run() {
                    // Neues Fenster erzeugen
                    JWindow w = new JWindow();

                    // Bild laden
                    w.add(t);
                    w.pack();
                    w.toFront();
                    w.setLocation(
                            (Toolkit.getDefaultToolkit().getScreenSize().width - w
                                    .getWidth()) / 2,
                            (Toolkit.getDefaultToolkit().getScreenSize().height - w
                                    .getHeight()) / 2);
                    w.setVisible(true);
                    try {
                        // Fuer 2000 Millisekunden warten
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        BugReport.recordBug(e);
                    }
                    // Fenster schlie�en und loeschen
                    w.dispose();
                    w = null;
                }
            }).start();
    }

    /**
     * Startet das Programm.
     */
    private void build() {
        // Fenster initialisieren
        setResizable(false);

        setJMenuBar(this.menu);

        JPanel left = new JPanel(new GridLayout(2, 0, 5, 5));
        JPanel right = new JPanel(new BorderLayout(5, 5));

        JPanel leftBottom = new JPanel(new GridLayout(4, 1, 5, 5));
        JPanel rightBottom = new JPanel(new GridLayout(0, 2, 5, 5));

        JPanel addPlayer = new JPanel(new BorderLayout(5, 5));
        JPanel settings = new JPanel(new GridLayout(0, 4, 5, 5));

        JPanel addPlayerLeft = new JPanel(new GridLayout(0, 2, 5, 5));
        JPanel addPlayerRight = new JPanel(new GridLayout(0, 2, 5, 5));
        JPanel startButtons = new JPanel(new GridLayout(0, 3, 5, 5));
        JPanel otherButtons = new JPanel(new GridLayout(0, 2, 5, 5));

        // Erste Zeile
        addPlayerLeft.add(getSpielerName());
        addPlayerLeft.add(getSpielerOption());
        addPlayerRight.add(getHinzufuegen());
        addPlayerRight.add(getEntfernen());

        addPlayer.add(addPlayerLeft, BorderLayout.CENTER);
        addPlayer.add(addPlayerRight, BorderLayout.EAST);

        // Zweite Zeile
        settings.add(new JLabel(Messages.getString("Creator.46"))); //$NON-NLS-1$
        settings.add(getRecord());
        settings.add(new JLabel(Messages.getString("Creator.47"))); //$NON-NLS-1$
        settings.add(getRandom());

        // Dritte Zeile
        startButtons.add(getSpielStarten());
        startButtons.add(getSimulieren());
        startButtons.add(getSpielLaden());

        // Vierte Zeile
        otherButtons.add(getSettings());
        otherButtons.add(getStatistik());

        leftBottom.add(addPlayer);
        // Naechste Zeile
        leftBottom.add(settings);
        // Naechste Zeile
        leftBottom.add(startButtons);
        // Naechste Zeile
        leftBottom.add(otherButtons);

        rightBottom.add(getKarteLaden());
        rightBottom.add(getEditor());

        left.add(getSpielerListe());
        left.add(leftBottom);

        right.add(getMapPreview(), BorderLayout.CENTER);
        right.add(rightBottom, BorderLayout.SOUTH);

        setLayout(new BorderLayout(5, 5));

        add(left, BorderLayout.CENTER);
        add(right, BorderLayout.EAST);

        pack();

        // In die Mitte positionieren
        setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
                (Toolkit.getDefaultToolkit().getScreenSize().height - getHeight()) / 2);

        // Icon setzen
        try {
            setIconImage(ImageIO.read(getClass().getResource(
                    "images/radioaktiv.xrt")));
        } catch (IOException e) {
            BugReport.recordBug(e);
        }

        addWindowListener(this);
        setVisible(true);
        toFront();
    }

    /**
     * Startet ein neues Spiel
     * 
     * @param g
     *            Spiel
     */
    protected void startGame(Spielfenster g) {
        setVisible(false);
        this.g = g;
        g.addWindowListener(this);
        g.build();
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        if (e.getSource() == this) {

            this.einstellungen.getStatistik().save();
            this.einstellungen.save();

            // Programm beenden
            System.exit(0);

        } else if (e.getSource() == this.s) {

            // Simulation schliessen
            this.s.closeRecord();
            this.s.dispose();
            this.s = null;

        } else if (e.getSource() == this.g) {

            // Spielfenster schliessen
            if (!this.g.isSaved())
                // Spiel noch nicht gespeichert
                switch (saveQuestion()) {
                    case 0:
                        // Spiel speichern
                        this.g.save();
                    case 1:
                        // Fenster schliessen und loeschen
                        this.g.gameClose();
                        this.g.dispose();
                        this.g = null;
                        break;
                    case 2:
                        // Zurueck zum Spiel
                        this.g.setVisible(true);
                        break;
                    default:
                        break;
                }
            else {

                // Fenster schliessen und loeschen
                this.g.gameClose();
                this.g.dispose();
                this.g = null;
            }

        } else if (e.getSource() == this.me) {

            // Karteneditor schliessen
            if (!this.me.isSaved())
                // Karte noch nicht gespeichert
                switch (saveQuestion()) {
                    case 0:
                        // Karte speichern
                        this.me.save();
                    case 1:
                        // Fenster schliessen
                        this.me.setVisible(false);
                        break;
                    case 2:
                        // Zurueck zum Editor
                        this.me.setVisible(true);
                        break;
                    default:
                        break;
                }
            else
                // Fenster schliessen
                this.me.setVisible(false);
        } else if (e.getSource() == this.einstellungen.getStatistik())
            // TODO Statistikfenster schliessen
            // Statistikfenster schliessen
            // this.einstellungen.getStatistik().dispose();
            ;
        else if (e.getSource() == this.einstellungen) {
            this.dispose();
            this.build();
        }

        // Speicher leeren und Fenster sichtbar machen
        System.gc();

        if (e.getSource() != this.einstellungen.getStatistik()) {
            getMapPreview().setKarte(this.einstellungen.getKarte());
            setVisible(true);
        }
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }
}
