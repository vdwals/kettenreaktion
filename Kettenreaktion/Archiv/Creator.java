package gui;

import gui.editor.MapEditor;
import gui.spiel.Spiel;
import io.DataMiner;
import io.MapIOData;
import io.SaveGameIO;

import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.LinkedList;
import java.util.Random;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JWindow;
import javax.swing.UIManager;

import simulation.Dichte;
import simulation.Greedy;
import simulation.GreedyOld;
import simulation.Strategie;
import simulation.Zufall;
import core.Game;
import core.Settings;

/**
 * Oberflaeche zum erstellen eines Spiels.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Creator extends JFrame implements ActionListener, WindowListener {
	// Komponenten
	private LinkedList<JComboBox> spielerWahl;
	private LinkedList<JTextField> spielerName;
	private JTextField bankChips, startChips;
	private JButton spielen, simulieren, laden, editor, spielLaden;
	private JCheckBox record, random;

	// Spieleinstellungen
	private Settings einstellungen;

	// Ausfuehrungsprogramme
	private Simulation s;
	private Spiel g;
	private MapEditor me;
	private SaveGameIO save;

	/**
	 * Default SerialID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Standardkonstruktor
	 */
	public Creator() {
		super();

		// Splashscreen zeigen
		showSplash();
		einstellungen = new Settings();
		setTitle("Reaktor v: " + einstellungen.getVersion());

		// Update durchfuehren
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			new DataMiner(einstellungen.getVersion(),
					"http://dionysios.di.ohost.de/apps/kettenreaktion/",
					"Reaktor.jar").getUpdate();
		} catch (Exception e) {
		}
	}

	/**
	 * Mainmethode
	 * 
	 * @param args
	 *            keine Funktion
	 */
	public static void main(String[] args) {
		Creator c = new Creator();
		c.start();
	}

	/**
	 * Startet das Programm.
	 */
	public void start() {
		// Fenster initialisieren
		setResizable(false);

		// Layout
		setLayout(new GridLayout(0, 2, 10, 10));

		// Listen erzeugen
		getSpielerwahl();
		getSpielerName();

		// Komponenten hinzufuegen
		for (int i = 0; i < spielerName.size(); i++) {
			add(spielerName.get(i));
			add(spielerWahl.get(i));
		}

		add(new JLabel("Bankchips:"));
		add(getBankChips());

		add(new JLabel("Startchips:"));
		add(getStartChips());

		add(getRecord());
		add(getRandom());

		add(getSpiel());
		add(getSimulieren());

		add(new JLabel("Karte: "));
		add(getSpielLaden());

		add(getLaden());
		add(getEditor());

		pack();

		// In die Mitte positionieren
		setLocation(
				(Toolkit.getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
				(Toolkit.getDefaultToolkit().getScreenSize().height - getHeight()) / 2);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * Gibt eine Liste mit Auswahllisten zurueck oder erzeugt diese.
	 * 
	 * @return Liste mit Spieler/KI-Auswahllisten
	 */
	private LinkedList<JComboBox> getSpielerwahl() {
		if (spielerWahl == null) {
			spielerWahl = new LinkedList<JComboBox>();
			// Auswahllisten fuer 4 Spieler
			for (int i = 0; i < einstellungen.getMaxSpieler(); i++) {
				spielerWahl.add(getSpielerOption());
			}
		}
		return spielerWahl;
	}

	/**
	 * Gibt eine Liste fuer Spielernamen zurueck.
	 * 
	 * @return Liste fuer Textfelder zur Eingabe von Spielernamen
	 */
	private LinkedList<JTextField> getSpielerName() {
		if (spielerName == null) {
			spielerName = new LinkedList<JTextField>();
			for (int i = 0; i < einstellungen.getMaxSpieler(); i++) {
				spielerName.add(new JTextField("Spieler " + (i + 1)));
			}
		}
		return spielerName;
	}

	/**
	 * Erzeugt den Button zum Starten eines Spieles zurueck
	 * 
	 * @return Button zum Starten eines Spieles
	 */
	public JButton getSpiel() {
		if (spielen == null) {
			spielen = new JButton("Spiel");
			spielen.addActionListener(this);
		}
		return spielen;
	}

	/**
	 * Gibt den Button zum Starten einer Simulation zurueck.
	 * 
	 * @return Button zum Starten einer Simulation
	 */
	public JButton getSimulieren() {
		if (simulieren == null) {
			simulieren = new JButton("Simulation");
			simulieren.addActionListener(this);
		}
		return simulieren;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// Spiel oder Simulation starten
		if (e.getSource() == getSimulieren() || e.getSource() == getSpiel()) {

			// KIs auslesen
			einstellungen.setKi(getSelection());

			// Weniger als 2 Spieler abfangen
			if (einstellungen.getKi().length < 2) {
				JOptionPane.showMessageDialog(null,
						"Sie m�ssen mindestens 2 Spieler ausw�hlen.");
			} else {

				if (e.getSource() == getSpiel()) {
					startGame(new Spiel(new Game(einstellungen), einstellungen));

				} else if (e.getSource() == simulieren
						&& nurKi(einstellungen.getKi())) {

					// Simulation erstellen und starten
					setVisible(false);
					s = new Simulation(einstellungen);
					s.addWindowListener(this);
					s.start();

				} else {

					// Spieler fuer Simulation abfangen
					JOptionPane.showMessageDialog(null,
							"Spieler k�nnen nicht simuliert werden");
				}
			}

		} else if (e.getSource() == getLaden()) {

			// Versuche Karte zu Laden
			try {
				MapIOData mapIO = new MapIOData();
				einstellungen.setKarte(mapIO.load());
			} catch (Exception a) {
			}

		} else if (e.getSource() == getEditor()) {

			// Erzeuge MapEditor
			if (me == null) {
				me = new MapEditor(einstellungen);
				me.start();
				me.addWindowListener(this);
			} else {
				me.setVisible(true);
			}

			// Mache Fenster unsichtbar
			setVisible(false);

		} else if (e.getSource() == getSpielLaden()) {

			// Laedt ein Spiel
			if (save == null)
				save = new SaveGameIO(einstellungen, null);

			try {
				save.load();

				// Einstellungen uebernehmen
				this.einstellungen = save.getEinstellungen();

				// Spiel uebernehmen
				startGame(new Spiel(save.getSpiel(), einstellungen));

			} catch (Exception e1) {
			}
		}
	}

	private void startGame(Spiel g) {
		// Neues Spielfeld erstellen und Spiel starten
		setVisible(false);
		this.g = g;
		g.addWindowListener(this);
		g.start();
	}

	/**
	 * Ueberprueft ob nur KIs gewaehlt wurden.
	 * 
	 * @param ki
	 *            Liste mit KIs
	 * @return Wahrheitswert
	 */
	private boolean nurKi(Strategie[] ki) {
		for (int i = 0; i < ki.length; i++) {
			if (ki[i] == null)
				return false;
		}
		return true;
	}

	/**
	 * Erzeugt aus der Auswahl eine Liste mit Strategien und Spielernamen.
	 * 
	 * @return Liste mit KIs
	 */
	private Strategie[] getSelection() {

		// Zaehler
		int n = 0;
		String[] namenBuffer = new String[einstellungen.getMaxSpieler()];
		Strategie[] kiBuffer = new Strategie[einstellungen.getMaxSpieler()];

		// Auslesen der Zahlen und Abfangen falscher Eingaben
		if (!Pattern.matches("\\d+", getStartChips().getText())) {
			einstellungen.setStartChips(7);
		} else {
			einstellungen.setStartChips(Integer.parseInt(getStartChips()
					.getText()));
		}

		if (!Pattern.matches("\\d+", getBankChips().getText())) {
			einstellungen.setBankChips(100);
		} else {
			einstellungen.setBankChips(Integer.parseInt(getBankChips()
					.getText()));
		}

		// Auslesen der Namen und der KI-Wahl
		for (int i = 0; i < spielerWahl.size(); i++) {

			JComboBox spieler = spielerWahl.get(i);

			if (spieler.getSelectedIndex() > 0) {
				namenBuffer[n] = spielerName.get(i).getText();
				Strategie tmp = null;
				switch (spieler.getSelectedIndex()) {

				/*
				 * Erzeugt eine KI fuer 2=KI-Dichte, 3=KI-Dichte+, 4=KI-Random,
				 * 5=KI-Greedy, 6=KI-Greedy+, 8=KI-GreedyOld, 7=KI-Border
				 */
				case 2:
					tmp = new Dichte(false);
					break;
				case 3:
					tmp = new Dichte(true);
					break;
				case 4:
					tmp = new Zufall();
					break;
				case 5:
					tmp = new Greedy(false);
					break;
				case 6:
					tmp = new Greedy(true);
					break;
				case 8:
					tmp = new GreedyOld(false);
					break;
				case 7:
					tmp = new Border();
					break;
				default:
					break;
				}
				kiBuffer[n++] = tmp;

				// KIs Standardnamen zuweisen, wenn nicht anders angegeben
				if (kiBuffer[n - 1] != null
						&& namenBuffer[n - 1].equals("Spieler " + (i + 1)))
					namenBuffer[n - 1] = kiBuffer[n - 1].toString();
			}
		}

		// Kuerzen der Arrays
		einstellungen.setNamen(new String[n]);
		Strategie[] ki = new Strategie[n];
		for (int i = 0; i < ki.length; i++) {
			einstellungen.getNamen()[i] = namenBuffer[i];
			ki[i] = kiBuffer[i];
		}

		// Mischen
		if (getRandom().isSelected()) {
			String tmpN = null;
			Strategie tmpKi = null;
			Random r = new Random();
			for (int i = 0; i < ki.length; i++) {
				int index = r.nextInt(ki.length);
				tmpN = einstellungen.getNamen()[i];
				einstellungen.getNamen()[i] = einstellungen.getNamen()[index];
				einstellungen.getNamen()[index] = tmpN;
				tmpKi = ki[i];
				ki[i] = ki[index];
				ki[index] = tmpKi;
			}
		}

		return ki;
	}

	/**
	 * Erzeugt eine neue Auswahlliste fuer 0=Keiner, 1=Spieler, 2=KI-Dichte,
	 * 3=KI-Dichte+, 4=KI-Random, 5=KI-Greedy, 6=KI-Greedy+, 8=KI-GreedyOld,
	 * 7=KI-Border
	 * 
	 * @return
	 */
	private JComboBox getSpielerOption() {
		JComboBox x = new JComboBox();
		x.addItem("Keiner");
		x.addItem("Spieler");
		x.addItem("KI-Dichte");
		x.addItem("KI-Dichte+");
		x.addItem("KI-Random");
		x.addItem("KI-Greedy");
		x.addItem("KI-Greedy+");
		x.addItem("KI-Border");
		x.addItem("KI-GreedyOld");
		return x;
	}

	/**
	 * Erzeugt den Button zum Laden einer Karte.
	 * 
	 * @return Button zum Laden einer Karte
	 */
	public JButton getLaden() {
		if (laden == null) {
			laden = new JButton("Laden");
			laden.addActionListener(this);
			laden.setToolTipText("<html>�ffnet ein Dateiauswahlfenster"
					+ "<br>" + "zum Laden einer Karte</html>");
		}
		return laden;
	}

	public JButton getSpielLaden() {
		if (spielLaden == null) {
			spielLaden = new JButton("Spiel Laden");
			spielLaden.addActionListener(this);
			spielLaden
					.setToolTipText("<html>L�dt ein gespeichertes Spiel</html>");
		}
		return spielLaden;
	}

	/**
	 * Erzeugt den Button zum Starten des Editors.
	 * 
	 * @return Button zum Starten des Editors
	 */
	public JButton getEditor() {
		if (editor == null) {
			editor = new JButton("Editor");
			editor.addActionListener(this);
			editor.setToolTipText("<html>�ffnet den Karteneditor</html>");
		}
		return editor;
	}

	/**
	 * Gibt eine CheckBox zur Auswahl der Aufzeichnung zurueck.
	 * 
	 * @return CheckBox zur Auswahl der Aufzeichnung
	 */
	public JCheckBox getRecord() {
		if (record == null) {
			record = new JCheckBox("Spiel aufzeichnen");
			record.setToolTipText("<html>Die Runden, Chips und Felder" + "<br>"
					+ "werden gespeichert</html>");
		}
		return record;
	}

	/**
	 * Gibt ein Textfeld zur Eingabe der Gesamtchipszahl zurueck.
	 * 
	 * @return Textfeld zur Eingabe der Gesamtchipszahl
	 */
	public JTextField getBankChips() {
		if (bankChips == null) {
			bankChips = new JTextField("100", 4);
			bankChips
					.setToolTipText("<html>Anzahl der Chips in der Bank</html>");
		}
		return bankChips;
	}

	/**
	 * Gibt ein Textfeld zur Eingabe der Startchipszahl zurueck.
	 * 
	 * @return Textfeld zur Eingabe der Startchipszahl
	 */
	public JTextField getStartChips() {
		if (startChips == null) {
			startChips = new JTextField("7", 4);
			startChips
					.setToolTipText("<html>0 = Spieler kann immer 3 Steine legen"
							+ "<br>" + "unabh�ngig vom Kontostand </html>");
		}
		return startChips;
	}

	/**
	 * Gibt eine CheckBox zur Auswahl einer zufaelligen Startreihenfolge
	 * zurueck.
	 * 
	 * @return CheckBox zur Auswahl einer zufaelligen Startreihenfolge
	 */
	public JCheckBox getRandom() {
		if (random == null) {
			random = new JCheckBox("Reihenfolge zuf�llig");
			random.setToolTipText("<html>Die Reihenfolge der Spieler wird"
					+ "<br>" + "jede Runde zuf�llig erstellt</html>");
		}
		return random;
	}

	/**
	 * Beim schlie�en der Fenster wird der Speicher geleert.
	 */
	public void windowClosing(WindowEvent e) {
		if (e.getSource() == s) {
			s.closeRecord();
			s.dispose();
			s = null;
		} else if (e.getSource() == g) {
			if (!g.isSaved()) {
				switch (SaveQuestion.done()) {
				case 0:
					g.save();
				case 1:
					g.closeRecord();
					g.dispose();
					g = null;
					break;
				case 2:
					g.setVisible(true);
					break;
				default:
					break;
				}
			} else {
				g.closeRecord();
				g.dispose();
				g = null;
			}

		} else if (e.getSource() == me) {
			if (!me.isSaved()) {
				switch (SaveQuestion.done()) {
				case 0:
					me.save();
				case 1:
					me.setVisible(false);
					break;
				case 2:
					me.setVisible(true);
					break;
				default:
					break;
				}
			} else {
				me.setVisible(false);
			}
		}
		System.gc();
		setVisible(true);
	}

	/**
	 * Erzeugt einen SplashScreen
	 */
	private void showSplash() {
		final SplashScreen t = new SplashScreen();
		if (t.getBild() != null)
			new Thread(new Runnable() {

				@Override
				public void run() {
					// Neues Fenster erzeugen
					JWindow w = new JWindow();

					// Bild laden
					w.add(t);
					w.pack();
					w.setAlwaysOnTop(true);
					w.setLocation(
							(Toolkit.getDefaultToolkit().getScreenSize().width - w
									.getWidth()) / 2,
							(Toolkit.getDefaultToolkit().getScreenSize().height - w
									.getHeight()) / 2);
					w.setVisible(true);
					try {
						// Fuer 2000 Millisekunden warten
						Thread.sleep(2000);
					} catch (InterruptedException e) {
					}
					// Fenster schlie�en und loeschen
					w.dispose();
					w = null;
				}
			}).start();
	}

	/*
	 * Ungenutzte Methoden (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
	 */

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}
}
