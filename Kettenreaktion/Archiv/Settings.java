/* ********************************************************************
 * Kettenreaktion                                                     *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de   *
 *                                                                    *
 * This library is free software; you can redistribute it and/or      *
 * modify it under the terms of the GNU Lesser General Public         *
 * License as published by the Free Software Foundation; either       *
 * version 2.1 of the License, or (at your option) any later version. *
 *                                                                    *
 * This library is distributed in the hope that it will be useful,    *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  *
 * Lesser General Public License for more details.                    *
 *                                                                    *
 * You should have received a copy of the GNU Lesser General Public   *
 * License along with this library; if not, write to the              *
 *     Free Software Foundation, Inc.,                                *
 *     51 Franklin St, Fifth Floor,                                   *
 *     Boston, MA  02110-1301  USA                                    *
 *                                                                    *
 * Or get it online:                                                  *
 *     http://www.gnu.org/copyleft/lesser.html                        *
 **********************************************************************/
package core;

import gui.language.Messages;
import gui.spiel.pfadalgorithmen.BreadthFirst;
import gui.spiel.pfadalgorithmen.DepthFirst;
import gui.spiel.pfadalgorithmen.PfadSuche;
import gui.spiel.pfadalgorithmen.Wave;
import implement.ArrOps;
import implement.BugReport;
import implement.Updater;
import io.SettingsIO;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Random;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import core.mod.Hexagon;
import core.mod.Mod;
import core.mod.Quadrat;
import core.mod.Triangel;
import core.simulation.Strategie;
import core.statistik.SpielStatistik;

/**
 * Klasse zum speichern der Einstellungen des Spiels.
 * 
 * @author Dennis van der Wals
 * 
 */
public class Settings extends JFrame implements ActionListener, ChangeListener,
        CaretListener {
    /**
     * Default SerialID
     */
    private static final long   serialVersionUID = 1L;
    private static final double version          = 0.999;

    // GUI-Komponenten
    private JCheckBox           autoUpdateCheckBox, chipsLimit,
            wuerfelnCheckBox, showAnimation, debugCheckBox;
    private JTextField          maxSpielerField, maxChipsToSetField,
            bankChipsField, startChipsField, wuerfelnVon, wuerfelnBis;
    private JButton             lookForUpdate, toDefault, set, cancel,
            farbenButton[], ok;
    private JSlider             speed;
    private JLabel              speedLabel;
    private JRadioButton[]      auswahl;
    private ButtonGroup         bg;
    private JComboBox           langSelection, modSelection;

    // Spielvariablen
    private int                 gesamtChips, loops, sleep, bankChips;
    private byte                startChips, maxSpieler, maxChipsToSet,
            originalPosition[], modus, wuerfelStart, wuerfelEnd;
    private String[]            namen;
    private Strategie[]         ki;
    private boolean             random, record, saved, showA, autoUpdate,
            debug, wuerfeln;
    private Reaktor             reaktor;
    private Color[]             farben;

    // Konstanten
    private final PfadSuche[]   animation        = { new DepthFirst(),
            new BreadthFirst(), new Wave()      };
    private SpielStatistik      statistik;
    private final Mod[]         mod              = { new Triangel(),
            new Quadrat(), new Hexagon()        };
    private final byte          Quadrat_MOD      = 1;
    private final String[]      sprachen         = {
            Messages.getString("Settings.1"), Messages.getString("Settings.6"), Messages.getString("Settings.12") }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    private final String[]      lang             = { "de", "en", "sv" };                                            //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

    // Ausfuehrungsprogramme
    private SettingsIO          io;
    private Updater             update;
    private Random              peter;

    /**
     * Standardkonstruktor mit Standardwerten.
     * 
     * @throws IOException
     */
    public Settings() {
        // Ausfuehrungsprogramme initialisieren
        this.peter = new Random();
        this.io = new SettingsIO();
        this.update = new Updater(version, "kettenreaktion/", "Reaktor.jar");
        this.statistik = new SpielStatistik();

        // Temporaere Werte erzeugen
        int selectedAnimation = 0;

        // Werte initialisieren
        this.namen = new String[0];
        this.ki = new Strategie[0];
        this.originalPosition = new byte[0];

        // Groesste Chipzahl abfragen
        int[] maxChips = new int[this.mod.length];
        for (int i = 0; i < maxChips.length; i++)
            maxChips[i] = this.mod[i].getMaxChips();
        this.farben = new Color[4 + maxChips[ArrOps.getMax(maxChips)]];
        // editable = true;

        try {
            this.io.load();

            // Aus Datei lesen
            this.modus = this.io.getModus();
            this.maxSpieler = this.io.getMaxSpieler();
            this.startChips = this.io.getStartChips();
            this.maxChipsToSet = this.io.getChipsToSet();
            this.bankChips = this.io.getBankChips();
            this.sleep = this.io.getSleep();
            selectedAnimation = this.io.getAnimation();
            this.wuerfelEnd = io.getWuerfelEnd();
            wuerfelStart = io.getWuerfelStart();

            this.showA = this.io.isShowA();
            this.autoUpdate = this.io.isAutoUpdate();
            this.debug = this.io.isDebug();
            this.wuerfeln = this.io.isWuerfeln();

            // Farben auslesen
            byte counter = 0;
            for (counter = 0; counter < this.io.getColors().length; counter++)
                this.farben[counter] = new Color(this.io.getColors()[counter]);
            /*
             * Aeltere Versionen verfuegen ueber weniger Farben, den Rest mit
             * der letzten Farbe dunkler werdend auffuellen.
             */
            Color last = this.farben[counter - 1];
            for (byte i = counter; i < this.farben.length; i++)
                this.farben[i] = last.darker();

        } catch (Exception e) {
            BugReport.recordBug(e);

            // Standards laden
            toDefault();
        }

        // Standardkarte erzeugen
        setKarte(this.mod[this.modus].getStandardKarte());

        this.reaktor = new Reaktor(getMod(), statistik);

        if (this.autoUpdate)
            try {
                this.update.getUpdate();
            } catch (IOException e1) {
                BugReport.recordBug(e1);
            }

        build(selectedAnimation);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == getLookForUpdate())
            // Versuche Update zu starten
            try {
                this.update.getUpdate();
            } catch (IOException e1) {
                BugReport.recordBug(e1);
            }
        else if (e.getSource() == getToDefault())

            // Standardwerte setzen
            toDefault();

        else if (e.getSource() == getUebernehmen())

            // Werte speichern
            save();

        else if (e.getSource() == getCancel())

            // Fenster schlie�en
            dispose();

        else if (e.getSource() == getOk()) {

            // Speichern und Fenster schlie�en
            save();
            dispose();

        } else {
            getUebernehmen().setEnabled(true);
            // Farbe aendern
            for (int i = 0; i < getFarbenButton().length; i++)

                // Ueberpruefe jeden Button
                if (getFarbenButton()[i] == e.getSource()) {
                    this.farben[i] = JColorChooser.showDialog(null,
                            Messages.getString("Settings.3"), this.farben[i]); //$NON-NLS-1$
                    getFarbenButton()[i].setBackground(this.farben[i]);
                    break;
                }
        }
    }

    @Override
    public void caretUpdate(CaretEvent e) {
        getUebernehmen().setEnabled(true);
    }

    /**
     * Gibt den Aenderungsbaumalgorithmus zurueck.
     * 
     * @return Aenderungsbaumalgorithmus
     */
    public PfadSuche getAnimation() {
        for (int i = 0; i < this.auswahl.length; i++)
            if (this.auswahl[i].isSelected())
                return this.animation[i];
        return null;
    }

    /**
     * Gibt zurueck, welche Animation ausgewaehlt wurde.
     * 
     * @return Index der gewaehlten Animation
     */
    private int getAnimationSelection() {
        for (int i = 0; i < this.auswahl.length; i++)
            if (this.auswahl[i].isSelected())
                return i;
        return -1;
    }

    /**
     * Gibt die Animationsauswahlbuttons zurueck und erstellt eine Buttongroup.
     * Fuegt die Buttons jedoch noch nicht der Buttongroup hinzu.
     * 
     * @return JRadioButton Array mit eintraegen fuer jede Animation
     */
    private JRadioButton[] getAuswahl() {
        if (this.auswahl == null) {
            this.auswahl = new JRadioButton[3];
            this.auswahl[0] = new JRadioButton(Messages.getString("Settings.7")); //$NON-NLS-1$
            this.auswahl[1] = new JRadioButton(Messages.getString("Settings.8")); //$NON-NLS-1$
            this.auswahl[2] = new JRadioButton(Messages.getString("Settings.9")); //$NON-NLS-1$
            this.bg = new ButtonGroup();
            for (JRadioButton element : this.auswahl)
                element.addChangeListener(this);
        }
        return this.auswahl;
    }

    /**
     * Gibt eine Auswahlbox fuer automatische Updates zurueck.
     * 
     * @return Auswahlbox fuer automatische Updates
     */
    private JCheckBox getAutoUpdate() {
        if (this.autoUpdateCheckBox == null) {
            this.autoUpdateCheckBox = new JCheckBox(
                    Messages.getString("Settings.10")); //$NON-NLS-1$
            this.autoUpdateCheckBox.setToolTipText(Messages
                    .getString("Settings.11")); //$NON-NLS-1$
            this.autoUpdateCheckBox.addChangeListener(this);
        }
        return this.autoUpdateCheckBox;
    }

    /**
     * Gibt die Anzahl der Chips in der Bank zurueck.
     * 
     * @return Anzahl der Chips in der Bank
     */
    public int getBankChips() {
        return this.bankChips;
    }

    /**
     * Gibt ein Textfeld zur Eingabe der Chips in der Bank zurueck.
     * 
     * @return Textfeld zur Eingabe der Chips in der Bank
     */
    private JTextField getBankChipsField() {
        if (this.bankChipsField == null) {
            this.bankChipsField = new JTextField("" + this.bankChips); //$NON-NLS-1$
            this.bankChipsField.setToolTipText(Messages
                    .getString("Settings.13")); //$NON-NLS-1$
            this.bankChipsField.addCaretListener(this);
        }
        return this.bankChipsField;
    }

    /**
     * @return the wuerfelStart
     */
    public byte getWuerfelStart() {
        return this.wuerfelStart;
    }

    /**
     * @return the wuerfelEnd
     */
    public byte getWuerfelEnd() {
        return this.wuerfelEnd;
    }

    private JButton getCancel() {
        if (this.cancel == null) {
            this.cancel = new JButton(Messages.getString("Creator.43")); //$NON-NLS-1$
            this.cancel.addActionListener(this);
        }
        return this.cancel;
    }

    /**
     * Gibt eine Auswahlbox fuer ein Chiplimit zurueck.
     * 
     * @return Auswahlbox fuer ein Chiplimit
     */
    private JCheckBox getChipsLimit() {
        if (this.chipsLimit == null) {
            this.chipsLimit = new JCheckBox(Messages.getString("Settings.14")); //$NON-NLS-1$
            this.chipsLimit.setToolTipText(Messages.getString("Settings.15")); //$NON-NLS-1$
            this.chipsLimit.addChangeListener(this);
        }
        return this.chipsLimit;
    }

    /**
     * Erstellt die Debugcheckbox und gibt sie zurueck.
     * 
     * @return Debugcheckbox
     */
    private JCheckBox getDebugCheckBox() {
        if (this.debugCheckBox == null) {
            this.debugCheckBox = new JCheckBox("Debug-Aufzeichnung");
            this.debugCheckBox.addChangeListener(this);
            this.debugCheckBox
                    .setToolTipText("Sammelt weite Informationen zum debugen.");
        }
        return this.debugCheckBox;
    }

    /**
     * Gibt die Farbe zurueck.
     * 
     * @param i
     *            0 = Hintergurnd, 1 = Pin1, 2 = Pin2, 3 = NoField, 4 = 1.
     *            Chip...
     * @return Farbe
     */
    public Color getFarbe(int i) {
        return this.farben[i];
    }

    /**
     * Gibt ein Array mit Buttons fuer jede Farbe zurueck.
     * 
     * @return Array mit Buttons fuer jede Farbe
     */
    private JButton[] getFarbenButton() {
        if (this.farbenButton == null) {
            this.farbenButton = new JButton[this.farben.length];

            for (int i = 0; i < this.farbenButton.length; i++) {
                this.farbenButton[i] = new JButton();
                this.farbenButton[i].addActionListener(this);
                this.farbenButton[i].setBackground(getFarbe(i));
            }
        }
        return this.farbenButton;
    }

    /**
     * Gibt eine Auswahlbox fuer die 5er-Regel zurueck.
     * 
     * @return Auswahlbox fuer die 5er-Regel
     */
    private JCheckBox getWuerfelnCheckBox() {
        if (this.wuerfelnCheckBox == null) {
            this.wuerfelnCheckBox = new JCheckBox("W�rfeln");
            this.wuerfelnCheckBox
                    .setToolTipText("Aktivieren um die zu maximale zu setzende Chipzahl zu erw�rfeln");
            this.wuerfelnCheckBox.addChangeListener(this);
        }
        return this.wuerfelnCheckBox;
    }

    /**
     * Gibt zurueck, wie viele Chips im Spiel sind.
     */
    public int getGesamtChips() {
        return this.gesamtChips;
    }

    /**
     * Gibt die Karte des Spiels zurueck.
     * 
     * @return Karte des Spiels
     */
    public boolean[][] getKarte() {
        return getMod().getKarte();
    }

    /**
     * Gibt die Spieler/KIs zurueck.
     * 
     * @return Spieler/KIs
     */
    public Strategie[] getKi() {
        return this.ki;
    }

    /**
     * Erzeugt die Auswahlbox fuer die Sprachen.
     * 
     * @return Auswahlbox fuer Sprachen
     */
    private JComboBox getLangSelection() {
        if (this.langSelection == null) {
            this.langSelection = new JComboBox(this.sprachen);
            this.langSelection.setToolTipText("Auswahl der Sprache");
            this.langSelection.addActionListener(this);
        }
        return this.langSelection;
    }

    /**
     * Gibt einen Button zum suchen nach einem Update zurueck.
     * 
     * @return Button zum suchen nach einem Update
     */
    private JButton getLookForUpdate() {
        if (this.lookForUpdate == null) {
            this.lookForUpdate = new JButton(Messages.getString("Settings.18")); //$NON-NLS-1$
            this.lookForUpdate.addActionListener(this);
            this.lookForUpdate
                    .setToolTipText(Messages.getString("Settings.19")); //$NON-NLS-1$
        }
        return this.lookForUpdate;
    }

    /**
     * Gibt zurueck, wie viele Simulationen durchgefuehrt werden sollen.
     * 
     * @return Anzahl der Simulationen
     */
    public int getLoops() {
        return this.loops;
    }

    /**
     * Gibt zurueck, wie viele Chips aktuell gelegt werden duerfen.
     * 
     * @return Anzahl der Chips, die gelegt werden duerfen
     */
    public byte getMaxChipsToSet() {
        return this.maxChipsToSet;
    }

    /**
     * Gibt eine Textfeld zur Eingabe der maximal legbaren Chips pro Runde
     * zurueck.
     * 
     * @return Textfeld zur Eingabe der maximal legbaren Chips
     */
    private JTextField getMaxChipsToSetField() {
        if (this.maxChipsToSetField == null) {
            this.maxChipsToSetField = new JTextField("" + this.maxChipsToSet); //$NON-NLS-1$
            this.maxChipsToSetField.setToolTipText(Messages
                    .getString("Settings.21")); //$NON-NLS-1$
            this.maxChipsToSetField.addCaretListener(this);
        }
        return this.maxChipsToSetField;
    }

    /**
     * Gibt die maximale Spieleranzahl zurueck.
     * 
     * @return Maximale Anzahl der Spieler
     */
    public int getMaxSpieler() {
        return this.maxSpieler;
    }

    /**
     * Gibt eine Textfeld zur Eingabe der maximalen Spieleranzahl zurueck.
     * 
     * @return Textfeld zur Eingabe der maximalen Spieleranzahl
     */
    private JTextField getMaxSpielerField() {
        if (this.maxSpielerField == null) {
            this.maxSpielerField = new JTextField("" + this.maxSpieler); //$NON-NLS-1$
            this.maxSpielerField.setToolTipText(Messages
                    .getString("Settings.23")); //$NON-NLS-1$
            this.maxSpielerField.addCaretListener(this);
        }
        return this.maxSpielerField;
    }

    /**
     * Gibt den aktuellen Mod zurueck.
     * 
     * @return aktueller Mod
     */
    public Mod getMod() {
        return this.mod[this.modus];
    }

    /**
     * Erzeugt die Auswahlbox fuer die Spielmodi.
     * 
     * @return Auswahlbox fuer die Spielmodi
     */
    private JComboBox getModSelection() {
        if (this.modSelection == null) {
            String[] modi = new String[this.mod.length];
            for (int i = 0; i < modi.length; i++)
                modi[i] = this.mod[i].toString();
            this.modSelection = new JComboBox(modi);
            this.modSelection.setToolTipText("Auswahl der Spielmodifikation");
            this.modSelection.addActionListener(this);
        }
        return this.modSelection;
    }

    /**
     * Gibt den Index des aktuellen Mods zurueck.
     * 
     * @return Index des aktuellen Mods
     */
    public int getModus() {
        return this.modus;
    }

    /**
     * Gibt die Namen der Spieler zurueck.
     * 
     * @return Namen der Spieler
     */
    public String[] getNamen() {
        return this.namen;
    }

    /**
     * Erstellt den Ok-Button und gibt ihn zurueck.
     * 
     * @return OK-Button
     */
    private JButton getOk() {
        if (this.ok == null) {
            this.ok = new JButton("OK");
            this.ok.addActionListener(this);
            this.ok.setToolTipText("Speichert die Einstellungen und schlie�t das Fenster");
        }
        return this.ok;
    }

    /**
     * Gibt die Liste mit den originalen Positionen zurueck, falls die
     * Reihenfolge durchgemischt wurde.
     * 
     * @return Liste mit urspruenglichen Positionen
     */
    public byte[] getOriginalPosition() {
        return this.originalPosition;
    }

    /**
     * Gibt den Spielreaktor zurueck.
     * 
     * @return Spielreaktor
     */
    public Reaktor getReaktor() {
        return this.reaktor;
    }

    private JButton getUebernehmen() {
        if (this.set == null) {
            this.set = new JButton(Messages.getString("Settings.4")); //$NON-NLS-1$
            this.set.addActionListener(this);
            this.set.setToolTipText(Messages.getString("Settings.5")); //$NON-NLS-1$
            this.set.setEnabled(false);
        }
        return this.set;
    }

    /**
     * Gibt eine Auswahlbox zum zeigen der Animation zurueck.
     * 
     * @return Auswahlbox zum zeigen der Animation
     */
    private JCheckBox getShowAnimation() {
        if (this.showAnimation == null) {
            this.showAnimation = new JCheckBox(
                    Messages.getString("Settings.24")); //$NON-NLS-1$
            this.showAnimation
                    .setToolTipText(Messages.getString("Settings.25")); //$NON-NLS-1$
            this.showAnimation.addChangeListener(this);
        }
        return this.showAnimation;
    }

    /**
     * Gibt zurueck, wie lange die Animation nach Legen eines Chips warten soll,
     * bis zum legen des naechsten Chips.
     * 
     * @return Zeit zwischen Animationsschritten in Millisekunden
     */
    public int getSleep() {
        return this.sleep;
    }

    /**
     * Gibt einen Animationsgeschwindigkeitsregler zurueck.
     * 
     * @return Animationsgeschwindigkeitsregler
     */
    private JSlider getSpeed() {
        if (this.speed == null) {
            this.speed = new JSlider(0, 20, 5);
            this.speed.setPaintTicks(true);
            this.speed.setMinorTickSpacing(1);
            this.speed.setMajorTickSpacing(5);
            this.speed.addChangeListener(this);
        }
        return this.speed;
    }

    /**
     * Gibt ein Label fuer die Animationsgeschwindigkeit zurueck.
     * 
     * @return Label fuer die Animationsgeschwindigkeit
     */
    private JLabel getSpeedLabel() {
        if (this.speedLabel == null)
            this.speedLabel = new JLabel(
                    Messages.getString("Settings.0") + (double) getSleep() / 1000 //$NON-NLS-1$
                            + Messages.getString("Settings.27")); //$NON-NLS-1$
        return this.speedLabel;
    }

    /**
     * Gibt zurueck, mit wie vielen Chips die Spieler starten.
     * 
     * @return Anzahl der Chips der Spieler zu Spielbeginn
     */
    public int getStartChips() {
        return this.startChips;
    }

    /**
     * Gibt ein Textfeld zur Eingabe der Startchipanzahl zurueck.
     * 
     * @return Textfeld zur Eingabe der Startchipanzahl
     */
    private JTextField getStartChipsField() {
        if (this.startChipsField == null) {
            this.startChipsField = new JTextField("" + this.startChips); //$NON-NLS-1$
            this.startChipsField.setToolTipText(Messages
                    .getString("Settings.31")); //$NON-NLS-1$
            this.startChipsField.addCaretListener(this);
        }
        return this.startChipsField;
    }

    /**
     * Gibt den Statistikmanager zurueck.
     * 
     * @return Statistikmanager
     */
    public SpielStatistik getStatistik() {
        return this.statistik;
    }

    /**
     * Gibt einen Button zum Laden der Standardeinstellungen zurueck.
     * 
     * @return Button zum Laden der Standardeinstellungen
     */
    private JButton getToDefault() {
        if (this.toDefault == null) {
            this.toDefault = new JButton(Messages.getString("MapEditor.0")); //$NON-NLS-1$
            this.toDefault.addActionListener(this);
            this.toDefault.addChangeListener(this);
            this.toDefault.setToolTipText(Messages.getString("Settings.33")); //$NON-NLS-1$
        }
        return this.toDefault;
    }

    /**
     * Gibt die Versionsnummer des Spiels zurueck.
     * 
     * @return Versionsnummer des Spiels
     */
    public double getVersion() {
        return version;
    }

    /**
     * Gibt zurueck, ob eine Animation angezeigt werden soll.
     * 
     * @return Wahrheitswert
     */
    public boolean isAnimation() {
        return this.showA;
    }

    /**
     * Gibt zurueck, ob ein automatisches Update ausgefuehrt werden soll.
     * 
     * @return Wahrheitswert
     */
    public boolean isAutoUpdate() {
        return this.autoUpdate;
    }

    /**
     * Gibt zurueck, ob die zu legenden Chips limitiert werden sollen.
     * 
     * @return Wahrheitswert
     */
    public boolean isChipsLimit() {
        return !this.chipsLimit.isSelected();
    }

    // /**
    // * Legt fest, ob die Einstellungen editierbar sind.
    // *
    // * @param editable
    // * Wahrheitswert
    // */
    // protected void setEditable(boolean editable) {
    // this.editable = editable;
    // getMaxChipsToSetField().setEditable(editable);
    // getMaxSpielerField().setEditable(editable);
    // getBankChipsField().setEditable(editable);
    // getStartChipsField().setEditable(editable);
    // }

    /**
     * Gibt zurueck, ob Debuginformationen gesammelt werden sollen
     * 
     * @return
     */
    public boolean isDebug() {
        return this.debug;
    }

    /**
     * Gibt zurueck, ob eine zufaellige Reihenfolge gewuenscht ist.
     * 
     * @return Wahrheitswert
     */
    public boolean isRandom() {
        return this.random;
    }

    /**
     * Gibt zurueck, ob das Spiel aufgezeichnet werden soll.
     * 
     * @return Wahrheitswert
     */
    public boolean isRecord() {
        return this.record;
    }

    /**
     * Gibt zurueck, ob das Spiel gespeichert wurde.
     * 
     * @return Wahrheitswert
     */
    public boolean isSaved() {
        return this.saved;
    }

    /**
     * Gibt zurueck, ob gewuerfelt werden soll.
     * 
     * @return Wahrheitswert
     */
    public boolean isWuerfeln() {
        return this.wuerfeln;
    }

    @Override
    public void repaint() {
        super.repaint();

        // Buttons neu einfaerben
        for (int i = 0; i < getFarbenButton().length; i++)
            getFarbenButton()[i].setBackground(getFarbe(i));

        // Auswahl uebertragen
        getMaxChipsToSetField().setText("" + this.maxChipsToSet);
        getMaxSpielerField().setText("" + this.maxSpieler);
        getBankChipsField().setText("" + this.bankChips);
        getStartChipsField().setText("" + this.startChips);
        getAutoUpdate().setSelected(this.autoUpdate);
        getShowAnimation().setSelected(this.showA);
        getDebugCheckBox().setSelected(this.debug);
        getSpeed().setValue(this.sleep / 100);
    }

    /**
     * Liest die Einstellungen aus und speichert sie.
     */
    public void save() {

        // Uebernehmenbutton sperren
        getUebernehmen().setEnabled(false);

        if (this.modus != getModSelection().getSelectedIndex()) {
            this.modus = (byte) getModSelection().getSelectedIndex();

            // Reaktor erneuern
            this.reaktor = new Reaktor(this.mod[this.modus], statistik);

            // Karte resetten
            this.setKarte(this.getMod().getStandardKarte());
        }

        // Sprache auswaehlen
        try {
            Messages.lang(this.lang[getLangSelection().getSelectedIndex()]);
        } catch (IOException e1) {
            BugReport.recordBug(e1);
        }

        // Alle Daten auslesen
        if (Pattern.matches(Messages.getString("MapEditor.15"),
                getMaxChipsToSetField().getText()))
            this.maxChipsToSet = (byte) Integer
                    .parseInt(getMaxChipsToSetField().getText());

        if (Pattern.matches(Messages.getString("MapEditor.15"),
                getBankChipsField().getText()))
            this.bankChips = Integer.parseInt(getBankChipsField().getText());

        if (Pattern.matches(Messages.getString("MapEditor.15"),
                getMaxSpielerField().getText()))
            this.maxSpieler = (byte) Integer.parseInt(getMaxSpielerField()
                    .getText());

        if (Pattern.matches(Messages.getString("MapEditor.15"),
                getStartChipsField().getText()))
            this.startChips = (byte) Integer.parseInt(getStartChipsField()
                    .getText());

        // Checkbuttonauswahl an Variablen uebertragen
        this.autoUpdate = getAutoUpdate().isSelected();
        this.debug = getDebugCheckBox().isSelected();
        this.showA = getShowAnimation().isSelected();
        this.wuerfeln = getWuerfelnCheckBox().isSelected();

        // Daten uebergeben
        this.io.setModus(this.modus);
        this.io.setAnimation((byte) getAnimationSelection());
        this.io.setAutoUpdate(this.autoUpdate);
        this.io.setBankChips(this.bankChips);
        this.io.setChipsToSet(this.maxChipsToSet);
        this.io.setMaxSpieler(this.maxSpieler);
        this.io.setShowA(this.showA);
        this.io.setSleep(this.sleep);
        this.io.setStartChips(this.startChips);
        this.io.setDebug(this.debug);
        // Farben
        int[] colors = new int[this.farben.length];
        for (byte i = 0; i < colors.length; i++)
            colors[i] = this.farben[i].getRGB();
        this.io.setColors(colors);

        // Daten speichern
        this.io.save();
    }

    /**
     * Legt fest, wie viele Chips im Spiel sind
     * 
     * @param chipsGesamt
     *            gesamtzahl der Chips im Spiel
     */
    public void setGesamtChips(int chipsGesamt) {
        // if (editable)
        this.gesamtChips = chipsGesamt;
    }

    /**
     * Legt die Karte des Spieles fest.
     * 
     * @param karte
     *            Karte des Spiels
     */
    public void setKarte(boolean[][] karte) {
        // if (editable)
        for (Mod modus : this.mod)
            modus.setKarte(ArrOps.arrayCopy(karte));
    }

    /**
     * Setzt die Spieler fest. Wenn KIs uebergeben wurden, werden ihnen
     * automatisch die Einstellungen uebertragen.
     * 
     * @param ki
     *            Liste mit KIs
     */
    public void setKi(Strategie[] ki) {
        // if (editable) {
        this.ki = ki;
        this.originalPosition = new byte[ki.length];
        for (byte i = 0; i < ki.length; i++) {
            this.originalPosition[i] = i;
            if (ki[i] != null)
                ki[i].setReaktor(this.reaktor);
        }
        // }
    }

    /**
     * Legt fest, wie viele Simulationen durchgefuehrt werden sollen.
     * 
     * @param loops
     *            Anzahl der Simulationen
     */
    public void setLoops(int loops) {
        this.loops = loops;
    }

    /**
     * Setzt den aktuellen Mod
     * 
     * @param modus
     *            Index des zu waehlenden Mods
     */
    public void setModus(byte modus) {
        this.modus = modus;
    }

    /**
     * Legt die Namen der Spieler fest.
     * 
     * @param namen
     *            Namen der Spieler
     */
    public void setNamen(String[] namen) {
        // if (editable)
        this.namen = namen;
    }

    /**
     * Erzeugt die Ansicht.
     * 
     * @param animation
     *            Index der gewaehlten animation
     */
    private void build(int animation) {

        // Sprache identifizieren
        for (int i = 0; i < this.lang.length; i++)
            if (this.lang[i].equals(Messages.getLang())) {
                getLangSelection().setSelectedIndex(i);
                break;
            }

        // Fenster erzeugen
        setLayout(new BorderLayout(5, 5));

        // Hilfspanel erzeugen
        JPanel center = new JPanel(new GridLayout(1, 2, 5, 5));
        JPanel leftCenter = new JPanel(new GridLayout(5, 2, 5, 5));
        JPanel rightCenter = new JPanel(new BorderLayout(5, 5));
        JPanel rightCenterTop = new JPanel(new GridLayout(4, 2, 5, 5));
        JPanel rightCenterBottom = new JPanel(new GridLayout(4, 1, 5, 5));
        JPanel rightCenterBottomRadioButton = new JPanel(new GridLayout(1, 3,
                5, 5));
        JPanel colorChooser = new JPanel(new GridLayout(2, 0, 5, 5));
        JPanel all = new JPanel(new BorderLayout(5, 5));
        JPanel bottom = new JPanel(new BorderLayout(5, 5));
        JPanel extraButtons = new JPanel(new GridLayout(1, 2, 5, 5));

        // Rahmen setzen:
        colorChooser.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), Messages.getString("Settings.42"))); //$NON-NLS-1$
        leftCenter.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), Messages.getString("Settings.43"))); //$NON-NLS-1$
        rightCenterTop.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), Messages.getString("Settings.44"))); //$NON-NLS-1$
        rightCenterBottom.setBorder(new TitledBorder(BorderFactory
                .createEtchedBorder(), Messages.getString("Settings.45"))); //$NON-NLS-1$

        // Panel fuellen, von innen nach aussen
        for (int i = 0; i < getAuswahl().length; i++) {
            rightCenterBottomRadioButton.add(getAuswahl()[i]);
            this.bg.add(getAuswahl()[i]);
        }
        rightCenterBottom.add(getShowAnimation());
        rightCenterBottom.add(rightCenterBottomRadioButton);
        rightCenterBottom.add(getSpeedLabel());
        rightCenterBottom.add(getSpeed());

        rightCenterTop.add(getAutoUpdate());
        rightCenterTop.add(getDebugCheckBox());
        rightCenterTop.add(getLookForUpdate());
        rightCenterTop.add(getToDefault());
        rightCenterTop.add(new JLabel(Messages.getString("Settings.28"))); //$NON-NLS-1$
        rightCenterTop.add(getLangSelection());
        rightCenterTop.add(new JLabel("Spielmodus: "));
        rightCenterTop.add(getModSelection());

        rightCenter.add(rightCenterTop, BorderLayout.NORTH);
        rightCenter.add(rightCenterBottom, BorderLayout.CENTER);

        leftCenter.add(new JLabel(Messages.getString("Settings.46"))); //$NON-NLS-1$
        leftCenter.add(getMaxSpielerField());
        leftCenter.add(new JLabel(Messages.getString("Settings.47"))); //$NON-NLS-1$
        leftCenter.add(getMaxChipsToSetField());
        leftCenter.add(new JLabel(Messages.getString("Settings.48"))); //$NON-NLS-1$
        leftCenter.add(getStartChipsField());
        leftCenter.add(new JLabel(Messages.getString("Settings.49"))); //$NON-NLS-1$
        leftCenter.add(getBankChipsField());
        leftCenter.add(getWuerfelnCheckBox());
        leftCenter.add(getChipsLimit());

        center.add(leftCenter);
        center.add(rightCenter);

        for (int i = 0; i < getFarbenButton().length; i++)
            colorChooser.add(getFarbenButton()[i]);
        colorChooser.add(new JLabel(Messages.getString("Settings.50"))); //$NON-NLS-1$
        colorChooser.add(new JLabel(Messages.getString("Settings.51"))); //$NON-NLS-1$
        colorChooser.add(new JLabel(Messages.getString("Settings.52"))); //$NON-NLS-1$
        colorChooser.add(new JLabel(Messages.getString("Settings.53"))); //$NON-NLS-1$
        for (int i = 0; i < (this.farben.length - 4); i++)
            colorChooser.add(new JLabel("Chip " + i));

        extraButtons.add(getOk());
        extraButtons.add(getUebernehmen());
        extraButtons.add(getCancel());

        bottom.add(new JLabel(), BorderLayout.CENTER);
        bottom.add(extraButtons, BorderLayout.EAST);

        all.add(center, BorderLayout.CENTER);
        all.add(colorChooser, BorderLayout.SOUTH);

        add(all, BorderLayout.CENTER);
        add(bottom, BorderLayout.SOUTH);

        pack();

        // In die Mitte positionieren
        setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width - getWidth()) / 2,
                (Toolkit.getDefaultToolkit().getScreenSize().height - getHeight()) / 2);

        // Icon setzen
        try {
            setIconImage(ImageIO.read(getClass().getResource(
                    "../gui/images/radioaktiv.xrt")));
        } catch (IOException e) {
            BugReport.recordBug(e);
        }

        // Selektionen uebertragen
        getAutoUpdate().setSelected(this.autoUpdate);
        getShowAnimation().setSelected(this.showA);
        getDebugCheckBox().setSelected(this.debug);
        getAuswahl()[animation].setSelected(true);
        getModSelection().setSelectedIndex(this.modus);
        getSpeed().setValue(this.sleep / 100);
    }

    /**
     * Legt fest, ob eine zufaellige Reihenfolge gewuenscht ist.
     * 
     * @param random
     *            Wahrheitswert
     */
    public void setRandom(boolean random) {
        // if (editable)
        this.random = random;
    }

    /**
     * Setzt, ob das Spiel aufgezeichnet werden soll.
     * 
     * @param record
     *            Wahrheitswert
     */
    public void setRecord(boolean record) {
        this.record = record;
    }

    /**
     * Setzt, dass das Spiel gespeichert wurde.
     * 
     * @param saved
     *            Wahrheitswert
     */
    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    /**
     * Mischt die Reihenfolge der KIs und merkt sich die neue Position
     */
    public void shuffle() {
        String tmpN;
        Strategie tmp;
        int rand;
        byte tmpi;
        for (int j = 0; j < 10; j++)
            for (int i = 0; i < this.ki.length; i++) {
                rand = this.peter.nextInt(this.ki.length);
                // Namen neu Ordnen
                tmpN = this.namen[i];
                this.namen[i] = this.namen[rand];
                this.namen[rand] = tmpN;

                // KIs neu Ordnen
                tmp = this.ki[i];
                this.ki[i] = this.ki[rand];
                this.ki[rand] = tmp;

                // Alte Position rand, neue Position i
                tmpi = this.originalPosition[rand];
                this.originalPosition[rand] = this.originalPosition[i];
                this.originalPosition[i] = tmpi;
            }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        getUebernehmen().setEnabled(true);

        if (e.getSource() == getSpeed()) {
            Settings.this.sleep = ((JSlider) e.getSource()).getValue() * 100;
            getSpeedLabel()
                    .setText(
                            Messages.getString("Settings.0") + (double) getSleep() / 1000 + Messages.getString("Settings.27")); //$NON-NLS-1$ //$NON-NLS-2$
        }

        // Checkbuttonauswahl an Variablen uebertragen
        if (e.getSource() == getWuerfelnCheckBox()) {
            if (getWuerfelnCheckBox().isSelected()) {
                getMaxChipsToSetField().setEnabled(false);
                getWuerfelnBis().setEnabled(true);
                getWuerfelnVon().setEnabled(true);
            } else {
                getMaxChipsToSetField().setEnabled(true);
                getWuerfelnBis().setEnabled(false);
                getWuerfelnVon().setEnabled(false);
            }
            this.wuerfeln = getWuerfelnCheckBox().isSelected();
        } else if (e.getSource() == getAutoUpdate())
            this.autoUpdate = getAutoUpdate().isSelected();
        else if (e.getSource() == getDebugCheckBox())
            this.debug = getDebugCheckBox().isSelected();
        else if (e.getSource() == getShowAnimation())
            this.showA = getShowAnimation().isSelected();

    }

    /**
     * Setzt alle Werte auf die Standardwerte zurueck.
     */
    private void toDefault() {
        this.maxSpieler = 4;
        this.startChips = 7;
        this.maxChipsToSet = 3;
        this.bankChips = 106;
        this.sleep = 500;
        this.wuerfelEnd = 6;
        this.wuerfelStart = 1;
        this.modus = this.Quadrat_MOD;
        this.reaktor = new Reaktor(getMod(), statistik);
        getAuswahl()[0].setSelected(true);

        this.farben[0] = new Color(51, 51, 51);
        this.farben[1] = new Color(0, 0, 100);
        this.farben[2] = Color.CYAN;
        this.farben[3] = new Color(255, 255, 255);
        this.farben[4] = new Color(237, 237, 0);
        for (int i = 5; i < this.farben.length; i++)
            this.farben[i] = this.farben[i - 1].darker();

        this.showA = true;
        this.autoUpdate = true;
        this.debug = true;

        repaint();
    }

    /**
     * Gibt zurueck, ob der Breitensuchalgorithmus verwendet werden soll.
     * 
     * @return Wahrheitswert
     */
    public boolean useBF() {
        return !getAuswahl()[0].isSelected();
    }

    /**
     * @return the wuerfelnVon
     */
    private JTextField getWuerfelnVon() {
        if (this.wuerfelnVon == null) {
            this.wuerfelnVon = new JTextField("" + this.wuerfelStart); //$NON-NLS-1$
            this.wuerfelnVon.setToolTipText("Minimale zu w�rfelnde Zahl");
            this.wuerfelnVon.addCaretListener(this);
        }
        return this.wuerfelnVon;
    }

    /**
     * @return the wuerfelnBis
     */
    private JTextField getWuerfelnBis() {
        if (this.wuerfelnBis == null) {
            this.wuerfelnBis = new JTextField("" + this.wuerfelEnd); //$NON-NLS-1$
            this.wuerfelnBis.setToolTipText("Maximale zu w�rfelnde Zahl");
            this.wuerfelnBis.addCaretListener(this);
        }
        return this.wuerfelnBis;
    }
}
